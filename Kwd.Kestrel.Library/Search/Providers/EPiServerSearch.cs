﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kwd.Kestrel.Library.Models.Search;
using EPiServer.Core;
using EPiServer.Search.Queries.Lucene;
using EPiServer.Security;
using System.Web;
using EPiServer.ServiceLocation;
using EPiServer.Search;

namespace Kwd.Kestrel.Library.Search.Providers
{
    using EPiServer;
    using EPiServer.Framework.Localization;
    using EPiServer.Web;

    using Kwd.Kestrel.Library.Epi.Types.Pages;
    using Kwd.Kestrel.Library.ExtensionMethods;
    using EPiServer.Web.Routing;

    public class EPiServerSearch : ISearchProvider
	{

		public ISearchResult Search(string searchString, string language, int hitsPerPage, int offset)
		{
            var result = new EPiServerSearchResult();
			result.Hits = new List<ISearchHit>();

            // We'll combine several queries and all must match
            var query = new GroupQuery(LuceneOperator.AND);
            var typeQuery = new GroupQuery(LuceneOperator.OR);
            typeQuery.QueryExpressions.Add(new ContentQuery<PageData>());
            typeQuery.QueryExpressions.Add(new ContentQuery<MediaData>());
            query.QueryExpressions.Add(typeQuery);
            query.QueryExpressions.Add(new FieldQuery(searchString));

            var searchHandler = ServiceLocator.Current.GetInstance<SearchHandler>();
            var contentSearchHandler = ServiceLocator.Current.GetInstance<ContentSearchHandler>();
 
            // Perform search
		    var results = searchHandler.GetSearchResults(query, offset, hitsPerPage);
		    foreach (var item in results.IndexResponseItems)
		    {
		        var content = contentSearchHandler.GetContent<IContent>(item);
		        var page = content as PageData;
                var file = content as MediaData;
		        DateTime d = DateTime.MinValue;
		        string intro = null;
		        if (page != null)
		        {
		            d = page.Saved;
		            var contentPage = page as ContentPageWitoutSecondaryContentBlockArea;
		            if (contentPage != null && contentPage.Introduction != null)
		            {
		                intro = contentPage.Introduction.ToString().RemoveHtml();

		            }
		        }
                else if (file != null)
                {
                    d = file.Saved;
                }

		        result.Hits.Add(
		            new EPiServerSearchHit() { Title = item.Title, Url = FriendlyUrl(content.ContentLink), Date = d, Snippet = intro });
		    }
			result.TotalHits = results.TotalHits;
			return result;
		}

        private string FriendlyUrl(ContentReference pageLink)
        {
            string url = ServiceLocator.Current.GetInstance<UrlResolver>().GetVirtualPath(pageLink).GetUrl();

            var urlBuilder = new UrlBuilder(url);
            Global.UrlRewriteProvider.ConvertToExternal(urlBuilder, null, System.Text.Encoding.UTF8);

            return UriSupport.AbsoluteUrlBySettings(url);
        }
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public class EPiServerSearchResult : ISearchResult
	{
		public IList<ISearchHit> Hits { get; set; }

		public int TotalHits { get; set; }

		public string SuggestionText { get; set; }

		public string SuggestionUrl { get; set; }
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public class EPiServerSearchHit : ISearchHit
	{
		public string Url { get; set; }

		public string Title { get; set; }

		public string Target { get; set; }

		public string Snippet { get; set; }

        private string _breadCrumb;
        public string BreadCrumb
        {
            get
            {
                if (!string.IsNullOrEmpty(_breadCrumb))
                    return _breadCrumb;

                _breadCrumb = BuildBreadCrumb(Url);
                return _breadCrumb;
            }
            set
            {
                _breadCrumb = value;
            }
        }
        private string BuildBreadCrumb(string url)
        {
            var linkFormat = "<a href='{0}'>{1}</a>";
            string mappedUrl;
            if (!PermanentLinkMapStore.TryToMapped(url, out mappedUrl))
            {
                return string.Empty;
            }
            var pageLink = PageReference.ParseUrl(mappedUrl);

            if (PageReference.IsNullOrEmpty(pageLink))
                return string.Empty;

            var repo = ServiceLocator.Current.GetInstance<IContentRepository>();

            PageData p;

            try
            {
                p = repo.Get<PageData>(pageLink);
            }
            catch (PageNotFoundException)
            {
                return string.Empty;
            }

            if (p == null)
                return string.Empty;

            string b = string.Format(linkFormat, p.LinkURL, p.PageName);
            while (p.ParentLink != ContentReference.StartPage && p.ParentLink != ContentReference.RootPage)
            {
                p = repo.Get<PageData>(p.ParentLink);
                if (p.HasTemplate() && p.VisibleInMenu)
                {
                    b = string.Format(linkFormat, p.LinkURL, p.PageName) + " / " + b;
                }
            }
            return "<a href='/'>" + LocalizationService.Current.GetString("/search/home") + "</a> / " + b;
        }

		public DateTime Date { get; set; }
	}
}
