﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models.Search;
using StructureMap;

namespace Kwd.Kestrel.Library.Search.Providers
{
	public class GoogleSiteSearch : ISearchProvider
	{
		public ISearchResult Search(string searchString, string language, int hitsPerPage, int page)
		{
			var url = GssHelper.BuildQuery(searchString, page, hitsPerPage);

			var doc = new XmlDocument();

			doc.Load(url);

			var results = doc.DocumentElement.SelectSingleNode("/GSP/RES");

			var result = new GssResult();
			result.Hits = new List<ISearchHit>();
			foreach (XmlNode r in doc.DocumentElement.SelectNodes("/GSP/RES/R"))
			{
				result.Hits.Add(new GssHit(r));
			}

			//Suggestion text
			var suggestion = doc.DocumentElement.SelectSingleNode("/GSP/Spelling/Suggestion");
			if (suggestion != null)
			{
				result.SuggestionText = suggestion.InnerText;
				result.SuggestionUrl = suggestion.Attributes["q"].InnerText;
			}

			//Total hits
			var totalHits = 0;
			var total = doc.DocumentElement.SelectSingleNode("/GSP/RES/M");
			if (total != null)
			{
				int.TryParse(total.InnerText, out totalHits);
			}

			result.TotalHits = totalHits;
			return result;
		}
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public class GssResult : ISearchResult
	{
		public IList<ISearchHit> Hits { get; set; }

		public int TotalHits { get; set; }

		public string SuggestionText { get; set; }

		public string SuggestionUrl { get; set; }
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public class GssHit : ISearchHit
	{
		private string _breadCrumb;
		public string Url { get; set; }

		public string Title { get; set; }

		public string Target { get; set; }

		public string Snippet { get; set; }

		public string BreadCrumb
		{
			get
			{
				if (!string.IsNullOrEmpty(_breadCrumb))
					return _breadCrumb;

				_breadCrumb = GssHelper.BuildBreadCrumb(Url);
				return _breadCrumb;
			}
			set
			{
				_breadCrumb = value;
			}
		}

		public DateTime Date { get; set; }

		public GssHit()
		{

		}

		public GssHit(XmlNode r)
		{
			Url = r.SelectSingleNode("U") != null ? r.SelectSingleNode("U").InnerText : "";
			Title = r.SelectSingleNode("T") != null ? r.SelectSingleNode("T").InnerText : "&lt;No Title&gt;";
			Snippet = r.SelectSingleNode("S") != null ? r.SelectSingleNode("S").InnerText.Replace("<br>", " ") : "";
		}
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public static class GssHelper
	{
		private const string SearchUrlFormat = "http://www.google.com/search?q={0}&client=google-csbe&output=xml_no_dtd&cx={1}&start={2}&num={3}";

		public static string BuildQuery(string query, int page, int hitsPerPage)
		{
			var cx = string.Empty;
			var configProvider = ObjectFactory.GetInstance<IConfiguration>();

			var currentSiteId = SiteDefinition.Current.Name;
			var settingsForCurrentSite = configProvider.GoogleSiteSearch.Sites[currentSiteId];

			if (settingsForCurrentSite != null)
			{
				if(string.IsNullOrEmpty(settingsForCurrentSite.GoogleCx))
					throw new NullReferenceException(string.Format("GoogleCx is not set for site {0} in GoogleSiteSearch Sites element", currentSiteId));

				cx = settingsForCurrentSite.GoogleCx;
			}
			else
			{
				cx = configProvider.GoogleSiteSearch.GlobalGoogleCx;
				if (string.IsNullOrEmpty(cx))
				{
					throw new NullReferenceException(string.Format("GlobalGoogleCx is not set in GoogleSiteSearch element"));
				}
			}

			var start = 0;

			if (page > 1)
				start = (page - 1) * hitsPerPage;

			return string.Format(SearchUrlFormat, query, cx, start, hitsPerPage);
		}

		public static string BuildBreadCrumb(string url)
		{
			var linkFormat = "<a href='{0}'>{1}</a>";

			var ub = new UrlBuilder(url);
			Global.UrlRewriteProvider.ConvertToInternal(ub);
			var pageLink = PageReference.ParseUrl(ub.ToString());

			if (PageReference.IsNullOrEmpty(pageLink))
				return string.Empty;

			var repo = ServiceLocator.Current.GetInstance<IContentRepository>();

			PageData p;

			try
			{
				p = repo.Get<PageData>(pageLink);
			}
			catch (PageNotFoundException)
			{
				return string.Empty;
			}

			if (p == null)
				return string.Empty;

			string b = string.Format(linkFormat, p.LinkURL, p.PageName);
			while (p.ParentLink != ContentReference.StartPage && p.ParentLink != ContentReference.RootPage)
			{
				p = repo.Get<PageData>(p.ParentLink);
				if (p.HasTemplate() && p.VisibleInMenu)
				{
					b = string.Format(linkFormat, p.LinkURL, p.PageName) + " / " + b;
				}
			}
			return "<a href='/'>" + LocalizationService.Current.GetString("/search/home") + "</a> / " + b;
		}
	}
}
