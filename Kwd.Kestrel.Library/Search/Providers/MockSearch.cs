﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kwd.Kestrel.Library.Models.Search;

namespace Kwd.Kestrel.Library.Search.Providers
{
	public class MockSearch : ISearchProvider
	{
		public ISearchResult Search(string searchString, string language, int hitsPerPage, int offset)
		{
			var result = new MockResult();
			result.Hits = new List<ISearchHit>();
			result.Hits.Add(new MockHit() { Title = "DuckDuckGo", Snippet = "The alternative", Url = "http://duckduckgo.com" });
			result.Hits.Add(new MockHit() { Title = "Google", Snippet = "The big one", Url = "http://google.com" });
			result.Hits.Add(new MockHit() { Title = "Bing", Snippet = "The wannabe", Url = "http://bing.com" });
			result.Hits.Add(new MockHit() { Title = "Yahoo", Snippet = "Yawho?", Url = "http://yahoo.com" });
			result.SuggestionText = "Search master";
			result.SuggestionUrl = "http://#";
			result.TotalHits = 33;
			return result;
		}
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public class MockResult : ISearchResult
	{
		public IList<ISearchHit> Hits { get; set; }

		public int TotalHits { get; set; }

		public string SuggestionText { get; set; }

		public string SuggestionUrl { get; set; }
	}

	[SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
	public class MockHit : ISearchHit
	{
		public string Url { get; set; }

		public string Title { get; set; }

		public string Target { get; set; }

		public string Snippet { get; set; }

		public string BreadCrumb { get; set; }

		public DateTime Date { get; set; }
	}
}
