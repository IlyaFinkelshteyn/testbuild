﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kwd.Kestrel.Library.Configuration.Search;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models.Search;
using log4net;
using StructureMap;

namespace Kwd.Kestrel.Library.Search
{
	public interface ISearchService
	{
		ISearchResult Search(string searchString, string language, int hitsPerPage, int offset);
	}

	public class SearchService : ISearchService
	{
		private readonly IConfiguration _configurationProvider;
		private readonly ISearchProvider _searchProvider;
		private readonly ILogger _logProvider;

		[DefaultConstructor]
		public SearchService(ILogger logProvider, IConfiguration configurationProvider)
		{
			_logProvider = logProvider;
			_configurationProvider = configurationProvider;
			_searchProvider = this.GetSearchProvider();
		}

		public SearchService(ILogger logProvider, IConfiguration configurationProvider, ISearchProvider searchProvider)
		{
			_searchProvider = searchProvider;
			_logProvider = logProvider;
			_configurationProvider = configurationProvider;
		}

		private ISearchProvider GetSearchProvider()
		{
			var search = _configurationProvider.Search;
			SearchProviderElement providerElement = null;
			foreach (SearchProviderElement element in search.Providers)
			{
				if (element.ProviderName == search.DefaultProvider)
				{
					providerElement = element;
					break;
				}
			}

			if (providerElement == null)
			{
				_logProvider.LogWarning(string.Format("Unable to find search provider {0}.", search.DefaultProvider));
				throw new Exception(string.Format("Unable to find search provider {0}.", search.DefaultProvider));
			}

			var instObj = Activator.CreateInstance(providerElement.TypeAssembly, providerElement.TypeType);
			var searchProvider = (ISearchProvider)instObj.Unwrap();
			return searchProvider;
		}

		public ISearchResult Search(string searchString, string language, int hitsPerPage, int offset)
		{
			return _searchProvider.Search(searchString, language, hitsPerPage, offset);
		}
	}
}
