﻿using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Security;
using EPiServer.Web.WebControls;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;

namespace Kwd.Kestrel.Library.Controls.WebControls
{
	public class KWDPageTree : PageTree
	{
		protected override void CreatePreSortFilters()
		{
			if (this.EnableVisibleInMenu)
			{
				FilterCompareTo to = new FilterCompareTo("PageVisibleInMenu", "True");
				this.PageLoader.Filter += new FilterEventHandler(to.Filter);
			}
			if (this.PublishedStatus != PagePublishedStatus.Ignore)
			{
				FilterPublished published = new FilterPublished(this.PublishedStatus);
				this.PageLoader.Filter += new FilterEventHandler(published.Filter);
			}
			if (this.RequiredAccess != AccessLevel.NoAccess)
			{
				FilterAccess access = new FilterAccess(this.RequiredAccess);
				this.PageLoader.Filter += new FilterEventHandler(access.Filter);
			}
			this.PageLoader.Filter += new FilterEventHandler(FilterTemplatePages);
		}

		private void FilterTemplatePages(object sender, FilterEventArgs e)
		{
			PageDataCollection pages = e.Pages;

			for (int i = pages.Count - 1; i >= 0; i--)
			{

				if ((!pages[i].HasTemplate() && !(pages[i] is INotLinkedInMenu)) || (pages[i] is INeverVisibleInMenu))
				{
					pages.RemoveAt(i);

				}
			}
		}
	}
}