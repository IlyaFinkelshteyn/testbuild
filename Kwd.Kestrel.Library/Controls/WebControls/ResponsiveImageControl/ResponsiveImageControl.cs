﻿using System.Web.UI;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models;
using Kwd.Kestrel.Library.Models.ResponsiveImage;
using StructureMap;

namespace Kwd.Kestrel.Library.Controls.WebControls.ResponsiveImageControl
{
    public class ResponsiveImageControl : Control
    {
        private readonly IResponsiveImage _responsiveImageManager;
		private readonly ILogger _logProvider;
        private readonly ICache _cacheProvider;

        public ResponsiveImageData ImageData { get; set; }

        public ResponsiveImageControl()
        {
			_responsiveImageManager = ObjectFactory.GetInstance<IResponsiveImage>();
			_logProvider = ObjectFactory.GetInstance<ILogger>();
			_cacheProvider = ObjectFactory.GetInstance<ICache>();
        }

		public ResponsiveImageControl(IResponsiveImage responsiveImageManager, ILogger logProvider, ICache cacheProvider)
        {
			_responsiveImageManager = responsiveImageManager;
            _logProvider = logProvider;
            _cacheProvider = cacheProvider;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            var result = _responsiveImageManager.GetImage(ImageData);

            writer.Write(result);
        }
    }
}
