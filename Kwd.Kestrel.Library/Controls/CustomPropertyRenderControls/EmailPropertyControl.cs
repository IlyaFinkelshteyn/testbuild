﻿using System.Web.UI.WebControls;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using EPiServer.Web.PropertyControls;

namespace Kwd.Kestrel.Library.Controls.CustomPropertyRenderControls
{
	[TemplateDescriptor(TagString = "email")]
	public class EmailPropertyControl : PropertyStringControl, IRenderTemplate<string> 
	{ 
		public override void CreateDefaultControls() 
		{
			var link = new HyperLink();

			if (PropertyData.Value != null)
			{
				link.Text = PropertyData.Value.ToString();
				link.NavigateUrl = "mailto:" + PropertyData.Value.ToString();
			}
			else
			{
				link.Text = string.Empty;
			}

			Controls.Add(link);
		}
	}
}