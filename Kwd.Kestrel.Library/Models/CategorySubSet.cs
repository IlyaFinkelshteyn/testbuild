﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.DataAbstraction;

namespace Kwd.Kestrel.Library.Models
{
	public class CategorySubSet
	{
		public string Name;
		public int Id;
		public CategoryCollection Categories;
	}
}
