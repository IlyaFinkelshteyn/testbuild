﻿namespace Kwd.Kestrel.Library.Models
{
    public class BaseViewModel
    {
        public bool InEditMode { get; set; }
		private LayoutCustomisation _layoutCustomisation;
		public LayoutCustomisation LayoutCustomisation
		{
			get
			{
				
			    if (_layoutCustomisation != null)
			    {
			        return _layoutCustomisation;
			    }
			    else
			    {
			        _layoutCustomisation = new LayoutCustomisation();

			        return _layoutCustomisation;
			    }
			}
		    set
			{
				_layoutCustomisation = value;
			}
		}
    }
}