﻿using System.ComponentModel.DataAnnotations;

namespace Kwd.Kestrel.Library.Models
{
	public class LayoutCustomisation
	{
		/*
		 Properties added or removed must also be changed in EPiToolbox.GetLayoutCustomisation(PageData page)
		 */
		[Display(Name = "Hide left area")]
		public bool HideLeftArea { get; set; }
		[Display(Name = "Hide breadcrumbs")]
		public bool HideBreadcrumb { get; set; }
		[Display(Name = "Hide secondary content column")]
		public bool HideSecondaryContentColumn { get; set; }
	}
}