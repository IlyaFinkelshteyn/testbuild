﻿using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models
{
	public class MenuTreeItem : BaseLinkData
	{
		public int PageId { get; set; }
		public int Level { get; set; }
		public bool VisibleInMenu { get; set; }
		public bool Selected { get; set; }
		public bool Open { get; set; }

		private List<MenuTreeItem> _children = new List<MenuTreeItem>();

		public bool HasChildren
		{
			get
			{
				var hasChildren = _children != null && _children.Count > 0;
				return hasChildren;
			}
		}

		public List<MenuTreeItem> Items
		{
			get { return _children; }
			set { _children = value; }
		}

		//TODO:Check if this property is used. Want to remove from business logic. /Ove 2014-01-31
		private string _cssClass;
		public override string CssClass
		{
			get
			{
				if (!string.IsNullOrEmpty(_cssClass))
					return _cssClass;

				var css = base.CssClass;
				if (HasChildren)
					css += " haschildren";

				_cssClass = css;

				return css;
			}
			set
			{
				_cssClass = value;
			}
		}
	}
}
