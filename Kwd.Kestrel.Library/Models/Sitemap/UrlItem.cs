﻿using System;
using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.Sitemap
{
    public class UrlItem
    {
        // https://support.google.com/webmasters/answer/183668?hl=en

        private string _loc = string.Empty;
        public string Loc
        {
            get
            {
                return _loc;
            }
            set
            {
                if (value.StartsWith("/") || value.StartsWith("http"))
                {
                    _loc = value;
                }
                else
                {
                    _loc = "/" + value;
                }
            }
        }
        
        public DateTime? LastModified { get; set; }

        /// <summary>
        /// always. Use for pages that change every time they are accessed.
        /// hourly
        /// daily
        /// weekly
        /// monthly
        /// yearly
        /// never. Use this value for archived URLs.
        /// </summary>
        private string _changeFreq = string.Empty;
        public string ChangeFreq
        {
            get
            {
                return _changeFreq;
            }
            set
            {
                if (value == "always" || value == "hourly" || value == "daily" || value == "weekly" || value == "monthly" || value == "yearly" || value == "never")
                {
                    _changeFreq = value;
                }
            }
        }

        /// <summary>
        /// Describes the priority of a URL relative to all the other URLs on the site. This priority can range from 1.0 (extremely important) to 0.1 (not important at all).
        /// Does not affect your site's ranking in Google search results. Because this value is relative to other pages on your site, assigning a high priority (or specifying the same priority for all URLs) will not help your site's search ranking. In addition, setting all pages to the same priority will have no effect.
        /// </summary>
        private int _priority = 0;
        public int Priority
        {
            get
            {
                return _priority;
            }
            set
            {
                if (value > 0 && value < 101)
                {
                    _priority = value;
                }
                else
                {
                    _priority = 0;
                }
            }
        }

        public List<AlternateItem> AlternateUrls { get; set; }

        public List<ImageItem> ImageUrls { get; set; }

        public List<VideoItem> VideoUrls { get; set; }
    }
}