﻿namespace Kwd.Kestrel.Library.Models.Sitemap
{
    public class AlternateItem
    {
        public string Language { get; set; }

        private string _loc = string.Empty;
        public string Loc
        {
            get
            {
                return _loc;
            }
            set
            {
                if (value.StartsWith("/") || value.StartsWith("http"))
                {
                    _loc = value;
                }
                else
                {
                    _loc = "/" + value;
                }
            }
        }
    }
}