﻿namespace Kwd.Kestrel.Library.Models.Sitemap
{
    public abstract class MediaItem
    {
        /// <summary>
        /// Full URL to the media!
        /// Specifies the URL. For images and video, specifies the landing page (aka play page, referrer page). Must be a unique URL.
        /// Media item loc does not add "/" auto because of the fact it could be pointing to another domain. 
        /// </summary>
        private string _loc = string.Empty;
        public string Loc
        {
            get
            {
                return _loc;
            }
            set
            {
                if (value.StartsWith("http"))
                {
                    _loc = value;
                }
            }
        }
    }
}