﻿namespace Kwd.Kestrel.Library.Models.Sitemap
{
    public class VideoItem : MediaItem
    {
        /// <summary>
        /// Full URL to player
        /// </summary>
        private string _playerLoc = string.Empty;
        public string PlayerLoc
        {
            get
            {
                return _playerLoc;
            }
            set
            {
                if (value.StartsWith("http"))
                {
                    _playerLoc = value;
                }
            }
        }

        public bool PlayerAllowEmbed { get; set; }
        public string Autoplay { get; set; }

        /// <summary>
        /// Full Url to thumbnail.
        /// </summary>
        private string _thumbnailLoc = string.Empty;
        public string ThumbnailLoc
        {
            get
            {
                return _thumbnailLoc;
            }
            set
            {
                if (value.StartsWith("http"))
                {
                    _thumbnailLoc = value;
                }
            }
        }

        public string Title { get; set; }
        public string Description { get; set; }
    }
}