﻿namespace Kwd.Kestrel.Library.Models.GoogleMaps
{
	public class GoogleMapSettings
	{
		public int Zoom = 9;
		public string FilterScript = "''";
		public string MapType = "roadmap";
		public string Center = "null";
		public bool UseFitBounds = true;
		public bool MarkerCluster = true;
		public bool Scrollwheel = true;
		public bool Draggable = true;
		public bool DisableDoubleClickZoom = false;
		public int MinZoom = 0;
		public int MaxZoom = 18;

		public bool PanControl = false;
		public bool ZoomControl = true;
		public bool ScaleControl = true;
		public bool MapTypeControl = false;
		public bool RotateControl = true;
		public bool StreetViewControl = true;

		public string ToScript()
		{
			return string.Format(@"
			'MapSettings': {{
				'zoom': {0},
				'center': {1},
				'useFitBounds': {2},
				'MarkerCluster': {3},
				'scrollwheel': {4},
				'mapType':'{5}',
				'draggable':{6},
				'disableDoubleClickZoom':{7},
				'minZoom':{8},
				'maxZoom':{9},
				'zoomControl':{10},
				'panControl':{11},

				'scaleControl':{12},
				'mapTypeControl':{13},
				'rotateControl':{14},
				'streetViewControl':{15},
				'filterScript':{16}

			}}", Zoom, Center, UseFitBounds.ToString().ToLower(), MarkerCluster.ToString().ToLower(), Scrollwheel.ToString().ToLower(), MapType, Draggable.ToString().ToLower(), DisableDoubleClickZoom.ToString().ToLower(), MinZoom, MaxZoom, ZoomControl.ToString().ToLower(), PanControl.ToString().ToLower(), ScaleControl.ToString().ToLower(), MapTypeControl.ToString().ToLower(), RotateControl.ToString().ToLower(), StreetViewControl.ToString().ToLower(), FilterScript);
		}
	}
}
