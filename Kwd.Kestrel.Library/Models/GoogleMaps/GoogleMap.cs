﻿using System;
using System.Collections.Generic;
using System.Text;
using Kwd.Kestrel.Library.Epi.Interfaces;

namespace Kwd.Kestrel.Library.Models.GoogleMaps
{
	public class GoogleMap
	{
		public string ID { get; set; }
		public List<IHasCoordinates> Markers { get; set; }
		public GoogleMapSettings Settings { get; set; }

		public GoogleMap()
		{
			ID = new Random().Next(1000).ToString();
			Settings = new GoogleMapSettings();
		}

		public string MarkersObject()
		{
			var sb = new StringBuilder();

			var markerstring = string.Empty;
			var markerObjectFormat = @"
				'Markers': [
				{0}
				]
			";
			var markerFormat = @"
				{{
					'Position': {{
						'Lat': '{0}',
						'Lng': '{1}'
					}},
					'FilterKeys': [],
					'Headline': '{2}',
					'Content': '{3}'
				}}
			";

			for (var i = 0; i < Markers.Count; i++)
			{
				var m = Markers[i];
				markerstring += string.Format(markerFormat, m.Latitude, m.Longitude, m.Headline, m.Content);
				if (i < Markers.Count - 1)
				{
					markerstring += ",";
				}
			}

			return sb.AppendFormat(markerObjectFormat, markerstring).ToString();
		}

		public string ToScript()
		{
			var sb = new StringBuilder();
			sb.Append(@"{
			'Id': 'map-container" + ID + @"',
			" + Settings.ToScript() + @",
			'MarkerFilter': [],
			'Language': {},
			" + MarkersObject() + @"
			}");

			return sb.ToString();
		}
	}
}
