﻿namespace Kwd.Kestrel.Library.Models
{
	public class PageListItem
	{
		public string Heading { get; set; }
		public string Introduction { get; set; }
		public string LinkURL { get; set; }
		public string StartPublish { get; set; }
	}

}
