﻿namespace Kwd.Kestrel.Library.Models
{
	using System;
	using System.Net;
	using System.Web;

	public class BaseLinkData
	{
		private string _cssClass;

		public string Href { get; set; }
		public string Target { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public virtual string CssClass
		{
			get
			{
				if (!string.IsNullOrEmpty(_cssClass))
					return _cssClass;

				if (string.IsNullOrEmpty(Href)) return "broken";
				if (!Href.ToLower().Contains("http")) return "broken";

				try
				{
					var uri = new Uri(Href);
					var currentHost = HttpContext.Current.Request.Url.Host;
					var host = uri.Host;

					if (currentHost == host)
					{
						_cssClass += "internal";
					}
					else
					{
						var wc = new WebClient();
						wc.OpenRead(Href);
						var header = wc.ResponseHeaders["Content-Type"];
						wc.Dispose();

						if (header.Contains("image"))
							_cssClass = "image";

						if (header.Contains("application"))
							_cssClass = "file";

						if (header.Contains("text"))
							_cssClass = "external";
					}

					return _cssClass;
				}
				catch
				{
					return "broken";
				}
			}
			set
			{
				_cssClass = value;
			}
		}
	}
}
