﻿using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Cache;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Temp;
using StructureMap;

namespace Kwd.Kestrel.Library.Models
{
	public class LinkData : BaseLinkData
	{
		private ICache _cache;
		private IEpiManager _epiManager;
		private static object _myLock = new object();
		private FileInformation _fileInformation;
		public string Type { get; set; }
		public FileInformation FileInformation
		{
			get
			{
				var cacheKey = string.Format("FileInformation-{0}", Href);
				_cache = ObjectFactory.GetInstance<ICache>();
				_epiManager = ObjectFactory.GetInstance<IEpiManager>();
				if (_fileInformation != null)
					return _fileInformation;

				var cachedFi = _cache.Get(cacheKey) as FileInformation;
				if (cachedFi != null)
					return cachedFi;
				
				var fi = new FileInformation();
				fi.Type = Href.GetExtensionClass();

				var url = new UrlBuilder(Href);
                var resolver = ServiceLocator.Current.GetInstance<UrlResolver>();

                var mediaData = resolver.Route(url) as MediaData;
                if (mediaData != null)
                {
                    using (var s = mediaData.BinaryData.OpenRead())
                    {
                        fi.SizeInBytes = s.Length.ToString();
                        fi.SizeInKiloBytes = (s.Length/1024).ToString();
                        if (!string.IsNullOrEmpty(fi.Type))
                        {
							fi.FileSummary = String.Format(" ({0}, {1})", Href.FilenameExtension().ToUpper(),
								s.Length.ToFileSize());
                        }
                    }

					lock (_myLock)
					{
						_cache.Insert(cacheKey, fi, _epiManager.GetCacheEvictionPolicy());
					}
                }

				_fileInformation = fi;

				return _fileInformation;
			}
			set
			{
				_fileInformation = value;
			}
		}
	}
}
