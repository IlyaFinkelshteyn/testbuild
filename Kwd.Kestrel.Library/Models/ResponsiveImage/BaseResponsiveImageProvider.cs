using Kwd.Kestrel.Library.Managers;
using StructureMap;

namespace Kwd.Kestrel.Library.Models.ResponsiveImage
{
    public abstract class BaseResponsiveImageProvider
    {
        protected ICache Cache;
        protected ILogger Log;

        protected BaseResponsiveImageProvider()
        {
			Cache = ObjectFactory.GetInstance<ICache>();
			Log = ObjectFactory.GetInstance<ILogger>();
        }

		protected BaseResponsiveImageProvider(ICache cache, ILogger logger)
        {
            Cache = cache;
            Log = logger;
        }
    }
}
