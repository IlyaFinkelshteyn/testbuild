using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.ResponsiveImage
{
    public interface IResponsiveImageProvider
    {
        IResponsiveImageResult GetImage(ResponsiveImageData imageData, List<ViewPort> viewPorts, int cacheTimeoutMinutes);
    }
}