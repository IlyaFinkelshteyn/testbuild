﻿using System;
using Kwd.Kestrel.Library.Configuration.ResponsiveImages;

namespace Kwd.Kestrel.Library.Models.ResponsiveImage
{
	public class ViewPort
	{
		public int Id { get; set; }
		public string ImageType { get; set; }

		public string Media { get; private set; }
		public int Width { get; private set; }
		public byte? RetinaQuality { get; private set; }
		public byte? DesktopQuality { get; private set; }

		public ViewPort(ImageTypeElement element)
		{
			Id = element.Id;
			ImageType = element.Name;
			Media = element.Media;
			Width = element.Width;

			byte? retinaQualityByte = Convert.ToByte(element.RetinaQuality);
			byte? desktopQualityByte = Convert.ToByte(element.DesktopQuality);
			RetinaQuality = retinaQualityByte;
			DesktopQuality = desktopQualityByte;
		}

		public ViewPort(string media, int width, int retinaQuality, int desktopQuality)
		{
			Media = media;
			Width = width;

			byte? retinaQualityByte = Convert.ToByte(retinaQuality);
			byte? desktopQualityByte = Convert.ToByte(desktopQuality);
			RetinaQuality = retinaQualityByte;
			DesktopQuality = desktopQualityByte;
		}

		public ViewPort(string media, int width)
		{
			Media = media;
			Width = width;
		}

	}
}
