using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.ResponsiveImage
{
    public interface IResponsiveImageResult
    {
        BaseImage Image { get; set; }
    }

    public class BaseImage
    {
        public string Url
        {
            get
            {
                var result = string.Empty;
                if (Path.StartsWith("http"))
                {
                    result = Path;
                }
                else
                {
                    result = string.Format("{0}{1}", SubDomain, Path);
                }
                return result;
            }
        }

        public string SubDomain { get; set; }
        public string Path { get; set; }
        public string Alt { get; set; }
        public string Title { get; set; }
        public string CssClass { get; set; }
        public string Attribution { get; set; }
        public List<ViewPortImage> ViewPortImages { get; set; }
        public int OriginalWidth { get; set; }
        public int OriginalHeight { get; set; }
    }

    public class ViewPortImage : BaseImage
    {
        public string Media { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public byte? Quality { get; set; }
    }
}