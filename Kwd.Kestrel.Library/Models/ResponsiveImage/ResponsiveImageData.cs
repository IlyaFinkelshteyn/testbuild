﻿namespace Kwd.Kestrel.Library.Models.ResponsiveImage
{
    public class ResponsiveImageData
    {
        public string Alt { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public string ImageType { get; set; }
        public string CssClass { get; set; }
		public string PropertyName { get; set; }
		public string Itemprop { get; set; }
		public string Attribution { get; set; }
        public string ResourceSubdomain { get; set; }
    }
}
