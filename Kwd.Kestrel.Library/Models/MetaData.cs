﻿using System;
using System.Globalization;

namespace Kwd.Kestrel.Library.Models
{
	/// <summary>
	/// This object should be used to contain all meta data for a page.
	/// It can be populated differently depending of the PageType. 
	/// See GetPageMetaData in the PageType to see the specific business logic. 
	/// If you can not find the GetPageMetaData method in PageType you need to look in derived class /es to find the method.
	/// </summary>
	public class MetaData
	{
		/// <summary>
		/// The ID for the specific page.
		/// </summary>
		public int PageId { get; set; }

		public string PageName { get; set; }

		/// <summary>
		/// The browser title for the specific page.
		/// </summary>
		public string MetaTitle { get; set; }

		/// <summary>
		/// The meta description content for the specific page.
		/// </summary>
		public string MetaDescription { get; set; }

		/// <summary>
		/// The meta keywords for the spcific page.
		/// </summary>
		public string MetaKeywords { get; set; }

		public bool RobotsNoIndex { get; set; }

		public DateTime StartPublish { get; set; }
		public DateTime Changed { get; set; }
		public DateTime StopPublish { get; set; }
	
		public string TypeOfContent { get; set; }
		public string LinkURL { get; set; }
		public string ContentOwner { get; set; }
		public CultureInfo Language { get; set; }
		public string PageUrl { get; set; }
		public string PageTargetGroup { get; set; }
		public string PageSubTargetGroup { get; set; }
	}
}
