﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Models
{
	public class ImageInformation
	{
		public int Height;
		public int Width;
		public float Dpi;
	}
}
