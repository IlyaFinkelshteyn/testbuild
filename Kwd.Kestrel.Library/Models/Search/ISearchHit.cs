using System;

namespace Kwd.Kestrel.Library.Models.Search
{
    public interface ISearchHit
    {
        string Url { get; set; }
        string Title { get; set; }
        string Target { get; set; }
        string Snippet { get; set; }
        string BreadCrumb { get; set; }
        DateTime Date { get; set; }
    }
}