namespace Kwd.Kestrel.Library.Models.Search
{
    public interface ISearchProvider
    {
        ISearchResult Search(string searchString, string language, int hitsPerPage, int page);
    }
}