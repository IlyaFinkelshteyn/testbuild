using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.Search
{
    public interface ISearchResult
    {
        IList<ISearchHit> Hits { get; set; }
        int TotalHits { get; set; }
        string SuggestionText { get; set; }
        string SuggestionUrl { get; set; }
    }
}