﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;

namespace Kwd.Kestrel.Library.Models
{
	public class MenuLinkData : BaseLinkData
	{
		public MenuLinkData(PageData pd)
		{
			var ub = new UrlBuilder(pd.LinkURL);
			Global.UrlRewriteProvider.ConvertToExternal(
			   ub,
			   pd.PageLink,
			   System.Text.Encoding.UTF8);

			var url = ub.Uri.OriginalString;

			PageData = pd;
			Text = pd.Name;
			Title = pd.Name;
			Href = url;
			ContentLink = pd.PageLink;
			Children = new List<MenuLinkData>();
		}

		public ContentReference ContentLink { get; set; }

		public PageData PageData { get; set; }

		public List<MenuLinkData> Children { get; set; }

		public bool HasChildren
		{
			get
			{
				return Children.Any();
			}
		}

		public string CssClass { get; set; }

		public string LinkCssClass { get; set; }

		/// <summary>
		/// Check if the currentpage is somewhere in the childs or grandchilds.
		/// </summary>
		public bool IsOpen(int currentPageId)
		{
			var isOpen = false;

			foreach (var child in Children)
			{
				if (!isOpen)
				{
					// first we check if the child is the page we are looking for.
					if (child.PageData.PageLink.ID == currentPageId)
					{
						isOpen = true;
					}
				}

				// We will check the grandchilds if the currentpage exist there.
				if (!isOpen)
				{
					isOpen = child.IsOpen(currentPageId);
				}
			}

			return isOpen;
		}

		/// <summary>
		/// Check if the currentPageId is equal to the the current object.
		/// </summary>
		public bool IsSelected(int currentPageId)
		{
			var isSelected = PageData.PageLink.ID == currentPageId;

			return isSelected;
		}
	}
}
