using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.Excel
{
    public class ExcelSheet
    {
        public string Name { get; set; }
        public List<ExcelDetailedRow> Rows { get; set; }

        public ExcelSheet()
        {
            SetUpDefault();
        }

		private ExcelRow[] _rows;
		public ExcelDetailedRow this[int row]
		{
			get
			{
				// This indexer is very simple, and just returns or sets
				// the corresponding element from the internal array.
				return Rows[row];
			}
		}

        public List<ExcelDetailedCell> GetHeadlineColumns()
        {
            var columns = new List<ExcelDetailedCell>();

            if (Rows != null && Rows.Count != 0)
            {
                columns = Rows[0].Cells;
            }

            return columns;
        }

        private void SetUpDefault()
        {
            Rows = new List<ExcelDetailedRow>();
        }
    }

}
