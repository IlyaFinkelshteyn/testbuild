using System.Collections.Generic;
using Syncfusion.XlsIO;

namespace Kwd.Kestrel.Library.Models.Excel
{
    public class ExcelDetailedRow : ExcelRow
    {
        public int OutlineLevel { get; private set; }
        public double Height { get; private set; }

		public ExcelCell this[int col]
		{
			get
			{
				// This indexer is very simple, and just returns or sets
				// the corresponding element from the internal array.
				return Cells[col];
			}
		}

        public ExcelDetailedRow(IRange row, IWorksheet sheet, int rowNr)
        {
            OutlineLevel = row.RowGroupLevel;
            Height = row.RowHeight;
            var list = new List<ExcelDetailedCell>();
            for (int j = 1; j <= row.Columns.Length + 10; ++j)
            {
                var cell = sheet.Range[rowNr, j];
                int colspan;
                if (cell.IsMerged)
                {
                    colspan = cell.MergeArea.Columns.Length;
                }
                else
                {
                    colspan = 1;
                }
                list.Add(new ExcelDetailedCell(cell, colspan));
                if (colspan > 1)
                {
                    j += colspan - 1;
                }
            }
            while (list.Count > 0 && list[list.Count - 1].IsEmpty)
            {
                list.RemoveAt(list.Count - 1);
            }
            Cells = list;
            var format = ExcelDetailedCell.ConsolidateFormats(Cells);
            if (row.Columns.Length > 0)
            {
                var firstCol = new ExcelDetailedCell(sheet.Range[rowNr, 1], 1);
                if (!string.IsNullOrEmpty(firstCol.Text))
                {
                    format.AddRange(firstCol.Text.Split(' '));
                }
            }
            if (OutlineLevel > 0)
            {
                format.Add("grouped");
                format.Add("lev" + OutlineLevel);
            }
            Format = string.Join(" ", format);
        }
    }
}
