using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using Syncfusion.XlsIO;

namespace Kwd.Kestrel.Library.Models.Excel
{
    public class ExcelDetailedCell : ExcelCell
    {
        public string Comment { get; private set; }
        public bool IsBold { get; private set; }
        public bool IsItalic { get; private set; }
        public ExcelLineStyle LineUnderStyle { get; private set; }
        public ExcelLineStyle LineOverStyle { get; private set; }
        public Color LineUnderColor { get; private set; }
        public Color LineOverColor { get; private set; }
        public double Width { get; private set; }
        public int DecimalPlaces { get; private set; }
        public int Colspan { get; private set; }

        public ExcelDetailedCell(IRange cell, int colspan)
        {
            Colspan = colspan;
            Width = cell.ColumnWidth;
            Format = Regex.Replace(cell.CellStyle.Name.ToLower(), "[^0-9a-z]", string.Empty);
            if (Format.Equals("Normal", StringComparison.InvariantCultureIgnoreCase))
            {
                Format = string.Empty;
            }
            string str = string.Empty;
            int k;
            try
            {
                if (cell.HasRichText)
                {
                    IRichTextString txt = cell.RichText;
                    for (k = 0; k < txt.Text.Length; ++k)
                    {
                        if (txt.GetFont(k).Subscript)
                        {
                            str = str + "<sub>" + txt.Text[k] + "</sub>";
                        }
                        else if (txt.GetFont(k).Superscript)
                        {
                            str = str + "<sup>" + txt.Text[k] + "</sup>";
                        }
                        else
                        {
                            str = str + txt.Text[k];
                        }
                    }
                    str = str.Replace("</sub><sub>", string.Empty);
                    str = str.Replace("</sup><sup>", string.Empty);
                }
                else
                {
                    str = cell.Value;
                }
            }
            catch
            {
                str = cell.Value;
            }
            if (str.StartsWith("  "))
            {
                str = "&nbsp;&nbsp;" + str.Trim();
            }
            Text = str.Replace("\n", "<br />");
            try
            {
                if (cell.HasNumber)
                {
                    double d = cell.Number;
                    Number = d;
                    NumberFormat = cell.NumberFormat;
                    int decimalPlaces = cell.CellStyle.NumberFormatSettings.DecimalPlaces;
                    if (decimalPlaces == -1)
                    {
                        decimalPlaces = 0;
                        double t = d;
                        while (Math.Abs(t - Math.Round(t)) > 0.0000001 && decimalPlaces < 10)
                        {
                            ++decimalPlaces;
                            t = t * 10;
                        }
                    }
                    DecimalPlaces = decimalPlaces;
                }
                if (cell.HasFormula)
                {
                    double d = cell.FormulaNumberValue;
                    Number = d;
                    NumberFormat = cell.NumberFormat;
                    int decimalPlaces = cell.CellStyle.NumberFormatSettings.DecimalPlaces;
                    if (decimalPlaces == -1)
                    {
                        decimalPlaces = 0;
                        double t = d;
                        while (Math.Abs(t - Math.Round(t)) > 0.0000001 && decimalPlaces < 10)
                        {
                            ++decimalPlaces;
                            t = t * 10;
                        }
                    }
                    DecimalPlaces = decimalPlaces;
                }
            }
            catch
            {
            }

            if (cell.Borders[ExcelBordersIndex.EdgeTop].LineStyle != ExcelLineStyle.None)
            {
                LineOverStyle = cell.Borders[ExcelBordersIndex.EdgeTop].LineStyle;
                LineOverColor = cell.Borders[ExcelBordersIndex.EdgeTop].ColorRGB;
            }
            else
            {
                LineOverStyle = ExcelLineStyle.None;
                LineOverColor = Color.Transparent;
            }
            if (cell.Borders[ExcelBordersIndex.EdgeBottom].LineStyle != ExcelLineStyle.None)
            {
                LineUnderStyle = cell.Borders[ExcelBordersIndex.EdgeBottom].LineStyle;
                LineUnderColor = cell.Borders[ExcelBordersIndex.EdgeBottom].ColorRGB;
            }
            else
            {
                LineUnderStyle = ExcelLineStyle.None;
                LineUnderColor = Color.Transparent;
            }
            if (cell.CellStyle.Font.Bold)
            {
                IsBold = true;
            }
            if (cell.CellStyle.Font.Italic)
            {
                IsItalic = true;
            }
            if (cell.Comment != null && cell.Comment.Text != null && cell.Comment.Text != string.Empty)
            {
                Comment = cell.Comment.Text;
            }
        }

        public static List<string> ConsolidateFormats(List<ExcelDetailedCell> cells)
        {
            if (cells == null || cells.Count == 0)
            {
                return new List<string>();
            }
            if (cells.Count == 1)
            {
                List<string> f = cells[0].Format.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                cells[0].Format = string.Empty;
                return f;
            }
            List<string>[] formats = new List<string>[cells.Count];
            for (int i = 0; i < cells.Count; ++i)
            {
                formats[i] = cells[i].Format.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            List<string> rowFormat = new List<string>();
            for (int i = 0; i < formats[0].Count; ++i)
            {
                string format = formats[0][i];
                bool sharedFormat = true;
                for (int j = 1; j < cells.Count; ++j)
                {
                    if (!formats[j].Contains(format))
                    {
                        sharedFormat = false;
                        break;
                    }
                }
                if (sharedFormat)
                {
                    rowFormat.Add(format);
                    for (int j = 0; j < cells.Count; ++j)
                    {
                        formats[j].Remove(format);
                    }
                    --i;
                }
            }
            for (int i = 0; i < cells.Count; ++i)
            {
                cells[i].Format = string.Join(" ", formats[i]);
            }
            return rowFormat;
        }
    }
}
