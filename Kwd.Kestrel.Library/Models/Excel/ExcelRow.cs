using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models.Excel
{
    public class ExcelRow
    {
        public List<ExcelDetailedCell> Cells { get; protected set; }
        public string Format { get; protected set; }

        public int Colums
        {
            get { return Cells.Count; }
        }
    }
}
