namespace Kwd.Kestrel.Library.Models.Excel
{
    public class ExcelCell
    {
        public string NumberFormat { get; protected set; }
        public string Format { get; internal set; }
        public double? Number { get; protected set; }
		public string Comment { get; private set; }
		public int Colspan { get; private set; }
		public int DecimalPlaces { get; private set; }
        public string Text { get; protected set; }

        public bool IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(Text) && !Number.HasValue;
            }
        }
    }
}
