﻿using System.Text;

namespace Kwd.Kestrel.Library.Models
{
	public class MetaTargetGroup
	{
		public StringBuilder TargetGroupValue { get; set; }
		public StringBuilder TargetSubGroupValue { get; set; }
	}
}
