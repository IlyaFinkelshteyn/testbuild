﻿using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Models
{
	/// <summary>
	/// Used by mobile menu
	/// </summary>
	public class MenuTreeData
	{
		public List<MenuTreeItem> Items { get; set; }
	}
}
