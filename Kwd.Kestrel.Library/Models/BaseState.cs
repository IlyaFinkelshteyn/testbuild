﻿using System;
using System.Globalization;

namespace Kwd.Kestrel.Library.Models
{
	[Obsolete("Should use BasePageData in the WebsiteContext.", true)]
    public class BaseState
    {
        public int PageId { get; set; }
        public string PageName { get; set; }
        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public bool RobotsNoIndex { get; set; }
        public DateTime StartPublish { get; set; }
        public DateTime Changed { get; set; }
        public DateTime StopPublish { get; set; }
        public string TypeOfContent { get; set; }
        public string LinkURL { get; set; }
        public string SiteLabel { get; set; }
        public string ContentOwner { get; set; }
        public CultureInfo Language { get; set; }
    }
}