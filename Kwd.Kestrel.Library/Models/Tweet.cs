﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kwd.Kestrel.Library.Interfaces;
using Kwd.Kestrel.Library.Managers;
using LinqToTwitter;
using StructureMap;

namespace Kwd.Kestrel.Library.Models
{
	using EPiServer;

	public class Tweet : ICanBePresentedAsNewsFeedItem
	{
		public Guid Id { get; set; }
		public string Sid { get; set; }
		public string ScreenName { get; set; }
		public string Text { get; set; }

		private DateTime _createAt;
		public DateTime CreatedAt
		{
			get
			{
				return _createAt;
			}
			set
			{
				PublishDate = value;
				_createAt = value;
			}
		}

		public DateTime PublishDate { get; set; }
		public bool PossiblySensitive { get; set; }
		public virtual Url Image { get; set; }
		
		public string CssClass
		{
			get
			{
				return "tweet";
			}
		}
		public string Title {
			get
			{
				return "@" + ScreenName;
			}
		}

		public DateTime Date
		{
			get
			{
				return CreatedAt;
			}
		}
		public string Body 
		{
			get
			{
				return Regex.Replace(Text, @"(http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$)", "<a target='_blank' href='$1'>$1</a>");
			}
		}

		public string LinkUrl
		{
			get
			{
				return string.Empty;
			}
		}

		public string ContentType
		{
			get
			{
				var mgr = ObjectFactory.GetInstance<IEpiManager>();
				return mgr.Translate("/newsroom/twitter");
			}
		}

		public string ImageAltText
		{
			get
			{
				return string.Empty;
			}
		}

		public string ImageTitleText
		{
			get
			{
				return string.Empty;
			}
		}

		public DateTime SortDate
		{
			get
			{
				return Date;
			}
		}

		public bool Sticky
		{
			get
			{
				return false;
			}
		}

		public int CompareTo(object obj)
		{
			return DateTime.Compare(((ICanBePresentedAsNewsFeedItem)obj).SortDate, this.SortDate);
		}
	}
}
