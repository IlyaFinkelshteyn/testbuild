﻿using System;
using System.Drawing;
using EPiServer.Core;

namespace Kwd.Kestrel.Library.Models
{
	public class FileInformation
	{
		private ImageInformation _imageInformation;
		public string Name { get; set; }
		public string Type { get; set; }
		public string FileSummary { get; set; }
		public string SizeInBytes { get; set; }
		public MediaData File { get; set; }
		public string SizeInKiloBytes { get; set; }
		public DateTime DatePublished { get; set; }

		public ImageInformation ImageInformation
		{
			get
			{
				if (_imageInformation != null)
					return _imageInformation;

				if (File == null)
					return new ImageInformation();

				Bitmap image;
				using (var stream = File.BinaryData.OpenRead())
				{
					image = (Bitmap)Image.FromStream(stream);
				}

				var ii = new ImageInformation();

				if (image != null)
				{
					ii.Dpi = image.HorizontalResolution;
					ii.Height = image.Height;
					ii.Width = image.Width;
				}

				_imageInformation = ii;

				return _imageInformation;
			}
		}
	}
}
