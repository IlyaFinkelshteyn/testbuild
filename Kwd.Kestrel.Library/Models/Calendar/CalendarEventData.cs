﻿using System;

namespace Kwd.Kestrel.Library.Models.Calendar
{
    public class CalendarEventData
    {
        public string Organizer { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string LinkUrl { get; set; }
        public string Summery { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
    }
}
