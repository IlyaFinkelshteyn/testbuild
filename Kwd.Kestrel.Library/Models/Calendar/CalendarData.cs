﻿using System;
using System.Collections.Generic;
using System.Text;
using Kwd.Kestrel.Library.ExtensionMethods;

namespace Kwd.Kestrel.Library.Models.Calendar
{
    public class CalendarData
    {
        private const string Dateformat = "yyyyMMddTHHmmssZ";
        public string FileName { get; set; }
        public List<CalendarEventData> Events { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append("BEGIN:VCALENDAR");
            sb.Append("\nVERSION:2.0");
            sb.Append("\nMETHOD:PUBLISH");

            // Create each event
            foreach (var calendarEvent in Events)
            {
                sb.Append("\nBEGIN:VEVENT");
                sb.Append("\nORGANIZER:MAILTO:" + calendarEvent.Organizer);
                if (calendarEvent.StartDate != DateTime.MinValue)
                {
                    sb.Append("\nDTSTART:" + calendarEvent.StartDate.ToUniversalTime().ToString(Dateformat));
                }
                if (calendarEvent.EndDate != DateTime.MinValue)
                {
                    sb.Append("\nDTEND:" + calendarEvent.EndDate.ToUniversalTime().ToString(Dateformat));
                }
                sb.Append("\nLOCATION:" + calendarEvent.Location);
                sb.Append("\nUID:" + calendarEvent.LinkUrl);
                sb.Append("\nDTSTAMP:" + DateTime.Now.ToUniversalTime().ToString(Dateformat));
                sb.Append("\nSUMMARY:" + calendarEvent.Summery);
                sb.Append("\nDESCRIPTION:" + calendarEvent.Description.RemoveHtml());
                sb.Append("\nPRIORITY:"+ calendarEvent.Priority);
                sb.Append("\nCLASS:PUBLIC");
                sb.Append("\nEND:VEVENT");
            }

            sb.Append("\nEND:VCALENDAR");

            return sb.ToString();
        }
    }
}
