﻿using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Models
{
	public class NewsListItem
	{
		public string Headline { get; set; }
		public string PublishedDate { get; set; }
		public string Introduction { get; set; }
		public string LinkUrl { get; set; }
		public ResponsiveImageData ImageData { get; set; }
	}
}
