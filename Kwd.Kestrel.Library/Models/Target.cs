﻿namespace Kwd.Kestrel.Library.Models
{
	public class Target
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Indent { get; set; }
	}
}
