﻿using System;
using Kwd.Kestrel.Library.Interfaces;

namespace Kwd.Kestrel.Library.Models
{
	using EPiServer;

	public class NewsFeedItem : IComparable, ICanBePresentedAsNewsFeedItem
	{
		public string CssClass { get; set; }
		public string Title { get; set; }
		public DateTime Date { get; set; }
		public string Body { get; set; }
		public string LinkUrl { get; set; }
		public string ContentType { get; set; }
		public string ImageAltText { get; set; }
		public string ImageTitleText { get; set; }
		public bool Sticky { get; set; }
		public DateTime SortDate { get; set; }
		public DateTime PublishDate { get; set; }
		public virtual Url Image { get; set; }

        public NewsFeedItem(string cssClass, string contentType, string title, DateTime date, string body, bool sticky, int feedCount, string linkUrl, string imageAltText, string imageTitleText)
        {
            if (string.IsNullOrEmpty(cssClass))
                CssClass = "y" + date.Year;
            else
                CssClass = cssClass + " y" + date.Year;

            Title = title;
            if (contentType.Length > 0)
            {
                ContentType = "<span class=\"contenttype\">" + contentType + "</span>"; //TODO: Try to move html to presentation layer and not in the logic.
            }
            else
            {
                ContentType = string.Empty;
            }
            Date = date;
            Body = body;
	        Sticky = sticky;
            SortDate = sticky ? Date.AddYears(50) : Date;
	        PublishDate = Date;

            LinkUrl = linkUrl;

            ImageAltText = imageAltText;
            ImageTitleText = imageTitleText;
        }

		public NewsFeedItem(ICanBePresentedAsNewsFeedItem item)
		{
			CssClass = item.CssClass + " y" + item.Date.Year;
			Title = item.Title;
			Date = item.Date;
			ContentType = item.ContentType.Length > 0 ? "<span class=\"contenttype\">" + item.ContentType + "</span>" : string.Empty;
			Body = item.Body;
			LinkUrl = item.LinkUrl;
			ImageAltText = item.ImageAltText;
			ImageTitleText = item.ImageTitleText;
		}

		public int CompareTo(object obj)
		{
			return DateTime.Compare(((ICanBePresentedAsNewsFeedItem)obj).SortDate, this.SortDate);
		}
	}
}