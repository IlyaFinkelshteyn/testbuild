﻿using EPiServer.SpecializedProperties;

namespace Kwd.Kestrel.Library.Models
{
    public class LinkListContainer
    {
        public int Index { get; set; }
        public string Headline { get; set; }
        public LinkItemCollection LinkCollection { get; set; }
    }
}