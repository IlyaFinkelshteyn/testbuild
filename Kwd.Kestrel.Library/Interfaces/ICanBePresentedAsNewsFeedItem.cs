﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.HtmlParsing;

namespace Kwd.Kestrel.Library.Interfaces
{
	using EPiServer;

	public interface ICanBePresentedAsNewsFeedItem : IComparable
	{
		string CssClass { get;}
		string Title { get; }
		DateTime Date { get; }
		string Body { get; }
		string LinkUrl { get; }
		string ContentType { get; }
		string ImageAltText { get; }
		string ImageTitleText { get; }
		DateTime SortDate { get; }
		DateTime PublishDate { get; }
		bool Sticky { get; }
		Url Image { get; }
	}
}
