﻿namespace Kwd.Kestrel.Library.Interfaces
{
    /// <summary>
    /// Interface used to indicate for UserControlBase if it should Render the HTML output without whitespaces.
    /// </summary>
    public interface INoneWhitespaceOutput
    {
    }
}