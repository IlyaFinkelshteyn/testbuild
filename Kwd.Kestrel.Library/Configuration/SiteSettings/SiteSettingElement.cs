﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.SiteSettings
{
    public class SiteSettingElement : ConfigurationElement
    {
        [ConfigurationProperty("SiteId", IsKey = true, DefaultValue = "NA", IsRequired = true)]
        public string SiteId
        {
            get
            {
                return (string)this["SiteId"];
            }
            set
            {
                this["SiteId"] = value;
            }
        }

		[ConfigurationProperty("BrowserTitlePattern", IsRequired = true)]
		public string BrowserTitlePattern
		{
			get { return (string)this["BrowserTitlePattern"]; }
		}

		[ConfigurationProperty("LogoUrl", DefaultValue = "", IsRequired = true)]
		public string LogoUrl
		{
			get { return (string)this["LogoUrl"]; }
		}

		[ConfigurationProperty("ResourceSubdomainList", DefaultValue = "", IsRequired = true)]
		public string ResourceSubdomainList
		{
			get { return (string)this["ResourceSubdomainList"]; }
		}

        [ConfigurationProperty("RobotsTxtAllowedSites", DefaultValue = "", IsRequired = true)]
        public string RobotsTxtAllowedSites
        {
            get { return (string)this["RobotsTxtAllowedSites"]; }
        }

        [ConfigurationProperty("SiteUrlForFront", DefaultValue = "", IsRequired = true)]
        public string SiteUrlForFront
        {
            get { return (string)this["SiteUrlForFront"]; }
        }

		[ConfigurationProperty("CacheMobileNavigation", DefaultValue = false)]
		public bool CacheMobileNavigation
		{
			get { return (bool)this["CacheMobileNavigation"]; }
		}
    }
}