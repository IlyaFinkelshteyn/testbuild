﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.SiteSettings
{
    public class SiteSettingCollection : ConfigurationElementCollection
    {
        public new SiteSettingElement this[string index]
        {
            get { return (SiteSettingElement)BaseGet(index); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteSettingElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteSettingElement)element).SiteId;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public void AddElement(SiteSettingElement element)
        {
            BaseAdd(element);
        }
    }
}