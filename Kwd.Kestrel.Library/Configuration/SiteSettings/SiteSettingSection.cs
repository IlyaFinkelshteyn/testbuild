﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.SiteSettings
{
	/// <summary>
	/// Site settings for the sites.
	/// </summary>
    public class SiteSettingSection : ConfigurationSection
    {
        private SiteSettingCollection _searchProviderCollection;

        [ConfigurationProperty("Sites")]
        public SiteSettingCollection Sites
        {
            get
            {
                if (_searchProviderCollection == null)
                {
                    if (base["Sites"] != null && ((ConfigurationElement)base["Sites"]).ElementInformation.IsPresent)
                    {
                        _searchProviderCollection = (SiteSettingCollection)base["Sites"];
                    }
                    else
                    {
                        _searchProviderCollection = new SiteSettingCollection();
                        _searchProviderCollection.AddElement(new SiteSettingElement() { SiteId = string.Empty });
                    }
                }

                return _searchProviderCollection;
            }
        }
    }
}