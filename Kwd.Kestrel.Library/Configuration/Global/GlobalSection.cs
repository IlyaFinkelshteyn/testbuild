﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.Global
{
	/// <summary>
	/// Contain global settings used on all sites in the installation, not settings that are website specific.
	/// </summary>
    public class GlobalSection : ConfigurationSection
    {
    }
}