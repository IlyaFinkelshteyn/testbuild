﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.Search
{
    public class SearchProviderCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SearchProviderElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SearchProviderElement)element).ProviderName;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public void AddElement(SearchProviderElement element)
        {
            BaseAdd(element);
        }
    }
}