﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Configuration.Search.GoogleSiteSearch
{
	public class GoogleSiteSearchSiteSettingCollection : ConfigurationElementCollection
	{
		public new GoogleSiteSearchSettingElement this[string index]
		{
			get { return (GoogleSiteSearchSettingElement)BaseGet(index); }
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new GoogleSiteSearchSettingElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((GoogleSiteSearchSettingElement)element).SiteId;
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}

		public void AddElement(GoogleSiteSearchSettingElement element)
		{
			BaseAdd(element);
		}
	}
}
