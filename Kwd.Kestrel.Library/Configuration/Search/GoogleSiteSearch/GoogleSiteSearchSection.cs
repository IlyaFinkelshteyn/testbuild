﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Configuration.Search.GoogleSiteSearch
{
	public class GoogleSiteSearchSection : ConfigurationSection
	{
		private GoogleSiteSearchSiteSettingCollection _googleSiteSearchSettingCollection;
		[ConfigurationProperty("GlobalGoogleCx")]
		public string GlobalGoogleCx
		{
			get
			{
				return this["GlobalGoogleCx"].ToString();
			}
			set
			{
				this["GlobalGoogleCx"] = value;
			}
		}

		[ConfigurationProperty("Sites")]
		public GoogleSiteSearchSiteSettingCollection Sites
		{
			get
			{
				if (_googleSiteSearchSettingCollection == null)
				{
					if (base["Sites"] != null && ((ConfigurationElement)base["Sites"]).ElementInformation.IsPresent)
					{
						_googleSiteSearchSettingCollection = (GoogleSiteSearchSiteSettingCollection)base["Sites"];
					}
					else
					{
						_googleSiteSearchSettingCollection = new GoogleSiteSearchSiteSettingCollection();
						_googleSiteSearchSettingCollection.AddElement(new GoogleSiteSearchSettingElement() { SiteId = string.Empty });
					}
				}

				return _googleSiteSearchSettingCollection;
			}
		}
	}
}
