﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Configuration.Search.GoogleSiteSearch
{
	public class GoogleSiteSearchSettingElement : ConfigurationElement
	{
		[ConfigurationProperty("SiteId", IsKey = true, DefaultValue = "NA", IsRequired = true)]
		public string SiteId
		{
			get
			{
				return (string)this["SiteId"];
			}
			set
			{
				this["SiteId"] = value;
			}
		}

		[ConfigurationProperty("GoogleCx", IsKey = false, DefaultValue = "NA", IsRequired = true)]
		public string GoogleCx
		{
			get
			{
				return (string)this["GoogleCx"];
			}
			set
			{
				this["GoogleCx"] = value;
			}
		}
	}
}
