﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.Search
{
	/// <summary>
	/// Handle settings for the search.
	/// </summary>
    public class SearchSection : ConfigurationSection
    {
        private SearchProviderCollection _searchProviderCollection;

        [ConfigurationProperty("DefaultProvider", IsRequired = true)]
        public string DefaultProvider {
            get
            {
                return this["DefaultProvider"].ToString();
            }
            set
            {
                this["DefaultProvider"] = value;
            }
        }

        [ConfigurationProperty("Providers")]
        public SearchProviderCollection Providers
        {
            get
            {
                if (_searchProviderCollection == null)
                {
                    if (base["Providers"] != null && ((ConfigurationElement)base["Providers"]).ElementInformation.IsPresent)
                    {
                        _searchProviderCollection = (SearchProviderCollection)base["Providers"];
                    }
                    else
                    {
                        _searchProviderCollection = new SearchProviderCollection();
                        _searchProviderCollection.AddElement(new SearchProviderElement() { ProviderName = string.Empty });
                    }
                }

                return _searchProviderCollection;
            }
        }
    }
}