﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration
{
    public abstract class BaseProviderElement : ConfigurationElement
    {
        [ConfigurationProperty("ProviderName", IsKey = true, IsRequired = true)]
        public string ProviderName
        {
            get
            {
                return (string)this["ProviderName"];
            }
            set
            {
                this["ProviderName"] = value;
            }
        }

        [ConfigurationProperty("Type", IsRequired = true, DefaultValue = "NA")]
        public string Type
        {
            get
            {
                return (string)this["Type"];
            }
            set
            {
                this["Type"] = value;
            }
        }


        public string TypeAssembly
        {
            get
            {
                var typeAsm = Type.Split(',');
                if (typeAsm.Length > 1)
                    return typeAsm[1].Trim();

                return string.Empty;
            }
        }

        public string TypeType
        {
            get
            {
                var typeAsm = Type.Split(',');
                if (typeAsm.Length > 0)
                    return typeAsm[0].Trim();

                return string.Empty;
            }
        }
    }
}
