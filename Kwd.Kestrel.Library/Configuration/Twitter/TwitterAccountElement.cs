﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.Twitter
{
    /// <summary>
    /// Class handle the twitter account configuration element.
    /// </summary>
    public class TwitterAccountElement : ConfigurationElement
    {
        [ConfigurationProperty("SiteName", IsKey = true, DefaultValue = "NA", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&()[]{}/;'\"|\\~!@#%&{};\"|", MinLength = 1, MaxLength = 150)]
        public string SiteName
        {
            get
            {
                return (string)this["SiteName"];
            }
            set
            {
                this["SiteName"] = value;
            }
        }

        [ConfigurationProperty("ConsumerKey", IsKey = false, DefaultValue = "NA", IsRequired = true)]
        public string ConsumerKey
        {
            get
            {
                return (string)this["ConsumerKey"];
            }
            set
            {
                this["ConsumerKey"] = value;
            }
        }

        [ConfigurationProperty("ConsumerSecret", IsKey = false, DefaultValue = "NA", IsRequired = true)]
        public string ConsumerSecret
        {
            get
            {
                return (string)this["ConsumerSecret"];
            }
            set
            {
                this["ConsumerSecret"] = value;
            }
        }

        [ConfigurationProperty("OAuthToken", IsKey = false, DefaultValue = "NA", IsRequired = true)]
        public string OAuthToken
        {
            get
            {
                return (string)this["OAuthToken"];
            }
            set
            {
                this["OAuthToken"] = value;
            }
        }

        [ConfigurationProperty("AccessToken", IsKey = false, DefaultValue = "NA", IsRequired = true)]
        public string AccessToken
        {
            get
            {
                return (string)this["AccessToken"];
            }
            set
            {
                this["AccessToken"] = value;
            }
        }

        [ConfigurationProperty("UseInNewsroom", DefaultValue = false, IsRequired = true)]
        public bool UseInNewsroom
        {
            get 
            { 
                return (bool)this["UseInNewsroom"]; 
            }
            set
            {
                this["UseInNewsroom"] = value;
            }
        }

        public override string ToString()
        {
            return SiteName + " - " + ConsumerKey;
        }
    }
}