﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.Twitter
{
    /// <summary>
    /// Twitter account settings
    /// </summary>
    public class TwitterSection : ConfigurationSection
    {
        TwitterAccountCollection _innerAccountsCollection = null;

        [ConfigurationProperty("Accounts", IsDefaultCollection = false)]
        public TwitterAccountCollection Accounts
        {
            get
            {
                if (_innerAccountsCollection == null)
                {
                    if (base["Accounts"] != null && ((ConfigurationElement)base["Accounts"]).ElementInformation.IsPresent)
                    {
                        _innerAccountsCollection = (TwitterAccountCollection)base["Accounts"];
                    }
                    else
                    {
                        _innerAccountsCollection = new TwitterAccountCollection();
                        _innerAccountsCollection.AddElement(new TwitterAccountElement { SiteName = string.Empty });
                    }
                }
                return _innerAccountsCollection;
            }
        }

    }
}