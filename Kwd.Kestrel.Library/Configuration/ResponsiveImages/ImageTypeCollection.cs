﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.ResponsiveImages
{
    public class ImageTypeCollection : ConfigurationElementCollection
    {
        public new ImageTypeElement this[string index]
        {
            get { return (ImageTypeElement)BaseGet(index); }
        }

        public ImageTypeElement this[int index]
        {
            get { return (ImageTypeElement)BaseGet(index); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ImageTypeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ImageTypeElement)element).Id;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public void AddElement(ImageTypeElement element)
        {
            BaseAdd(element);
        }
    }
}