﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.ResponsiveImages
{
    public class ResponsiveImageProviderCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ResponsiveImageProviderElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ResponsiveImageProviderElement)element).ProviderName;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public void AddElement(ResponsiveImageProviderElement element)
        {
            BaseAdd(element);
        }
    }
}