﻿using System.Collections.Generic;
using System.Configuration;
using Kwd.Kestrel.Library.Models;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Configuration.ResponsiveImages
{
	/// <summary>
	/// Handle configuration about responsive images.
	/// </summary>
    public class ResponsiveImageSection : ConfigurationSection
    {
        private ResponsiveImageProviderCollection _responsiveImageProviderCollection;
        private ImageTypeCollection _imageTypeCollection;

        [ConfigurationProperty("DefaultProvider", IsRequired = true)]
        public string DefaultProvider
        {
            get
            {
                return this["DefaultProvider"].ToString();
            }
        }

        [ConfigurationProperty("Providers")]
        public ResponsiveImageProviderCollection Providers
        {
            get
            {
                if (_responsiveImageProviderCollection == null)
                {
                    if (base["Providers"] != null && ((ConfigurationElement)base["Providers"]).ElementInformation.IsPresent)
                    {
                        _responsiveImageProviderCollection = (ResponsiveImageProviderCollection)base["Providers"];
                    }
                    else
                    {
                        _responsiveImageProviderCollection = new ResponsiveImageProviderCollection();
                        _responsiveImageProviderCollection.AddElement(new ResponsiveImageProviderElement() { ProviderName = string.Empty });
                    }
                }

                return _responsiveImageProviderCollection;
            }
        }

        [ConfigurationProperty("ImageTypes")]
        public ImageTypeCollection ImageTypes
        {
            get
            {
                if (_imageTypeCollection == null)
                {
                    if (base["ImageTypes"] != null && ((ConfigurationElement)base["ImageTypes"]).ElementInformation.IsPresent)
                    {
                        _imageTypeCollection = (ImageTypeCollection)base["ImageTypes"];
                    }
                    else
                    {
                        _imageTypeCollection = new ImageTypeCollection();
                        _imageTypeCollection.AddElement(new ImageTypeElement { Id = 0 });
                    }
                }

                return _imageTypeCollection;
            }
        }

        [ConfigurationProperty("CacheTimeoutMinutes", IsRequired = true)]
        public int CacheTimeoutMinutes
        {
            get
            {
                return (int)this["CacheTimeoutMinutes"];
            }
        }

        /// <summary>
        /// Get a list of all ViewPorts from configuration.
        /// </summary>
        /// <returns></returns>
        public List<ViewPort> ViewPortList()
        {
            var list = new List<ViewPort>();

            var items = this.ImageTypes.Count;
            for (var i = 0; i < items; i++)
            {
                var element = this.ImageTypes[i];
                list.Add(new ViewPort(element)); 
            }

            return list;
        }
    }
}