﻿using System.Configuration;

namespace Kwd.Kestrel.Library.Configuration.ResponsiveImages
{
	/// <summary>
	/// Information about what type of image.
	/// </summary>
    public class ImageTypeElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsKey = true, IsRequired = true)]
        public int Id
        {
            get
            {
                return (int)this["id"];
            }
            set
            {
                this["id"] = value;
            }
        }
        
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("media")]
        public string Media
        {
            get
            {
                return (string)this["media"];
            }
            set
            {
                this["media"] = value;
            }
        }

        [ConfigurationProperty("width", IsRequired = true)]
        public int Width
        {
            get
            {
                return (int)this["width"];
            }
            set
            {
                this["width"] = value;
            }
        }

        [ConfigurationProperty("retinaquality", IsRequired = true)]
        public int RetinaQuality
        {
            get
            {
                return (int)this["retinaquality"];
            }
            set
            {
                this["retinaquality"] = value;
            }
        }

        [ConfigurationProperty("desktopquality", IsRequired = true)]
        public int DesktopQuality
        {
            get
            {
                return (int)this["desktopquality"];
            }
            set
            {
                this["desktopquality"] = value;
            }
        }
    }
}