﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.HttpModules
{
	[ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
	public class UrlSegmentHandlingInitialization : IInitializableModule
	{
		public void Initialize(InitializationEngine context)
		{
			UrlSegment.CreatedUrlSegment += UrlSegmentOnCreatedUrlSegment;
		}

		private void UrlSegmentOnCreatedUrlSegment(
			object sender, UrlSegmentEventArgs urlSegmentEventArgs)
		{
			var segment = urlSegmentEventArgs.RoutingSegment.RouteSegment
							   .ToLowerInvariant();
			
			// Remove all dubble -- chars.
			while (segment.Contains("--"))
			{
				segment = segment.Replace("--", "-");
			}

			// Remove - chars in the end.
			if (segment.EndsWith("-"))
			{
				segment = segment.Substring(0, segment.Length - 1);
			}

			urlSegmentEventArgs.RoutingSegment.RouteSegment = segment;
		}

		public void Uninitialize(InitializationEngine context)
		{
			UrlSegment.CreatedUrlSegment -= UrlSegmentOnCreatedUrlSegment;
		}

		public void Preload(string[] parameters)
		{
		}
	}
}
