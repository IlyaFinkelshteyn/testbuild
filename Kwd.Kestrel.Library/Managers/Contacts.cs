﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using Kwd.Kestrel.Library.Epi;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Service that contains busniess logic for contacts.
	/// </summary>
	public interface IContacts
	{
		/// <summary>
		/// Gets all child pages that are of type ContactItemPageType.
		/// </summary>
		/// <returns></returns>
		List<ContactItemPageType> ListChildContacts(int rootContentId);
	}

	/// <summary>
	/// Service that contains busniess logic for contacts.
	/// </summary>
	public class Contacts : EpiManager, IContacts
	{

		public Contacts(ILogger logger, ICache cache, IEpiWrapper epiWrapper)
			: base(logger, cache, epiWrapper)
		{

		}

		[DefaultConstructor]
		public Contacts(ILogger logger, ICache cache)
			: base(logger, cache)
		{

		}

		/// <summary>
		/// Gets all child pages that are of type ContactItemPageType.
		/// </summary>
		/// <returns></returns>
		public List<ContactItemPageType> ListChildContacts(int rootContentId)
		{
			var contactsList = new List<ContactItemPageType>();

			var contentReference = new ContentReference(rootContentId);

			var contacts = GetPages<ContactItemPageType>(contentReference);
			//var contacts = GetChildren<ContactItemPageType>(contentReference);

			if (contacts != null)
			{
				contactsList = contacts.ToList();
			}

			return contactsList;
		}
	}
}
