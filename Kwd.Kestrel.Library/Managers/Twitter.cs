﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Data.Dynamic;
using EPiServer.DataAbstraction;
using EPiServer.Web;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Models;
using LinqToTwitter;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface ITwitter
	{
		List<Tweet> GetTweetsByScreenName(string screenName, int count = 20);
	}

	public class Twitter : EpiManager, ITwitter
	{
		private const string CacheKeyName = "GetTweetsByScreenName";
		private const string CacheKeyFormat = "{0}%{1}%{2}";

		static SingleUserAuthorizer _auth = new SingleUserAuthorizer
		{
			Credentials = new InMemoryCredentials
			{
				ConsumerKey = ConfigurationManager.AppSettings[SiteDefinition.Current.Name + "_twitterConsumerKey"], //OAuth Consumer key
				ConsumerSecret = ConfigurationManager.AppSettings[SiteDefinition.Current.Name + "_twitterConsumerSecret"], //OAuth Consumer secret
				OAuthToken = ConfigurationManager.AppSettings[SiteDefinition.Current.Name + "_twitterOAuthToken"], //Twitter Access token
				AccessToken = ConfigurationManager.AppSettings[SiteDefinition.Current.Name + "_twitterAccessToken"] // Twitter Access token secret
			}
		};

		[DefaultConstructor]
		public Twitter(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}

		public Twitter(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache, contentRepository)
		{
		}

		public Twitter(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		public List<Tweet> GetTweetsByScreenName(string screenName, int count = 20)
		{
			if (string.IsNullOrEmpty(_auth.Credentials.ConsumerKey))
				return new List<Tweet>();

			var cacheKey = string.Format(CacheKeyFormat, CacheKeyName, screenName, count);

			if (string.IsNullOrEmpty(screenName))
				return null;

			var tweets = (List<Tweet>)Cache.Get(cacheKey);

			if (tweets != null)
			{
				return tweets.ToList();
			}

			try
			{
				using (var twitterCtx = new TwitterContext(_auth))
				{
					var statusTweets =
						from tweet in twitterCtx.Status
						where tweet.Type == StatusType.User
							  && tweet.ScreenName == screenName
							  && tweet.Count == count
						select tweet;

					var list = statusTweets.Select(s => s.ToTweet()).ToList();

					var store = DynamicDataStoreFactory.Instance.GetStore(typeof (Tweet)) ??
					            DynamicDataStoreFactory.Instance.CreateStore("Tweets", typeof(Tweet));

					var storeTweets = store.LoadAll<Tweet>();

					foreach (var s in list.Where(s => storeTweets.All(ss => ss.Sid != s.Sid)))
					{
						store.Save(s);
					}

					var tweetsFromStore = store.LoadAll<Tweet>();

					foreach (var tweet in tweetsFromStore.OrderByDescending(s => s.CreatedAt).Skip(100))
					{
						store.Delete(tweet);
					}

					Cache.Insert(cacheKey, list, DateTime.Now.AddMinutes(10) - DateTime.Now);

					return list;
				}
			}
			catch (TwitterQueryException)
			{	
				//Twitter API is down :(
				//Grab from DDS, insert it to cache and return
				var store = DynamicDataStoreFactory.Instance.GetStore(typeof (Tweet)) ??
							DynamicDataStoreFactory.Instance.CreateStore("Tweets", typeof(Tweet));

				var storeTweets = store.LoadAll<Tweet>().OrderByDescending(s => s.CreatedAt).Take(count).ToList();
				Cache.Insert(cacheKey, storeTweets, DateTime.Now.AddMinutes(10) - DateTime.Now);

				return storeTweets.ToList();
			}
			
		}
	}
}
