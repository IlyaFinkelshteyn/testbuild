﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Web;
using log4net;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface ILogger
	{
		void LogInfo(string message);
		void LogDebug(string message);
		void LogError(Exception ex);
		void LogError(Exception ex, string message);
		void LogError(string message);
		void LogFatal(string message);
		void LogWarning(string message);
	}

	public class Logger : ILogger
	{
		protected readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString());
		private readonly bool _isInfoEnabled;
		private readonly bool _isDebugEnabled;

		[DefaultConstructor]
		public Logger()
		{
			_isInfoEnabled = Log.IsInfoEnabled;
			_isDebugEnabled = Log.IsDebugEnabled;
		}

        //public Logger(string logger, bool isDebugLoggingEnabled, bool isInfoLoggingEnabled)
        //{
        //    Log = LogManager.GetLogger(logger);
        //    _isInfoEnabled = isInfoLoggingEnabled;
        //    _isDebugEnabled = isDebugLoggingEnabled;
        //}

		#region PUBLIC methods
		/// <summary>
		/// Logs the info.
		/// </summary>
		/// <param name="message">The message.</param>
		public void LogInfo(string message)
		{
			if (_isInfoEnabled)
			{
				Log.Info(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message));
			}
		}

		/// <summary>
		/// Logs the debug.
		/// </summary>
		/// <param name="message">The message.</param>
		public void LogDebug(string message)
		{
			if (_isDebugEnabled)
			{
				Log.Debug(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message));
			}
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="ex">The ex.</param>
		public void LogError(Exception ex)
		{
			Log.Error(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), ex.Message), ex);
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="ex">The ex.</param>
		/// <param name="message">The message.</param>
		public void LogError(Exception ex, string message)
		{
			Log.Error(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message), ex);
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="message">The message.</param>
		public void LogError(string message)
		{
			Log.Error(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message));
		}

		/// <summary>
		/// Logs the fatal.
		/// </summary>
		/// <param name="message">The message.</param>
		public void LogFatal(string message)
		{
			Log.Fatal(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message));
		}

		/// <summary>
		/// Logs the warning.
		/// </summary>
		/// <param name="message">The message.</param>
		public void LogWarning(string message)
		{
			Log.Warn(string.Format("{0}.{1}[Line={2}][User={3}]: {4}", GetCallingClassName(), GetCallingMethodName(), GetCallingLineNumber().ToString(CultureInfo.InvariantCulture), GetUsername(), message));
		}

		#endregion

		#region PRIVATE methods
		/// <summary>
		/// Gets the name of the class that calls the logging.
		/// </summary>
		/// <returns></returns>
		private static string GetCallingClassName()
		{
			var sf = new StackFrame(2, true);
			string fileName = string.Empty;
			try
			{
				fileName = sf.GetFileName();
				fileName = fileName.Substring(fileName.LastIndexOf(@"\") + 1);
				if (fileName.EndsWith(".cs"))
				{
					fileName = fileName.Substring(0, fileName.Length - 3);
				}
			}
			catch
			{
				// Will do nothing here. If we can´t get the name we will ignore it and return empty string. 
			}

			return fileName;
		}

		/// <summary>
		/// Gets the name of the method that calls the logging.
		/// </summary>
		/// <returns>Method name</returns>
		private static string GetCallingMethodName()
		{
			var sf = new StackFrame(2, true);
			System.Reflection.MethodBase mb = sf.GetMethod();
			string methodName = mb != null ? mb.Name : string.Empty;

			return methodName;
		}

		/// <summary>
		/// Gets the line number of the calling method.
		/// </summary>
		/// <returns>The linenumber</returns>
		private static int GetCallingLineNumber()
		{
			var sf = new StackFrame(2, true);
			int lineNumber = sf.GetFileLineNumber();

			return lineNumber;
		}

		/// <summary>
		/// Gets the username.
		/// </summary>
		/// <returns></returns>
		private static string GetUsername()
		{
			var username = string.Empty;

			try
			{
				// If the user is authenticated we want to log the username.
				if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
				{
					username = HttpContext.Current.User.Identity.Name;
				}
				else
				{
					username = "NA";
				}
			}
			catch
			{
				username = "NA";
			}

			return username;
		}
		#endregion
	}
}
