﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.SpecializedProperties;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IBreadcrumb
	{
		/// <summary>
		/// Return the current breadcrumb links.
		/// </summary>
		/// <returns></returns>
		LinkItemCollection GetLinks();
	}

	public class Breadcrumb : EpiManager, IBreadcrumb
	{
		[DefaultConstructor]
		public Breadcrumb(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}

		public Breadcrumb(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		public LinkItemCollection GetLinks()
		{
			//TODO: Refactor to return List<LinkData>
			//TODO: Refactor to support unittest.
           
            

			var linkItemCollection = new LinkItemCollection();

			var p = EpiWrapper.CurrentPage;

            var layoutCustomisation = GetLayoutCustomisation(p);

			while (p.PageLink != PageReference.StartPage && p.PageLink != PageReference.RootPage)
			{
				if (p.VisibleInMenu  || layoutCustomisation != null && !layoutCustomisation.HideBreadcrumb)
				{
					var linkItem = new LinkItem();

					if (p.PageLink.ID != EpiWrapper.CurrentPage.PageLink.ID)
					{
						if (p.HasTemplate())
						{
							linkItem.Href = p.LinkURL;
						}
					}
					linkItem.Text = p.PageName;

					linkItemCollection.Add(0, linkItem);
				}
				p = EpiWrapper.GetPage(p.ParentLink);
                layoutCustomisation = GetLayoutCustomisation(p);
			}

			return linkItemCollection;
		}
	}
}
