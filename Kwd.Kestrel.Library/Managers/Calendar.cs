﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.Models.Calendar;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Service that contains busniess logic for calendar.
	/// </summary>
	public interface ICalendar
	{
		/// <summary>
		/// Gets all child pages that are of type CalendarEventPageType.
		/// </summary>
		/// <returns></returns>
		List<CalendarEventPageType> ListChildCalendarEvents(int rootCalendarId);

		/// <summary>
		/// Create a CalendarData and populate with all events that are childs to that calendar list page.
		/// </summary>
		CalendarData GenerateCalendarEvents(int calendarPageId, string language);

		/// <summary>
		/// Create a CalendarData and populate with the specified event.
		/// </summary>
		CalendarData GenerateCalendarEvent(int eventId);

		/// <summary>
		/// Creates a URL for outlook to add the event to calendar
		/// </summary>
		string GetOutlookUrl(int eventId);
	}

	/// <summary>
	/// Service that contains busniess logic for calendar.
	/// </summary>
	public class Calendar : EpiManager, ICalendar
	{
		private const string OUTLOOK_URL = "/Handlers/DownloadEvent.ashx";

		[DefaultConstructor]
		public Calendar(ILogger logger, ICache cache)
			: base(logger, cache)
		{

		}

		public Calendar(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache, contentRepository)
		{

		}

		public Calendar(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{

		}

		/// <summary>
		/// Gets all child pages that are of type CalendarEventPageType.
		/// </summary>
		/// <returns></returns>
		public List<CalendarEventPageType> ListChildCalendarEvents(int rootCalendarId)
		{
			var calendarEvents = new List<CalendarEventPageType>();

			var contentReference = new ContentReference(rootCalendarId);

			var children = GetChildren<CalendarEventPageType>(contentReference);

			if (children != null)
			{
				calendarEvents = children.ToList();
			}

			return calendarEvents;
		}

		/// <summary>
		/// Create a CalendarData and populate with all events that are childs to that calendar list page.
		/// </summary>
		public CalendarData GenerateCalendarEvents(int calendarPageId, string language)
		{
			var calendarName = string.Empty;

			var calendarEvents = new List<CalendarEventData>();

			// First we will get the calendar container page.
			var calendarContainerPage = GetContent(calendarPageId) as CalendarListPageType;
			if (calendarContainerPage != null)
			{
				// Set the calendar name = to the calendar page name.
				calendarName = calendarContainerPage.PageName.Replace(" ", "-");

				// Get all events that is in the future and sort it by DateStart ASC
				var calenderDates = GetUpcomingEventsFromCalendarList(calendarContainerPage.PageLink, language);

				// Convert all events to type List<CalendarEventData>
				calendarEvents = ConvertToListOfEventData(calenderDates);

				// Sort the calendarEvents StartDate ASC
				calendarEvents = calendarEvents.OrderBy(x => x.StartDate).ToList();
			}

			// Create the CalendarData and add the result.
			var calendarData = new CalendarData
			{
				FileName = calendarName,
				Events = calendarEvents
			};

			return calendarData;
		}

		/// <summary>
		/// Create a CalendarData and populate with the specified event.
		/// </summary>
		public CalendarData GenerateCalendarEvent(int eventId)
		{
			var calendarName = string.Empty;

			CalendarEventData calendarEvent = null;

			// First we will get the calendar container page.
			var calendarEventPage = GetContent(eventId) as CalendarEventPageType;

			if (calendarEventPage != null)
			{
				calendarName = calendarEventPage.PageName.Replace(" ", "-");

				calendarEvent = ConvertToEventData(calendarEventPage);
			}

			var calendarData = new CalendarData
			{
				FileName = calendarName,
				Events = calendarEvent != null ? new List<CalendarEventData> { calendarEvent } : new List<CalendarEventData>(),
			};

			return calendarData;
		}

		/// <summary>
		/// Creates a URL for outlook to add the event to calendar
		/// </summary>
		public string GetOutlookUrl(int pageId)
		{
			return string.Format("{0}?EventId={1}", OUTLOOK_URL, pageId);
		}

		#region Private methods
		private IEnumerable<CalendarEventPageType> GetUpcomingEventsFromCalendarList(PageReference pageReference, string language)
		{
			// Setup criteria collection to find calendar events in the feuture.
			var calendarDatesCritera = new PropertyCriteriaCollection();
			var criteria = new PropertyCriteria
			{
				Type = PropertyDataType.Date,
				Name = "StartDate",
				Value = DateTime.Now.ToString(CultureInfo.InvariantCulture),
				Condition = CompareCondition.GreaterThan,
				Required = true
			};
			calendarDatesCritera.Add(criteria);

			var calenderDates = GetPages<CalendarEventPageType>(pageReference, language).Where(c => c.StartDate > DateTime.Now);

			return calenderDates;
		}

		/// <summary>
		/// Convert a PageDataCollection to a List of CalendarEventData.
		/// We expect that all pages in the PageDataCollection is of typ CalendarEventPageType.
		/// </summary>
		private List<CalendarEventData> ConvertToListOfEventData(IEnumerable<PageData> pdc)
		{
			var calendarEvents = new List<CalendarEventData>();

			foreach (CalendarEventPageType eventPage in pdc)
			{
				var calendarEvent = ConvertToEventData(eventPage);
				calendarEvents.Add(calendarEvent);
			}

			return calendarEvents;
		}

		private CalendarEventData ConvertToEventData(CalendarEventPageType eventPage)
		{
			var calendarEvent = new CalendarEventData
			{
				Location = eventPage.Location,
				Summery = eventPage.PageName,
				//Description = eventPage.Introduction.ToString(),
				Organizer = eventPage.Organizer,
				StartDate = eventPage.StartDate,
				EndDate = eventPage.EndDate,
				LinkUrl = eventPage.LinkURL.Replace(":", string.Empty).Replace("/", string.Empty),
				Priority = 5
			};
			try
			{
				calendarEvent.Description = eventPage.Introduction.ToString();
			}
			catch
			{
				calendarEvent.Description = string.Empty;
			}


			return calendarEvent;
		}
		#endregion
	}
}
