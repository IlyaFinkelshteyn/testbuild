﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi;
using Kwd.Kestrel.Library.Models.Excel;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IFinancialTable
	{
		Table GetTablesFromSheet(ExcelSheet sheet, bool isQuarters);
	}

	public class FinancialTable : EpiManager, IFinancialTable
	{
		public FinancialTable(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache, contentRepository)
		{

		}

        [DefaultConstructor]
		public FinancialTable(ILogger logger, ICache cache)
            : base(logger, cache)
        {

        }
		public Table GetTablesFromSheet(ExcelSheet sheet, bool isQuarters)
		{
			if (sheet.Rows.Count <= 0)
				return new Table();

			var cols = 0;
			for (var i = 0; i < sheet.Rows.Count; ++i)
			{
				if (sheet[i].Colums > cols)
					cols = sheet[i].Colums;
			}

			var tab = new Table();
			for (var i = 0; i < sheet.Rows.Count; ++i)
			{
				var thead = i <= 1;
				TableRow tr;
				if (thead)
				{
					tr = new TableHeaderRow { TableSection = TableRowSection.TableHeader };
				}
				else
				{
					tr = new TableRow { TableSection = TableRowSection.TableBody };
				}
				ExcelDetailedRow row = sheet[i];
				tr.CssClass = row.Format;
				//skip first column ("A") by starting from 1
				for (var j = 1; j < cols; ++j)
				{
					TableCell td;
					if (thead || j == 1)
					{
						td = new TableHeaderCell();
					}
					else
					{
						td = new TableCell();
					}
					var cssClass = string.Empty;
					if (j < row.Colums)
					{
						var cell = row[j];
						if (cell.Number.HasValue && cell.NumberFormat != "@")
						{
							td.Text = cell.Number.Value.ToString("N" + cell.DecimalPlaces);
						}
						else
						{
							td.Text = cell.Text;
						}
						if (cell.Colspan != 1)
						{
							td.ColumnSpan = cell.Colspan;
						}
						if (!string.IsNullOrEmpty(cell.Comment))
						{
							td.ToolTip = cell.Comment;
						}
						cssClass = cell.Format;
					}
					if (!thead && ((isQuarters && j % 4 == 1) || j == 2))
					{
						cssClass += " current";
					}
					td.CssClass = cssClass.Trim();
					tr.Controls.Add(td);
				}
				tab.Rows.Add(tr);
			}

			return tab;
		}
	}
}
