﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using Kwd.Kestrel.Library.Configuration.ResponsiveImages;
using Kwd.Kestrel.Library.Models.ResponsiveImage;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IResponsiveImage
	{
		string GetImage(ResponsiveImageData imageData);
	}

	public class ResponsiveImage : IResponsiveImage
	{
		private readonly IConfiguration _configurationProvider;
		private readonly IResponsiveImageProvider _responsiveImageProvider;
		private readonly ILogger _logProvider;

		[DefaultConstructor]
		public ResponsiveImage(ILogger logProvider, IConfiguration configurationProvider)
		{
			_logProvider = logProvider;
			_configurationProvider = configurationProvider;
			_responsiveImageProvider = this.GetResponsiveImageProvider();
		}

		public ResponsiveImage(ILogger logProvider, IConfiguration configurationProvider, IResponsiveImageProvider responsiveImageProvider)
		{
			_logProvider = logProvider;
			_configurationProvider = configurationProvider;
			_responsiveImageProvider = responsiveImageProvider;
		}

		private IResponsiveImageProvider GetResponsiveImageProvider()
		{
			var responsiveImage = _configurationProvider.ResponsiveImages;
			ResponsiveImageProviderElement providerElement = null;
			foreach (ResponsiveImageProviderElement element in responsiveImage.Providers)
			{
				if (element.ProviderName == responsiveImage.DefaultProvider)
				{
					providerElement = element;
					break;
				}
			}

			if (providerElement == null)
			{
				_logProvider.LogWarning(string.Format("Unable to find ResponsiveImage provider {0}.", responsiveImage.DefaultProvider));
				throw new Exception(string.Format("Unable to find ResponsiveImage provider {0}.", responsiveImage.DefaultProvider));
			}

			var instObj = Activator.CreateInstance(providerElement.TypeAssembly, providerElement.TypeType);
			var searchProvider = (IResponsiveImageProvider)instObj.Unwrap();
			return searchProvider;
		}

		public string GetImage(ResponsiveImageData imageData)
		{
			if (imageData == null || imageData.Path == null)
			{
				return string.Empty;
			}

			//TODO: We need to implment logic that gets the HTMl structure from settings. /Ove 2013-10-07

			var viewPorts = _configurationProvider.ResponsiveImages.ViewPortList();

			var imageResult = _responsiveImageProvider.GetImage(imageData, viewPorts, _configurationProvider.ResponsiveImages.CacheTimeoutMinutes);

			var output = new StringBuilder();

			var itemprop = string.Empty;
			if (!string.IsNullOrEmpty(imageData.Itemprop))
			{
				itemprop = string.Format("itemprop=\"{0}\"", imageData.Itemprop);
			}

			if (imageResult.Image != null)
			{
				var image = imageResult.Image;

				// If no viewports
				if (image.ViewPortImages == null || image.ViewPortImages.Count == 0)
				{
					output.AppendFormat("<img src=\"{0}\" alt=\"{1}\" title=\"{2}\" class=\"{3}\" {4}/>", image.Url, image.Alt, image.Title, imageData.CssClass, itemprop);
					// If we have attribution information we will render that tag. Otherwise we don´t want to show it.
					if (!string.IsNullOrWhiteSpace(image.Attribution))
					{
						output.AppendFormat("<div class=\"attribution\">{0}</div>", image.Attribution);
					}
				}
				else
				{
					// We got viewports so we should render responsive images.

					output.AppendFormat("<div data-picture data-alt=\"{0}\" data-title=\"{0}\" class=\"{1}\" data-orgwidth=\"{2}\" data-orgheight=\"{3}\" {4}>", HttpUtility.HtmlEncode(image.Alt), imageData.ImageType + " " + imageData.CssClass, image.OriginalWidth, image.OriginalHeight, itemprop);

					foreach (var viewPortImage in image.ViewPortImages)
					{
						output.Append("<div");
						if (string.IsNullOrEmpty(viewPortImage.Media) == false)
						{
							output.AppendFormat(" data-media=\"{0}\"", viewPortImage.Media);
						}

						output.AppendFormat(" data-src=\"{0}\" data-maxwidth=\"{1}\" data-width=\"{2}\" data-height=\"{3}\" data-quality=\"{4}\"></div>", viewPortImage.Url, viewPortImage.Width, viewPortImage.Width, viewPortImage.Height, viewPortImage.Quality.ToString());
					}

					// if we have a fallback viewport we will add that too.
					var fallbackViewPortImage = image.ViewPortImages.FirstOrDefault(x => string.IsNullOrWhiteSpace(x.Media));
					if (fallbackViewPortImage != null)
					{
						output.AppendFormat("<!--[if (lt IE 9) & (!IEMobile)]><div data-src=\"{0}\"></div><![endif]-->", fallbackViewPortImage.Url);
						output.AppendFormat("<noscript><img src=\"{0}\" alt=\"{1}\" title=\"{2}\" class=\"{3}\" /></noscript>", fallbackViewPortImage.Url, fallbackViewPortImage.Alt, fallbackViewPortImage.Title, fallbackViewPortImage.CssClass);
					}

					// If we have attribution information we will render that tag. Otherwise we don´t want to show it.
					if (!string.IsNullOrWhiteSpace(image.Attribution))
					{
						output.AppendFormat("<div class=\"attribution\">{0}</div>", image.Attribution);
					}
					output.Append("</div>");
				}
			}
			else
			{
				output.AppendFormat("<!-- Error loading file or unknown imagetype: {0} -->", imageData.ImageType);
			}

			return output.ToString();
		}
	}
}
