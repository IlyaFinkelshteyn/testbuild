﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotless.Core.Parser.Tree;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Service that contains busniess logic for news.
	/// </summary>
	public interface INews
	{
		/// <summary>
		/// Gets all child pages that are of type NewsItemPageType.
		/// </summary>
		/// <returns></returns>
		IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language);

		/// <summary>
		/// Gets all child pages that are of type NewsItemPageType and filters according to <param name="listPage">the list page</param>.
		/// </summary>
		/// <returns></returns>
		IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language, NewsListPageType listPage);

		IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language, string newsTypes, CategoryList categories);

		IEnumerable<NewsItemPageType> FilterNewsByType(IEnumerable<NewsType> types, IEnumerable<NewsItemPageType> news);

		List<NewsListItem> ListNews(PageReference rootPageReference, string sortOrder, int numberOfItems, int pageIndex, int articleTypeId);

		List<NewsListItem> ListNews(PageReference rootPageReference, string sortOrder, int numberOfItems, int pageIndex, int articleTypeId, out int totalNumberOfItems);

		IEnumerable<Category> GetActiveNewsCategories(NewsItemPageType newsItem);
	}

	/// <summary>
	/// Service that contains busniess logic for news.
	/// </summary>
	public class News : EpiManager, INews
	{
		private readonly IFilterManager _filterManager;
		private readonly ICategoryManager _categoryManager;

		[DefaultConstructor]
		public News(ILogger logger, ICache cache, IFilterManager filter, ICategoryManager categoryManager)
			: base(logger, cache)
		{
			_filterManager = filter;
			_categoryManager = categoryManager;
		}

		public News(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		public IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language)
		{
			return ListNewsItems(rootId, language, null);
		}

		public IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language, string newsTypes, CategoryList categories)
		{
			IEnumerable<NewsItemPageType> newsItems = null;

			// get all new items in the root
			var children = FindSortedNewsInRoot(rootId, language);

			if (children != null)
			{
				newsItems = children.Cast<NewsItemPageType>();
			}

			if (!string.IsNullOrEmpty(newsTypes))
			{
				var newsTypeArray = newsTypes.Split(',');
				var types = new List<NewsType>();

				foreach (var s in newsTypeArray)
				{
					types.Add((NewsType)Enum.Parse(typeof(NewsType), s));
				}

				newsItems = FilterNewsByType(types, newsItems).ToList();
			}

			if (categories != null && categories.Any())
			{
				newsItems = _filterManager.FilterByCategory(newsItems, categories).ToList();
			}

			return newsItems;
		}

		public IEnumerable<NewsItemPageType> ListNewsItems(int rootId, string language, NewsListPageType listPage)
		{
			IEnumerable<NewsItemPageType> newsItems = null;

			// get all new items in the root
			var children = FindSortedNewsInRoot(rootId, language);

			if (children != null)
			{
				newsItems = children.Cast<NewsItemPageType>();
			}

			if (listPage != null && newsItems != null)
			{
				if (!string.IsNullOrEmpty(listPage.NewsTypes))
				{
					var newsTypes = listPage.NewsTypes.Split(',');
					var types = new List<NewsType>();

					foreach (var s in newsTypes)
					{
						types.Add((NewsType)Enum.Parse(typeof(NewsType), s));
					}

					newsItems = FilterNewsByType(types, newsItems).ToList();
				}

				if (listPage.Category.Any())
				{
					newsItems = _filterManager.FilterByCategory(newsItems, listPage.Category).ToList();
				}
			}

			return newsItems;
		}

		public IEnumerable<NewsItemPageType> FilterNewsByType(IEnumerable<NewsType> types, IEnumerable<NewsItemPageType> news)
		{
			var newsTypes = types as NewsType[] ?? types.ToArray();
			var newsItemPages = news as NewsItemPageType[] ?? news.ToArray();

			var filteredNews = new List<NewsItemPageType>();

			if (newsTypes.Contains(NewsType.FinancialReport))
				filteredNews.AddRange(newsItemPages.Where(n => n.FinancialReport));

			if (newsTypes.Contains(NewsType.PressRelease))
				filteredNews.AddRange(newsItemPages.Where(n => n.PressRelease));

			if (newsTypes.Contains(NewsType.Story))
				filteredNews.AddRange(newsItemPages.Where(n => n.Story));

			if (newsTypes.Contains(NewsType.NewsItem))
				filteredNews.AddRange(newsItemPages.Where(n => n.News));

			return filteredNews;
		}

		public List<NewsListItem> ListNews(PageReference rootPageReference, string sortOrder, int numberOfItems, int pageIndex, int articleTypeId)
		{
			var totalNumberOfItems = 0;
			return ListNews(rootPageReference, sortOrder, numberOfItems, pageIndex, articleTypeId, out totalNumberOfItems);
		}

		public List<NewsListItem> ListNews(PageReference rootPageReference, string sortOrder, int numberOfItems, int pageIndex, int articleTypeId, out int totalNumberOfItems)
		{
			var pageList = new List<NewsListItem>();

			var pages = new PageDataCollection();

			// Get all ContentPageWitoutSecondaryContentBlockArea in the root that we specified.
			var tempPages = GetPages<NewsItemPageType>(rootPageReference);

			if (articleTypeId != 0)
			{
				// Only add the pages that are in the specified Type.
				pages.Add(tempPages.Where(x => x.ContentTypeID == articleTypeId));
			}
			else
			{
				// Add all childpages
				pages.Add(tempPages);
			}

			// If we have specified sort order we new to sort the pages.
			if (!string.IsNullOrEmpty(sortOrder))
			{
				pages = SortPageCollection(sortOrder, pages);
			}

			totalNumberOfItems = pages.Count;

			// Only itterate through the specified number or get all.
			foreach (NewsItemPageType page in numberOfItems > 0 ? pages.Skip(pageIndex).Take(numberOfItems) : pages)
			{
				var news = new NewsListItem
				{
					Headline = page.Headline ?? page.PageName, // Heading?
					Introduction = page.IntroductionShort != null ? page.IntroductionShort.ToHtmlString() : page.Introduction != null ? page.Introduction.ToHtmlString() : string.Empty,
					LinkUrl = page.LinkURL,
					PublishedDate = page.StartPublish.ToString(Translate("/common/iso8601date")),
					ImageData = page.ImageData,
				};
				pageList.Add(news);
			}

			return pageList;
		}

		public IEnumerable<Category> GetActiveNewsCategories(NewsItemPageType newsItem)
		{
			if (newsItem.Category == null || !newsItem.Category.Any())
				return new List<Category>();

			return newsItem.Category.GetActiveSubCategories(_categoryManager.NewsCategoryRoot.ID).Cast<Category>();
		}

		#region Private methods
		private IEnumerable<PageData> FindSortedNewsInRoot(int rootId, string language)
		{
			// Create the content reference.
			var rootPageReference = new PageReference(rootId);

			// get all new items in the root
			var children = GetPages<NewsItemPageType>(rootPageReference, language);

			if (children != null)
			{
				// Sort the news in right order
				children = children.OrderByDescending(c => c.StartPublish);
			}

			return children;
		}
		#endregion
	}
}
