﻿namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Base manager that all manager should derive from.
	/// </summary>
	public abstract class BaseManager
	{
		/// <summary>
		/// Kestrel logging procedures
		/// </summary>
		public readonly ILogger Logger;

		/// <summary>
		/// Kestrel Cache procedures
		/// </summary>
		public readonly ICache Cache;

		public BaseManager(ILogger logger, ICache cache)
		{
			Logger = logger;
			Cache = cache;
		}
	}
}
