﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IMediaPage
	{
		IEnumerable<Tweet> GetTweets(int count);
	}

	public class MediaPage : EpiManager, IMediaPage
	{
		private readonly ITwitter _twitterManager;

		[DefaultConstructor]
		public MediaPage(ILogger logger, ICache cache)
			: base(logger, cache)
		{
			_twitterManager = ObjectFactory.GetInstance<ITwitter>();
		}

		public MediaPage(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache, contentRepository)
		{
		}

		public MediaPage(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		public IEnumerable<Tweet> GetTweets(int count)
		{
			return _twitterManager.GetTweetsByScreenName(
				ConfigurationManager.AppSettings[SiteDefinition.Current.Name + "_twitterScreenName"],
				count);
		}
	}
}
