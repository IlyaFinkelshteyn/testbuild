﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Filters;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Filterer class that makes us able to filter PageDataCollection.
	/// </summary>
	public interface IEpiFilterer
	{
		PageDataCollection FilterCollectionForVisitor(PageDataCollection collection);
		IEnumerable<PageData> FilterCollectionForVisitor(IEnumerable<PageData> pages);
	}

	public class EpiFilterer : IEpiFilterer
	{
		public readonly ILogger Logger;

		[DefaultConstructor]
		public EpiFilterer()
		{
			Logger = ObjectFactory.GetInstance<ILogger>();
		}

		public EpiFilterer(ILogger logger)
		{
			Logger = logger;
		}

		public PageDataCollection FilterCollectionForVisitor(PageDataCollection collection)
		{
			PageDataCollection result = null;
			if (collection != null)
			{
				result = FilterForVisitor.Filter(collection);
			}
			else
			{
				Logger.LogWarning("Tried to filter a PageDataCollection that where null.");
			}
			return result;
		}

		public IEnumerable<PageData> FilterCollectionForVisitor(IEnumerable<PageData> pages)
		{
			return FilterForVisitor.Filter(pages).Cast<PageData>();
		}
	}
}
