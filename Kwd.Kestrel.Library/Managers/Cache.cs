﻿using System;
using System.Collections.Generic;
using System.Web;
using EPiServer;
using EPiServer.Framework.Cache;

namespace Kwd.Kestrel.Library.Managers
{
	public interface ICache
	{
		//http://world.episerver.com/Blogs/Mattias-Lovstrom/Dates/2010/7/Caching-object-EPiServer-is-easier-then-you-probably-think/

		/// <summary>
		/// Remove the cached item equal to the key.
		/// </summary>
		/// <param name="key">The key</param>
		void Remove(string key);

		void Add(string key, object item);

		object Get(string key);

		bool TryGet<T>(string key, out T cacheItem);

		/// <summary>
		/// Insert a object into cache. Will be cached until someone remove it from the cache or the application will be restarted.
		/// </summary>
		void Insert(string key, object value);

		/// <summary>
		/// Insert a object into cache. Will be cached until the dependency has been changed.
		/// </summary>
		void Insert(string key, object value, CacheEvictionPolicy dependencies);

		/// <summary>
		/// Insert a object into cache. Will be cached until the specified DateTime.
		/// </summary>
		void Insert(string key, object value, System.TimeSpan timeSpan);

		/// <summary>
		/// Lists all cache keys that exist in the cache
		/// </summary>
		List<string> ListKeys();

		/// <summary>
		/// Lists all cache keys and filter on specified string
		/// </summary>
		List<string> ListKeys(string filter);

		/// <summary>
		/// Clear all objects in cache.
		/// </summary>
		void ClearCache();
	}

	public class Cache : ICache
	{
		private static object _lock = new object();

		/// <summary>
		/// Remove the cached item equal to the key.
		/// </summary>
		/// <param name="key"></param>
		public void Remove(string key)
		{
			CacheManager.Remove(key);
		}

		public void Add(string key, object item)
		{
			CacheManager.Insert(key, item);
		}

		public object Get(string key)
		{
			return CacheManager.Get(key);
		}

		public bool TryGet<T>(string key, out T cacheItem)
		{
			var cacheObject = CacheManager.Get(key);
			if (cacheObject != null)
			{
				cacheItem = (T)cacheObject;
				return true;
			}
			cacheItem = default(T);
			return false;
		}

		public void Insert(string key, object value)
		{
			lock (_lock)
			{
				Remove(key);
				CacheManager.Insert(key, value);
			}
		}

		public void Insert(string key, object value, CacheEvictionPolicy dependencies)
		{
			lock (_lock)
			{
				Remove(key);
				CacheManager.Insert(key, value, dependencies);
			}
		}

		public void Insert(string key, object value, TimeSpan timeSpan)
		{
			lock (_lock)
			{
				Remove(key);
				CacheManager.Insert(key, value, new CacheEvictionPolicy(timeSpan, CacheTimeoutType.Absolute));
			}
		}

		public List<string> ListKeys()
		{
			var keys = new List<string>();

			var enumerator = HttpRuntime.Cache.GetEnumerator();
			while ((enumerator.MoveNext()) && (enumerator.Current != null))
			{
				keys.Add(enumerator.Key.ToString());
			}

			return keys;
		}

		public List<string> ListKeys(string filter)
		{
			var keys = new List<string>();

			var enumerator = HttpRuntime.Cache.GetEnumerator();
			while ((enumerator.MoveNext()) && (enumerator.Current != null))
			{
				if (enumerator.Key.ToString().Contains(filter))
				{
					keys.Add(enumerator.Key.ToString());
				}
			}

			return keys;
		}

		public void ClearCache()
		{
			var enumerator = HttpRuntime.Cache.GetEnumerator();
			while ((enumerator.MoveNext()) && (enumerator.Current != null))
			{
				Remove(enumerator.Key.ToString());
			}
		}
	}
}
