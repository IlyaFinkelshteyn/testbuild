﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.DataAbstraction;

namespace Kwd.Kestrel.Library.Managers
{
	public interface ICategoryManager
	{
		Category NewsCategoryRoot { get; }
	}

	public class CategoryManager : ICategoryManager
	{
		private Category _newsCategoryRoot;
		public Category NewsCategoryRoot
		{
			get
			{
				if (_newsCategoryRoot != null)
					return _newsCategoryRoot;

				_newsCategoryRoot = Category.Find("NewsTags");

				return _newsCategoryRoot;
			}
		}
	}
}
