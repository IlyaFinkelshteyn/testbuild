﻿using System.Web;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IHttpContextWrapper
	{
		bool IsDebuggingEnabled { get; }

		/// <summary>
		/// Return the current httpcontext URL Scheme. Ex Http or HTTPS.
		/// </summary>
		string CurrentUrlScheme { get; }

		/// <summary>
		/// Return the current httpcontext URL host. Ex www.site.com or admin.site.com.
		/// </summary>
		string CurrentUrlHost { get; }

		/// <summary>
		/// Return the current httpcontext domain URL. Ex http://www.site.com or https://admin.site.com.
		/// </summary>
		string CurrentDomainUrl { get; }
	}

	public class HttpContextWrapper : IHttpContextWrapper
	{
		[DefaultConstructor]
		public HttpContextWrapper()
		{ }

		public bool IsDebuggingEnabled
		{
			get
			{
				return HttpContext.Current.IsDebuggingEnabled;
			}
		}

		/// <summary>
		/// Return the current httpcontext URL Scheme. Ex Http or HTTPS.
		/// </summary>
		public string CurrentUrlScheme
		{
			get
			{
				return HttpContext.Current.Request.Url.Scheme;
			}
		}

		/// <summary>
		/// Return the current httpcontext URL host. Ex www.site.com or admin.site.com.
		/// </summary>
		public string CurrentUrlHost
		{
			get
			{
				return HttpContext.Current.Request.Url.Host;
			}
		}

		/// <summary>
		/// Return the current httpcontext domain URL. Ex http://www.site.com or https://admin.site.com.
		/// </summary>
		public string CurrentDomainUrl
		{
			get
			{
				return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
			}
		}

	}
}
