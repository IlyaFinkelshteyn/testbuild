﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Framework.Cache;
using EPiServer.ServiceLocation;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using Kwd.Kestrel.Library.Epi;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Base manager klass för alla service som är relaterade till epi. 
	/// Man skall kunna köra emot denna klass direkt om det man vill göra inte är tillräkligt stort/avancerat för att ha i en egen klass/service.
	/// </summary>
	public interface IEpiManager
	{
		StartPageType GetStartPage();

		/// <summary>
		/// Tells if the context is in Epi Server editmode.
		/// </summary>
		bool InEditMode { get; }

		/// <summary>
		/// Get the current culture translation for the specified resourcekey
		/// </summary>
		string Translate(string resourceKey);

		/// <summary>
		/// Get the translation for the specified resourcekey in the specified culture
		/// </summary>
		string Translate(string resourceKey, string culture);

		/// <summary>
		/// Takes a LinkItemCollection and create a PageDataCollection of the pages that are equal to the specified type. 
		/// When LinkItem is not of the correct type it will look in decendent pages.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		IEnumerable<T> GetPagesOf<T>(LinkItemCollection linkItemCollection) where T : BasePageType;

		IEnumerable<T> GetPages<T>(LinkItemCollection linkItemCollection);
		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the startpage.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		PageDataCollection GetPagesOf<T>() where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		PageDataCollection GetPagesOf<T>(PageReference rootPageReference) where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		PageDataCollection GetPagesOf<T>(PageReference rootPageReference, string language) where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		PageDataCollection GetPagesOf<T>(PageReference rootPageReference, PropertyCriteriaCollection criterias)
			where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		PageDataCollection GetPagesOf<T>(PageReference rootPageReference, PropertyCriteriaCollection criterias,
			string language) where T : BasePageType;

		/// <summary>
		/// Return a PageDataCollection of all PUBLIC children to the specified contentReference.
		/// </summary>
		PageDataCollection GetChildren(ContentReference contentReference);

		/// <summary>
		/// Get a enumerable list of children that are of a specified type.
		/// </summary>
		IEnumerable<T> GetChildren<T>(ContentReference contentReference) where T : PageData;

		/// <summary>
		/// Get a enumerable list of children that are of a specified type and inspec language.
		/// </summary>
		IEnumerable<T> GetChildren<T>(ContentReference contentReference, ILanguageSelector languageSelector)
			where T : BasePageType;

		/// <summary>
		/// Get a enumerable list of sibling that are of a specified type.
		/// </summary>
		IEnumerable<T> GetSiblings<T>(ContentReference contentReference) where T : PageData;

		/// <summary>
		/// Get a enumerable list of parents that are of a specified type.
		/// </summary>
		IEnumerable<T> GetUncles<T>(ContentReference contentReference) where T : PageData;

		/// <summary>
		/// Get/Load a IContent that represent the specified content id.
		/// </summary>
		IContent GetContent(int contentId);

		/// <summary>
		/// Get/Load a IContent that represent the specified content reference.
		/// </summary>
		IContent GetContent(ContentReference contentReference);

		/// <summary>
		/// Get/Load a IContent that represent the specified content reference in the specified language.
		/// </summary>
		IContent GetContent(ContentReference contentReference, ILanguageSelector languageSelector);

		/// <summary>
		/// Get/Create a CacheEvictionPolicy that are refered to the Database version.
		/// </summary>
		CacheEvictionPolicy GetCacheEvictionPolicy(object obj);

		/// <summary>
		/// Get/Create a CacheEvictionPolicy that are refered to the Database version.
		/// </summary>
		CacheEvictionPolicy GetCacheEvictionPolicy();

		/// <summary>
		/// Get language links for the startpage.
		/// </summary>
		List<LinkData> GetPageLanguageLinks();

		/// <summary>
		/// Get language links for the specified page.
		/// </summary>
		List<LinkData> GetPageLanguageLinks(int pageId);

		/// <summary>
		/// Get language links for the specified page.
		/// </summary>
		List<LinkData> GetPageLanguageLinks(PageReference pageReference);

		PageDataCollection SortPageCollection(string sortType, PageDataCollection pages);

		/// <summary>
		/// Filter the PageDataCollection so that all pages that the visitor should not see is removed from the collection.
		/// </summary>
		PageDataCollection FilterCollectionForVisitor(PageDataCollection pages);

		/// <summary>
		/// Listpages from specified root and sort, take and filter after spcification.
		/// </summary>
		List<PageListItem> ListPages(PageReference rootPageReference, string sortOrder, int numberOfItems, int articleTypeId);

		/// <summary>
		/// Returns an IEnumerable of the requested type. This replaces GetPagesOf<T>.
		/// Result is filtered, removing anything not visible for visitors.
		/// Language is automatically detected.
		/// </summary>
		/// <typeparam name="T">Can be any page type (including base classes) or interface.</typeparam>
		/// <param name="root">ContentReference from which descendants are fetched.</param>
		/// <returns></returns>
		IEnumerable<T> GetPages<T>(ContentReference root);

		/// <summary>
		/// Returns an IEnumerable of the requested type. This replaces GetPagesOf<T>.
		/// Result is filtered, removing anything not visible for visitors.
		/// This method is slightly more expensive before the cache has been built up, but worth it considering we can use any Type and not limited to PageType instances.
		/// GetPagesOf runs for ~8-12ms and this runs for ~12-17ms.
		/// </summary>
		/// <typeparam name="T">Can be any page type (including base classes) or interface.</typeparam>
		/// <param name="root">ContentReference from which descendants are fetched.</param>
		/// <param name="language">Language code, autodetect if empty.</param>
		/// <returns></returns>
		IEnumerable<T> GetPages<T>(ContentReference root, string language, bool filter = true);

		PageType GetPageType(Type type);

		PageData CurrentPage { get; }

		/// <summary>
		/// Gets layout customization for current page.
		/// </summary>
		/// <returns></returns>
		LayoutCustomisation GetLayoutCustomisation();

		string SiteId { get; }

		string SiteUrl { get; }

		string GetExternalUrl(ContentReference contentReference);

		string GetExternalUrl(string internalUrl);

		string GetFriendlyURL(PageData page, bool includeSiteUrl);
	}

	public class EpiManager : BaseManager, IEpiManager
	{
		protected readonly IEpiWrapper EpiWrapper;
		private static object _myLock = new object();
		private readonly IContentTypeRepository _contentTypeRepository;
		private readonly IPageCriteriaQueryService _pageCriteriaQueryService;
		private readonly IContentRepository _contentRepository;
		private readonly IEpiFilterer _epiFilterer;
		private readonly IFilterManager _filterManager;

		[Obsolete("You should refactor and not use EpiWrapper.", false)]
		public EpiManager(ILogger logger, ICache cache, IEpiWrapper epiWrapper)
			: base(logger, cache)
		{
			EpiWrapper = epiWrapper;
			_contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
			_pageCriteriaQueryService = ServiceLocator.Current.GetInstance<IPageCriteriaQueryService>();
			_contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
			_epiFilterer = ObjectFactory.GetInstance<IEpiFilterer>();
		}

		public EpiManager(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache)
		{
			_contentTypeRepository = contentTypeRepository;
			_pageCriteriaQueryService = pageCriteriaQueryService;
			_contentRepository = contentRepository;
			_epiFilterer = epiFilterer;
		}

		public EpiManager(ILogger logger, ICache cache, IEpiWrapper epiWrapper, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache)
		{
			EpiWrapper = epiWrapper;
			_contentTypeRepository = contentTypeRepository;
			_pageCriteriaQueryService = pageCriteriaQueryService;
			_contentRepository = contentRepository;
			_epiFilterer = epiFilterer;
		}

		[DefaultConstructor]
		public EpiManager(ILogger logger, ICache cache)
			: base(logger, cache)
		{
			EpiWrapper = ObjectFactory.GetInstance<IEpiWrapper>();
			_contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
			_pageCriteriaQueryService = ServiceLocator.Current.GetInstance<IPageCriteriaQueryService>();
			_contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
			_epiFilterer = ObjectFactory.GetInstance<IEpiFilterer>();
			_filterManager = ObjectFactory.GetInstance<IFilterManager>();
		}

		public EpiManager(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache)
		{
			_contentRepository = contentRepository;
		}

		public StartPageType GetStartPage()
		{
			return _contentRepository.Get<StartPageType>(ContentReference.StartPage);
		}

		public bool InEditMode
		{
			get { return EpiWrapper.InEditMode; }
		}

		/// <summary>
		/// Get the current culture translation for the specified resourcekey
		/// </summary>
		public string Translate(string resourceKey)
		{
			return EpiWrapper.Translate(resourceKey);
		}

		/// <summary>
		/// Get the translation for the specified resourcekey in the specified culture
		/// </summary>
		public string Translate(string resourceKey, string culture)
		{
			return EpiWrapper.Translate(resourceKey, culture);
		}

		/// <summary>
		/// Takes a LinkItemCollection and create a PageDataCollection of the pages that are equal to the specified type. 
		/// When LinkItem is not of the correct type it will look in decendent pages.
		/// </summary>
		[Obsolete("Use GetPages<T> instead.", true)]
		public IEnumerable<T> GetPagesOf<T>(LinkItemCollection linkItemCollection) where T : BasePageType
		{
			var list = new List<T>();

			//foreach (var page in linkItemCollection.ToPages())
			//{
			//	if (page is T)
			//	{
			//		list.Add(page as T);
			//	}
			//	else
			//	{
			//		var decendents = GetPagesOf<T>(page.PageLink);
			//		foreach (var decendent in decendents)
			//		{
			//			list.Add(decendent as T);
			//		}
			//	}
			//}

			return list;
		}

		public IEnumerable<T> GetPages<T>(LinkItemCollection linkItemCollection)
		{
			var list = new List<T>();

			var links = linkItemCollection.ToContentReferenceLIst();

			list.AddRange(links.Where(l => l is T).Cast<T>());

			foreach (var link in links)
			{
				list.AddRange(GetPages<T>(link));
			}

			return list;
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the startpage.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		public PageDataCollection GetPagesOf<T>() where T : BasePageType
		{
			var rootPageReference = GetStartPage().PageLink;
			return GetPagesOf<T>(rootPageReference, null, string.Empty);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		public PageDataCollection GetPagesOf<T>(PageReference rootPageReference) where T : BasePageType
		{
			return GetPagesOf<T>(rootPageReference, null, string.Empty);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		public PageDataCollection GetPagesOf<T>(PageReference rootPageReference, string language) where T : BasePageType
		{
			return GetPagesOf<T>(rootPageReference, null, language);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		public PageDataCollection GetPagesOf<T>(PageReference rootPageReference, PropertyCriteriaCollection criterias) where T : BasePageType
		{
			return GetPagesOf<T>(rootPageReference, criterias, string.Empty);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		[Obsolete("This method is obsolete. Use GetPages<T> instead.", true)]
		public PageDataCollection GetPagesOf<T>(PageReference rootPageReference, PropertyCriteriaCollection criterias, string language) where T : BasePageType
		{
			// Check that the PageReference is not null or empty.
			if (PageReference.IsNullOrEmpty(rootPageReference))
			{
				throw new ArgumentException("The specified search root PageReference is empty or null.");
			}

			// Load the contenttype for the specified ContentType.
			var contentType = PageTypeOf<T>();

			// Create criterias object if criterias is not specified in the method call.
			if (criterias == null)
			{
				criterias = new PropertyCriteriaCollection();
			}

			// Create a criteria that will be used to find content of that type.
			var criteria = new PropertyCriteria
			{
				Name = "PageTypeID",
				Required = true,
				Type = PropertyDataType.PageType,
				Condition = CompareCondition.Equal,
				Value = contentType.ID.ToString(CultureInfo.InvariantCulture)
			};
			criterias.Add(criteria);

			var cacheKey = string.Format("FindPagesWithCriteria{0}{1}{2}", rootPageReference.ID, CreateCacheKey(criterias), language);
			var result = Cache.Get(cacheKey) as PageDataCollection;
			if (result == null)
			{
				PageDataCollection pages;
				if (string.IsNullOrWhiteSpace(language))
				{
					pages = _pageCriteriaQueryService.FindPagesWithCriteria(rootPageReference, criterias);
				}
				else
				{
					pages = _pageCriteriaQueryService.FindPagesWithCriteria(rootPageReference, criterias, language);
				}

				pages = _epiFilterer.FilterCollectionForVisitor(pages);

				if (pages != null)
				{
					result = pages;

					lock (_myLock)
					{
						Cache.Insert(cacheKey, result, GetCacheEvictionPolicy());
					}
				}
			}

			if (result == null)
			{
				result = new PageDataCollection();
			}

			return result.ShallowCopy();
		}

		public IEnumerable<T> GetPages<T>(ContentReference root)
		{
			return GetPages<T>(root, string.Empty);
		}

		public IEnumerable<T> GetPages<T>(ContentReference root, string language, bool filter = true)
		{
			var cacheKey = string.Format("GetPages{0}{1}{2}{3}", typeof(T), root.ID, language, filter);

			var pages = Cache.Get(cacheKey) as IEnumerable<T>;

			if (pages != null)
				return pages;

			var pageDataObjects = EpiWrapper.GetDescendants(root, language).Where(p => p is T);

			var enumerable = pageDataObjects as PageData[] ?? pageDataObjects.ToArray();

			if (filter)
				pageDataObjects = FilterCollectionForVisitor(enumerable);

			pages = pageDataObjects.Cast<T>();

			lock (_myLock)
			{
				Cache.Insert(cacheKey, pages, GetCacheEvictionPolicy());
			}

			return pages;
		}

		/// <summary>
		/// Return a PageDataCollection of all PUBLIC children to the specified contentReference.
		/// </summary>
		public PageDataCollection GetChildren(ContentReference contentReference)
		{
			var pageDataCollection = EpiWrapper.GetChildren(contentReference);

			var publicPageDataCollection = FilterCollectionForVisitor(pageDataCollection);

			return publicPageDataCollection;
		}

		/// <summary>
		/// Get a enumerable list of children that are of a specified type.
		/// </summary>
		public IEnumerable<T> GetChildren<T>(ContentReference root) where T : PageData
		{
			return _contentRepository.GetChildren<T>(root);
		}

		/// <summary>
		/// Get enumerable list of all children.
		/// </summary>
		public IEnumerable<T> GetChildren<T>(ContentReference contentReference, ILanguageSelector languageSelector) where T : BasePageType
		{
            //_contentRepository.GetChildren<T>(contentReference, languageSelector.)
			return _contentRepository.GetChildren<T>(contentReference, (LanguageSelector)languageSelector);
		}

		/// <summary>
		/// Get a enumerable list of siblings that are of a specified type.
		/// </summary>
		public IEnumerable<T> GetSiblings<T>(ContentReference root) where T : PageData
		{
			// Get the PageData for the specified ContentReference.
			var pageData = GetContent(root);

			// Get the parent link.
			var parent = pageData.ParentLink;

			// get all childs for the parent. That will get us all siblings for the specified root.
			return _contentRepository.GetChildren<T>(parent);
		}

		/// <summary>
		/// Get a enumerable list of parents that are of a specified type.
		/// </summary>
		public IEnumerable<T> GetUncles<T>(ContentReference root) where T : PageData
		{
			// Get the PageData for the specified ContentReference.
			var pageData = GetContent(root);

			// Get the parent link.
			var parent = pageData.ParentLink;

			// get all siblings for the parent. That will get us all parents for the specified root.
			return GetSiblings<T>(parent);
		}

		/// <summary>
		/// Get/Load a IContent that represent the specified content id.
		/// </summary>
		public IContent GetContent(int contentId)
		{
			var contentReference = new ContentReference(contentId);

			return GetContent(contentReference);
		}

		/// <summary>
		/// Get/Load a IContent that represent the specified content reference.
		/// </summary>
		public IContent GetContent(ContentReference contentReference)
		{
			return _contentRepository.Get<IContent>(contentReference);
		}

		/// <summary>
		/// Get/Load a IContent that represent the specified content reference in the specified language.
		/// </summary>
		public IContent GetContent(ContentReference contentReference, ILanguageSelector languageSelector)
		{
            return _contentRepository.Get<IContent>(contentReference, (LanguageSelector)languageSelector);
		}

		/// <summary>
		/// Get/Create a CacheEvictionPolicy that are refered to the Database version.
		/// </summary>
		public CacheEvictionPolicy GetCacheEvictionPolicy(object obj)
		{
			return GetCacheEvictionPolicy();
		}

		/// <summary>
		/// Get/Create a CacheEvictionPolicy that are refered to the Database version.
		/// </summary>
		public CacheEvictionPolicy GetCacheEvictionPolicy()
		{
			return new CacheEvictionPolicy(new[] { DataFactoryCache.VersionKey });
		}

		/// <summary>
		/// Get language links for the startpage.
		/// </summary>
		public List<LinkData> GetPageLanguageLinks()
		{
			return GetPageLanguageLinks(PageReference.StartPage.ID);
		}

		/// <summary>
		/// Get language links for the specified page.
		/// </summary>
		public List<LinkData> GetPageLanguageLinks(int pageId)
		{
			return GetPageLanguageLinks(new PageReference(pageId));
		}

		/// <summary>
		/// Get language links for the specified page.
		/// </summary>
		public List<LinkData> GetPageLanguageLinks(PageReference pageReference)
		{
			var langs = new List<LinkData>();

			if (!PageReference.IsNullOrEmpty(pageReference))
			{
				var rootPage = (PageData)GetContent(pageReference);
				foreach (var cultureInfo in rootPage.ExistingLanguages)
				{
					if (cultureInfo.Equals(rootPage.Language))
						continue;

					var languagePage = (PageData)GetContent(pageReference, new LanguageSelector(cultureInfo.Name));

					var link = new LinkData
					{
						Href = languagePage.LinkURL,
						Text = cultureInfo.Name,
						Title = cultureInfo.Name
					};

					langs.Add(link);
				}
			}

			return langs;
		}

		public PageDataCollection SortPageCollection(string sortType, PageDataCollection pages)
		{
			if (sortType == "alphabetical")
			{
				new FilterPropertySort("PageName", FilterSortDirection.Ascending).Filter(pages);
				return pages;
			}

			if (sortType == "createdasc")
			{
				new FilterSort(FilterSortOrder.CreatedAscending).Sort(pages);
				return pages;
			}

			if (sortType == "createddesc")
			{
				new FilterSort(FilterSortOrder.CreatedDescending).Sort(pages);
				return pages;
			}

			if (sortType == "changeddesc")
			{
				new FilterSort(FilterSortOrder.ChangedDescending).Sort(pages);
				return pages;
			}

			if (sortType == "publisheddesc")
			{
				new FilterSort(FilterSortOrder.PublishedDescending).Sort(pages);
				return pages;
			}

			return pages;
		}

		/// <summary>
		/// Filter the PageDataCollection so that all pages that the visitor should not see is removed from the collection.
		/// </summary>
		/// <param name="pages"></param>
		/// <returns></returns>
		public PageDataCollection FilterCollectionForVisitor(PageDataCollection pages)
		{
			return _epiFilterer.FilterCollectionForVisitor(pages);
		}

		/// <summary>
		/// Filter the collection so that all pages that the visitor should not see is removed from the collection.
		/// </summary>
		/// <param name="pages"></param>
		/// <returns></returns>
		public IEnumerable<PageData> FilterCollectionForVisitor(IEnumerable<PageData> pages)
		{
			return _epiFilterer.FilterCollectionForVisitor(pages);
		}

		/// <summary>
		/// Listpages from specified root and sort, take and filter after spcification.
		/// </summary>
		public List<PageListItem> ListPages(PageReference rootPageReference, string sortOrder, int numberOfItems, int articleTypeId)
		{
			var pageList = new List<PageListItem>();

			var pages = new PageDataCollection();

			// Get all ContentPageWitoutSecondaryContentBlockArea in the root that we specified.
			var tempPages = GetChildren<ContentPageWitoutSecondaryContentBlockArea>(rootPageReference);

			if (articleTypeId != 0)
			{
				// Only add the pages that are in the specified Type.
				pages.Add(tempPages.Where(x => x.ContentTypeID == articleTypeId));
			}
			else
			{
				// Add all childpages
				pages.Add(tempPages);
			}

			// If we have specified sort order we new to sort the pages.
			if (!string.IsNullOrEmpty(sortOrder))
			{
				pages = SortPageCollection(sortOrder, pages);
			}

			// Only itterate through the specified number or get all.
			foreach (ContentPageWitoutSecondaryContentBlockArea page in numberOfItems > 0 ? pages.Take(numberOfItems) : pages)
			{
				var p = new PageListItem
				{
					Heading = page.Headline ?? page.PageName,
					Introduction = page.Introduction != null ? page.Introduction.ToHtmlString() : string.Empty,
					LinkURL = page.LinkURL,
					StartPublish = page.StartPublish.ToString(Translate("/common/publishdateformat"))
				};
				pageList.Add(p);
			}

			return pageList;
		}

		public PageType GetPageType(Type type)
		{
			return EpiWrapper.GetPageType(type);
		}

		public PageData CurrentPage
		{
			get { return EpiWrapper.CurrentPage; }
		}

		public LayoutCustomisation GetLayoutCustomisation()
		{
			return GetLayoutCustomisation(CurrentPage);
		}

        public LayoutCustomisation GetLayoutCustomisation(PageData page)
        {
            var layoutCustomisationData = new LayoutCustomisation();

            if (page is BasePageType)
            {
                var layoutCustomisationString = page.GetPropertyValue("LayoutCustomisation", string.Empty);
                layoutCustomisationData.HideBreadcrumb = layoutCustomisationString.Contains("HideBreadcrumb");
                layoutCustomisationData.HideLeftArea = layoutCustomisationString.Contains("HideLeftArea");
                layoutCustomisationData.HideSecondaryContentColumn = layoutCustomisationString.Contains("HideSecondaryContentColumn");
            }

            return layoutCustomisationData;
        }

		public string SiteId
		{
			get { return EpiWrapper.SiteId; }
		}

		public string SiteUrl
		{
			get { return EpiWrapper.SiteUrl; }
		}

		#region Private methods
		/// <summary>
		/// Get all the available PageTypes as a list.
		/// </summary>
		/// <returns></returns>
		private List<ContentType> GetPageTypes()
		{
			return _contentTypeRepository.List().ToList<ContentType>();
		}

		/// <summary>
		/// Get all available BlockTypes as a list.
		/// </summary>
		/// <returns></returns>
		private List<ContentType> GetBlockTypes()
		{
			//var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
			var blockTypeRepository = new BlockTypeRepository(_contentTypeRepository);
			return blockTypeRepository.List().ToList<ContentType>(); ;
		}

		/// <summary>
		/// Load the pagetype object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		private PageType PageTypeOf<T>() where T : BasePageType
		{
			var pageType = _contentTypeRepository.Load(typeof(T));
			if (pageType == null)
			{
				throw new ApplicationException("Specified pagetype could not be loaded");
			}
			return pageType as PageType;
		}

		/// <summary>
		/// Creates a cache key for a PropertyCriteriaCollection. 
		/// Will go through all criterias and make a unique key that can be used for caching the collection.
		/// </summary>
		private string CreateCacheKey(PropertyCriteriaCollection collection)
		{
			var returnValue = new StringBuilder();

			if (collection != null && collection.Count != 0)
			{
				returnValue.AppendFormat("[{0}]", collection.Count);
				var itterator = 1;
				foreach (var criteria in collection)
				{
					returnValue.Append("{");
					returnValue.AppendFormat("[{0}]", itterator);
					returnValue.Append(criteria.Name);
					returnValue.Append(criteria.Value);
					returnValue.Append(criteria.IsNull);
					returnValue.Append(criteria.Required);
					returnValue.Append(criteria.Type.ToString());
					returnValue.Append(criteria.Condition.ToString());
					returnValue.Append("}");
					itterator++;
				}
			}

			return returnValue.ToString();
		}

		
		#endregion

		public string GetExternalUrl(ContentReference contentReference)
		{
			var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
			var url = urlResolver.GetUrl(contentReference);

			if (url.Contains('?'))
				url = url.Split('?')[0];

			return url;
		}

		public string GetExternalUrl(string internalUrl)
		{
			var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
			var url = urlResolver.GetUrl(internalUrl);

			if (url.Contains('?'))
				url = url.Split('?')[0];

			return url;
		}

		public string GetFriendlyURL(PageData page, bool includeSiteUrl)
		{
			return EpiWrapper.GetFriendlyURL(page, includeSiteUrl);
		}
	}
}
