﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Service that contains busniess logic for reports.
	/// </summary>
	public interface IReports
	{
		/// <summary>
		/// Gets all found reports on the site and group them by year.
		/// </summary>
		Dictionary<int, List<ReportItemPageType>> ListPublicReportsByYear(int startpageId, string language);

		/// <summary>
		/// Get the latest report that is of type ReportItemPageType.
		/// </summary>
		ReportItemPageType GetLatestReport();
	}

	/// <summary>
	/// Service that contains busniess logic for reports.
	/// </summary>
	public class Reports : EpiManager, IReports
	{
		[DefaultConstructor]
		public Reports(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}

		public Reports(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		/// <summary>
		/// Gets all found reports on the site and group them by year.
		/// </summary>
		public Dictionary<int, List<ReportItemPageType>> ListPublicReportsByYear(int startpageId, string language)
		{
			var pageReference = new PageReference(startpageId);

			var reports = GetPages<ReportItemPageType>(pageReference, language);

			var filteredReports = reports;

			var reportsByYear = filteredReports.GroupBy(r => r.Year).OrderByDescending(rby => rby.Key).ToDictionary(rby => rby.Key, rby => rby.OrderBy(r => r.Quarter).ToList());

			return reportsByYear;
		}

		/// <summary>
		/// Get the latest report that is of type ReportItemPageType.
		/// </summary>
		public ReportItemPageType GetLatestReport()
		{
			// Get all reports.
			var reports = GetPages<ReportItemPageType>(ContentReference.StartPage);
			// Sort the reports after StartPublishing and get the first.			
			var report = reports.OrderByDescending(r => r.StartPublish).First();

			return report;
		}
	}
}
