﻿using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	/// <summary>
	/// Service that contains busniess logic for InfoMap.
	/// </summary>
	public interface IInfoMap
	{
		/// <summary>
		/// Go through all containers and load the countries that are related to each container.
		/// </summary>
		List<InfoMapCountryContainerPageType> PopulateCountries(List<InfoMapCountryContainerPageType> containers);
	}

	/// <summary>
	/// Service that contains busniess logic for InfoMap.
	/// </summary>
	public class InfoMap : EpiManager, IInfoMap
	{
		[DefaultConstructor]
		public InfoMap(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}

		public InfoMap(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		/// <summary>
		/// Go through all containers and load the countries that are related to each container.
		/// </summary>
		public List<InfoMapCountryContainerPageType> PopulateCountries(List<InfoMapCountryContainerPageType> containers)
		{
			foreach (var container in containers)
			{
				var countries = GetChildren<InfoMapCountryPageType>(container.ContentLink);
				container.Countries = countries;
			}

			return containers;
		}
	}
}
