﻿using Kwd.Kestrel.Library.Configuration.Global;
using Kwd.Kestrel.Library.Configuration.ResponsiveImages;
using Kwd.Kestrel.Library.Configuration.Search;
using Kwd.Kestrel.Library.Configuration.Search.GoogleSiteSearch;
using Kwd.Kestrel.Library.Configuration.SiteSettings;
using Kwd.Kestrel.Library.Configuration.Twitter;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IConfiguration
	{
		GlobalSection Global { get; }
		TwitterSection Twitter { get; }
		SearchSection Search { get; }
		SiteSettingSection SiteSettings { get; }
		SiteSettingElement CurrentSiteSettings { get; }
		ResponsiveImageSection ResponsiveImages { get; }
		GoogleSiteSearchSection GoogleSiteSearch { get; }
	}

	public class Configuration : IConfiguration
	{
		private string _epiCurrentSiteId = string.Empty;
		protected string EpiCurrentSiteId
		{
			get
			{
				return _epiCurrentSiteId;
			}
			set
			{
				_epiCurrentSiteId = value;
			}
		}

		private string _rootSection = "Kestrel";
		protected string Rootsection
		{
			get
			{
				return _rootSection;
			}
			set
			{
				_rootSection = value;
			}
		}

		public GlobalSection Global
		{
			get
			{
				return GetSection<GlobalSection>(Rootsection + "/Global");
			}
		}

		public TwitterSection Twitter
		{
			get
			{
				return GetSection<TwitterSection>(Rootsection + "/Twitter");
			}
		}

		public SearchSection Search
		{
			get
			{
				return GetSection<SearchSection>(Rootsection + "/Search");
			}
		}

		public SiteSettingElement CurrentSiteSettings
		{
			get
			{
				var section = GetSection<SiteSettingSection>(Rootsection + "/SiteSettings");
				var sitesCollection = section.Sites;
				var siteElement = sitesCollection[_epiCurrentSiteId];
				return siteElement;
			}
		}

		public SiteSettingSection SiteSettings
		{
			get
			{
				return GetSection<SiteSettingSection>(Rootsection + "/SiteSettings");
			}
		}

		public ResponsiveImageSection ResponsiveImages
		{
			get
			{
				return GetSection<ResponsiveImageSection>(Rootsection + "/ResponsiveImages");
			}
		}

		public GoogleSiteSearchSection GoogleSiteSearch
		{
			get
			{
				return GetSection<GoogleSiteSearchSection>(Rootsection + "/GoogleSiteSearch");
			}
		}

		#region Private methods
		protected static T GetSection<T>(string path)
		{
			var config = (T)System.Configuration.ConfigurationManager.GetSection(path);
			return config;
		}
		#endregion
	}
}
