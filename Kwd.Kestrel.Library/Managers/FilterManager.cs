﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.ExtensionMethods;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IFilterManager
	{
		IEnumerable<T> FilterByCategory<T>(IEnumerable<T> content, CategoryList categories) where T : PageData;

		IEnumerable<T> FilterByPageType<T>(IEnumerable<T> content, PageType type) where T : PageData;

		IEnumerable<IContent> FilterForMenu(List<IContent> list);
	}

	public class FilterManager : IFilterManager
	{
		private Category _newsCategories;
		private IContentRepository _contentRepository;

		[DefaultConstructor]
		public FilterManager()
		{
			_contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
		}

		public FilterManager(IContentRepository contentRepository)
		{
			_contentRepository = contentRepository;
		}

		public IEnumerable<T> FilterByCategory<T>(IEnumerable<T> content, CategoryList categories) where T : PageData
		{
			foreach (var cat in categories)
			{
				foreach (var c in content)
				{
					var subCats = c.Category.GetActiveSubCategories(cat);
					if (c.Category.Contains(cat) || (subCats != null && subCats.Count > 0))
						yield return c;
				}
			}
		}

		public IEnumerable<T> FilterByPageType<T>(IEnumerable<T> content, PageType type) where T : PageData
		{
			return content.Where(c => c.PageTypeID == type.ID);
		}

		/// <summary>
		/// Filter the <paramref name="content">content</paramref> by one <paramref name="cat">category</paramref> or it's parent category.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="content">Content to filter</param>
		/// <param name="cat">(sub)Category to filter by</param>
		/// <returns></returns>
		public IEnumerable<T> FilterByOneCategory<T>(IEnumerable<T> content, Category cat)
		{
			foreach (var c in content)
			{
				var cCat = c as ICategorizable;
				if (cCat == null)
					continue;

				if (cCat.Category.Contains(cat.ID) || cCat.Category.Contains(cat.Parent.ID))
				{
					yield return c;
				}
			}
		}

		public IEnumerable<IContent> FilterForMenu(List<IContent> list)
		{
			var to = new FilterCompareTo("PageVisibleInMenu", "True");
			to.Filter(list);

			var published = new FilterPublished(PagePublishedStatus.Published);
			published.Filter(list);

			var access = new FilterAccess(AccessLevel.Read);
			access.Filter(list);

			foreach (var i in list)
			{
				var page = _contentRepository.Get<PageData>(i.ContentLink);
				if ((page.HasTemplate() || !(page is INotLinkedInMenu)) && !(page is INeverVisibleInMenu))
				{
					yield return i;
				}
			}
		}
	}
}
