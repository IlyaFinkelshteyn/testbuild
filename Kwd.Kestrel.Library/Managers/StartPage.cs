﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.Models;
using StructureMap;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IStartPage
	{
		/// <summary>
		/// Return the startpage as StartPageType.
		/// </summary>
		/// <returns></returns>
		StartPageType GetPage();

		/// <summary>
		/// Return the link Url to the startpage.
		/// </summary>
		string LinkUrl { get; }

		string GoogleAnalyticsId { get; }

		List<MenuLinkData> GetMobileMenuLinks();
	}

	public class StartPage : EpiManager, IStartPage
	{
		[DefaultConstructor]
		public StartPage(ILogger logger, ICache cache)
			: base(logger, cache){}

		public StartPage(ILogger logger, ICache cache, IContentTypeRepository contentTypeRepository, IPageCriteriaQueryService pageCriteriaQueryService, IContentRepository contentRepository, IEpiFilterer epiFilterer)
			: base(logger, cache, contentTypeRepository, pageCriteriaQueryService, contentRepository, epiFilterer)
		{
		}

		public StartPageType GetPage()
		{
			return GetStartPage();
		}

		public string LinkUrl
		{
			get { return GetStartPage().ExternalURL; }
		}

		public string GoogleAnalyticsId
		{
			get
			{
				var googleAnalyticsId = GetStartPage().GoogleAnalyticsId;
				return googleAnalyticsId;
			}
		}

		public List<MenuLinkData> GetMobileMenuLinks()
		{
			var menuLinks = new List<MenuLinkData>();

			var level1 = GetChildren<PageData>(ContentReference.StartPage);

			if (level1 != null)
			{
				menuLinks = level1.Where(x => x.VisibleInMenu).Select(p => new MenuLinkData(p)).ToList();

				foreach (var menuLinkData in menuLinks)
				{
					var childs = GetMobileMenuChildLinks(menuLinkData.ContentLink as PageReference);
					if (childs != null)
					{
						menuLinkData.Children = childs.ToList();
					}
				}
			}

			return menuLinks;
		}

		public List<MenuLinkData> GetMobileMenuChildLinks(PageReference pageReference)
		{
			var menuLinks = new List<MenuLinkData>();
			var children = GetChildren<PageData>(pageReference);

			if (children != null)
			{

				menuLinks = children.Where(x => x.VisibleInMenu).Select(p => new MenuLinkData(p)).ToList();
				foreach (var menuLinkData in menuLinks)
				{
					var childs = GetMobileMenuChildLinks(menuLinkData.ContentLink as PageReference);
					if (childs != null)
					{
						menuLinkData.Children = childs.ToList();
					}
				}

				return menuLinks;
			}

			return new List<MenuLinkData>();
		}
	}
}
