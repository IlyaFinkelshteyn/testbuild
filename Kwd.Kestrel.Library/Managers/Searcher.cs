﻿using System;
using EPiServer.Core;
using Kwd.Kestrel.Library.Configuration.Search;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Models.Search;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface ISearcher
	{
		/// <summary>
		/// Return the Url to the search page.
		/// </summary>
		string SearchPageUrl { get; }

		ISearchResult Search(string searchString, string language, int hitsPerPage, int offset);
	}

	public class Searcher : BaseManager, ISearcher
	{
		private readonly IConfiguration _configuration;
		private readonly ISearchProvider _searchProvider;
		private readonly IEpiManager _epiManager;

		[DefaultConstructor]
		public Searcher(ILogger logger, ICache cache, IConfiguration configuration, IEpiManager epiManager)
			:base(logger, cache)
		{
			_configuration = configuration;
			_searchProvider = this.GetSearchProvider();
			_epiManager = epiManager;
		}

		public Searcher(ILogger logger, ICache cache, IConfiguration configuration, ISearchProvider searchProvider)
			:base(logger, cache)
		{
			_searchProvider = searchProvider;
			_configuration = configuration;
		}

		/// <summary>
		/// Return the Url to the search page.
		/// </summary>
		public string SearchPageUrl
		{
			get
			{
				var searchPage = (PageData)_epiManager.GetContent(_epiManager.GetStartPage().SearchPage);
				return searchPage.LinkURL;
			}
		}

		public ISearchResult Search(string searchString, string language, int hitsPerPage, int offset)
		{
			return _searchProvider.Search(StringExtensions.RemoveForbiddenChars(searchString), language, hitsPerPage, offset);
		}

		#region Private methods
		private ISearchProvider GetSearchProvider()
		{
			var search = _configuration.Search;
			SearchProviderElement providerElement = null;
			foreach (SearchProviderElement element in search.Providers)
			{
				if (element.ProviderName == search.DefaultProvider)
				{
					providerElement = element;
					break;
				}
			}

			if (providerElement == null)
			{
				Logger.LogWarning(string.Format("Unable to find search provider {0}.", search.DefaultProvider));
				throw new Exception(string.Format("Unable to find search provider {0}.", search.DefaultProvider));
			}

			var instObj = Activator.CreateInstance(providerElement.TypeAssembly, providerElement.TypeType);
			var searchProvider = (ISearchProvider)instObj.Unwrap();
			return searchProvider;
		}
		#endregion
	}
}
