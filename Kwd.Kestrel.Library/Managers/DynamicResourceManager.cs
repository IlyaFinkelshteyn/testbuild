﻿using System;
using System.Web;
using System.Web.UI;
using Kwd.Kestrel.Library.Epi.Interfaces;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IDynamicResourceManager
	{

		void AddCss(string path, string media = null);
		/*
        void AddJs(string path, bool placeLast = false, bool placeBeforeRegularScripts = false, bool defer = false);
		void AddScriptText(string scriptText, bool placeLast = false, bool placeBeforeRegularScripts = false);
		*/
		void AddJs(string path, ResourcePosition placement = ResourcePosition.HeaderAfterRegularScripts, bool defer = false);
		void AddScriptText(string scriptText, ResourcePosition placement = ResourcePosition.HeaderAfterRegularScripts);
	}

	public enum ResourcePosition
	{
		HeaderBeforeRegularScripts,
		HeaderAfterRegularScripts,
		BodyTagStart,
		BodyTagEnd
	}

	public class DynamicResourceManager : BaseManager, IDynamicResourceManager
	{
		[DefaultConstructor]
		public DynamicResourceManager(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}
		private IMasterAddHeadControls Master
		{
			get
			{
				var page = HttpContext.Current.Handler as Page;
				if (page == null)
				{
					var msg = "Dynamic resource can only be placed on a page";
					Logger.LogFatal(msg);
					throw new Exception(msg);
				}
				var master = page.Master as IMasterAddHeadControls;
				if (master == null)
				{
					var msg = "Dynamic resource can only be placed on a master page that implements IMasterAddHeadControl";
					Logger.LogFatal(msg);
					throw new Exception(msg);
				}
				return master;
			}
		}
		public void AddCss(string path, string media = null)
		{
			Master.AddCss(path, media);
		}
		public void AddJs(string path, ResourcePosition placement, bool defer = false)
		{
			Master.AddJs(path, placement, defer);
		}

		public void AddScriptText(string scriptText, ResourcePosition placement)
		{
			Master.AddScriptText(scriptText, placement);
		}
	}
}
