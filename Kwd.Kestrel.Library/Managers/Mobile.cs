﻿using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Managers
{
	public interface IMobile
	{
		/// <summary>
		/// Create a mobile navigation structure from the startpage.
		/// </summary>
		MenuTreeData GetMobileNavigationStructure(int selectedPageId, string language);

		/// <summary>
		/// Create a mobile navigation structure from the specified root page.
		/// </summary>
		MenuTreeData GetMobileNavigationStructure(int rootPageId, int selectedPageId, string language);
	}

	public class Mobile : EpiManager, IMobile
	{
		private ILanguageSelector _languageSelector;
		private int _selectedPageId = 0;

		[DefaultConstructor]
		public Mobile(ILogger logger, ICache cache)
			: base(logger, cache)
		{
		}

		public Mobile(ILogger logger, ICache cache, IContentRepository contentRepository)
			: base(logger, cache, contentRepository)
		{

		}

		public MenuTreeData GetMobileNavigationStructure(int selectedPageId, string language)
		{
			return GetMobileNavigationStructure(PageReference.StartPage, selectedPageId, language);
		}

		public MenuTreeData GetMobileNavigationStructure(int rootPageId, int selectedPageId, string language)
		{
			var pageReference = new PageReference(rootPageId);

			return GetMobileNavigationStructure(pageReference, selectedPageId, language);
		}

		private MenuTreeData GetMobileNavigationStructure(PageReference pageReference, int selectedPageId, string language)
		{
			// Set private variables so that we can used them in other methods.
			_languageSelector = new LanguageSelector(language);
			_selectedPageId = selectedPageId;

			var menuTreeData = new MenuTreeData();
			menuTreeData.Items = new List<MenuTreeItem>();

			var children = GetChilds(pageReference);
			PopulateMenuTreeData(menuTreeData, children);

			return menuTreeData;
		}

		private void PopulateMenuTreeData(MenuTreeData menuTreeData, IEnumerable<PageData> items)
		{
			foreach (var pageData in items)
			{
				var menuTreeItem = CreateTreeItem(pageData, 0);

				// If the item is selected we need to set the parent as open.
				if (menuTreeItem.Selected)
				{
					menuTreeItem.Open = true;
				}

				// Check if the menuTreeItem has childs. If so we need to add them.
				var childs = GetChilds(pageData.ContentLink);
				if (childs != null)
				{
					PopulateMenuTreeItems(menuTreeItem, childs);
				}

				menuTreeData.Items.Add(menuTreeItem);
			}
		}

		private void PopulateMenuTreeItems(MenuTreeItem menuTreeItem, IEnumerable<PageData> items)
		{
			foreach (var pageData in items)
			{
				var childMenuTreeItem = CreateTreeItem(pageData, menuTreeItem.Level);

				// If the item is selected we need to set the parent as open.
				if (childMenuTreeItem.Selected)
				{
					menuTreeItem.Open = true;
				}

				// Check if the menuTreeItem has childs. If so we need to add them.
				var childs = GetChilds(pageData.ContentLink);
				if (childs != null)
				{
					PopulateMenuTreeItems(childMenuTreeItem, childs);
					// If the item is selected we need to set the parent as open.
					if (childMenuTreeItem.Open)
					{
						menuTreeItem.Open = true;
					}

				}

				menuTreeItem.Items.Add(childMenuTreeItem);
			}
		}

		private MenuTreeItem CreateTreeItem(PageData pageData, int parentLevel)
		{
			var menuTreeItem = new MenuTreeItem
			{
				PageId = pageData.ContentLink.ID,
				Level = parentLevel + 1,
				Text = pageData.Name,
				Title = pageData.Name,
				Href = string.Format("{0}&epslanguage={1}", pageData.StaticLinkURL, pageData.Language.Name),
				VisibleInMenu = pageData.VisibleInMenu,
				Selected = (_selectedPageId == pageData.ContentLink.ID),
			};

			if (pageData is INotLinkedInMenu)
			{
				menuTreeItem.Href = string.Empty;
			}

			if (pageData is INeverVisibleInMenu)
			{
				menuTreeItem.VisibleInMenu = false;
			}

			return menuTreeItem;
		}

		private IEnumerable<PageData> GetChilds(ContentReference contentReference)
		{
			var childs = GetChildren<BasePageType>(contentReference, _languageSelector);
			var children = new PageDataCollection(childs);
			new FilterCompareTo("PageVisibleInMenu", "true").Filter(children);
			return children;
		}
	}
}
