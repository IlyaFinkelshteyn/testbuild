﻿using System;
using System.IO;
using System.Linq;
using EPiServer.Core;
using Kwd.Kestrel.Library.Epi;
using Kwd.Kestrel.Library.Models.Excel;
using Syncfusion.XlsIO;

namespace Kwd.Kestrel.Library.Managers
{
    public interface IExcelReader
    {
        /// <summary>
        /// Get all data from the specified sheet in the specified excel file.
        /// </summary>
        /// <param name="mediaData">The excel file.</param>
        /// <param name="sheetName">The name on the sheet that should be read.</param>
        /// <returns></returns>
        ExcelSheet LoadExcelData(MediaData mediaData, string sheetName);
		ExcelSheet LoadFinancialExcelData(MediaData mediaData, string sheetName);
    }

    public class ExcelReader : EpiManager, IExcelReader
    {
        public ExcelReader(ILogger logProvider, ICache cache, IEpiWrapper epiWrapper)
            : base(logProvider, cache, epiWrapper)
        {
        }

	    public ExcelSheet LoadFinancialExcelData(MediaData mediaData, string sheetName)
	    {
			var excelSheet = new ExcelSheet
			{
				Name = sheetName
			};

			if (mediaData == null)
				return excelSheet;

			using (var excelEngine = new ExcelEngine())
			{
				var s = mediaData.BinaryData.OpenRead();
				var buf = new byte[s.Length];
				s.Read(buf, 0, buf.Length);
				s.Close();
				s = new MemoryStream(buf);

				var application = excelEngine.Excel;
				var workbook = application.Workbooks.Open(s);
				ExcelDetailedRow[] _rows;
				IWorksheet sheet = workbook.Worksheets[sheetName];
				if (sheet == null)
				{
					_rows = new ExcelDetailedRow[0];
				}
				else
				{
					int i;

					// OBS Hoppar över första raden
					_rows = new ExcelDetailedRow[sheet.Rows.Length - 1];
					for (i = 2; i <= sheet.Rows.Length; ++i)
					{
						var row = sheet.Rows[i - 1];
						_rows[i - 2] = new ExcelDetailedRow(row, sheet, i);
					}
				}
				workbook.Close();
				s.Close();

				excelSheet.Rows = _rows.ToList();
			}

		    return excelSheet;
	    }

        public ExcelSheet LoadExcelData(MediaData mediaData, string sheetName)
        {
            var excelSheet = new ExcelSheet
            {
	            Name = sheetName
            };

	        if (mediaData == null) 
				return excelSheet;
	        
			try
	        {
		        using (var excelEngine = new ExcelEngine())
		        {
			        var s = mediaData.BinaryData.OpenRead();
			        var buf = new byte[s.Length];
			        s.Read(buf, 0, buf.Length);
			        s.Close();
			        s = new MemoryStream(buf);

			        var application = excelEngine.Excel;
			        var workbook = application.Workbooks.Open(s);
			        var sheet = string.IsNullOrWhiteSpace(sheetName) ? workbook.Worksheets[0] : workbook.Worksheets[sheetName];
			        if (sheet != null)
			        {
				        for (var i = 1; i <= sheet.Rows.Length; ++i)
				        {
					        var row = sheet.Rows[i - 1];
					        excelSheet.Rows.Add(new ExcelDetailedRow(row, sheet, i));
				        }
			        }
			        workbook.Close();
			        s.Close();
		        }
	        }
	        catch (Exception ex)
	        {
		        Logger.LogError(ex, string.Format("Could not load data from excel file. {0}", mediaData.Name));
	        }
	        return excelSheet;
        }
    }

}
