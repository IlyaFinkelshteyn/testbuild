﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "88F1F178-A96D-4739-9F47-4EE6EF06ECA0",
		DisplayName = "Point of interest",
		Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class PointOfInterestPageType : PointOfInterest, IHasCoordinates
	{
		public PointOfInterestPageType()
	    {
		    
	    }

		public PointOfInterestPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Display(
			Name = "Headline",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Content",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string Content { get; set; }

		
	}
}