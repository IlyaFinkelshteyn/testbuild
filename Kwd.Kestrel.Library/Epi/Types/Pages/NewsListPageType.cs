﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Kwd.Kestrel.Library.Epi.Attributes;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(DisplayName = "News List", GUID = "70C92E07-6C83-4D6D-868F-DFA9B61BEFBE", Description = "Page listing news items in chronological order.")]
	public class NewsListPageType : ContentPageNormalWithWideFooterBlockAreas
	{
		public NewsListPageType()
		{

		}

		public NewsListPageType(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{

		}

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Fetch news items from",
			Description = "Leave blank if the news items are the children of this page",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual PageReference FetchNewsItemsFrom { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Below main content block area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual ContentArea BelowMainContentBlockArea { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Bottom block area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		public virtual ContentArea BottomBlockArea { get; set; }

		[NewsTypeSelection]
		public virtual string NewsTypes { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			BottomBlocksAreaTheme = BlockTheme.Basic;
			BottomBlocksArea2Theme = BlockTheme.Basic;
			BottomBlocksArea3Theme = BlockTheme.Basic;

		}
	}
}