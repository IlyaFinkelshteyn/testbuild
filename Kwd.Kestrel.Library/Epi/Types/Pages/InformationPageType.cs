﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [ContentType(DisplayName = "InformationPage", GUID = "DDC6B997-2029-411F-AD2F-255D8F12FCC2", Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class InformationPageType : ContentPageNormal
	{
	    public InformationPageType()
	    {
	    }

	    public InformationPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
	    }

		[CultureSpecific]
        [Display(
                    Name = "Below main content block area",
                    Description = "",
                    GroupName = SystemTabNames.Content,
                    Order = 100)]
        public virtual ContentArea BelowMainContentBlockArea { get; set; }
	}
}