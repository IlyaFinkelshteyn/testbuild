﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "A6029A4C-6429-45DC-B362-6541289EA34E",
		DisplayName = "Calendar List",
		Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class CalendarListPageType : BasePageType, ISecondaryContentBlockArea
	{
		public CalendarListPageType()
		{
			
		}

		public CalendarListPageType(IEpiWrapper epiWrapper) : 
			base(epiWrapper)
		{
			
		}

		[CultureSpecific]
		[Display(
			Name = "Alternate page heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }

		[CultureSpecific]
		[Display(
					Name = "Secondary content block area",
					Description = "",
					GroupName = SystemTabNames.Content,
					Order = 100)]
		public virtual ContentArea SecondaryContentBlockArea { get; set; }
	}
}