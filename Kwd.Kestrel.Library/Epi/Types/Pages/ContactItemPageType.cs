﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	[ContentType(GUID = "3838A698-98B1-40FA-A322-AC16D4EC4346",
		DisplayName = "Contact item",
		Description = "Contact person or office")]
	public class ContactItemPageType : PointOfInterest, ISecondaryContentBlockArea, IHasResponsiveImage, IHasCoordinates
	{
		private ResponsiveImageData _responsiveImageData;

		public ContactItemPageType()
		{

		}

		public ContactItemPageType(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{

		}

		[Display(
			Name = "Is office",
			Description = "If the contact is a office and not a person you should check this box.",
			GroupName = SystemTabNames.Content,
			Order = 3)]
		public virtual bool IsOffice { get; set; }

		[Display(
			Name = "Is prioritized",
			Description = "If this contact should be shown first on the list page",
			GroupName = SystemTabNames.Content,
			Order = 5)]
		public virtual bool IsPrioritized { get; set; }


		[Display(
			Name = "Name",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string ContactName { get; set; }

		[Display(
			Name = "Title",
			Description = "Title",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string Title { get; set; }

		[Display(
			Name = "Role",
			Description = "Role",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual string Role { get; set; }


		[UIHint("email")]
		[Display(
			Name = "Email",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual string Email { get; set; }

		[Display(
			Name = "Phone",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual string Phone { get; set; }

		[Display(
			Name = "Mobile",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 50)]
		public virtual string Mobile { get; set; }

		[Display(
			Name = "Fax",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual string Fax { get; set; }

		[Display(
			Name = "Address",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 70)]
		public virtual XhtmlString Address { get; set; }

		[Display(
			Name = "Visiting Address",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 80)]
		public virtual XhtmlString VisitingAddress { get; set; }

		[Display(
			Name = "Goods address",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 90)]
		public virtual XhtmlString GoodsAddress { get; set; }

		[Display(
			Name = "Country",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 100)]
		public virtual string Country { get; set; }



		[Display(
			Name = "LinkedIn",
			Description = "URL link to the contact linkedin profile.",
			GroupName = SystemTabNames.Content,
			Order = 105)]
		public virtual string LinkedIn { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		public virtual Url Image { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Alternative Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 120)]
		public virtual string ImageAltText { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Title Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 130)]
		public virtual string ImageTitleText { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Main body",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 140)]
		public virtual XhtmlString MainBody { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Secondary content block area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 180)]
		public virtual ContentArea SecondaryContentBlockArea { get; set; }

		[ScaffoldColumn(false)]
		public virtual string Headline
		{
			get
			{
				var name = string.Empty;
				if (!string.IsNullOrEmpty(ContactName))
				{
					name = ContactName;
				}
				else if (!string.IsNullOrEmpty(PageTitle))
				{
					name = PageTitle;
				}
				else
				{
					name = Name;
				}

				return name.Replace("'", "\\'");
			}
			set { }
		}

		[ScaffoldColumn(false)]
		public virtual string Content
		{
			get
			{
				var _content = Title + "<br/>" + Phone;
				return _content.Replace("'", "\\'");
			}
			set { }
		}

		[ScaffoldColumn(false)]
		public virtual string FilterKeys
		{
			get
			{
				string result = string.Empty;
				if (Category.Count > 0)
				{
					result = string.Format("'{0}'", string.Join("','", Category.ToArray()));
				}

				return result;
			}
			set { }
		}

		[ScaffoldColumn(false)]
		public virtual string MarkerId
		{
			get { return PageGuid.ToString(); }
			set { }
		}

		[ScaffoldColumn(false)]
		private string _imageType;
		[ScaffoldColumn(false)]
		public string ImageType
		{
			get
			{
				return string.IsNullOrEmpty(_imageType) ? "image-col-1" : _imageType;
			}
			set
			{
				_imageType = value;
			}
		}

		public virtual ResponsiveImageData ImageData
		{
			get
			{
				if (_responsiveImageData != null)
					return _responsiveImageData;

				if (Image == null)
					return new ResponsiveImageData();

				var rid = new ResponsiveImageData
				{
					Path = Image.ToString(),
					Alt = ImageAltText,
					Title = !string.IsNullOrEmpty(ImageTitleText) ? ImageTitleText : ContactName,
					ImageType = "image-col-1",
					PropertyName = "Image",
				};

				_responsiveImageData = rid;

				return rid;
			}
		}
	}
}