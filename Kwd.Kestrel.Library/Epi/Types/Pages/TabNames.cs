﻿namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	/// <summary>
	/// Provides constants for the different tabs
	/// </summary>
	public static class TabNames
	{
		/// <summary>
		/// Constant representing the Info tab.
		/// </summary>
		public const string Info = "Info";

		/// <summary>
		/// Constant representing the Search engines tab.
		/// </summary>
		public const string SearchEngines = "Search engines";

		/// <summary>
		/// Constant representing the SiteConfiguration tab.
		/// </summary>
		public const string SiteConfiguration = "SiteConfiguration";

		/// <summary>
		/// Constant representing the BlockConfiguration tab.
		/// </summary>
		public const string BlockConfiguration = "Block configuration";

		/// <summary>
		/// Constant representing the SiteConfiguration tab.
		/// </summary>
		public const string ExtendedFooter = "Extended footer";

		/// <summary>
		/// Constant representing the SiteConfiguration tab.
		/// </summary>
		public const string MapInformation = "MapInformation";

		public const string Imported = "Imported properties from old site";

		public const string PageType = "Page type (News item, Story, Press Release, Financial Report)";

		public const string Cision = "Cision";
	}
}