﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    public abstract class ContentPageNormal : ContentPageWitoutSecondaryContentBlockArea, ISecondaryContentBlockArea
    {
		public ContentPageNormal()
	    {
	    }

		public ContentPageNormal(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
	    }

		[CultureSpecific]
        [Display(
                    Name = "Secondary content block area",
                    Description = "",
                    GroupName = SystemTabNames.Content,
                    Order = 100)]
        public virtual ContentArea SecondaryContentBlockArea { get; set; }
    }
}
