﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Interfaces;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models.ResponsiveImage;
using StructureMap;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(DisplayName = "News Item", GUID = "F3991B88-F196-4988-AE36-BA928119FE10", Description = "A news page displaying press releases, news, stories and financial news.")]
	public class NewsItemPageType : StandardPageType, IHasResponsiveImage, ICanBePresentedAsNewsFeedItem
	{
		private string _imageType;

		public NewsItemPageType()
		{

		}

		public NewsItemPageType(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{

		}

		[Ignore]
		public DateTime PublishDate
		{
			get { return this.StartPublish; }
		}

		[CultureSpecific]
		[Display(
			Name = "Introduction short",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 6)]
		public virtual XhtmlString IntroductionShort { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "List Image",
			GroupName = SystemTabNames.Content,
			Order = 7)]
		public virtual Url Image { get; set; }
		
		[CultureSpecific]
		[Display(
			Name = "Image Alternative Text",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 8)]
		public virtual string ImageAltText { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Image Title Text",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 9)]
		public virtual string ImageTitleText { get; set; }

		public DateTime SortDate { get; private set; }

		public bool Sticky { get; private set; }

		[CultureSpecific]
		[Display(
			Name = "Page Contact",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual PageReference PageContact { get; set; }

		public override bool ShowUpdateDate
		{
			get
			{
				return true;
			}
		}


		public ResponsiveImageData ImageData
		{
			get
			{
				return new ResponsiveImageData
				{
					ImageType = ImageType,
					Path = Image != null ? Image.ToString() : string.Empty,
					Title = ImageTitleText,
					Alt = ImageAltText,
					PropertyName = "Image",
				};
			}
		}

		#region Ignores

		[Ignore]
		public string CssClass
		{
			get
			{
				return string.Empty;
			}
		}

		[Ignore]
		public string Title
		{
			get
			{
				return PageName;
			}
		}

		[Ignore]
		public DateTime Date
		{
			get
			{
				return StartPublish;
			}
		}

		[Ignore]
		public string Body
		{
			get
			{
				return IntroductionShort != null ? IntroductionShort.ToHtmlString() : string.Empty;
			}
		}

		[Ignore]
		public string LinkUrl
		{
			get
			{
				return LinkURL;
			}
		}

		[Ignore]
		public string ContentType
		{
			get
			{
				var mgr = ObjectFactory.GetInstance<IEpiManager>();
				return mgr.Translate("/newsroom/news");
			}
		}

		[ScaffoldColumn(false)]
		public string ImageType
		{
			get
			{
				return string.IsNullOrEmpty(_imageType) ? "pagelistingblock-image" : _imageType;
			}
			set
			{
				_imageType = value;
			}
		}

		#endregion
		
		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			VisibleInMenu = false;
		}

		public int CompareTo(object obj)
		{
			return DateTime.Compare(((ICanBePresentedAsNewsFeedItem)obj).SortDate, this.SortDate);
		}

		#region Cision

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
			Name = "Cision GUID",
			GroupName = TabNames.Cision,
			Order = 115)]
		public virtual string CisionGUID { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
			Name = "ExtraCisionID",
			Description = "",
			GroupName = TabNames.Cision,
			Order = 117)]
		public virtual string ExtraCisionID { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Media Images",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 120)]
		public virtual LinkItemCollection MediaImages { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Company",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 130)]
		public virtual string CisionCompany { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Contact",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 140)]
		public virtual string CisionContact { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Media Documents",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 150)]
		public virtual LinkItemCollection CisionMediaDocuments { get; set; }

		[ScaffoldColumn(false)]
		[Display(
				Name = "Type Code",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 160)]
		public virtual string CisionTypeCode { get; set; }

		[ScaffoldColumn(false)]
		[Display(
				Name = "Type Code Name",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 170)]
		public virtual string CisionTypeCodeName { get; set; }

		[ScaffoldColumn(false)]
		[Display(
				Name = "CommonId",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 116)]
		public virtual string CommonId { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Replaced By Page ID",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 180)]
		public virtual PageReference ReplacedByPageID { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(
				Name = "Replaces Cision ID",
				Description = "",
				GroupName = TabNames.Cision,
				Order = 190)]
		public virtual string ReplacesCisionID { get; set; }

		#endregion

		#region imported properties

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyString))]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual string Heading { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyString))]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual string SubHeading { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual XhtmlString FooterBody { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyImageUrl))]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual Url ThumbnailImage { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyImageUrl))]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual Url PressImage { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyString))]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual string PressImageText { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyString))]
		[Display(GroupName = TabNames.Imported)]
		public virtual string RssItemId { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyLongString))]
		[Display(GroupName = TabNames.Imported)]
		public virtual string SearchCategory { get; set; }

		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyLongString))]
		[Display(GroupName = TabNames.Imported)]
		public virtual string SearchSection { get; set; }

		[ScaffoldColumn(false)]
		[Display(GroupName = TabNames.Imported)]
		public virtual bool ExcludePageFromSearch { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual LinkItemCollection RelatedPages { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[BackingType(typeof(PropertyString))]
		[Display(GroupName = TabNames.Imported)]
		public virtual string NewsLanguage { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		public virtual PageReference ConnectedToCampaign { get; set; }

		[ScaffoldColumn(false)]
		[Display(GroupName = TabNames.Imported)]
		public virtual PageReference SlideshowMedia { get; set; }

		[ScaffoldColumn(false)]
		[Display(GroupName = TabNames.Imported)]
		[BackingType(typeof(PropertyString))]
		public virtual string ApiHeading { get; set; }

		[ScaffoldColumn(false)]
		[Display(GroupName = TabNames.Imported)]
		[BackingType(typeof(PropertyString))]
		public virtual string ApiSubHeading { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.Imported)]
		[BackingType(typeof(PropertyString))]
		public virtual string SubMenuHeading { get; set; }
		
		#endregion

		#region PageTypes

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.PageType)]
		public virtual bool News { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.PageType)]
		public virtual bool PressRelease { get; set; }

		[ScaffoldColumn(false)]
		[CultureSpecific]
		[Display(GroupName = TabNames.PageType)]
		public virtual bool FinancialReport { get; set; }

		[ScaffoldColumn(false)]
		[Display(GroupName = TabNames.PageType)]
		public virtual bool Story { get; set; }

		#endregion
	}
}