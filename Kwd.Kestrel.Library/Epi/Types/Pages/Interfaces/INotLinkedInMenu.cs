﻿namespace Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces
{
	/// <summary>
	/// Pagetypes can implement this interface to tell example mobile navigation that not create A links for this type of pages.
	/// </summary>
	public interface INotLinkedInMenu
	{
	}
}
