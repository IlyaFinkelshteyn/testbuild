﻿namespace Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces
{
	/// <summary>
	/// Pagetypes can implement this interface to tell example mobile navigation that not show this type of pages in the meny. 
	/// They should not have any footprint in menu.
	/// </summary>
	public interface INeverVisibleInMenu
	{
	}
}
