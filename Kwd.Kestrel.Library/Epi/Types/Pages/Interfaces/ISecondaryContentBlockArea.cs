﻿using EPiServer.Core;

namespace Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces
{
    public interface ISecondaryContentBlockArea
    {
        ContentArea SecondaryContentBlockArea { get; set; }
    }
}
