﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[AvailableContentTypes(Availability.None, IncludeOn = new Type[] { })]
    [ContentType(GUID = "F9CD9FBF-36C8-44F6-8DDE-7EA808C5DB79",
		DisplayName = "IframePage",
		Description = "Iframe page-")]
	public class IframePageType : ContentPageNormal
	{
		public IframePageType()
	    {
		    
	    }

		public IframePageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Iframe URL",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual string IframeURL { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Required]
		[Display(
			Name = "Iframe Height",
			GroupName = SystemTabNames.Content,
			Order = 70)]
		public virtual int IframeHeight { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Iframe replacement text",
			GroupName = SystemTabNames.Content,
			Order = 80)]
		public virtual XhtmlString IframeReplacementText { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Show Iframe in all viewports",
			Description = "If selected, the iframe replacement text will not be used and the iframe will be shown in all viewports",
			GroupName = SystemTabNames.Content,
			Order = 90)]
		public virtual bool ShowIframeInAllViewports { get; set; }
	}
}