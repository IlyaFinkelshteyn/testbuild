﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "51666C8B-BB4C-4597-9BDA-52F16A6A3FCD",
		DisplayName = "Calendar Event",
		Description = "")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class CalendarEventPageType : ContentPageWitoutSecondaryContentBlockArea
	{
		public CalendarEventPageType()
		{
		}

		public CalendarEventPageType(IEpiWrapper epiWrapper)
			: base (epiWrapper)
		{
		}

		[Display(
			Name = "Start date",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual DateTime StartDate { get; set; }

		[Display(
			Name = "End date",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		public virtual DateTime EndDate { get; set; }

		[Display(
			Name = "Location",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 22)]
		public virtual string Location { get; set; }

		[Display(
			Name = "Organizer",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 23)]
		public virtual string Organizer { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			LayoutCustomisation = "HideSecondaryContentColumn";

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}