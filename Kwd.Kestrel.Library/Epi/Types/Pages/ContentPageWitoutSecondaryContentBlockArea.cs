﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.ExtensionMethods;
using Kwd.Kestrel.Library.Models;
using EPiServer.ServiceLocation;
using EPiServer;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    public abstract class ContentPageWitoutSecondaryContentBlockArea : BasePageType
    {
        public ContentPageWitoutSecondaryContentBlockArea()
        {
        }

        public ContentPageWitoutSecondaryContentBlockArea(IEpiWrapper epiWrapper)
            : base(epiWrapper)
        {
        }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Topic section headline",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        public virtual string TopicHeadline
        {
            get
            {
                string topicHeading = this.GetPropertyValue(p => p.TopicHeadline);
                if (string.IsNullOrEmpty(topicHeading) && this.ParentLink.ID == PageReference.StartPage.ID)
                {
                    topicHeading = this.Name;
                }
                else if (string.IsNullOrEmpty(topicHeading) && this.ParentLink != null && this.ParentLink.ID != PageReference.StartPage.ID)
                {
                    var repository = ServiceLocator.Current.GetInstance<IContentRepository>();
                    var parentPage = repository.Get<PageData>(this.ParentLink);
                    while (!(parentPage is ContentPageWitoutSecondaryContentBlockArea) && parentPage.ParentLink.ID != PageReference.StartPage.ID)
                    {
                        parentPage = repository.Get<PageData>(this.ParentLink);
                    }
                    if (parentPage is ContentPageWitoutSecondaryContentBlockArea)
                    {
                        topicHeading = ((ContentPageWitoutSecondaryContentBlockArea)parentPage).TopicHeadline;
                    }
                }
                return topicHeading;
            }
            set { this.SetPropertyValue(p => p.TopicHeadline, value); }
        }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Topic section content.",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 3)]
        [UIHint(UIHint.Textarea)]
        public virtual string TopicContent
        {
            get
            {
                string topicContent = this.GetPropertyValue(p => p.TopicContent);
                if (string.IsNullOrEmpty(topicContent) && this.ParentLink != null && this.ParentLink.ID != PageReference.StartPage.ID)
                {
                    var repository = ServiceLocator.Current.GetInstance<IContentRepository>();
                    var parentPage = repository.Get<PageData>(this.ParentLink);
                    while (!(parentPage is ContentPageWitoutSecondaryContentBlockArea) && parentPage.ParentLink.ID != PageReference.StartPage.ID)
                    {
                        parentPage = repository.Get<PageData>(this.ParentLink);
                    }
                    if (parentPage is ContentPageWitoutSecondaryContentBlockArea)
                    {
                        topicContent = ((ContentPageWitoutSecondaryContentBlockArea)parentPage).TopicContent;
                    }
                }
                return topicContent;
            }
            set { this.SetPropertyValue(p => p.TopicContent, value); }
        }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Page heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Headline { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Introduction",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual XhtmlString Introduction { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Main body",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual XhtmlString MainBody { get; set; }

        public override MetaData GetPageMetaData()
        {
            var metaData = base.GetPageMetaData();

            if (string.IsNullOrWhiteSpace(PageTitle) && !string.IsNullOrWhiteSpace(Headline))
            {
                metaData.MetaTitle = Headline;
            }

            if (string.IsNullOrWhiteSpace(MetaDescription) && Introduction != null)
            {
                metaData.MetaDescription = Introduction.ToString().RemoveHtml().GetFirstTwoSentences();
            }
            else if (string.IsNullOrWhiteSpace(MetaDescription) && !string.IsNullOrWhiteSpace(Headline))
            {
                metaData.MetaDescription = Headline;
            }
            else if (string.IsNullOrWhiteSpace(MetaDescription))
            {
                metaData.MetaDescription = PageName;
            }

            return metaData;
        }

    }
}
