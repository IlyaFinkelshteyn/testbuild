﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [AvailableContentTypes(Availability.None, Include = new Type[] { typeof(InfoMapCountryPageType) })]
    [ContentType(GUID = "BDDFA25C-936B-4E92-82F8-FAE3CE82CF89",
        DisplayName = "Info map country container",
        Description = "")]
	public class InfoMapCountryContainerPageType : BasePageType
    {
		public InfoMapCountryContainerPageType()
	    {
		    
	    }

		public InfoMapCountryContainerPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

        [CultureSpecific]
        [Display(
            Name = "Map Title",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Map Subtitle",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Subtitle { get; set; }

        [Ignore]
        public IEnumerable<InfoMapCountryPageType> Countries { get; set; }
    }
}