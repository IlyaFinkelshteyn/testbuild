﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "1549ADCF-D6F1-454E-90BA-AE9D97049600",
		DisplayName = "Report list",
		Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class ReportListPageType : ContentPageNormal
	{
		public ReportListPageType()
	    {
		    
	    }

		public ReportListPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[Display(
			Name = "Include Annual reports",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual bool IncludeAR { get; set; }

		[Display(
			Name = "Include Interim reports",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual bool IncludeIR { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			//Include annual and interim by default
			IncludeAR = true;
			IncludeIR = true;
		}
	}
}