﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(DisplayName = "Financial Table", GUID = "96E4AA7C-CA1B-4317-8B8A-038A6ACD784A", Description = "-")]
	public class FinancialTablePageType : BasePageType
	{
		[CultureSpecific]
		[UIHint(UIHint.MediaFile)]
		[Display(
			Name = "Excel file",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual ContentReference ExcelFile { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Sheet name",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string SheetName { get; set; }
	}
}
