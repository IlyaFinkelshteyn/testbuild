﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [ContentType(DisplayName = "Media page", GUID = "78B330A4-9430-440C-B8FE-4067CEC16EA1", Description = "")]
	public class MediaPageType : StandardPageType, IHasIntroduction
    {
		private const string NewsGroup = "News";
        [CultureSpecific]
        [Display(
            Name = "Number of items per page",
            Description = "",
			GroupName = NewsGroup,
            Order = 15)]
        public virtual int NumerOfItemsPerPage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Feed",
            Description = "",
			GroupName = NewsGroup,
            Order = 20)]
        public virtual LinkItemCollection Feed { get; set; }
		
		//[CultureSpecific]
		//[Display(
		//	Name = "Exclude Twitter feed",
		//	Description = "",
		//	GroupName = SystemTabNames.Content,
		//	Order = 30)]
		//public virtual bool ExcludeTwitter { get; set; }
    }
}