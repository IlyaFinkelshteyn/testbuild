﻿using System;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	/// <summary>
	/// This is typed page data for the Standard page
	/// This pagetype should not be linked in menu. That is why we implement interface INotLinkedInMenu
	/// </summary>
	[ContentType(GUID = "4E37014E-647D-4577-88EA-C148CF1346C7", DisplayName = "Menu Placeholder", Description = "Menu Placeholder-")]
	[AvailableContentTypes(Availability.All, IncludeOn = new Type[] { })]

	public class MenuPlaceholderPageType : PageData, INotLinkedInMenu
	{
	}
}