﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.DataAbstraction;
using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	public abstract class PointOfInterest : BasePageType
	{
		public PointOfInterest()
	    {
		    
	    }

		public PointOfInterest(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Latitude",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual double Latitude { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Longitude",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual double Longitude { get; set; }
	}
}
