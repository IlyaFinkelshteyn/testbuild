﻿using System;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [AvailableContentTypes(Availability.None, IncludeOn = new Type[] { })]
    [ContentType(GUID = "2F2FCD0B-F509-45B2-A77B-7C6F158E1BF0",
		DisplayName = "Site Map",
		Description = "")]
	public class SiteMapPageType : BasePageType
	{
		public SiteMapPageType()
	    {
		    
	    }

		public SiteMapPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
        [Editable(true)]
        [Display(
			Name = "Alternate page heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Headline { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			LayoutCustomisation = "HideLeftArea,HideBreadcrumb,HideSecondaryContentColumn";

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}