﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
    [ContentType(GUID = "3838A698-98B1-40FA-A322-AC16D4EC4346", 
		DisplayName = "Contact Person", 
		Description = "Contact person-")]
    public class ContactPersonPageType : BasePageType, ISecondaryContentBlockArea, IHasResponsiveImage
	{
		private ResponsiveImageData _responsiveImageData;

		public ContactPersonPageType()
	    {
		    
	    }

		public ContactPersonPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[Display(
			Name = "Title",
			Description = "Title",
			GroupName = SystemTabNames.Content,
			Order = 5)]
		public virtual string Title { get; set; }

		[Display(
			Name = "First name",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string FirstName { get; set; }

		[Display(
			Name = "Last name",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string LastName { get; set; }

		[UIHint("email")]
		[Display(
			Name = "Email",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual string Email { get; set; }

		[Display(
			Name = "Phone",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual string Phone { get; set; }

		[Display(
			Name = "Mobile",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 50)]
		public virtual string Mobile { get; set; }

		[Display(
			Name = "Address field 1",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual string AddressField1 { get; set; }

		[Display(
			Name = "Address field 2",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 70)]
		public virtual string AddressField2 { get; set; }

		[Display(
			Name = "Postal code",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 80)]
		public virtual string PostalCode { get; set; }

		[Display(
			Name = "City",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 90)]
		public virtual string City { get; set; }

		[Display(
			Name = "Country",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 100)]
		public virtual string Country { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		public virtual Url Image { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Alternative Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 120)]
		public virtual string ImageAltText { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Title Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 130)]
		public virtual string ImageTitleText { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Main body",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 140)]
		public virtual XhtmlString MainBody { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Secondary content block area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 150)]
		public virtual ContentArea SecondaryContentBlockArea { get; set; }

		private string _imageType;
		public string ImageType
		{
			get
			{
				return string.IsNullOrEmpty(_imageType) ? "image-col-1" : _imageType;
			}
			set
			{
				_imageType = value;
			}
		}

		public virtual ResponsiveImageData ImageData
		{
			get
			{
				if (_responsiveImageData != null)
					return _responsiveImageData;

				if (Image == null)
					return new ResponsiveImageData();

				var rid = new ResponsiveImageData
				    {
				        Path = Image.ToString(),
				        Alt = ImageAltText,
				        Title = !string.IsNullOrEmpty(ImageTitleText) ? ImageTitleText : FirstName + " " + LastName,
				        ImageType = "image-col-1",
                        PropertyName = "Image",
				    };

			    _responsiveImageData = rid;

				return rid;
			}
		}
	}
}