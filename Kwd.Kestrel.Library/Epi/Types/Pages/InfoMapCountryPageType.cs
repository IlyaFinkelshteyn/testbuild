﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [AvailableContentTypes(Availability.None, IncludeOn = new Type[] { typeof(InfoMapCountryContainerPageType) })]
    [ContentType(GUID = "2BBCAFAB-4F17-41AB-9B35-E4F4420D8B6E",
        DisplayName = "Info Map Country",
        Description = "")]
    public class InfoMapCountryPageType : BasePageType
    {
		public InfoMapCountryPageType()
	    {
		    
	    }

		public InfoMapCountryPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }


        [CultureSpecific]
        [Display(
            Name = "Short Description",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string ShortDescription { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Blocks",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea Blocks { get; set; }

        [CultureSpecific]
        [Required]
        [Display(
            Name = "Region/Country code",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Code { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Inactive",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual bool InActive { get; set; }

    }
}