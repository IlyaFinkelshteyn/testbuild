﻿using System;
using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "9060F53B-B338-4FD8-8468-066830752BA3",
		DisplayName = "Report Year",
		Description = "-")]
    [AvailableContentTypes(IncludeOn = new Type[] { typeof(ReportListPageType) })]
	public class ReportYear : PageData
	{

	}
}