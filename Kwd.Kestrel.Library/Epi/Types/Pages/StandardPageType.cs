﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Core;
using EPiServer.SpecializedProperties;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{

    [ContentType(GUID = "20434AC9-41D2-49E7-B7BF-15F0DE9DA487",
    DisplayName = "Standard Page",
    Description = "")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
    public class StandardPageType : ContentPageWitoutSecondaryContentBlockArea
    {
        public StandardPageType()
        {

        }

        public StandardPageType(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{

		}

        [CultureSpecific]
        [Display(
            Name = "Top wide content block area",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 4)]
        public virtual ContentArea TopWideContentBlockArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Above main content block area",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 5)]
        public virtual ContentArea AboveMainContentBlockArea { get; set; }


        [CultureSpecific]
        [Display(
            Name = "Below main content block area",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 100)]
        public virtual ContentArea BelowMainContentBlockArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Bottom wide content block area",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 200)]
        public virtual ContentArea BottomWideContentBlockArea { get; set; }


        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            LayoutCustomisation = "HideSecondaryContentColumn";
        }
    }
}
