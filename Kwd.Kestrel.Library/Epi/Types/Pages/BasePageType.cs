﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	using System;

	public abstract class BasePageType : PageData
    {
		private const string NotificationGroup = "Notification";
	    private readonly IEpiWrapper _epiWrapper; 

	    public BasePageType()
	    {
			_epiWrapper = ObjectFactory.GetInstance<IEpiWrapper>();
	    }

		public BasePageType(IEpiWrapper epiWrapper)
		{
			_epiWrapper = epiWrapper;
		}

		[Display(
			Name = "Type of content",
			Description = "",
			GroupName = TabNames.SearchEngines,
			Order = 10)]
		[UIHint("DdlTypeOfContent", null)]
		public virtual string TypeOfContent { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Title tag",
			GroupName = TabNames.SearchEngines,
			Order = 20)]
		public virtual string PageTitle { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Meta Description (Max 160 characters)",
			Description = "",
			GroupName = TabNames.SearchEngines,
			Order = 30)]
		[UIHint(UIHint.Textarea)]
		public virtual string MetaDescription { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Meta Keywords",
			Description = "",
			GroupName = TabNames.SearchEngines,
			Order = 40)]
		[UIHint(UIHint.Textarea)]
		public virtual string MetaKeywords { get; set; }

		[Display(
			Name = "Robots no-index",
			GroupName = TabNames.SearchEngines,
			Order = 50)]
		public virtual bool RobotsNoIndex { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Show updated date",
			GroupName = SystemTabNames.Settings,
			Order = 60)]
		public virtual bool ShowUpdateDate { get; set; }

		[Display(
			Name = "Layout Customisation",
			Description = "",
			GroupName = SystemTabNames.Settings,
			Order = 500)]
		[SelectMany(SelectionFactoryType = typeof(LayoutCustomisationFactory))]
		public virtual string LayoutCustomisation { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Notification type",
			Description = "",
			GroupName = NotificationGroup,
			Order = 40)]
		[UIHint("DdlNotification", null)]
		public virtual string NotificationType { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Notification message",
			Description = "",
			GroupName = NotificationGroup,
			Order = 41)]
        public virtual XhtmlString NotificationMessage { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Notification start",
			Description = "",
			GroupName = NotificationGroup,
			Order = 42)]
		public virtual DateTime NotificationStart { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Notification stop",
			Description = "",
			GroupName = NotificationGroup,
			Order = 43)]
		public virtual DateTime NotificationStop { get; set; }


		public override void SetDefaultValues(ContentType contentType)
		{
            base.SetDefaultValues(contentType);

            //var startPage = EPiServer.DataFactory.Instance.Get<PageData>(ContentReference.StartPage) as StartPageType; //TODO: Need refactoring.
            //if (startPage != null && !PageReference.IsNullOrEmpty(startPage.ArchiveContainer))
            //{
            //    ArchiveLink = startPage.ArchiveContainer;
            //}
		}

		/// <summary>
		/// This method create a MetaData object and populate with default business logic all meta information.
		/// This method should be overriden in PageTypes where meta information should be gathered in any other way.
		/// </summary>
		public virtual MetaData GetPageMetaData()
		{
			var hostName = System.Web.HttpContext.Current.Request.Url.Host;

			var metaData = new MetaData
			{
				PageId = this.ContentLink.ID,
				PageName = this.PageName,

				MetaTitle = string.Format("{0} | {1}", (this.PageTitle ?? this.PageName), hostName), //TODO: Get format (pattern) from configuration that describes how this title should be formated. ex ("{0} - Sitenamn")
				MetaDescription = this.MetaDescription,
				MetaKeywords = this.MetaKeywords,
				RobotsNoIndex = this.RobotsNoIndex,

				Changed = this.Changed,
				LinkURL = this.LinkURL,
				StartPublish = this.StartPublish,
				StopPublish = this.StartPublish,
				TypeOfContent = this.TypeOfContent,
				Language = this.Language,
				PageUrl = _epiWrapper.PageUrl,

			};

			_epiWrapper.SetTargetGroups(metaData);

			return metaData;
		}
	}
}