﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	using EPiServer.Web;

	[ContentType(DisplayName = "SearchPage", GUID = "4A71E9C4-7C4E-4CDD-A5AE-BF213C0B8798", Description = "")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
    public class SearchPageType : ContentPageWitoutSecondaryContentBlockArea
	{
		public SearchPageType()
	    {
		    
	    }

		public SearchPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

        [CultureSpecific]
        [Display(
            Name = "Number of search hits per page",
            Description = "",
            GroupName = "Search",
            Order = 15)]
        public virtual int NumerOfItemsPerPage { get; set; }
		
		[CultureSpecific]
		[Display(
			Name = "No pages found message",
			Description = "",
			GroupName = "Search",
			Order = 16)]
		[UIHint(UIHint.Textarea)]
		public virtual string NoPagesFoundMessage { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Page heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        [ScaffoldColumn(false)]
        public override string Headline { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Introduction",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        [ScaffoldColumn(false)]
        public override XhtmlString Introduction { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Main body",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        [ScaffoldColumn(false)]
        public override XhtmlString MainBody { get; set; }



		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
            LayoutCustomisation = "HideLeftArea,HideBreadcrumb,HideSecondaryContentColumn";

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}