﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	[ContentType(GUID = "1A5E3141-AD82-4372-828F-AF0A3B0F8E2D",
		DisplayName = "Report",
		Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class ReportItemPageType : ContentPageWitoutSecondaryContentBlockArea
	{
		public ReportItemPageType()
	    {
		    
	    }

		public ReportItemPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Display(
			Name = "GUID",
			Description = "",
			GroupName = "Cision",
			Order = 10)]
		public virtual string GUID { get; set; }

		[Display(
			Name = "Year",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual int Year { get; set; }

		[Display(
			Name = "Quarter",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual int Quarter { get; set; }

		[BackingType(typeof(PropertyDocumentUrl))]
		[Display(
			Name = "Main Excel file",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual Url XLS { get; set; }

		[BackingType(typeof(PropertyDocumentUrl))]
		[Display(
			Name = "Main PDF file",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 50)]
		public virtual Url MainPDF { get; set; }

		[BackingType(typeof(PropertyLinkCollection))]
		[Display(
			Name = "Additional files",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual LinkItemCollection ExtraFiles { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "List Image",
			GroupName = SystemTabNames.Content,
			Order = 70)]
		public virtual Url ListImage { get; set; }

		[Display(
			Name = "Report key feature",
			GroupName = SystemTabNames.Content,
			Order = 80)]
		public virtual string ReportKeyFeature { get; set; }

		[ScaffoldColumn(false)]
		public bool HasDocuments
		{
			get
			{
				return MainPDF != null || XLS != null || ExtraFiles != null;
			}
		}

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}