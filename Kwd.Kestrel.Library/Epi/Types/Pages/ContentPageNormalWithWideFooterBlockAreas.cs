﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	public abstract class ContentPageNormalWithWideFooterBlockAreas : ContentPageNormal
	{
		public ContentPageNormalWithWideFooterBlockAreas()
		{
		}

		public ContentPageNormalWithWideFooterBlockAreas(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{
		}

		[CultureSpecific]
		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<BlockTheme>))]
		[Display(
			Name = "Full width area above footer 1 theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 100)]
		public virtual BlockTheme BottomBlocksAreaTheme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Full width area above footer 1",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 105)]
		public virtual ContentArea BottomBlocksArea { get; set; }

		[CultureSpecific]
		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<BlockTheme>))]
		[Display(
			Name = "Full width area above footer 2 theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		public virtual BlockTheme BottomBlocksArea2Theme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Full width area above footer 2",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 115)]
		public virtual ContentArea BottomBlocksArea2 { get; set; }

		[CultureSpecific]
		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<BlockTheme>))]
		[Display(
			Name = "Full width area above footer 3 theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 120)]
		public virtual BlockTheme BottomBlocksArea3Theme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Full width area above footer 3",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 125)]
		public virtual ContentArea BottomBlocksArea3 { get; set; }
	}
}
