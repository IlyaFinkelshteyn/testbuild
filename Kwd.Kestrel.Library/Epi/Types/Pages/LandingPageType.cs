﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Models;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	/// <summary>
	/// This is typed page data for the Standard page
	/// </summary>
    [AvailableContentTypes(Availability.None, IncludeOn = new Type[] { })]
    [ContentType(GUID = "DB2570E8-BDBE-4600-8969-FC00700ADFD5",
		DisplayName = "LandingPage",
		Description = "Landing page-")]
	public class LandingPageType : BasePageType
	{
		public LandingPageType()
	    {
		    
	    }

		public LandingPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Alternate page heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Wide widget area 1",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual ContentArea WideBlockArea1 { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Wide widget area 2",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual ContentArea WideBlockArea2 { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Wide widget area 3",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual ContentArea WideBlockArea3 { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Wide widget area 4",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 50)]
		public virtual ContentArea WideBlockArea4 { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Wide widget area 5",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 60)]
		public virtual ContentArea WideBlockArea5 { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			LayoutCustomisation = "HideSecondaryContentColumn";
		}

		//public override MetaData GetPageMetaData()
		//{
		//	var metaData = new MetaData
		//	{
		//		MetaTitle = this.PageTitle + "LandingPage", //TODO: Get format (pattern) from configuration that describes how this title should be formated. ex ("{0} - Sitenamn")
		//	};

		//	return metaData;
		//}

	}

	
}