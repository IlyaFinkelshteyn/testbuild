﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [ContentType(GUID = "7F71AA95-EA0D-4575-B564-79964E7B0849", 
		DisplayName = "Contact List", 
		Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class ContactListPageType : ContentPageNormal
	{
		public ContactListPageType()
	    {
		    
	    }

		public ContactListPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[Editable(true)]
		[Display(
			Name = "Hide link to contacts",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual bool HideContactLink { get; set; }

		[Editable(true)]
		[Display(
			Name = "Hide Main body of contacts",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual bool HideContactMainBody { get; set; }
	}
}