﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Types.Pages.Interfaces;
using Kwd.Kestrel.Library.Interfaces;
using EPiServer.DataAbstraction;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	/// <summary>
	/// This is typed page data for the Standard page
	/// This pagetype should not be linked in menu. That is why we implement interface INotLinkedInMenu.
	/// This pagetype should not be shown in menu. That is why we implement interface INeverVisibleInMenu.
	/// </summary>
	[ContentType(GUID = "704051F8-CB39-4528-9F8B-529C17F46170", DisplayName = "Placeholder", Description = "Placeholder-")]
	[AvailableContentTypes(EPiServer.DataAbstraction.Availability.All, IncludeOn = new Type[] { })]

	public class PlaceholderPageType : PageData, INeverVisibleInMenu, INotLinkedInMenu
	{
		/*[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Placeholder name",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string PlaceholderName { get; set; }*/

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}