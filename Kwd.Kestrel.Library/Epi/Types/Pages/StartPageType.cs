﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Kwd.Kestrel.Library.Epi.Types.Blocks;
using EPiServer;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
	/// <summary>
	/// This is typed page data for the Standard page
	/// </summary>
    [AvailableContentTypes(Availability.None, IncludeOn = new Type[] { typeof(StandardPageType) })]
	[ContentType(GUID = "FA903BBC-109E-4EA7-859F-57AFC0EAFFCA",
		DisplayName = "StartPage",
		Description = "The start page-")]
	public class StartPageType : BasePageType
	{
		public StartPageType()
		{

		}

		public StartPageType(IEpiWrapper epiWrapper)
			: base(epiWrapper)
		{

		}

		[Display(
			Name = "Site header color theme",
			Description = "",
			GroupName = TabNames.SiteConfiguration,
			Order = 10)]
		[UIHint("DdlTheme")]
        [ScaffoldColumn(false)]
		public virtual string Theme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "SiteLabel",
			Description = "",
			GroupName = TabNames.SiteConfiguration,
			Order = 20)]
		public virtual string SiteLabel { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Copyright text",
            Description = "",
            GroupName = TabNames.SiteConfiguration,
            Order = 25)]
        public virtual string CopyrightText { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Content area 1",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual ContentArea WideBlockArea1 { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Support info text",
            Description = "",
            GroupName = TabNames.ExtendedFooter,
            Order = 50)]
        public virtual XhtmlString SupportInfoText { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Contact info text",
            Description = "",
            GroupName = TabNames.ExtendedFooter,
            Order = 100)]
        public virtual XhtmlString ContactInfoText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer links",
            Description = "",
            GroupName = TabNames.ExtendedFooter,
            Order = 200)]
        public virtual LinkItemCollection FooterLinks { get; set; }
        
        [CultureSpecific]
		[Display(
			Name = "Header links",
			Description = "",
			GroupName = TabNames.SiteConfiguration,
			Order = 300)]
		public virtual LinkItemCollection HeaderLinks { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header login button url",
            Description = "",
            GroupName = TabNames.SiteConfiguration,
            Order = 310)]
        public virtual Url HeaderLinkButton { get; set; }

		[CultureSpecific]
		[Display(
			Name = "CodeAfterFooter",
			Description = "",
			GroupName = TabNames.SiteConfiguration,
			Order = 320)]
		[UIHint(UIHint.Textarea)]
		public virtual string CodeAfterFooter { get; set; }

        [CultureSpecific]
		[Display(
			Name = "Search results page",
			GroupName = TabNames.SiteConfiguration,
			Order = 340)]
		public virtual PageReference SearchPage { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Archive container",
			Description = "",
			GroupName = TabNames.SiteConfiguration,
			Order = 350)]
		public virtual PageReference ArchiveContainer { get; set; }

        [CultureSpecific]
		[Display(
			Name = "Google analytics",
			Description = "The id for the sites Google Analytics account.",
			GroupName = TabNames.SiteConfiguration,
			Order = 360)]
		public virtual string GoogleAnalyticsId { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			LayoutCustomisation = "HideLeftArea,HideBreadcrumb,HideSecondaryContentColumn";
		}
	}
}