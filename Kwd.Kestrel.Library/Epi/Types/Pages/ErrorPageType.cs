﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Pages
{
    [ContentType(DisplayName = "ErrorPage", GUID = "F4466C49-A5B3-49BB-9D03-A6DBEC4A52D8", Description = "-")]
    [AvailableContentTypes(ExcludeOn = new Type[] { typeof(StartPageType) })]
	public class ErrorPageType : BasePageType
	{
		public ErrorPageType()
	    {
		    
	    }

		public ErrorPageType(IEpiWrapper epiWrapper) 
			: base(epiWrapper)
	    {
		    
	    }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Alternate page heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Introduction",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual XhtmlString Introduction { get; set; }


		[Display(
			Name = "Status Code", 
			Description = "Example: 404", 
			GroupName = SystemTabNames.Content, 
			Order = 10)]
		public virtual int StatusCode { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			LayoutCustomisation = "HideLeftArea,HideBreadcrumb,HideSecondaryContentColumn";

			////EPiServer native properties
			VisibleInMenu = false;
		}
	}
}