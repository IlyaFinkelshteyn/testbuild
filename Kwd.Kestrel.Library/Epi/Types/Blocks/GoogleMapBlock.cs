﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Blocks.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(DisplayName = "Google Map", GUID = "28E35A4D-3822-43AD-AB29-9F7CBC3582AF", Description = "-")]
	public class GoogleMapBlock : BlockBase, IThemeable
	{
		[Display(
			Name = "Theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		[UIHint("DdlContentBlockTheme", null)]
		public virtual string Theme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Points",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 50)]
		public virtual LinkItemCollection Points { get; set; }
	}
}