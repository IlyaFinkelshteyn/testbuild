﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using System.ComponentModel.DataAnnotations;
using Kwd.Kestrel.Library.Models.ResponsiveImage;
using Kwd.Kestrel.Library.Epi.Types.Blocks.Interfaces;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(DisplayName = "Rich Content Block", GUID = "F1F37B67-0676-4122-A730-89D1D9F2F75C", Description = "Content, image, video, and link list block all in rolled into one.")]
	public class RichContentBlock : BlockBase, IHasResponsiveImage, IThemeable
	{
		#region Design
		private const string DesignGroup = "Design & layout";
		
		[Ignore]
		public virtual string Theme { get; set; }

		[Display(
			Name = "Block background",
			Description = "",
			GroupName = DesignGroup,
			Order = 15)]
		[UIHint("DdlWidgetAreaTheme", null)]
		public virtual string BlockColor { get; set; }

		[Display(
			Name = "Block alignment",
			Description = "",
			GroupName = DesignGroup,
			Order = 20)]
		[UIHint("DdlAlignment", null)]
		public virtual string BlockAlignment { get; set; }

		[Display(
			Name = "Width",
			Description = "",
			GroupName = DesignGroup,
			Order = 25)]
		public override Width Width { get; set; }

		#endregion
		
		#region Content
		
		[Display(
			Name = "Headline tag",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 18)]
		[UIHint("DdlHeadlineTag", null)]
		public virtual string HeadlineTag { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string Heading { get; set; }

		[Display(
			Name = "Heading color",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		[UIHint("DdlTextColor", null)]
		public virtual string HeadLineColorCss { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Preamble",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 25)]
		[UIHint(UIHint.Textarea)]
		public virtual string Preamble { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Text content.",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual XhtmlString Content { get; set; }

		[Ignore]
		[CultureSpecific]
		[Display(
			Name = "Make text as qoute",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 31)]
		public virtual bool BlockIsQuote { get; set; }

		#endregion

		#region Links
		//Tab name
		private const string LinkGroup = "Link";

		[Editable(true)]
		[Display(
			Name = "Show link with arrow",
			Description = "",
			GroupName = LinkGroup,
			Order = 34)]
		public virtual bool LinkIsArrow { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Block link title",
			Description = "Text on button.",
			GroupName = LinkGroup,
			Order = 35)]
		public virtual string BlockLinkTitle { get; set; }

        [CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Block link",
			Description = "",
			GroupName = LinkGroup,
			Order = 36)]
		public virtual Url BlockLink { get; set; }

		#endregion

		#region Image
		//Tab name
		private const string ImageGroup = "Image";

		[CultureSpecific]
		[Display(
			Name = "Make image round",
			Description = "",
			GroupName = ImageGroup,
			Order = 49)]
		public virtual bool ImageRound { get; set; }

		[Display(
			Name = "Round image size",
			Description = "",
			GroupName = ImageGroup,
			Order = 50)]
		[UIHint("DdlSizeModifier", null)]
		public virtual string ImageRoundSize { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image path",
			GroupName = ImageGroup,
			Order = 51)]
		public virtual Url Image { get; set; }

        [CultureSpecific]
		[Display(
		   Name = "Image Caption",
		   Description = "Shown below image.",
		   GroupName = ImageGroup,
		   Order = 55)]
		public virtual string ImageTitleText { get; set; }

        [CultureSpecific]
		[Display(
		   Name = "Image Alt Text",
		   Description = "Shown if image can't be displayed.",
		   GroupName = ImageGroup,
		   Order = 56)]
		public virtual string ImageAltText { get; set; }

		#endregion

		#region Video
		//Tab name
		private const string VideoGroup = "Video";

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Video Url",
			Description = "",
			GroupName = VideoGroup,
			Order = 80)]
		[UIHint(UIHint.Textarea)]
		public virtual string VideoURL { get; set; }

		#endregion

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			Width = Width.One;
			Theme = "basic";
			HeadlineTag = "h3";
		}

		[ScaffoldColumn(false)]
		public string ImageType
		{
			get
			{
				switch (Width)
				{
					case Width.Four:
						return "image-col-4";
					case Width.Three:
						return "image-col-3";
					case Width.One:
						return "image-col-1";
					case Width.Two:
					default:
						return "image-col-2";
				}
			}
			set
			{
				throw new global::System.NotImplementedException();
			}
		}

		public ResponsiveImageData ImageData
		{
			get
			{
				return new ResponsiveImageData()
				{
					Alt = ImageAltText,
					Title = ImageTitleText,
					Path = Image != null ? Image.ToString() : string.Empty,
					PropertyName = "Image",
					ImageType = ImageType
				};
			}
		}
	}
}
