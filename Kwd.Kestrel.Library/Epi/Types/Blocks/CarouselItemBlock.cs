﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;


namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Carousel item block", GUID = "0AD680DA-7552-40B4-B316-7E87A6432502", Description = "-")]
	public class CarouselItemBlock : BlockBase, IHasResponsiveImage
	{
		[ScaffoldColumn(false)]
		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<Width>))]
		[Display(
			Name = "Width",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual Width Width { get; set; }

		[CultureSpecific]
		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image",
			GroupName = SystemTabNames.Content,
			Order = 100)]
		public virtual Url Image { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Main image text",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		public virtual XhtmlString Content { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Alternative Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 120)]
		public virtual string ImageAltText { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Title Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 130)]
		public virtual string ImageTitleText { get; set; }

		[CultureSpecific]
		[Display(
		   Name = "Image Attribution",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 140)]
		public virtual string ImageAttribution{ get; set; }


		public ResponsiveImageData ImageData
		{
			get
			{
				return new ResponsiveImageData()
				{
					Alt = ImageAltText,
					Title = ImageTitleText,
					Path = Image != null ? Image.ToString() : string.Empty,
					ImageType = ImageType,
					Attribution = ImageAttribution,
                    PropertyName = "Image",
				};
			}
		}

		[ScaffoldColumn(false)]
		[Editable(false)]
		public string ImageType
		{
			get
			{
				switch (Width)
				{
					case Width.Four:
						return "image-col-4";
					case Width.Three:
						return "image-col-3";
					case Width.One:
						return "image-col-1";
					case Width.Two:
					default:
						return "image-col-2";
				}
			}
			set
			{
				throw new global::System.NotImplementedException();
			}
		}
	}
}