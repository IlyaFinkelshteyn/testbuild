﻿namespace Kwd.Kestrel.Library.Epi.Types.Blocks.Interfaces
{
	public interface IThemeable
	{
		string Theme { get; }
	}
}