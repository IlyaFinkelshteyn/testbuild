﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Blocks.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Pages;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Page listing Block", GUID = "6962776B-8762-4DA5-8FAB-2727327B9662", Description = "-")]
	public class PageListingBlock : BlockBase, IThemeable
	{
		// BEST PRACTICE TIP: To easily allow for page type-specific localizations, appe-nd property names with the name of the block type

		[Display(
			Name = "Theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		[UIHint("DdlContentBlockTheme", null)]
		public virtual string Theme { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual string Heading { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Number of items to show",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 50)]
		public virtual int NumerOfItemsToShow { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Fetch items from",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 100)]
		public virtual PageReference FetchItemsFrom { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Article type",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 100)]
		public virtual PageType ArticleType { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Show Read More link",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 140)]
		public virtual bool ShowReadMoreLink { get; set; }

		[Display(
			Name = "Sort order",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 220)]
		[UIHint("DdlSortOrder", null, "excludeitems", new string[] { "createdasc", "createddesc" })]
		public virtual string SortOrder { get; set; }
	}
}