﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	using EPiServer.Core;
	using EPiServer.DataAbstraction;
	using EPiServer.DataAnnotations;
	using EPiServer.SpecializedProperties;

	using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;

	using global::System.ComponentModel.DataAnnotations;

	using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

	[ContentType(DisplayName = "Link list Block", GUID = "85DEE7AB-E1C7-4774-A2B3-E8ADEB97E03D", Description = "Publish link collections as block.")]
	public class LinkListBlock : BlockBase
	{
		[Display(
			Name = "Block background",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 15)]
		[UIHint("DdlWidgetAreaTheme", null)]
		public virtual string BlockColor { get; set; }

		[Display(
			Name = "Headline tag",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 18)]
		[UIHint("DdlHeadlineTag", null)]
		public virtual string HeadlineTag { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string Heading { get; set; }

		[Display(
			Name = "Heading color",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		[UIHint("DdlTextColor", null)]
		public virtual string HeadLineColorCss { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Preamble",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 25)]
		public virtual string Preamble { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Text content.",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual XhtmlString Content { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Link collection.",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 35)]
		public virtual LinkItemCollection Links { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			Width = Width.One;
			HeadlineTag = "h3";
		}
	}
}
