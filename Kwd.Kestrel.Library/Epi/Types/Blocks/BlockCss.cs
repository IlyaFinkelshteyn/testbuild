﻿using System;
using System.Text;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    public class BlockCss
    {
        public string TypeName { get; set; }
        public string Theme { get; set; }
        public string Width { get; set; }
		public string Tag { get; set; }
		public string AdditionalClasses { get; set; }

        public string GetClassNames()
        {
			
            var resultBuilder = new StringBuilder();

	        resultBuilder.Append("block ");

            //Set Width from property
            var widthValue = Width;
            if (string.IsNullOrEmpty(Width))
            {
                widthValue = "1";
            }

			//Add tag name
			if (!string.IsNullOrEmpty(Tag))
			{
				if (Tag.Contains("width-col-"))
				{
					widthValue = Tag.Substring("width-col-".Length,1);
				}
				else
				{
					resultBuilder.Append(" " + Tag);
				}
			}

			//If the blocks template has anything to add
			if (!string.IsNullOrEmpty(AdditionalClasses))
			{
				resultBuilder.Append(" " + AdditionalClasses);
			}

            //Add width to class
            resultBuilder.Append(string.Format("col col-{0}", widthValue));

			//Add theme name
			if (!string.IsNullOrEmpty(Theme))
			{
				resultBuilder.Append(" " + Theme);
			}


            //Add block type name
            resultBuilder.Append(" " + TypeName);

            return resultBuilder.ToString();
        }
    }
}