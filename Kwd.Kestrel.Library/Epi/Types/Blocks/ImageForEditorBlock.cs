﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Editor Image Block", GUID = "544B5BF4-1742-48E5-836D-48DEA3AC20C7", Description = "To be used in editors in i.e Standard page, News page....")]
    public class ImageForEditorBlock : BlockBase, IHasResponsiveImage
    {
		[Display(
		   Name = "Width",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 20)]
		[UIHint("DdlWidth", null, "excludeitems", new [] { 4 })]
        [ScaffoldColumn(false)]
		public override Width Width { get; set; }

        [Display(
            Name = "Image alignment",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 200)]
		[UIHint("DdlAlignment", null)]
        public virtual string ImagekAlignment { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image",
			GroupName = SystemTabNames.Content,
			Order = 210)]
		public virtual Url Image { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Byline alignment",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 213)]
        [UIHint("DdlAlignment", null)]
        public virtual string BylineAlignment { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Image Byline Text",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 215)]
        public virtual string ImageBylineText { get; set; }

        [CultureSpecific]
		[Display(
		   Name = "Image Alternative Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 220)]
		public virtual string ImageAltText { get; set; }

        [CultureSpecific]
		[Display(
		   Name = "Image Title Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 230)]
		public virtual string ImageTitleText { get; set; }

		[ScaffoldColumn(false)]
		public string ImageType
		{
			get
			{
                switch (Width)
                {
                    case Width.Four:
                        return "image-col-4";
                    case Width.Three:
                        return "image-col-3";
                    case Width.One:
                        return "image-col-1";
                    case Width.Two:
                    default:
                        return "image-col-2";
                }
            }
			set
			{
				ImageType = value;
			}
		}

		public override void SetDefaultValues(ContentType contentType)
		{
			Width = Width.Two;

			base.SetDefaultValues(contentType);
		}

		public ResponsiveImageData ImageData
		{
			get 
			{
				return new ResponsiveImageData
				{
					Alt = ImageAltText,
					Path = Image != null ? Image.ToString() : string.Empty,
					ImageType = ImageType,
					Title = ImageTitleText,
                    PropertyName = "Image",
				};
			}
		}
	}
}
