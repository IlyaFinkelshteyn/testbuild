﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using EPiServer.SpecializedProperties;
using System;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(GUID = "1D5253C0-0814-406D-A862-D805962F027D",
    DisplayName = "Accordion block",
    Description = "-")]
    public class AccordionBlock : BlockBase
    {
        [CultureSpecific]
        [Display(
            Name = "Accordion items",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual LinkItemCollection AccordionItems { get; set; }

    }
}
