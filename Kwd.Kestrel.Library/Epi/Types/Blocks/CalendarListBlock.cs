﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(GUID = "3D749ED8-92F3-400E-9770-708B729F5CE5",
		DisplayName = "Calendar list block",
		Description = "-")]
	public class CalendarListBlock : BlockBase
	{
		[CultureSpecific]
		[Display(
			Name = "Headline",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }

		[Required]
		[Display(
			Name = "Fetch events from",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 15)]
		public virtual PageReference Rootpage {get; set;}


		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			Width = Width.One;
		}
	}
}