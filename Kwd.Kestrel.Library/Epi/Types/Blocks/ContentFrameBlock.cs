﻿
namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	using EPiServer;
	using EPiServer.Core;
	using EPiServer.DataAbstraction;
	using EPiServer.DataAnnotations;

	using global::System.ComponentModel.DataAnnotations;
	using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

	[AvailableContentTypesAttribute(Include = new[] { typeof(RichContentBlock), typeof(LinkListBlock) })]
	[ContentType(DisplayName = "Content frame block", GUID = "C194BF15-67DB-491E-97C9-DD9AB3BF3673", Description = "Only used as reference")]
	public class ContentFrameBlock : BlockBaseWithTheme
	{
		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		public virtual string Heading { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Main content area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 22)]
		public virtual ContentArea MainContentArea { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Block link title",
			Description = "Text on button.",
			GroupName = SystemTabNames.Content,
			Order = 25)]
		public virtual string BlockLinkTitle { get; set; }

        [CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Block link",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 26)]
		public virtual Url BlockLink { get; set; }
	}
}
