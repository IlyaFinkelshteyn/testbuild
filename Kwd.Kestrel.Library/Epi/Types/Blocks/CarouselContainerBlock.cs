﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Carousel container block", GUID = "64219F80-E707-439B-91DD-E4D40507ACE3", Description = "-")]
	public class CarouselContainerBlock : BlockBase//, IThemeable
	{
		[Display(
			Name = "Width",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public override Width Width { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Carousel item block area",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		[AllowedTypes(new[] { typeof(CarouselItemBlock) })]
		public virtual ContentArea CarouselItemBlockArea { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			Width = Width.Three;
		}
	}
}