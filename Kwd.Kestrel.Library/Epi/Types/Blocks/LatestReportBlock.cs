﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(GUID = "926CDCB6-97DD-4590-A3EB-8A58B06684D9",
		DisplayName = "Latest report",
		Description = "-")]
	public class LatestReportBlock : BlockBase
	{
		[CultureSpecific]
		[Display(
			Name = "Headline",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 10)]
		public virtual string Headline { get; set; }
	}
}