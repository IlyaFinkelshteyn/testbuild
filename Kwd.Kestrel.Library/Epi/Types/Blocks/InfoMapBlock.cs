﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using EPiServer.Core;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Info Map Block", GUID = "66E18738-EA0E-4215-AEE9-7D32180A57C1", Description = "Info map block with info on locations.")]
    public class InfoMapBlock : BlockBase
    {
        [Display(
           Name = "Info Map Title",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 10)]
        public virtual XhtmlString InfoMapTitle { get; set; }

        [Display(
           Name = "Info Map Caption",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 20)]
        public virtual XhtmlString InfoMapCaption { get; set; }

        [Display(
           Name = "Width",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 30)]
        [UIHint("DdlWidth", null, "excludeitems", new [] { Width.One, Width.Three, Width.Four })]
        public override Width Width { get; set; }

        [Display(
           Name = "Info map containers",
           Description = "",
           GroupName = SystemTabNames.Content,
           Order = 40)]
        public virtual LinkItemCollection InfoMapContainers { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            Width = Width.Two;
            base.SetDefaultValues(contentType);
        }
    }
}