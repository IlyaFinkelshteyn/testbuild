﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.Attributes;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.Interfaces;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Pages;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(DisplayName = "Latest news", GUID = "A3469065-AE52-46A2-8AE6-825831AF784E", Description = "Displays the latest news items.")]
	public class NewsListBlock : BlockBase, IThemeable
	{
		[Display(
			Name = "Theme",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 21)]
		[UIHint("DdlContentBlockTheme", null)]
		public virtual string Theme { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual string Heading { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Number of items to show",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 50)]
		public virtual int NumerOfItemsToShow { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Fetch items from",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 100)]
		public virtual PageReference FetchItemsFrom { get; set; }

		[NewsTypeSelection]
		public virtual string NewsTypes { get; set; }

		//[CultureSpecific]
		//[Display(
		//	Name = "News type",
		//	Description = "",
		//	GroupName = TabNames.BlockConfiguration,
		//	Order = 100)]
		//public virtual PageType NewsType { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Hide short introduction",
			Description = "If selected, the introduction of a newsitem will not be shown in the list",
			GroupName = TabNames.BlockConfiguration,
			Order = 180)]
		public virtual bool HideShortIntroduction { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Show images on all items",
			Description = "If selected, news item images are shown in newslist. This also shows the read more link.",
			GroupName = TabNames.BlockConfiguration,
			Order = 140)]
		public virtual bool ShowItemImages { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Show image on first item",
			Description = "If selected, item image is shown on the first item in the newslist.",
			GroupName = TabNames.BlockConfiguration,
			Order = 160)]
		public virtual bool ShowFirstItemImage { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Hide the archive link",
			Description = "",
			GroupName = TabNames.BlockConfiguration,
			Order = 180)]
		public virtual bool HideArchiveLink { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			Width = Width.Two;
			Theme = "basic";
			NumerOfItemsToShow = 3;
		}
	}
}