﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(DisplayName = "Google Graph Chart Block", GUID = "06DA51EB-1EDF-40A2-B77B-1A1153C44224", Description = "-")]
	public class GoogleChartBlock : BlockBase
	{
		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Chart Title",
			Description = "Title of graph.",
			GroupName = SystemTabNames.Content,
			Order = 5)]
		public virtual string ChartTitle { get; set; }

		[Display(
		   Name = "Chart Caption",
		   Description = "Caption below graph.",
		   GroupName = SystemTabNames.Content,
		   Order = 10)]
		public virtual XhtmlString ChartCaption { get; set; }

		[Display(
		   Name = "Width",
		   Description = "1, 2, or 3 column width.",
		   GroupName = SystemTabNames.Content,
		   Order = 15)]
		[UIHint("DdlWidth", null, "excludeitems", new [] { 4 })]
		public override Width Width { get; set; }

		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ChartType>))]
		[Display(
		   Name = "Chart Type",
		   Description = "Type of chart to display.",
		   GroupName = SystemTabNames.Content,
		   Order = 15)]
		public virtual ChartType ChartType { get; set; }

        [Display(
            Name = "Xls",
            Description = "The Xls file that contain the chart data.",
            GroupName = SystemTabNames.Content,
            Order = 210)]
        [UIHint(UIHint.MediaFile)]
        public virtual ContentReference ChartXls { get; set; }
        
        [CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Chart horizontal title",
			Description = "Horizontal unit title.",
			GroupName = SystemTabNames.Content,
			Order = 230)]
		public virtual string ChartHorizontalTitle { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Chart vertical title",
			Description = "Virtical unit title.",
			GroupName = SystemTabNames.Content,
			Order = 250)]
		public virtual string ChartVerticalTitle { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			Width = Width.One;
		}
	}
}
