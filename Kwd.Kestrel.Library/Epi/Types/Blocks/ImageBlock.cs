﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(DisplayName = "Image Block", GUID = "7EC956D0-4CEE-416A-8C61-B50FC89A684C", Description = "-")]
	public class ImageBlock : BlockBase, IHasResponsiveImage
	{
		[Display(
		   Name = "Width",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 20)]
		[UIHint("DdlWidth", null, "excludeitems", new [] { 4 })]
		public override Width Width { get; set; }

		[BackingType(typeof(PropertyUrl))]
		[Display(
			Name = "Image",
			GroupName = SystemTabNames.Content,
			Order = 210)]
		public virtual Url Image { get; set; }

		[Display(
		   Name = "Image Alternative Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 220)]
		public virtual string ImageAltText { get; set; }

		[Display(
		   Name = "Image Title Text",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 230)]
		public virtual string ImageTitleText { get; set; }

		[ScaffoldColumn(false)]
		public string ImageType
		{
			get
			{
                switch (Width)
                {
                    case Width.Four:
                        return "image-col-4";
                    case Width.Three:
                        return "image-col-3";
                    case Width.One:
                        return "image-col-1";
                    case Width.Two:
                    default:
                        return "image-col-2";
                }
            }
			set
			{
				ImageType = value;
			}
		}

		public override void SetDefaultValues(ContentType contentType)
		{
			Width = Width.Two;

			base.SetDefaultValues(contentType);
		}

		public ResponsiveImageData ImageData
		{
			get 
			{
				return new ResponsiveImageData
				{
					Alt = ImageAltText,
					Path = Image != null ? Image.ToString() : string.Empty,
					ImageType = ImageType,
					Title = ImageTitleText,
                    PropertyName = "Image",
				};
			}
		}
	}
}