﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks.System
{
	public class BlockBase : BlockData
	{
		[BackingType(typeof(PropertyNumber))]
		[EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<Width>))]
		[Display(
			Name = "Width",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual Width Width { get; set; }

		[ScaffoldColumn(false)]
		public virtual Type LocalCopyType
		{
			get { return null; }
		}
	}
}