﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using EPiServer;
using EPiServer.SpecializedProperties;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks.System
{
    public class BlockBaseWithTheme : BlockBase
    {
        private const string NormalContainerClass = "section-container";
        private const string FullwidthContainerClass = "fullwidth-container";
        private const string ContentAreaBackgroundClass = " contentareabackground";

        [CultureSpecific]
        [Display(
            Name = "Fullwidth",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 31)]
        public virtual bool BlockShowAsFullwidth { get; set; }

        [Display(
            Name = "Block background",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 32)]
        [UIHint("DdlWidgetAreaTheme", null)]
        public virtual string BlockTheme { get; set; }
		
        [BackingType(typeof(PropertyUrl))]
        [Display(
            Name = "Background image",
            GroupName = SystemTabNames.Content,
            Order = 33)]
        public virtual Url BackgroundImage { get; set; }
		
		[Display(
            Name = "Block alignment",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 33)]
		[UIHint("DdlAlignment", null)]
        public virtual string BlockAlignment { get; set; }
		
		public string BlockContainerClass
        {
            get
            {
				return string.Format("{0} {1} {2}", (BlockShowAsFullwidth ? FullwidthContainerClass : NormalContainerClass), (BackgroundImage != null ? "background-image" : BlockTheme), BlockAlignment);
            }
        }

	    public string BlockContainerStyle
	    {
		    get
		    {
			    return BackgroundImage != null ? string.Format(@"background-image: url('{0}');", BackgroundImage.Uri) : string.Empty;
		    }
	    }

        public string BlockContentClass
        {
            get
            {
				return (!BlockShowAsFullwidth && !string.IsNullOrEmpty(BlockTheme))
					? string.Format("{0}{1}{2}", BlockTheme, ContentAreaBackgroundClass, BlockAlignment) 
					: string.Empty;
            }
        }

    }
}