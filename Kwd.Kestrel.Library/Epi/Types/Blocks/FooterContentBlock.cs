﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using System.ComponentModel.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
	[ContentType(DisplayName = "Footer Block", GUID = "704FB13A-A83C-4725-9590-FA56FBD75538", Description = "Block for footer area.")]
	public class FooterContentBlock : BlockBase
	{
		[Display(
		   Name = "Width",
		   Description = "",
		   GroupName = SystemTabNames.Content,
		   Order = 10)]
		public override Width Width { get; set; }
		
		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Heading",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 20)]
		public virtual string Heading { get; set; }

		[CultureSpecific]
		[Editable(true)]
		[Display(
			Name = "Content",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 30)]
		public virtual XhtmlString Content { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Links",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 40)]
		public virtual LinkItemCollection Links { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);
			Width = Width.One;
		}
	}
}
