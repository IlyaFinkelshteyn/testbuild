﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Interfaces;
using Kwd.Kestrel.Library.Models.ResponsiveImage;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{   
    [ContentType(DisplayName = "Elevator Pitch Block", GUID = "E3CE4DF6-D72A-4AC1-A308-566EDBD909C2", Description = "Elevator Pitch Block")]
	public class ElevatorPitchBlock : BlockBase
	{
        [Display(
       Name = "Title",
       Description = "",
       GroupName = SystemTabNames.Content,
       Order = 100)]
        public virtual string Title { get; set; }

        [Display(
        Name = "Text",
        Description = "",
        GroupName = SystemTabNames.Content,
        Order = 150)]
        public virtual XhtmlString Text { get; set; }

        [Display(
        Name = "Link Title",
        Description = "Title of the link",
        GroupName = SystemTabNames.Content,
        Order = 160)]
        public virtual string LinkTitle { get; set; }

        [Display(
        Name = "Link URL",
        Description = "Destination of the link",
        GroupName = SystemTabNames.Content,
        Order = 180)]
        public virtual Url LinkURL { get; set; }

        [UIHint(UIHint.Image)]
        [Display(
        Name = "Image",
        GroupName = SystemTabNames.Content,
        Order = 200)]
        public virtual ContentReference Image { get; set; }

	}
}