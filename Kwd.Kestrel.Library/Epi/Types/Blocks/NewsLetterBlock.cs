﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using EPiServer.SpecializedProperties;
using System;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties;
using EPiServer.Web;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(GUID = "F540B98D-63FC-4C19-991E-952983A1710C",
    DisplayName = "Newsletter block",
    Description = "-")]
    public class NewsLetterBlock : BlockBase
    {
        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Heading",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Text content.",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        [UIHint(UIHint.Textarea)]
        public virtual string Content { get; set; }
        
        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Mailing list ID",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string MailingListId { get; set; }


    }
}
