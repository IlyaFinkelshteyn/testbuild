﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;
using Kwd.Kestrel.Library.Epi.Types.Blocks.System;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Microsoft.IdentityModel.Protocols.WSFederation.Metadata;

namespace Kwd.Kestrel.Library.Epi.Types.Blocks
{
    [ContentType(GUID = "F5D418D6-1017-4EFC-AAD7-AEC7949AEEEB", 
		DisplayName = "Contact list block", 
		Description = "-")]
	public class ContactListBlock : BlockBase
	{
		[CultureSpecific]
		[Display(
			Name = "Headline",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 100)]
		public virtual string Headline { get; set; }

		[CultureSpecific]
		[Display(
			Name = "Contacts",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 110)]
		////AllowedTypes only works for ContentReference for now	////[AllowedTypes(new[] { typeof(ContactPersonPageType) })]
		public virtual LinkItemCollection Contacts { get; set; }


		[CultureSpecific]
		[Display(
			Name = "Show images",
			Description = "",
			GroupName = SystemTabNames.Content,
			Order = 130)]
		public virtual bool ShowImages { get; set; }

		public override void SetDefaultValues(ContentType contentType)
		{
			base.SetDefaultValues(contentType);

			Width = Width.One;
		}
	}
}