﻿using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Files
{
    [ContentType(GUID = "c543a274-892c-4847-9ac7-9b8c9a6dfaa8")]
    public class GenericMedia : MediaData
    {
        public virtual string Description { get; set; }
    }
}
