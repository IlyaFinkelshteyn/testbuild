﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Files
{
    [ContentType(GUID = "5c9b947a-a424-48ff-967a-a6ba5200693b")]
    [MediaDescriptor(ExtensionString = "jpg,jpeg,jpe,ico,gif,bmp,png")]
    public class ImageFile : ImageData
    {
        public virtual string Description { get; set; }
        public virtual string Copyright { get; set; }
    }
}
