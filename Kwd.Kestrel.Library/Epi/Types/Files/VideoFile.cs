﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.Types.Files
{
    [ContentType(GUID = "a051ad04-643b-4e82-849b-b8d5cf8bfb05")]
    [MediaDescriptor(ExtensionString = "flv,mp4,webm")]
    public class VideoFile : VideoData
    {
        public virtual string Description { get; set; }
    }
}
