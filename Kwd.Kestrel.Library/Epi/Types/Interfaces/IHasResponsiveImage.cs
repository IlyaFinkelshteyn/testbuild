﻿using EPiServer;
using Kwd.Kestrel.Library.Models.ResponsiveImage;

namespace Kwd.Kestrel.Library.Epi.Types.Interfaces
{
	public interface IHasResponsiveImage
	{
		ResponsiveImageData ImageData { get; }
		Url Image { get; set; }
		string ImageType { get; set; }
		string ImageAltText { get; set; }
		string ImageTitleText { get; set; }
	}
}
