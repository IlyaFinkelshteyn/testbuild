﻿using EPiServer.Core;
using EPiServer.DynamicContent;
using EPiServer.SpecializedProperties;
using Kwd.Kestrel.Library.Epi.CustomProperties.ButtonThemeProperty;

namespace Kwd.Kestrel.Library.Epi.Types.DynamicContent
{
	[DynamicContentPlugIn(DisplayName = "Button",
		Description = "Displays a button",
		ViewUrl = "~/Units/DynamicContent/Button.ascx")]
	public partial class DynamicContentButton : EPiServer.UserControlBase
	{
		public string ButtonText { get; set; }
		public PropertyUrl ButtonUrl { get; set; }
		public bool OpenLinkInNewWindow { get; set; }
		public PropertyLongString TextBeforeButton { get; set; }
		public PropertyButtonThemeDropDown ButtonTheme { get; set; }
	}
}