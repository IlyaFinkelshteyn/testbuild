﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;

namespace Kwd.Kestrel.Library.Epi.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class NewsTypeSelectionAttribute : SelectManyAttribute
	{
		public override Type SelectionFactoryType
		{
			get { return typeof (NewsTypeSelectionFactory); }
			set { base.SelectionFactoryType = value; }
		}
	}
}

