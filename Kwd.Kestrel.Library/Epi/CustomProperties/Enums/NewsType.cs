﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using Kwd.Kestrel.Library.Epi.Types.Pages;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.Enums
{
	public enum NewsType
	{
		NewsItem,
		PressRelease,
		Story,
		FinancialReport
	}
}
