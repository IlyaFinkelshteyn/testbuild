﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.Enums
{
	public enum ChartType
	{
		ColumnChart = 0,
		PieChart = 1,
		LineChart = 2,
		ComboChart = 3
	}
}
