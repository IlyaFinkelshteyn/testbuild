﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.Enums
{
	public enum BlockTheme
	{
		Basic = 0,
		Dark = 1,
		Gray = 2,
		Black = 3,
		DarkBlue = 4
	}
}
