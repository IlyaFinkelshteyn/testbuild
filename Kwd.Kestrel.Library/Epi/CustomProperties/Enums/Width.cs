﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.Enums
{
	public enum Width
	{
        NoGrid = 0,
		One = 1,
		Two = 2,
		Three = 3,
		Four = 4
	}
}
