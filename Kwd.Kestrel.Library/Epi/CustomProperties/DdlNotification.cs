﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties
{
	using EPiServer.Shell.ObjectEditing;
	using EPiServer.Shell.ObjectEditing.EditorDescriptors;
	using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;
	
	[EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "DdlNotification")]
	public class DdlNotification : EditorDescriptor
	{
		public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
		{
			SelectionFactoryType = typeof(NotificationTypeSelectionFactory);
			ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";
			base.ModifyMetadata(metadata, attributes);
		}
	}
}