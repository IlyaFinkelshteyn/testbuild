﻿using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class TypeOfContentSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = "Product information", Text = "Product information" });
			items.Add(new SelectItem() { Value = "Product area", Text = "Product area" });
			items.Add(new SelectItem() { Value = "Information page", Text = "Information page" });
			items.Add(new SelectItem() { Value = "News", Text = "News" });
			items.Add(new SelectItem() { Value = "Calendar", Text = "Calendar" });
			items.Add(new SelectItem() { Value = "Press release", Text = "Press release" });
			items.Add(new SelectItem() { Value = "Reports", Text = "Reports" });
			items.Add(new SelectItem() { Value = "Tools", Text = "Tools" });
			items.Add(new SelectItem() { Value = "Locations", Text = "Locations" });
			items.Add(new SelectItem() { Value = "Teaser", Text = "Teaser" });
            items.Add(new SelectItem() { Value = "Contact", Text = "Contact" });
			items.Add(new SelectItem() { Value = "Startpage", Text = "Startpage" });
			return items;
		}
	}
}