﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	using EPiServer.Shell.ObjectEditing;

	public class SizeModifierSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = string.Empty, Text = "default size" });
			items.Add(new SelectItem() { Value = "small", Text = "small size" });
			items.Add(new SelectItem() { Value = "medium", Text = "medium size" });
			items.Add(new SelectItem() { Value = "large", Text = "large size" });

			return items;
		}
	}
}