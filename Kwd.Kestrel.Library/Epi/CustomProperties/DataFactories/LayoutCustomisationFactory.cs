﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Models;
using System.ComponentModel.DataAnnotations;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class LayoutCustomisationFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
	
			Type layoutCustomisationType = typeof(LayoutCustomisation);
			var layoutCustomisationProperties = layoutCustomisationType.GetProperties();

			foreach (var property in layoutCustomisationProperties)
			{
				var displayAttributeName = property.Name;
				var displayAttributes = property.GetCustomAttributes(typeof(DisplayAttribute), true);

				if (displayAttributes != null && displayAttributes.Length == 1)
				{
					displayAttributeName = ((DisplayAttribute)displayAttributes[0]).Name;
				}

				items.Add(new SelectItem() { Value = property.Name, Text = displayAttributeName });
			}

			return items;
		}
	}
}