﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class HeadlineTagSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = "h2", Text = "H2" });
			items.Add(new SelectItem() { Value = "h3", Text = "H3" });
			items.Add(new SelectItem() { Value = "h4", Text = "H4" });
			items.Add(new SelectItem() { Value = "h5", Text = "H5" });
			return items;
		}
	}
}