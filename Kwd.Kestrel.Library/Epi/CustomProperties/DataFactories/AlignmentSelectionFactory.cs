﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	using EPiServer.Shell.ObjectEditing;

	public class AlignmentSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = string.Empty, Text = "No alignment" });
			items.Add(new SelectItem() { Value = "left", Text = "Left aligned" });
			items.Add(new SelectItem() { Value = "right", Text = "Right aligned" });
			items.Add(new SelectItem() { Value = "center", Text = "Center aligned" });
			
			return items;
		}
	}
}