﻿namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	using System.Collections.Generic;
	using EPiServer.Shell.ObjectEditing;

	public class NotificationTypeSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = string.Empty, Text = "No type" });
			items.Add(new SelectItem() { Value = "infobox-message", Text = "Message type" });
            items.Add(new SelectItem() { Value = "infobox-notification", Text = "Notification type" });
            items.Add(new SelectItem() { Value = "infobox-alert", Text = "Alert type" });
			return items;
		}
	}
}