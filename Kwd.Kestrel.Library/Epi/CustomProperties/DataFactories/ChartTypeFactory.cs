﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;
using System;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class ChartTypeSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var uiHintAttribute = metadata.Attributes.OfType<UIHintAttribute>().SingleOrDefault();

			string[] excludeList;

			try
			{
				excludeList = uiHintAttribute.ControlParameters.Count > 0 && uiHintAttribute.ControlParameters["excludeitems"] != null ? (string[])uiHintAttribute.ControlParameters["excludeitems"] : null;
			}
			catch
			{
				excludeList = null;
			}

			var types = new Dictionary<string, string> { 
                            { "columnchart", "Column Chart" }, 
                            { "piechart", "Pie Chart" }, 
                            { "linechart", "Line Chart" },
                            { "combochart", "Combo Chart" } 
                        };

			var items = new List<SelectItem>();

			object typeOfChartsObject;

			uiHintAttribute.ControlParameters.TryGetValue("numberofcolums", out typeOfChartsObject);

			//this isn't right
			int numberOftypes = typeOfChartsObject != null ? Convert.ToInt32(typeOfChartsObject) : 0;

			foreach (var type in types)
			{
				if (excludeList == null || !excludeList.Contains(type.Key))
				{
					items.Add(new SelectItem() { Value = type.Key, Text = type.Value });
				}
			}

			if (numberOftypes > 0)
			{
				return items.Take(numberOftypes);
			}
			else
			{
				return items;
			}
		}
	}
}
