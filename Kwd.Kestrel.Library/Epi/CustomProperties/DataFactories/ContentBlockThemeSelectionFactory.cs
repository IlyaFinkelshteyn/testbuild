﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class ContentBlockThemeSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var uiHintAttribute = metadata.Attributes.OfType<UIHintAttribute>().SingleOrDefault();
			string[] excludeList;

			try
			{
				excludeList = uiHintAttribute.ControlParameters.Count > 0 && uiHintAttribute.ControlParameters["excludeitems"] != null ? (string[])uiHintAttribute.ControlParameters["excludeitems"] : null;
			}
			catch
			{
				excludeList = null;
			}

			var themes = new Dictionary<string, string> {
			    {
			        "basic", 
                    "Basic"
			    }
            };

			var items = new List<SelectItem>();
	
			foreach (var theme in themes)
			{
				if (excludeList == null || !excludeList.Contains(theme.Key))
				{
					items.Add(new SelectItem() { Value = theme.Key, Text = theme.Value });
				}
			}

			return items;
		}
	}
}