﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.Framework.Localization;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class EnumSelectionFactory<TEnum> : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var uiHintAttribute = metadata.Attributes.OfType<UIHintAttribute>().FirstOrDefault();
			TEnum[] excludeList;

			try
			{
				excludeList = uiHintAttribute.ControlParameters.Count > 0 && uiHintAttribute.ControlParameters["excludeitems"] != null ? (TEnum[])uiHintAttribute.ControlParameters["excludeitems"] : null;
			}
			catch
			{
				excludeList = null;
			}

			var values = Enum.GetValues(typeof(TEnum));
			foreach (var value in values)
			{
				if (excludeList != null && excludeList.Contains((TEnum) value))
					continue;

				yield return new SelectItem
				{
					Text = GetValueName(value),
					Value = value
				};
			}
		}

		private string GetValueName(object value)
		{
			var staticName = Enum.GetName(typeof(TEnum), value);

			var localizationPath = string.Format(
				"/property/enum/{0}/{1}",
				typeof(TEnum).Name.ToLowerInvariant(),
				staticName.ToLowerInvariant());

			string localizedName;
			return LocalizationService.Current.TryGetString(localizationPath, out localizedName) ? localizedName : staticName;
		}
	}
}
