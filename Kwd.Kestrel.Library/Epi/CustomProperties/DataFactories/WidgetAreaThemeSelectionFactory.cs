﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class WidgetAreaThemeSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
            items.Add(new SelectItem() { Value = string.Empty, Text = "Transparant background" });
			items.Add(new SelectItem() { Value = "gray", Text = "Gray background" });
			items.Add(new SelectItem() { Value = "blue", Text = "Blue background" });
			items.Add(new SelectItem() { Value = "green", Text = "Green background" });
			items.Add(new SelectItem() { Value = "white", Text = "White background" });

			return items;
		}
	}
}