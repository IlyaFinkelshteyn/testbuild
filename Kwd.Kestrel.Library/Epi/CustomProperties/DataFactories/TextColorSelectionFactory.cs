﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	using EPiServer.Shell.ObjectEditing;

	class TextColorSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			var items = new List<SelectItem>();
			items.Add(new SelectItem() { Value = string.Empty, Text = "No color" });
			items.Add(new SelectItem() { Value = "green", Text = "Green color" });
			items.Add(new SelectItem() { Value = "blue", Text = "Blue color" });
			
			return items;
		}
	}
}