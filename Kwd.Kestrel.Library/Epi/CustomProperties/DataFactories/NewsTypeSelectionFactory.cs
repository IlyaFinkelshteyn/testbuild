﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Kwd.Kestrel.Library.Epi.CustomProperties.Enums;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class NewsTypeSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{
			foreach (var value in Enum.GetValues(typeof(NewsType)))
			{
				yield return new SelectItem()
				{
					Text = Enum.GetName(typeof (NewsType), value),
					Value = ((int) value).ToString()
				};
			}
		}
	}
}
