﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
    public class ImagePlacementSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {

            var uiHintAttribute = metadata.Attributes.OfType<UIHintAttribute>().SingleOrDefault();
			var excludeList = uiHintAttribute.ControlParameters.Count > 0 && uiHintAttribute.ControlParameters["excludeitems"] != null ? (string[])uiHintAttribute.ControlParameters["excludeitems"] : null;
            
			var imageTypes = new Dictionary<string, string> { 
                { "leftimage", "Left aligned image" }, 
                { "topimage", "Top aligned image" }, 
                { "rightimage", "Right aligned image" } 
            };

            var items = new List<SelectItem>();
            foreach (var imageType in imageTypes)
            {
                if (excludeList == null || !excludeList.Contains(imageType.Key))
                {
                    items.Add(new SelectItem() { Value = imageType.Key, Text = imageType.Value });
                }
            }
            return items;
        }
    }
}