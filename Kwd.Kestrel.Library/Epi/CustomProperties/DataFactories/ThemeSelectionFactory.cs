﻿using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class ThemeSelectionFactory : ISelectionFactory
	{
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var items = new List<SelectItem>();
            items.Add(new SelectItem() { Value = "normal", Text = "Normal" });
			return items;
        }
	}
}