﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories
{
	public class SortOrderSelectionFactory : ISelectionFactory
	{
		public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
		{

			var uiHintAttribute = metadata.Attributes.OfType<UIHintAttribute>().SingleOrDefault();
			string[] excludeList = uiHintAttribute.ControlParameters.Count > 0 && uiHintAttribute.ControlParameters["excludeitems"] != null ? (string[])uiHintAttribute.ControlParameters["excludeitems"] : null;

			var sortOrderTypes = new Dictionary<string, string> { 
                { "alphabetical", "Alphabetical order" }, 
                { "createdasc", "Created date ascending" }, 
                { "createddesc", "Created date descending" },
				{ "changeddesc", "Changed date descending" }
            };

			var items = new List<SelectItem>();
			foreach (var sortOrder in sortOrderTypes)
			{
				if (excludeList == null || !excludeList.Contains(sortOrder.Key))
				{
					items.Add(new SelectItem() { Value = sortOrder.Key, Text = sortOrder.Value });
				}
			}
			return items;
		}
	}
}