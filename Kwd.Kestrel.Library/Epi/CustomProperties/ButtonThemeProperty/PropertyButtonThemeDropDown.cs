﻿using EPiServer.Core;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.ButtonThemeProperty
{
	public class PropertyButtonThemeDropDown : PropertyString
	{
		public override IPropertyControl CreatePropertyControl()
		{
			return new PropertyButtonThemeDropDownControl();
		}
	}	
}