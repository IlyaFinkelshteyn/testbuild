﻿using System;
using System.Collections.Generic;
using EPiServer.Web.PropertyControls;

namespace Kwd.Kestrel.Library.Epi.CustomProperties.ButtonThemeProperty
{
	public class PropertyButtonThemeDropDownControl : PropertySelectControlBase
	{
		protected override void SetupEditControls()
		{
			this.SetupDropDownList();
		}

		private void SetupDropDownList()
		{
			this.EditControl.DataSource = GetValues;
			this.EditControl.DataTextField = "Item1";
			this.EditControl.DataValueField = "Item2";

			if (this.ButtonThemeDropDown.Value != null)
			{
				this.EditControl.SelectedValue = this.ButtonThemeDropDown.Value as string;
			}

			this.EditControl.DataBind();
		}

		private static List<Tuple<string, string>> GetValues
		{
			get
			{
				var themes = new List<Tuple<string, string>>();
				themes.Add(new Tuple<string, string>("normal", "normal"));

				return themes;
			}
		}

		public PropertyButtonThemeDropDown ButtonThemeDropDown
		{
			get
			{
				return PropertyData as PropertyButtonThemeDropDown;
			}
		}
	}
}