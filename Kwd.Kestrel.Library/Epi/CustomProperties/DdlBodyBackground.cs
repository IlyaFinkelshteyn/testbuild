﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;

namespace Kwd.Kestrel.Library.Epi.CustomProperties
{
	[EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "DdlTheme")]
	public class ThemeEditorDescriptor : EditorDescriptor
	{
		public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
		{
			SelectionFactoryType = typeof(ThemeSelectionFactory);
			ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";
			base.ModifyMetadata(metadata, attributes);
		}
	}
}