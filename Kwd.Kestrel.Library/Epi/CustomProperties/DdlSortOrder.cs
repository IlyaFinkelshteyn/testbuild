﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;

namespace Kwd.Kestrel.Library.Epi.CustomProperties
{
	[EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "DdlSortOrder")]
	public class DdlSortOrder : EditorDescriptor
	{
		public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
		{
			SelectionFactoryType = typeof(SortOrderSelectionFactory);
			ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";
			base.ModifyMetadata(metadata, attributes);
		}
	}
}