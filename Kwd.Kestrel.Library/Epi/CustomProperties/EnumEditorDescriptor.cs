﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;

namespace Kwd.Kestrel.Library.Epi.CustomProperties
{
	public class EnumEditorDescriptor<TEnum> : EditorDescriptor
	{
		public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
		{
			SelectionFactoryType = typeof(EnumSelectionFactory<TEnum>);

			ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";

			base.ModifyMetadata(metadata, attributes);
		}
	}
}
