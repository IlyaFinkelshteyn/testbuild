﻿using System;
using System.Collections.Generic;

namespace Kwd.Kestrel.Library.Epi.CustomProperties
{
	using EPiServer.Shell.ObjectEditing;
	using EPiServer.Shell.ObjectEditing.EditorDescriptors;
	using Kwd.Kestrel.Library.Epi.CustomProperties.DataFactories;
	
	[EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "DdlSizeModifier")]
	public class DdlSizeModifier : EditorDescriptor
	{
		public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
		{
			SelectionFactoryType = typeof(SizeModifierSelectionFactory);
			ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";
			base.ModifyMetadata(metadata, attributes);
		}
	}
}