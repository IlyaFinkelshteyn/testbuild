﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Hosting;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Editor;
using EPiServer.Filters;
using EPiServer.Framework.Cache;
using EPiServer.Framework.Localization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using Kwd.Kestrel.Library.Epi.Types.Pages;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models;
using StructureMap;

namespace Kwd.Kestrel.Library.Epi
{
	public interface IEpiWrapper
	{
		/// <summary>
		/// Tells if the context is in Epi Server editmode.
		/// </summary>
		bool InEditMode { get; }

		/// <summary>
		/// Returns the StartPage.
		/// </summary>
		/// <returns></returns>
		StartPageType GetStartPage();

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot) where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, PropertyCriteriaCollection criterias) where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, string language) where T : BasePageType;

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, PropertyCriteriaCollection criterias, string language) where T : BasePageType;

		///// <summary>
		///// Will find all public pages of a specific pagetype that are childs or grandchild to the specified root.
		///// Note: Will only show public pages that can be accessed by visitors.
		///// </summary>
		//PageDataCollection FindPublicPagesWithPageType<T>(PageReference searchRoot, PropertyCriteriaCollection criterias) where T : BasePageType;

		PageDataCollection GetChildren(ContentReference root);

		/// <summary>
		/// Get a enumerable list of children that are of a specified type.
		/// </summary>
		IEnumerable<T> GetChildren<T>(ContentReference root) where T : BasePageType;

		IEnumerable<PageData> GetDescendants(ContentReference root, string language);

		/// <summary>
		/// Get the current culture translation for the specified resourcekey
		/// </summary>
		string Translate(string resourceKey);

		/// <summary>
		/// Get the translation for the specified resourcekey in the specified culture
		/// </summary>
		string Translate(string resourceKey, string culture);

		/// <summary>
		/// Get/Load a PageData that represent the specified page id.
		/// </summary>
		PageData GetPage(int pageId);

		/// <summary>
		/// Get/Load a PageData that represent the specified page reference.
		/// </summary>
		PageData GetPage(PageReference pageReference);

		/// <summary>
		/// Load the filepath as VirtualFile
		/// </summary>
		VirtualFile GetFile(string path);

		PageType GetPageType(Type type);

		PageType GetPageType(int pageTypeId);

		PageData CurrentPage { get; }

		//BasePageData BasePageData { get; }

		/// <summary>
		/// Return the currentpage URL.
		/// </summary>
		string PageUrl { get; }

		/// <summary>
		/// Return a CacheEvictionPolicy tied to the DatabaseVersion. So that it will release/timeout when database version is updated.
		/// </summary>
		/// <returns></returns>
		CacheEvictionPolicy GetDatabaseVersionCacheEvictionPolicy();

		/// <summary>
		/// Populate page target group information into page meta data.
		/// </summary>
		/// <param name="pageMetaData"></param>
		void SetTargetGroups(MetaData pageMetaData);

		string SiteId { get; }

		string SiteUrl { get; }

		string GetPropertyDisplayName(string propertyName);

		string GetPropertyDisplayName(string propertyName, BlockData currentBlockData);

		string GetFriendlyURL(PageData page, bool includeSiteUrl);
	}

	public class EpiWrapper : IEpiWrapper
	{
		private readonly ILogger _logger;
		private readonly PageTypeRepository _pageTypeRepository;
		private readonly IContentRepository _contentRepository;
		private readonly LocalizationService _localizationService;
		private static object _myLock = new object();
		public readonly ICache _cache;

		[StructureMap.DefaultConstructor]
		public EpiWrapper()
		{
			_logger = ObjectFactory.GetInstance<ILogger>();
			_pageTypeRepository = ServiceLocator.Current.GetInstance<PageTypeRepository>();
			_contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
			_localizationService = LocalizationService.Current;
			_cache = ObjectFactory.GetInstance<ICache>();
		}

		public EpiWrapper(ILogger logger, ICache cache, IContentRepository contentRepository, PageTypeRepository pageTypeRepository, LocalizationService localizationService)
		{
			_logger = logger;
			_cache = cache;
			_pageTypeRepository = pageTypeRepository;
			_contentRepository = contentRepository;
			_localizationService = localizationService;
		}

		public bool InEditMode
		{
			get { return PageEditing.PageIsInEditMode; }
		}

		public StartPageType GetStartPage()
		{
			return _contentRepository.Get<StartPageType>(ContentReference.StartPage);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		public PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot) where T : BasePageType
		{
			return FindPagesWithPageType<T>(searchRoot, null, string.Empty);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		public PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, PropertyCriteriaCollection criterias) where T : BasePageType
		{
			return FindPagesWithPageType<T>(searchRoot, criterias, string.Empty);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		public PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, string language) where T : BasePageType
		{
			return FindPagesWithPageType<T>(searchRoot, null, language);
		}

		/// <summary>
		/// Will find all pages of a specific pagetype that are childs or grandchild to the specified root.
		/// Note: Will only show pages that can be accessed by visitors.
		/// </summary>
		public PageDataCollection FindPagesWithPageType<T>(PageReference searchRoot, PropertyCriteriaCollection criterias, string language) where T : BasePageType
		{
			if (PageReference.IsNullOrEmpty(searchRoot))
			{
				// We don´t have a serachRoot. We will return null.
				return null;
			}

			// Load the pagetype for the specified PageType.
			var pageTypeObject = LoadPageType<T>();

			// Get the ID of pagetype.
			var pageTypeId = pageTypeObject.ID;


			if (criterias == null)
			{
				criterias = new PropertyCriteriaCollection();
			}
			var criteria = new PropertyCriteria
			{
				Name = "PageTypeID",
				Required = true,
				Type = PropertyDataType.PageType,
				Condition = CompareCondition.Equal,
				Value = pageTypeId.ToString(CultureInfo.InvariantCulture)
			};
			criterias.Add(criteria);

			var languageSelector = string.IsNullOrEmpty(language) ? LanguageSelector.MasterLanguage() : new LanguageSelector(language);
			
			
			var pages = DataFactory.Instance.FindPagesWithCriteria(searchRoot, criterias, null, languageSelector);

			FilterForVisitor.Filter(pages);

			return pages;
		}

		public PageDataCollection GetChildren(ContentReference root)
		{
			var pageDataCollection = _contentRepository.GetChildren<PageData>(root) as PageDataCollection;

			return pageDataCollection;
		}

		/// <summary>
		/// Get a enumerable list of children that are of a specified type.
		/// </summary>
		public IEnumerable<T> GetChildren<T>(ContentReference root) where T : BasePageType
		{
			return _contentRepository.GetChildren<T>(root);
		}

		public IEnumerable<PageData> GetDescendants(ContentReference root, string language)
		{
			return _contentRepository.GetDescendents(root).Select(r => _contentRepository.Get<PageData>(r.ToPageReference(), new LanguageSelector(language)));
		}

		/// <summary>
		/// Get the current culture translation for the specified resourcekey
		/// </summary>
		public string Translate(string resourceKey)
		{
			return _localizationService.GetString(resourceKey);
		}

		/// <summary>
		/// Get the translation for the specified resourcekey in the specified culture
		/// </summary>
		public string Translate(string resourceKey, string language)
		{
			var cultureInfo = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
			return _localizationService.GetStringByCulture(resourceKey, cultureInfo);
		}

		/// <summary>
		/// Get/Load a PageData that represent the specified page id.
		/// </summary>
		public PageData GetPage(int pageId)
		{
			return GetPage(new PageReference(pageId));
		}

		/// <summary>
		/// Get/Load a PageData that represent the specified page reference.
		/// </summary>
		public PageData GetPage(PageReference pageReference)
		{
			return DataFactory.Instance.GetPage(pageReference);
		}

		public VirtualFile GetFile(string path)
		{
			VirtualFile file = null;

			try
			{
				var decodedPath = HttpUtility.UrlDecode(path);
				var virtualFile = HostingEnvironment.VirtualPathProvider.GetFile(decodedPath);
				if (virtualFile != null && virtualFile.Name != null)
				{
					file = virtualFile;
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, string.Format("Could not load file {0}", path));
			}

			return file;
		}

		public PageType GetPageType(Type type)
		{
			return _pageTypeRepository.Load(type);
		}

		public PageType GetPageType(int pageTypeId)
		{
			return _pageTypeRepository.Load(pageTypeId);
		}

		public PageData CurrentPage
		{
			get
			{
				var pageBase = HttpContext.Current.Handler as PageBase;

				if (pageBase != null)
				{
					return pageBase.CurrentPage;
				}
				else
				{
					return null;
				}
			}
		}

		//public BasePageData BasePageData
		//{
		//	get
		//	{
		//		var page = (BasePageType)CurrentPage;

		//		var pageMetaData = page.GetPageMetaData();

		//		var basePageData = new BasePageData
		//		{
		//			PageId = pageMetaData.PageId,
		//			MetaDescription = pageMetaData.MetaDescription,
		//			Changed = page.Changed,
		//			LinkURL = page.LinkURL,
		//			MetaKeywords = pageMetaData.MetaKeywords,
		//			PageName = page.PageName,
		//			PageTitle = pageMetaData.MetaTitle, //page.PageTitle ?? page.PageName,
		//			RobotsNoIndex = page.RobotsNoIndex,
		//			StartPublish = page.StartPublish,
		//			StopPublish = page.StartPublish,
		//			TypeOfContent = page.TypeOfContent,
		//			Language = page.Language,
		//			PageUrl = PageUrl,
		//		};
		//		// Populate the target groups.
		//		SetTargetGroups(basePageData);

		//		return basePageData;
		//	}
		//}

		public string PageUrl
		{
			get
			{
				var url = new UrlBuilder(CurrentPage.LinkURL);
				Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink, UTF8Encoding.UTF8);
				var pageUrl = "http://" + HttpContext.Current.Request.Url.Authority + url;

				return pageUrl;
			}
		}

		public CacheEvictionPolicy GetDatabaseVersionCacheEvictionPolicy()
		{
			return new CacheEvictionPolicy(new[] { DataFactoryCache.VersionKey });
		}

		public void SetTargetGroups(MetaData pageMetaData)
		{
			var categoriesCacheKey = string.Format("AllCategories");
			var allCategories = _cache.Get(categoriesCacheKey) as List<Target>;
			if (allCategories == null)
			{
				var rootCategory = Category.GetRoot();
				allCategories = new List<Target>();
				allCategories = CreateFlatCategoryList(allCategories, rootCategory.Categories);

				if (allCategories != null)
				{
					lock (_myLock)
					{
						_cache.Insert(categoriesCacheKey, allCategories, GetDatabaseVersionCacheEvictionPolicy());
					}
				}
			}

			var cacheKey = string.Format("PageMetaTargetGroup{0}", CurrentPage.PageLink.ID);
			var metaTargetGroup = _cache.Get(cacheKey) as MetaTargetGroup;
			if (metaTargetGroup == null)
			{
				var targetsNames = new StringBuilder();
				var subTargetsNames = new StringBuilder();
				foreach (var categoryInt in CurrentPage.Category)
				{
					var target = allCategories.FirstOrDefault(t => t.Id == categoryInt);
					if (target != null)
					{
						if (target.Indent == 1)
						{
							targetsNames.AppendFormat("{0};", target.Name);
						}
						else
						{
							subTargetsNames.AppendFormat("{0};", target.Name);
						}
					}
				}

				metaTargetGroup = new MetaTargetGroup
				{
					TargetGroupValue = targetsNames,
					TargetSubGroupValue = subTargetsNames,
				};

				lock (_myLock)
				{
					_cache.Insert(cacheKey, metaTargetGroup, GetDatabaseVersionCacheEvictionPolicy());
				}
			}

			pageMetaData.PageTargetGroup = metaTargetGroup.TargetGroupValue.ToString();
			pageMetaData.PageSubTargetGroup = metaTargetGroup.TargetSubGroupValue.ToString();
		}

		public string SiteId
		{
			get { return SiteDefinition.Current.Name; }
		}

		public string SiteUrl
		{
			get { return SiteDefinition.Current.SiteUrl.ToString(); }
		}

		public string GetPropertyDisplayName(string propertyName)
		{
			var propertyDisplayName = string.Empty;

			// Get the current page pagetype.
			var currentPageType = LoadPageType(CurrentPage.PageTypeName);

			if (currentPageType != null)
			{
				// get the property with the name that is specified.

				var currentProperty = currentPageType.ModelType.GetProperty(propertyName);
				if (currentProperty != null)
				{
					// Get the display attribute
					var displayatt = (DisplayAttribute)currentProperty.GetCustomAttribute(typeof(DisplayAttribute));
					if (displayatt != null)
					{
						propertyDisplayName = displayatt.Name;
					}
				}
			}
			if (string.IsNullOrEmpty(propertyDisplayName))
			{
				propertyDisplayName = propertyName;
			}

			return propertyDisplayName;
		}

		public string GetPropertyDisplayName(string propertyName, BlockData currentBlockData)
		{
			var propertyDisplayName = string.Empty;
			var currentBlockType = currentBlockData.GetType().BaseType;
			if (currentBlockType != null)
			{
				// get the property with the name that is specified.
				var currentProperty = currentBlockType.GetProperty(propertyName);
				if (currentProperty != null)
				{
					// Get the display attribute
					var displayatt = (DisplayAttribute)currentProperty.GetCustomAttribute(typeof(DisplayAttribute));
					if (displayatt != null)
					{
						propertyDisplayName = displayatt.Name;
					}
				}
			}
			return propertyDisplayName;
		}

		public string GetFriendlyURL(PageData page, bool includeSiteUrl)
		{
			var url = new UrlBuilder(UriSupport.AddLanguageSelection(page.LinkURL, page.LanguageBranch));

			EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, page.PageLink, UTF8Encoding.UTF8);

			string retval = url.Uri.OriginalString;

			if (includeSiteUrl)
			{
				retval = SiteUrl + retval;
			}

			return retval;
		}

		#region Private methods
		private PageType LoadPageType(string pageTypeName)
		{
			var pageTypeObject = _pageTypeRepository.Load(pageTypeName);

			return pageTypeObject;
		}

		private PageType LoadPageType<TPageType>()
		{
			var pageTypeObject = _pageTypeRepository.Load(typeof(TPageType));

			return pageTypeObject;
		}

		private static List<Target> CreateFlatCategoryList(List<Target> list, CategoryCollection categoryList)
		{
			if (categoryList != null)
			{
				// Go through all categories
				foreach (Category cat in categoryList)
				{
					// Create a target and populate with category information
					var target = new Target
					{
						Id = cat.ID,
						Name = cat.Name,
						Indent = cat.Indent
					};

					// Add the target to the flat list.
					list.Add(target);

					// Let the method go through all child categories.
					list = CreateFlatCategoryList(list, cat.Categories);
				}
			}

			return list;
		}
		#endregion
	}
}
