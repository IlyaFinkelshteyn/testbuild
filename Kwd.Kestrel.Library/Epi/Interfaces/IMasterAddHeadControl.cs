﻿using Kwd.Kestrel.Library.Managers;

namespace Kwd.Kestrel.Library.Epi.Interfaces
{
	public interface IMasterAddHeadControls
	{
		void AddCss(string path, string media);
		void AddJs(string path, ResourcePosition placement, bool defer);
		void AddScriptText(string scriptText, ResourcePosition placement);
	}
}
