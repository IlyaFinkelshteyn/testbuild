﻿namespace Kwd.Kestrel.Library.Epi.Interfaces
{
	public interface IHasCoordinates
	{
		double Latitude { get; set; }
		double Longitude { get; set; }
		string Headline { get; set; }
		string Content { get; set; }
	}
}
