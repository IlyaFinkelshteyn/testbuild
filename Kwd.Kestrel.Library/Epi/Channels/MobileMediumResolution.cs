﻿namespace Kwd.Kestrel.Library.Epi.Channels
{
	/// <summary>
	/// Defines resolution for a vertical Android handheld device
	/// </summary>
	public class MobileMediumResolution : DisplayResolutionBase
	{
		public MobileMediumResolution()
			: base("Mobile Medium (480px)", 480, 800)
		{
		}
	}
}