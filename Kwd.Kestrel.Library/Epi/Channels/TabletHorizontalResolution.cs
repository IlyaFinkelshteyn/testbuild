﻿namespace Kwd.Kestrel.Library.Epi.Channels
{
	/// <summary>
	/// Defines resolution for a horizontal iPad
	/// </summary>
	public class TabletHorizontalResolution : DisplayResolutionBase
	{
		public TabletHorizontalResolution() 
            : base("Tablet Horizontal (1024px)", 1024, 768)
		{
		}
	}
}