﻿namespace Kwd.Kestrel.Library.Epi.Channels
{
	/// <summary>
	/// Defines resolution for a vertical iPad
	/// </summary>
	public class MobileSmall : DisplayResolutionBase
	{
		public MobileSmall() 
            : base("Mobile Small (320px)", 320, 480)
		{
		}
	}
}