﻿namespace Kwd.Kestrel.Library.Epi.Channels
{
	/// <summary>
	/// Defines resolution for a horizontal iPad
	/// </summary>
	public class TabletVerticalResolution : DisplayResolutionBase
	{
		public TabletVerticalResolution() 
            : base("Tablet Vertical (768px)", 768, 1024)
		{
		}
	}
}