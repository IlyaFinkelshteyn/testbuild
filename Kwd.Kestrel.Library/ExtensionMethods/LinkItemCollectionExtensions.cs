﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Kwd.Kestrel.Library.Models;
using EPiServer.ServiceLocation;
using System;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class LinkItemCollectionExtensions
	{
		public static List<LinkData> ToLinkData(this LinkItemCollection linkItems)
		{
			var list = new List<LinkData>();
            if (linkItems != null)
            {
                foreach (var l in linkItems)
                {
                    var ld = new LinkData();

	                var href = l.Href;
	                if (!href.ToLower().Contains("http"))
	                {
						var urlBuilder = new UrlBuilder(l.Href);
						Global.UrlRewriteProvider.ConvertToExternal(urlBuilder, null, System.Text.Encoding.UTF8);
						href = UriSupport.AbsoluteUrlBySettings(l.Href);
	                }

                    ld.Href = href;
                    ld.Text = l.Text;
                    ld.Title = l.Title;
                    ld.Target = l.Target;
                    list.Add(ld);
                }
            }
			
			return list;
		}

        public static List<ContentReference> ToContentReferenceLIst(this LinkItemCollection collection)
        {
            var pages = new List<ContentReference>();

            foreach (var item in collection)
            {
                    Guid id = EPiServer.Web.PermanentLinkUtility.GetGuid(item.Href);
                    ContentReference pageLink = EPiServer.Web.PermanentLinkUtility.FindContentReference(id);

                    if (PageReference.IsNullOrEmpty(pageLink))
                    {
                        //This sample only extract page references
                        continue;
                    }

                pages.Add(pageLink);

            }

            return pages;
        }

		public static List<PageData> ToPagesUsingOldReferences(this LinkItemCollection linkItemCollection)
		{
			var pages = new List<PageData>();

			foreach (var linkItem in linkItemCollection)
			{
				string linkUrl;
				if (!PermanentLinkMapStore.TryToMapped(linkItem.Href, out linkUrl))
					continue;

				if (string.IsNullOrEmpty(linkUrl))
					continue;

				var pageReference = PageReference.ParseUrl(linkUrl);

				if (PageReference.IsNullOrEmpty(pageReference))
					continue;

				pages.Add(DataFactory.Instance.GetPage((pageReference)));
			}

			return pages;
		}

		public static List<T> ToPages<T>(this LinkItemCollection linkItemCollection)
		{
			var pages = new List<T>();

			var basePages = new PageDataCollection();
			if (linkItemCollection != null)
			{
                var contentReferenceCollection = linkItemCollection.ToContentReferenceLIst();

                foreach (var contentReference in contentReferenceCollection)
				{
                    var page = DataFactory.Instance.GetPage(contentReference.ToPageReference());
					if (page is T)
					{
						basePages.Add(page);
					}
				}
			}

			pages = basePages.Cast<T>().ToList<T>();

			return pages;
		}
	}
}