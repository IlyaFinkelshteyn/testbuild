﻿using System;
using EPiServer;
using EPiServer.Core;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class PageDataExtensions
	{
		/// <summary>
		/// Finds page ancestor at given level
		/// </summary>
		/// <param name="page">Page to start from</param>
		/// <param name="rootPage">Root page, don't go further than this</param>
		/// <param name="offset">How far up the tree to go</param>
		/// <returns>PageData for the requested page</returns>
		public static PageData GetAncestorPage(this PageData page, PageReference rootPage, int offset)
		{
			if (offset < 1)
			{
				throw new Exception("Offset must be greather than zero");
			}

			if (page == null)
			{
				throw new NullReferenceException("Parameter Page cannot be null");
			}

			if (page.PageLink == rootPage)
				return null;

			var pages = new PageDataCollection();

			for (var iteratorPage = page; iteratorPage.PageLink != rootPage; iteratorPage = DataFactory.Instance.GetPage(iteratorPage.ParentLink))
			{
				// If iteratorPage is null, something is wrong (tried to get parent of root?), let's just return the last known actual page
				if (iteratorPage == null)
				{
					break;
				}

				pages.Insert(0, iteratorPage);
				if (PageReference.IsNullOrEmpty(iteratorPage.ParentLink))
				{
					break;
				}
			}

			return pages.Count > --offset ? pages[offset] : null;
		}
	}
}