﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Kwd.Kestrel.Library.Models;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
    public static class UrlExtensions
    {
        public static FileInformation GetFileInformation(this Url url)
        {
			var fi = new FileInformation();
			if (url == null)
				return fi;

			fi.Type = url.ToString().GetExtensionClass();

			var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
			var mediaData = urlResolver.Route(new UrlBuilder(url)) as MediaData;

			if (mediaData == null)
				return fi;

	        if (string.IsNullOrEmpty(fi.Type))
	        {
		        fi.Type = mediaData.RouteSegment.GetExtensionClass();
	        }

			fi.File = mediaData;
			fi.Name = mediaData.Name;

			using (var s = mediaData.BinaryData.OpenRead())
			{
				fi.SizeInBytes = s.Length.ToString();
				fi.SizeInKiloBytes = (s.Length / 1024).ToString();
			}

			return fi;
        }
    }
}
