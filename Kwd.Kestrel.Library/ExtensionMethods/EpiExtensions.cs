﻿using EPiServer.Core;
using EPiServer.Filters;
using Kwd.Kestrel.Library.Epi;
using StructureMap;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class EpiExtensions
	{
		/// <summary>
		/// Tells you if you should show the ContentArea on the page or not.
		/// </summary>
		public static bool ShowContentArea(this ContentArea contentArea, bool inEditMode)
		{
			// If in editmode or the ContentArea is not null or empty. Then we are going to show the area.
			return inEditMode || (contentArea != null && contentArea.Count > 0);
		}

		public static PageDataCollection SortPublishedDescending(this PageDataCollection pages)
		{
			if (pages != null)
			{
				new FilterSort(FilterSortOrder.PublishedDescending).Sort(pages);
			}

			return pages;
		}

		/// <summary>
		/// Return the sitetree level the page exist in.
		/// </summary>
		public static int SiteTreeLevel(this PageData pageData)
		{
			var epiWrapper = ObjectFactory.GetInstance<IEpiWrapper>();

			var level = -1;

			if (pageData.PageLink != PageReference.StartPage && pageData.PageLink != PageReference.RootPage)
			{
				var i = 1;
				var parent = pageData.ParentLink;

				while (parent != PageReference.StartPage)
				{
					i++;
					parent = epiWrapper.GetPage(parent).ParentLink;
				}

				level = i;
			}

			return level;
		}
	}
}
