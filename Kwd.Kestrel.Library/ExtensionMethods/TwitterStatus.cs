﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kwd.Kestrel.Library.Models;
using LinqToTwitter;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class TwitterStatus
	{
		public static Tweet ToTweet(this Status status)
		{
			var t = new Tweet
			{
				Sid = status.StatusID,
				Text = status.Text,
				PossiblySensitive = status.PossiblySensitive,
				ScreenName = status.ScreenName,
				CreatedAt = status.CreatedAt
			};

			return t;
		}
	}
}
