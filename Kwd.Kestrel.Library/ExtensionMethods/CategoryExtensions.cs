﻿using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Kwd.Kestrel.Library.Models;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class CategoryExtensions
	{
		public static CategoryCollection GetActiveSubCategories(this CategoryList allActiveCategoryIds, string parentCategoryName)
		{
			return allActiveCategoryIds.GetActiveSubCategories(Category.Find(parentCategoryName));
		}

		public static CategoryCollection GetActiveSubCategories(this CategoryList allActiveCategoryIds, int parentCategoryId)
		{
			return allActiveCategoryIds.GetActiveSubCategories(Category.Find(parentCategoryId));
		}

		public static CategoryCollection GetActiveSubCategories(this CategoryList allActiveCategoryIds, Category parentCategory)
		{
			if (allActiveCategoryIds == null || allActiveCategoryIds.Count < 1 || parentCategory == null)
			{
				return null;
			}

			CategoryCollection subCategories = Category.Find(parentCategory.ID).Categories;

			var activeCategories = new CategoryCollection();

			for (int i = 0; i < allActiveCategoryIds.Count; i++)
			{
				Category category = Category.Find(allActiveCategoryIds.ElementAt(i));

				foreach (Category subCategory in subCategories)
				{
					if (subCategory.ID == category.ID)
					{
						activeCategories.Add(category);
					}
				}
			}

			return activeCategories;
		}

		public static CategorySubSet GetCategorySubset(string categoryName)
		{
			var parentCategory = Category.Find(categoryName);
			CategoryCollection onlyAvailableCategories = new CategoryCollection();

			foreach (Category category in parentCategory.Categories)
			{
				if (category.Available == true && category.Selectable == true)
				{
					onlyAvailableCategories.Add(category);
				}
			}
			var categorySubSet = new CategorySubSet()
			{
				Name = categoryName,
				Id = parentCategory.ID,
				Categories = onlyAvailableCategories
			};
			return categorySubSet;
		}
	}
}
