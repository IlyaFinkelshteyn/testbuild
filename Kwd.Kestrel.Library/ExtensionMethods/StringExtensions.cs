﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Kwd.Kestrel.Library.ExtensionMethods
{
	public static class StringExtensions
	{
		/// <summary>
		/// Remove all HTML tags from a string.
		/// </summary>
		public static string RemoveHtml(this string html)
		{
			if (html == null)
			{
				return string.Empty;
			}
			return Regex.Replace(html, "<[^>]*>", string.Empty);
		}

		/// <summary>
		/// Gets the first sentences out of a text
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string GetFirstSentence(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;

			var sentences = text.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);

			// If array is not empty get the first sentences
			if (sentences.Length > 0 && !string.IsNullOrEmpty(sentences[0]))
			{
				// Make first sentence the base return string (append dot to complete the setence)
				text = string.Concat(sentences[0], ".");

				//// If array contains atleast one more sentence, append the second sentence to the text
				//if (sentences.Length > 1 && !string.IsNullOrEmpty(sentences[1]))
				//	text = string.Concat(text, sentences[1], ".");

				//// If the text conatins more than two sentences, append additional dots to make ellipses
				//if (sentences.Length > 2)
				//	text = string.Concat(text, " (&hellip;)");
			}

			return text;
		}

		/// <summary>
		/// Gets the first two sentences out of a text
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string GetFirstTwoSentences(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;

			var sentences = text.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);

			// If array is not empty get the first two sentences
			if (sentences.Length > 0 && !string.IsNullOrEmpty(sentences[0]))
			{
				// Make first sentence the base return string (append dot to complete the setence)
				text = string.Concat(sentences[0], ".");

				// If array contains atleast one more sentence, append the second sentence to the text
				if (sentences.Length > 1 && !string.IsNullOrEmpty(sentences[1]))
					text = string.Concat(text, sentences[1], ".");

				// If the text conatins more than two sentences, append additional dots to make ellipses
				if (sentences.Length > 2)
					text = string.Concat(text, " (&hellip;)");
			}

			return text;
		}

		/// <summary>
		/// Generates a truncated string within the specified character limit followed by [...]
		/// </summary>
		/// <param name="originalText">The text to truncate</param>
		/// <param name="maxCharacters">The maximum number of characters to return from the original text</param>
		/// <returns>The truncated text followed by appended string (if any)</returns>
		public static string TruncateText(this string originalText, int maxCharacters)
		{
			// Treat null as an empty string
			if (string.IsNullOrEmpty(originalText))
			{
				return string.Empty;
			}

			originalText = originalText.Trim();

			const string stopChars = " .!?";

			if (originalText.Length > maxCharacters && maxCharacters > 0)
			{
				string stringToReturn = originalText.Substring(0, maxCharacters);

				while (stringToReturn.Length > 0 && !stopChars.Contains(stringToReturn.Substring(stringToReturn.Length - 1)))
				{
					stringToReturn = stringToReturn.Substring(0, stringToReturn.Length - 1);
				}

				if (stringToReturn.Length == 0)
					stringToReturn = originalText.Substring(maxCharacters);

				stringToReturn = stringToReturn.Trim();

				if (!stopChars.Contains(stringToReturn.Substring(stringToReturn.Length - 1)))
				{
					// Remove any trailing commas
					while (stringToReturn.EndsWith(","))
						stringToReturn = stringToReturn.Substring(0, stringToReturn.Length - 1);

					stringToReturn += "...";
				}

				return stringToReturn;
			}
			else
			{
				return originalText;
			}
		}

		public static string CreateScriptSafeIdentifier(this string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return text;
			}

			var allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-".ToCharArray();

			var output = new StringBuilder();
			foreach (char item in text)
			{
				switch (item)
				{
					case 'å':
						output.Append("ao");
						break;
					case 'ä':
						output.Append("ae");
						break;
					case 'ö':
						output.Append("oe");
						break;
					case 'Å':
						output.Append("AO");
						break;
					case 'Ä':
						output.Append("AE");
						break;
					case 'Ö':
						output.Append("OE");
						break;
					default:
						if (allowedChars.Contains(item))
							output.Append(item);
						break;
				}
			}

			return output.ToString();
		}

		/// <summary>
		/// Generates a truncated string within the specified character limit followed by [...]
		/// </summary>
		/// <param name="originalText">The text to truncate</param>
		/// <param name="maxCharacters">The maximum number of characters to return from the original text</param>
		/// <param name="stringToAppend">The string, if any, that should be appended after the last word</param>
		/// <returns>The truncated text followed by appended string (if any)</returns>
		public static string WordsWithinCharacterLimit(this string originalText, int maxCharacters, string stringToAppend)
		{
			if (string.IsNullOrEmpty(originalText)) return string.Empty;

			originalText = (originalText ?? string.Empty).Trim(); // Treat null as an empty string

			var stopChars = " .!?";

			maxCharacters -= stringToAppend.Length;

			if (originalText.Length > maxCharacters && maxCharacters > 0)
			{
				var stringToReturn = originalText.Substring(0, maxCharacters);

				while (stringToReturn.Length > 0 && !stopChars.Contains(stringToReturn.Substring(stringToReturn.Length - 1)))
				{
					stringToReturn = stringToReturn.Substring(0, stringToReturn.Length - 1);
				}

				if (stringToReturn.Length == 0)
					stringToReturn = originalText.Substring(maxCharacters);

				stringToReturn = stringToReturn.Trim();

				if (!stopChars.Contains(stringToReturn.Substring(stringToReturn.Length - 1)))
				{
					//Remove any trailing commas
					while (stringToReturn.EndsWith(","))
						stringToReturn = stringToReturn.Substring(0, stringToReturn.Length - 1);

					stringToReturn += "...";
				}

				return stringToReturn;
			}
			else
			{
				return originalText;
			}
		}

		/// <summary>
		/// Return the file extension depending on the specified mime type.
		/// </summary>
		/// <param name="mimeType"></param>
		/// <returns></returns>
		public static string FileExtensionFromMimeType(this string mimeType)
		{
			string type = string.Empty;
			switch (mimeType)
			{
				case "text/plain":
					type = "txt";
					break;
				case "application/rtf":
					type = "rtf";
					break;
				case "application/pdf":
					type = "pdf";
					break;
				case "application/postscript":
					type = "ps";
					break;
				case "application/vnd.ms-powerpoint":
					type = "ppt";
					break;
				case "application/vnd.ms-excel":
					type = "xls";
					break;
				case "application/msword":
					type = "doc";
					break;
				case "image/bmp":
					type = "bmp";
					break;
				case "image/cmu-raster":
					type = "ras";
					break;
				case "image/fif":
					type = "fif";
					break;
				case "image/florian":
					type = "flo";
					break;
				case "image/g3fax":
					type = "g3";
					break;
				case "image/gif":
					type = "gif";
					break;
				case "image/ief":
					type = "ief";
					break;
				case "image/jpeg":
					type = "jpg";
					break;
				case "image/jutvision":
					type = "jut";
					break;
				case "image/naplps":
					type = "nap";
					break;
				case "image/pict":
					type = "pic";
					break;
				case "image/pjpeg":
					type = "jpg";
					break;
				case "image/png":
					type = "png";
					break;
				case "image/tiff":
					type = "tif";
					break;
				case "image/vasa":
					type = "mcf";
					break;
				case "image/vnd.dwg":
					type = "dwg";
					break;
				case "image/vnd.fpx":
					type = "fpx";
					break;
				case "image/vnd.net-fpx":
					type = "fpx";
					break;
				case "image/vnd.rn-realflash":
					type = "rf";
					break;
				case "image/vnd.rn-realpix":
					type = "rp";
					break;
				case "image/vnd.wap.wbmp":
					type = "wbmp";
					break;
				case "image/vnd.xiff":
					type = "xif";
					break;
				case "image/xbm":
					type = "xbm";
					break;
				case "image/x-cmu-raster":
					type = "ras";
					break;
				case "image/x-dwg":
					type = "dwg";
					break;
				case "image/x-icon":
					type = "ico";
					break;
				case "image/x-jg":
					type = "art";
					break;
				case "image/x-jps":
					type = "jps";
					break;
				case "image/x-niff":
					type = "nif";
					break;
				case "image/x-pcx":
					type = "pcx";
					break;
				case "image/x-pict":
					type = "pct";
					break;
				case "image/xpm":
					type = "xpm";
					break;
				case "image/x-portable-anymap":
					type = "pnm";
					break;
				case "image/x-portable-bitmap":
					type = "pbm";
					break;
				case "image/x-portable-graymap":
					type = "pgm";
					break;
				case "image/x-portable-pixmap":
					type = "ppm";
					break;
				case "image/x-quicktime":
					type = "qif";
					break;
				case "image/x-rgb":
					type = "rgb";
					break;
				case "image/x-tiff":
					type = "tif";
					break;
				case "image/x-windows-bmp":
					type = "bmp";
					break;
				case "image/x-xbitmap":
					type = "xbm";
					break;
				case "image/x-xbm":
					type = "xbm";
					break;
				case "image/x-xpixmap":
					type = "pm";
					break;
				case "image/x-xwd":
					type = "xwd";
					break;
				case "image/x-xwindowdump":
					type = "xwd";
					break;
			}
			return type;
		}

		public static string ToFileSize(this long size)
		{
			if (size < 1024)
			{
				return (size).ToString("F0") + " bytes";
			}
			else if (size < Math.Pow(1024, 2))
			{
				return (size / 1024).ToString("F0") + " KB";
			}
			else if (size < Math.Pow(1024, 3))
			{
				return (size / Math.Pow(1024, 2)).ToString("F0") + " MB";
			}
			else if (size < Math.Pow(1024, 4))
			{
				return (size / Math.Pow(1024, 3)).ToString("F0") + " GB";
			}
			else if (size < Math.Pow(1024, 5))
			{
				return (size / Math.Pow(1024, 4)).ToString("F0") + " TB";
			}
			else if (size < Math.Pow(1024, 6))
			{
				return (size / Math.Pow(1024, 5)).ToString("F0") + " PB";
			}
			else
			{
				return (size / Math.Pow(1024, 6)).ToString("F0") + " EB";
			}
		}

		public static string FilenameExtension(this string filename)
		{
			string extension = System.IO.Path.GetExtension(filename);

			if (extension.StartsWith("."))
				extension = extension.Remove(0, 1);

			return extension;
		}

		public static string GetExtensionClass(this string filename)
		{
			//TODO: Maybe we could move this to settings. So we could add new extensions without compile. 
			string extension = FilenameExtension(filename).ToLower();

			switch (extension)
			{
				case "jpg":
				case "gif":
				case "png":
				case "tif":
					return "jpg";
				case "pdf":
					return "pdf";
				case "xls":
				case "xlsx":
					return "xls";
				case "htm":
				case "html":
					return "html";
				case "doc":
				case "docx":
					return "doc";
				case "ppt":
				case "pptx":
					return "ppt";
				case "wav":
				case "mp3":
				case "flac":
				case "wma":
				case "ogg":
				case "aac":
					return "music";
				case "fla":
				case "flv":
					return "flash";
				case "avi":
				case "mp4":
				case "mpeg":
				case "wmv":
				case "mkv":
					return "video";
				default:
					return string.Empty;
			}
		}

		public static string RemoveForbiddenChars(string stringToTrim)
		{
			char[] forbiddenChars = { '>', '<', '/', '\\', '[', ']', '\'' };

			foreach (var forbiddenChar in forbiddenChars)
			{
				stringToTrim = stringToTrim.Replace(forbiddenChar.ToString(CultureInfo.InvariantCulture), string.Empty);
			}
			return stringToTrim;
		}

	}
}