﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kwd.Kestrel.Library.Managers;
using Kwd.Kestrel.Library.Models.Sitemap;
using StructureMap;

namespace Kwd.Kestrel.Library.Temp.Helpers
{
    public class SiteMapGenerator
    {
        public List<UrlItem> Items;
        private readonly string _baseUrl;
		private readonly ILogger _logProvider;

        /// <summary>
        /// SitemapGenerator constructor
        /// </summary>
        /// <param name="baseUrl">The baseUrl to the site</param>
        public SiteMapGenerator(string baseUrl)
        {
            Items = new List<UrlItem>();

			_logProvider = ObjectFactory.GetInstance<ILogger>();

            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                throw new ArgumentNullException("baseUrl");
            }

            // Make sure that the baseUrl does not endswith "/".
            if (baseUrl.EndsWith("/"))
            {
                _baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            else
            {
                _baseUrl = baseUrl;    
            }
            
        }

        public string GenerateSiteMap()
        {
            _logProvider.LogDebug("Start GenerateSitemap");

            var response = new StringBuilder();
            response.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            response.Append("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\">");

            foreach (var urlItem in Items)
            {
                response.Append("<url>");
                response.Append(string.Format("<loc>{0}</loc>", GenerateUrl(urlItem.Loc)));

                AppendLastModified(response, urlItem);

                AppendChangeFreq(response, urlItem);

                AppendPriority(response, urlItem);

                AppendAlternateUrls(response, urlItem);

                AppendImageUrls(response, urlItem);

                AppendVideoUrls(response, urlItem);

                response.Append("</url>");
            }

            response.Append("</urlset>");

            _logProvider.LogDebug("Finished GenerateSitemap");

            _logProvider.LogInfo("Sitemap is generated.");

            return response.ToString();
        }

        #region Private methods
        /// <summary>
        /// Method that will check the url and if it not starts with "http" it will add baseUrl in the beginning.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GenerateUrl(string url)
        {
            var result = url.StartsWith("http") ? url : string.Format("{0}{1}", _baseUrl, url);

            return result;
        }

        private void AppendLastModified(StringBuilder sb, UrlItem urlItem)
        {
            if (urlItem.LastModified != null)
            {
                // <lastmod>2011-05-04T15:02:52+02:00</lastmod>
                var hours = TimeZoneInfo.Local.BaseUtcOffset.Hours;
                var offset = string.Format("{0}{1}:00", ((hours > 0) ? "+" : string.Empty), hours.ToString("00"));
                var isoformat = ((DateTime)urlItem.LastModified).ToString("s") + offset;
                sb.Append(string.Format("<lastmod>{0}</lastmod>", isoformat));
            }
        }

        private void AppendChangeFreq(StringBuilder sb, UrlItem urlItem)
        {
            if (!string.IsNullOrWhiteSpace(urlItem.ChangeFreq))
            {
                // <changefreq>always</changefreq>
                sb.Append(string.Format("<changefreq>{0}</changefreq>", urlItem.ChangeFreq));
            }
        }

        private void AppendPriority(StringBuilder sb, UrlItem urlItem)
        {
            if (urlItem.Priority > 0)
            {
                // <priority>0.5</priority>
                sb.Append(string.Format("<priority>{0}</priority>", urlItem.Priority == 100 ? "1" : "0." + urlItem.Priority));
            }
        }
        
        private void AppendAlternateUrls(StringBuilder sb, UrlItem urlItem)
        {
            // Add alternate language urls to the sitemap if specified.
            // http://support.google.com/webmasters/bin/answer.py?hl=en&answer=2620865&topic=2370587&ctx=topic
            if (urlItem.AlternateUrls != null && urlItem.AlternateUrls.Any())
            {
                foreach (var altLangUrl in urlItem.AlternateUrls)
                {
                    sb.Append(string.Format("<xhtml:link rel=\"alternate\" hreflang=\"{0}\" href=\"{1}\"/>", altLangUrl.Language, GenerateUrl(altLangUrl.Loc)));
                }
            }
        }

        private void AppendImageUrls(StringBuilder sb, UrlItem urlItem)
        {
            if (urlItem.ImageUrls != null && urlItem.ImageUrls.Any())
            {
                foreach (var imageItem in urlItem.ImageUrls)
                {
                    sb.Append(string.Format("<image:image><image:loc>{0}</image:loc></image:image>", imageItem.Loc));
                }
            }
        }

        private void AppendVideoUrls(StringBuilder sb, UrlItem urlItem)
        {
            if (urlItem.VideoUrls != null && urlItem.VideoUrls.Any())
            {
                foreach (var videoItem in urlItem.VideoUrls)
                {
                    if (!string.IsNullOrWhiteSpace(videoItem.Loc))
                    {
                        sb.Append("<video:video>");
                        sb.Append(string.Format("<video:content_loc>{0}</video:content_loc>", videoItem.Loc));
                        if (!string.IsNullOrWhiteSpace(videoItem.PlayerLoc))
                        {
                            sb.Append("<video:player_loc");
                            sb.Append(videoItem.PlayerAllowEmbed ? " allow_embed=\"yes\"" : " allow_embed=\"no\"");
                            if (!string.IsNullOrWhiteSpace(videoItem.Autoplay))
                            {
                                sb.Append(string.Format(" autoplay=\"{0}\"", videoItem.Autoplay));
                            }
                            sb.Append(string.Format(">{0}</video:player_loc>", videoItem.PlayerLoc));
                        }
                        if (!string.IsNullOrWhiteSpace(videoItem.ThumbnailLoc))
                        {
                            sb.Append(string.Format("<video:thumbnail_loc>{0}</video:thumbnail_loc>", videoItem.ThumbnailLoc));
                        }
                        if (!string.IsNullOrWhiteSpace(videoItem.Title))
                        {
                            sb.Append(string.Format("<video:title>{0}</video:title>", videoItem.Title));
                        }
                        if (!string.IsNullOrWhiteSpace(videoItem.Description))
                        {
                            sb.Append(string.Format("<video:description>{0}</video:description>", videoItem.Description));
                        }
                        sb.Append("</video:video>");
                    }
                }
            }
        }
        #endregion
    }
}