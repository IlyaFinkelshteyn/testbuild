
# Import SetupDevelopmentEnvironment script
. .\SetupDevelopmentEnvironmentLib.ps1

# Modify variables below

# Hostname for the site (default binding)
$hostName = "validoo.local"

# Application pool name
$appPoolName = 'validoo.local'

# Application pool user
$appPoolUser = $null

# Application pool password
$appPoolPass = $null

# Will be prepended to $hostName (i.e scripts.$hostName etc.)
# Setting this to null will not create any additional bindings for the site.
$additionalHostNames = "images", "ui"

$websiteFolderName = 'Website'

# Remove existing website from IIS
RemoveIISWebsite

# Create a new website in IIS
CreateIISWebSite

# Add additional bindings
AddAdditionalBindings

# Add the site to hosts file
AddToHostsFile
