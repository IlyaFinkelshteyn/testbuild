/*! Picturefill - Responsive Images that work today. (and mimic the proposed Picture element with divs). Author: Scott Jehl, Filament Group, 2012 | License: MIT/GPLv2 */

(function (w)
{
	// Enable strict mode
	"use strict";
	var images = [],
		threshhold = 100,
		blankImgSrc = '/UI/images/blank.gif';

	w.picturefill = function ()
	{
		var ps = w.document.getElementsByTagName("div");

		// Loop the pictures
		for (var i = 0, il = ps.length; i < il; i++)
		{
			if (ps[i].getAttribute("data-picture") !== null)
			{
				var sources = ps[i].getElementsByTagName("div"),
					matches = [];
				
				// See if which sources match
				for (var j = 0, jl = sources.length; j < jl; j++)
				{
					var source = sources[j];
					if (!source.className || source.className.indexOf('attribution') == -1)
					{
						var media = source.getAttribute("data-media");
						// if there's no media specified, OR w.matchMedia is supported 
						if (!media || (w.matchMedia && w.matchMedia(media).matches))
						{
							matches.push(source);
						}
					}
					
				}
				
				// Find any existing img element in the picture element
				var picImg = ps[i].getElementsByTagName("img")[0];
				
				if (matches.length)
				{
					var match = matches.pop();

					if (!picImg)
					{
						picImg = w.document.createElement("img");
						picImg.alt = ps[i].getAttribute("data-alt");
						picImg.title = ps[i].getAttribute("data-title");
						
						picImg.setAttribute("data-media", match.getAttribute("data-media"));
						picImg.setAttribute("data-width", match.getAttribute("data-width"));
						picImg.setAttribute("data-height", match.getAttribute("data-height"));
						picImg.setAttribute("data-quality", match.getAttribute("data-quality"));
						picImg.setAttribute("data-maxwidth", match.getAttribute("data-maxwidth"));
						
						ps[i].appendChild(picImg);
					}
					images.push(picImg);
					picImg.removeAttribute("data-loaded");
					picImg.setAttribute("data-src", match.getAttribute("data-src"));
					
					if(elementInViewport(picImg,threshhold))
					{
						loadImage(picImg);
					}
					else
					{
						picImg.src = blankImgSrc;
						var ratio = parseInt(picImg.getAttribute("data-height"),10) / parseInt(picImg.getAttribute("data-width"),10),
							height = parseInt(picImg.parentElement.getBoundingClientRect().width * ratio,10);

						if (typeof height === 'number' && picImg.height === 0)
						{
							//picImg.setAttribute("height",height);
						}
					}
				}
				else if (picImg)
				{
					ps[i].removeChild(picImg);
				}
				
			}
		}

		//Trigger images loaded event
		$('body').trigger('picturefillcomplete');
	};

	function loadImage(elm)
	{
		var dataSrc = elm.getAttribute("data-src");
		
		if(elm.src !== dataSrc)
		{
			
			elm.src = dataSrc;
			elm.setAttribute("data-loaded","true");
		}
	}

	function processScroll()
	{
		var imagecount = images.length;
		
		for (var j = 0; j < imagecount; j++)
		{
			var img = images[j];
			if(elementInViewport(img,threshhold))
			{
				loadImage(img);
			}

		}
	}

	function triggerEvent(element, eventName)
	{
		/* safari, webkit, gecko */
		if (document.createEvent)
		{
			var evt = document.createEvent('HTMLEvents');
			evt.initEvent(eventName, true, true);
			return element.dispatchEvent(evt);

		}
		/* Internet Explorer */
		if (element.fireEvent)
		{
			return element.fireEvent('on' + eventName);
		}
	}
	
	function elementInViewport(el,threshold)
	{
		if(el.getAttribute("data-loaded") === null)
		{
			var rect = el.getBoundingClientRect();
			threshold = (typeof threshold === 'number') ? threshold : 0;
			
			return (
				(rect.top >= 0) &&
				(rect.left >= 0) &&
				(rect.top <= ((window.innerHeight || document.documentElement.clientHeight)+threshold))
			);
		}
		else
		{
			return false;
		}
	}

	// Run on resize and domready (w.load as a fallback)
	if (w.addEventListener)
	{
		w.addEventListener("resize", w.picturefill, false);
		w.addEventListener("DOMContentLoaded", function ()
		{
			w.picturefill();
			// Run once only
			w.removeEventListener("load", w.picturefill, false);
		}, false);
		w.addEventListener("load", w.picturefill, false);
		w.addEventListener("scroll", processScroll, false);
	}
	else if (w.attachEvent)
	{
		w.attachEvent("onload", w.picturefill);
		w.attachEvent("onscroll", processScroll);
	}

}(this));