﻿/* jshint unused:false */
(function($)
{
	$.fn.communicateArea = function(el, options)
	{
		var base = this;
		base.$el = $(el);
		base.el = el;
		console.log(el);

		$.communicateArea.defaultOptions = {
			createNavigation : true,
			createPager : true,
			createPagerButtons : true,
			showSlideTime : 6000,
			showSlideAnimationTime : 0,
			slideAnimation : 'scrollVert',
			activateSlideSupport : false,
			createStopResumeButtons : true,
			stopOnClick : false,
			pauseOnHover : false,
			fnSlideInitialize : null,
			selector_slideList : '> ul:eq(0)',
			selector_slides : '> li',
			selector_navigation : '> ul:eq(1)',
			selector_navigationSlideTitle : '> img',
			selector_pager : '> ul:eq(2)',
			autostart : true,
			stopOnPagerClick : true
		};

		base.init = function()
		{
			
			base.options = $.extend({}, $.communicateArea.defaultOptions, options);
			base.$el.addClass('initialized')
				.find('ul:eq(0)').addClass('slidelist');
			var commAreaId = base.$el.prop('id'),
				$communicateList = base.$el.find(base.options.selector_slideList),
				$communicateNav = base.$el.find(base.options.selector_navigation),
				$communicateSlides = $communicateList.find(base.options.selector_slides),
				$communicatePager = base.$el.find(base.options.selector_pager),
				nextButton = null,
				previousButton = null,
				pagerInput = null;

			if ($.isFunction(base.options.fnSlideInitialize) === true)
			{
				$($communicateSlides).each(base.options.fnSlideInitialize);
			}

			if (base.options.createPager === true)
			{
				$communicatePager = $('<ul class="slidelist"></ul>');
				$('<div id="' + base.$el.prop('id') + '-pager" class="paginator"></div>').append($communicatePager).insertAfter($communicateList);
				if (base.options.createNavigation === false)
				{
					pagerInput = $communicatePager;
				}
				else
				{
					$communicatePager.delegate("li", "click", function(e)
					{
						$communicateNav.find('li:eq(' + $(this).index() + ')').trigger('click');
					});
				}
			}

			if (base.options.createPagerButtons === true && $communicateSlides.length > 1)
			{
				nextButton = $('<div id="' + commAreaId + '-nextbutton">Next</div>').appendTo(base.$el);
				previousButton = $('<div id="' + commAreaId + '-prevbutton">Previous</div>').appendTo(base.$el);
				if (base.options.activateSlideSupport === true && typeof jQuery.fn.swipe === 'function')
				{
					base.$el.swipe({
						swipe: function(event, direction, distance, duration, fingerCount)
						{
							if (direction && direction === 'right')
							{
								nextButton.trigger('click');
							}
							if (direction && direction === 'left')
							{
								previousButton.trigger('click');
							}
						},
						fallbackToMouseEvents: false
					});
				}
			}

			if (base.options.createNavigation === true)
			{
				$communicateNav = $('<ul></ul>');
				$('<div id="' + base.$el.prop('id') + '-navigation"></div>')
					.append($communicateNav)
					.append($('<img src="/css/img/bg-communicatearea-navigation.png" alt="" class="footerBackground"/>'))
					.insertAfter($communicateList);

				pagerInput = $communicateNav;
			}

			var buildNavigationLinks = function(i, slide)
			{
				if (base.options.createPager === true && base.options.createNavigation === false)
				{
					return '<li><a title="' + (i + 1) + '" class="counter" href="#"><span>' + (i + 1) + '</span></a></li>';
				}
				else
				{
					if (base.options.createPager === true)
					{
						$('<li>&#160;</li>').appendTo($communicatePager);
						if (i === 0) { $communicatePager.children().eq(0).addClass('current'); }
					}
					if (base.options.createNavigation === true)
					{
						return '<li>' + $(slide).find(base.options.selector_navigationSlideTitle).prop('alt') + '</li>';
					}
				}
			};

			

			var afterSlidechange = function(currSlideElement, nextSlideElement, options, forwardFlag)
			{

				if ($communicateNav.length !== 0)
				{
					$communicateNav.children().removeClass('current active').eq(options.currSlide).addClass('current active');
				}
				if ($communicatePager.length !== 0)
				{
					$communicatePager.children().removeClass('current active').eq(options.currSlide).addClass('current active');
				}
			};

			//Loop through all slide images and set the height
			$communicateSlides.find('img').each(function(i,img)
			{
				var $img = $(img);
				if ($img.height() < 2)
				{
					$img.css('height',$img.attr('height') + 'px');
				}
			});
			
			$communicateList.cycle({
				activePagerClass: 'current active',
				fx: base.options.slideAnimation,
				speed: base.options.showSlideAnimationTime,
				timeout: base.options.showSlideTime,
				after: afterSlidechange,
				pause: base.options.pauseOnHover,
				pagerAnchorBuilder: buildNavigationLinks,
				pager: pagerInput,
				next: nextButton,
				prev: previousButton,
				width: '100%'
			});

			if (base.options.stopOnClick === true)
			{
				$communicateList.on('click',function()
				{
					$communicateList.cycle('stop');
				});
			}
			if (base.options.autostart === false)
			{
				$communicateList.cycle('pause');
			}

			if (base.options.createStopResumeButtons === true && $communicateSlides.length > 1)
			{
				$('<li class="playpause"><a title="play/pause" class="icon-play" href="#"></a></li>')
					.on('click', function()
					{
						var $link = $(this).children('a:eq(0)');
						if ($link.hasClass('icon-play'))
						{
							$communicateList.cycle('resume');
							$link.removeClass('icon-play').addClass('icon-pause');
						}
						else
						{
							$communicateList.cycle('pause');
							$link.removeClass('icon-pause').addClass('icon-play');
						}
					})
					.appendTo(base.$el.find('.paginator > .slidelist'));
			}

			if (base.options.stopOnPagerClick === true)
			{
				$communicatePager.find('li:not(.playpause)').on('click',function()
				{
					$communicateList.cycle('pause');
					$communicatePager.find('li.playpause a').removeClass('icon-pause').addClass('icon-play');
				});
			}

			$(window).trigger('communicateAreaLoaded');

			base.$el.addClass('activated').removeClass('initialized');

		};
		base.init();
	};

	$.fn.communicateArea = function(options) { return this.each(function() { (new $.communicateArea(this, options)); }); };
})(jQuery);