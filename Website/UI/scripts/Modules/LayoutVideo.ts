/// <reference path="../main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.Layout {
    export class VideoBase {
        private MainBase: MainBase;
        private VideoContainers: JQuery;
        interval: number;

        constructor(mainBase: MainBase) {
            //We expect this method to be run after document ready
            var base = this;
            base.MainBase = mainBase;
            base.Init();
        }

        Init() {
            var base = this;
            base.VideoContainers = $('div.video-container');
            if ($('.fancybox').length !== 0) {
                base.InitFancybox();
            }

            if (base.VideoContainers.length !== 0) {
                base.VideoContainers.each(function (i, videocontainer) {
                    var $videocontainer = $(videocontainer),
                        $iframe = $videocontainer.find('iframe'),
                        $object = $videocontainer.find('object');

                    if ($iframe.length !== 0) {
                        base.CheckAndSetWidthIframe($videocontainer);
                        base.SetWmode($videocontainer);
                    }
                    else if ($object.length !== 0) {
                        base.CheckAndSetObjectWidth($videocontainer);
                    }
                    else if ($videocontainer.find('div[data-qbrick-mcid]').length !== 0) {
                        base.CheckWidthQBrick($videocontainer);
                    }

                    base.InitiateCoverImage($videocontainer);
                });
            }
        }

        InitFancybox() {
            $(".fancybox").fancybox(
                {
                    width: '70%',
                    height: '90%'
                });
        }

        InitiateCoverImage($container: JQuery) {
            var base = this;
            $container.on('click touchend', 'img', $.proxy(base.onCoverImageClick, base));
        }

        private CheckAndSetObjectWidth($container: JQuery) {
            var containerWidth = $container.width(),
                $object = $container.find('object'),
                videoOrgWidth = parseInt($object.attr('width'), 10),
                videoOrgHeight = parseInt($object.attr('height'), 10),
                videoRatio = 0;

            $container.addClass('objectembed');

            if (containerWidth > 0) {
                if ($.isNumeric(videoOrgWidth) === true && $.isNumeric(videoOrgHeight) === true) {
                    videoRatio = videoOrgHeight / videoOrgWidth;
                    var containerId = "Video" + (Math.floor(Math.random() * 100)) + ((new Date()).valueOf()),
                        newVideoHeight = (videoRatio * containerWidth);
                    $object
                        .attr('width', containerWidth)
                        .attr('height', newVideoHeight)
                        .attr('id', containerId);
                }
            }
        }

        onCoverImageClick(e: JQueryEventObject) {
            var base = this,
                $img = $(e.target),
                $parent = $img.parents('div:eq(0)'),
                $iframe = $parent.find('iframe:eq(0)');

            if ($parent.hasClass('youtube')) {
                base.Init();

                var src = $iframe.attr('src'),
                    autoplayString = 'autoplay=1';

                if (base.hasQueryString(src) === true) {
                    src = src + "&" + autoplayString;
                }
                else {
                    src = src + "?" + autoplayString;
                }
                $img.css({ 'z-index': '5', 'visibility': 'hidden' });
                $iframe.css({ 'opacity': '1' }).attr('src', src);
            }
        }

        SetWmode($container: JQuery) {
            //This function is not relevant since we add the wmode querystring in the widget itself
            var base = this,
                $videoIframe = $container.find('iframe:eq(0)'),
                wModeString = 'wmode=transparent';

            if ($videoIframe.length > 0) {
                var iframeSrc = $videoIframe.attr('src');
                if (base.hasQueryString(iframeSrc) === true) {
                    iframeSrc += '&' + wModeString;
                }
                else {
                    iframeSrc += '?' + wModeString;
                }
                $videoIframe.attr('src', iframeSrc);
            }
        }

        hasQueryString(src: string) {
            return (src.indexOf('?') !== -1);
        }

        CheckAndSetWidthIframe($container: JQuery) {
            var containerWidth = $container.width(),
                $videoIframe = $container.find('iframe:eq(0)'),
                videoOrgWidth = parseInt($videoIframe.attr('width'), 10),
                videoOrgHeight = parseInt($videoIframe.attr('height'), 10),
                videoRatio = 0;

            $container.addClass('youtube');

            if (containerWidth > 0) {
                if ($.isNumeric(videoOrgWidth) === true && $.isNumeric(videoOrgHeight) === true) {
                    videoRatio = videoOrgHeight / videoOrgWidth;
                    var containerId = "Video" + (Math.floor(Math.random() * 100)) + ((new Date()).valueOf()),
                        newVideoHeight = (videoRatio * containerWidth);

                    $videoIframe
                        .attr('width', containerWidth)
                        .attr('height', newVideoHeight)
                        .attr('id', containerId);
                }
            }

            //exception for weird formats
            //var specialVideo = $('.dynamic.small'), newRatio = .5625;

            /*if ($('.dynamic.small').length !== 0) {
                //specialVideo.height(specialVideo.width() * newRatio);
            }*/
        }

        CheckWidthQBrick($container: JQuery) {
            var base = this,
                containerWidth = $container.width(),
                $videoDiv = $container.find('> div:eq(0)'),
                videoOrgWidth = parseInt($videoDiv.attr('data-qbrick-width'), 10),
                videoOrgHeight = parseInt($videoDiv.attr('data-qbrick-height'), 10),
                videoRatio = 0;

            $container.addClass('qbrick');

            if ($.isNumeric(videoOrgWidth) === true && $.isNumeric(videoOrgHeight) === true && (videoOrgWidth > containerWidth)) {
                videoRatio = videoOrgHeight / videoOrgWidth;

                var containerId = "Video" + (Math.floor(Math.random() * 100)) + ((new Date()).valueOf()),
                    newVideoHeight = (videoRatio * containerWidth);

                $videoDiv
                    .attr('data-qbrick-width', containerWidth)
                    .attr('data-qbrick-height', newVideoHeight)
                    .attr('id', containerId);

                //base.interval = window.setInterval($.proxy(base.SetWidthQBrich('#' + containerId),base), 500);
                var intervalFn = $.proxy(function () { base.SetWidthQBrich('#' + containerId); }, base);
                base.interval = window.setInterval(intervalFn, 200);

                //base.interval = window.setInterval($.proxy(base.SetWidthQBrich('#' + containerId),base), 200);
                //base.interval = window.setInterval("Layout.Video.SetWidthQBrich('#" + containerId + "')", 200);
            }
        }
        SetWidthQBrich(containerId: string) {
            var base = this,
                $container = $(containerId);

            if ($container.length !== 0) {
                var $children = $container.find('object, embed, video');

                if ($children.length !== 0) {
                    $children.css(
                        {
                            'width': ($container.attr('data-qbrick-width') + 'px'),
                            'height': ($container.attr('data-qbrick-height') + 'px')
                        });
                    window.clearInterval(base.interval);
                }
            }
            else {
                window.clearInterval(base.interval);
            }
        }
    }
}