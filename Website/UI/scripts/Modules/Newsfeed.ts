﻿module Comprend.Layout
{
	export class NewsfeedBase
	{
		private MainBase: MainBase;
		public pageId: any;
		private iteration: number;
		private take: number;

		constructor(mainBase: MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
		}

		Init()
		{
			var base = this;
			base.InitNewsfeed();
		}

		InitNewsfeed() {
			if ($('.js-newsfeed-container').length !== 0) {
				var base = this;
				var pageId = $('.js-newsfeed-add').data('pageid');
				var iteration = $('.js-newsfeed-add').data('iteration');
				var take = $('.js-newsfeed-add').data('take');
				
				base.getFeed(pageId, iteration, take);
				
				$('.js-newsfeed-add').on('click', function (e)
				{
					iteration = $('.js-newsfeed-add').data('iteration') + 1;
					take = $('.js-newsfeed-add').data('take');
					base.getFeed(null, iteration, take);

					$('.js-newsfeed-add').data('iteration', (iteration));
				});
			}
		}

		public setPageId(pageId: number) {
			this.pageId = pageId;
		}

		public getFeed(id: string, iterations: any, take: any) {
			var base = this;
			base.pageId = id == null ? base.pageId : id;
			base.iteration = iterations == null ? base.iteration : iterations;
			base.take = take == null ? base.take : take;
			
			var params = { id: base.pageId, iterations: base.iteration, count: base.take };

			jQuery.support.cors = true;
			$.ajax({
				url: '/webapi/news/',
				type: 'GET',
				dataType: 'json',
				data: params,
				success: function (response) {
					for (var i = 0; i < response.length; i++) {
						base.addNewsItem(response[i]);
					}

					$('.js-newsfeed-container').find('li:hidden').show(300);
					if (response.length == 0) {
						$('.js-newsfeed-add').hide(600);
					}
				},
				error: function (x, y, z)
				{
					alert(x + '\n' + y + '\n' + z);
				}
			});

			base.iteration++;
		}
		
		private addNewsItem(item) {
			
			//<li>
			//	<a href = "<%# Eval("LinkUrl") %>" title = "<%#Eval("Title").ToString() %>" >
			//		<div class="figure" >
			//			<Common:ResponsiveImages ID = "ListImage" runat = "server" ImageType = "squaresmall" AllowEdit ="false" / >
			//		< / div >
			//		<div class="teasertext" >
			//			<%# Eval("Title").ToString().WithTag("h2", string.Empty) % >
			//			<p class="lastupdated" ><%# ((DateTime) Eval("Date")).ToString(WebsiteContext.Translate("/common/publishdateformat")) % >< / p >
			//			<%# Eval("Body") % >
			//		< / div >
			//	< / a >
			//< / li>

			//<NewsItem>
			//	<Body><p>Short introduction news item 4 < /p >< / Body >
			//	<ContentType>News < /ContentType >
			//	<Date>2015 - 04 - 28T10:07:22 < /Date >
			//	<ImageAltText i:nil ="true"/ >
			//	<ImageTitleText i:nil ="true"/ >
			//	<ImageUrl/ >
			//	<LinkUrl>http://validoo.local/link/47d85d18f7584cab9576c3d17bbc26d3.aspx?epslanguage=en</LinkUrl>
			//	<SortDate>0001 - 01 - 01T00:00:00 < /SortDate >
			//	<Title>News item 4 < /Title >
			//< / NewsItem>

			var ul = $('.js-newsfeed-container');

			var li = $('<li>');
			li.hide();

			var a = $('<a>');
			a.attr('title', item.Title);
			a.attr('Href', item.LinkUrl);
			
			if (item.ImageUrl != null && item.ImageUrl != '')
			{
				var divImg = $('<div>');
				divImg.attr('class', 'figure');
				
				var img = $('<img>');
				img.attr('src', item.ImageUrl);
				img.attr('title', item.Title);

				divImg.append(img);
				a.append(divImg);
			}

			var divText = $('<div>');
			divText.attr('class', 'teasertext');
			var h2 = $('<h2>');
			h2.text(item.Title);
			divText.append(h2);

			var pTime = $('<p>');
			pTime.attr('class', 'lastupdated');
			pTime.text(item.Date);
			divText.append(pTime);

			divText.append(item.Body);
			a.append(divText);

			li.append(a);

			ul.append(li);
		}
	}
} 