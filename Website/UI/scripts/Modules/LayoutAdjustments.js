﻿/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../main.ts" />
var Comprend;
(function (Comprend) {
    (function (LayoutAdjustments) {
        var Base = (function () {
            function Base(mainBase) {
                //Before onload
                var base = this;
                base.MainBase = mainBase;
            }
            Base.prototype.Init = function () {
                //After onload
                //Load all additional modules here
                var base = this;
				
               base.AccessibilityAdjustments();
            };

            /* ===================== Accessibility adjustments =====================*/
            //#region AccessibilityAdjustments
            Base.prototype.AccessibilityAdjustments = function () {
                var base = this;

                $("a[href^='#']").on('click', function () {
                    $("#" + $(this).attr("href").slice(1) + "").focus();
                });
            };
            return Base;
        })();
        LayoutAdjustments.Base = Base;
    })(Comprend.LayoutAdjustments || (Comprend.LayoutAdjustments = {}));
    var LayoutAdjustments = Comprend.LayoutAdjustments;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=LayoutAdjustments.js.map
