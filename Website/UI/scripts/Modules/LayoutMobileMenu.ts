﻿/// <reference path="../definitions/iscroll-5-lite.d.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
module Comprend.Layout
{
	export class MobileMenuBase
	{
		private MainBase: MainBase;
		private mobilemenuSelector = '.js-mainmenu';
		private $mobilemenu: JQuery = null;
		private iScroll: any = null;

		constructor(mainBase: MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
		}

		Init()
		{
			var base = this;

			base.$mobilemenu = $(base.mobilemenuSelector);
			base.addEvents();
			base.InitMobileMenu();

			$.getScript('/UI/scripts/Vendor/iscroll-lite.js', $.proxy(base.onGetIscroll, base));
		}

		private addEvents()
		{
			var base = this;
			$(window).on('orientationchange', $.proxy(base.onOrientationChange, base));
		}

		private onOrientationChange()
		{
			var base = this;
			//base.initiateIScroll();
			if (base.iScroll !== null)
			{
				//alert('refresh');
				//base.iScroll.refresh();
				setTimeout(function () {
					base.iScroll.refresh();
				}, 100);
			}
		}

		private onGetIscroll()
		{
			var base = this;
			base.initiateIScroll();
		}
		private initiateIScroll()
		{
			var base = this;
			base.iScroll = new IScroll(base.mobilemenuSelector,
				{
					mouseWheel: true,
					scrollbars: true,
					click: true
				});
		}

		InitMobileMenu()
		{

			if ($('.js-mainmenu').length !== 0)
			{
				var base = this;
				
				//$('a.expand').hover(e => {
				//	console.log($(e).val());
				//	var itemLevel = $(e).data('child');
					
				//	//nav-list-2
					
				//});
				var $dataPath = $('li.active').attr('data-path');
				if ($dataPath && $dataPath.length !== 0) {
					var $dataPathArray = $dataPath.split(',');

					$.each($dataPathArray, function (i, val) {
						if (val != '5') {
							$('li[data-contentid=' + val + ']').addClass('semi-active');
							$('ul[data-parent=' + val + '], li[data-contentid=' + val + '] > a.expand').addClass('is-open');
						}
					});
				}
			}
			$('.js-toggelmobilemenu').on('click', function (e) {
				e.preventDefault();
				$('.mobile.js-mainmenu').toggleClass('is-open');
			});
			$('.mobile.js-mainmenu a.expand').on('click', function (e) {
				var $dataContentid = $(this).parent('li').attr('data-contentid');
				var $clickedLi = $(this).parent('li');

				$(this).toggleClass('is-open');
				$('ul[data-parent='+$dataContentid+']').toggleClass('is-open');
			});
		}
	}
}