﻿/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Main.ts" />

module Comprend.Layout
{
    export class SearchBase
	{
        private MainBase: MainBase;
        private requestUrl: string = "/webapi/search";
        private loadMoreSelector: string = ".js-loadmore";
        private searchListSelector: string = ".js-searchresult";

        //Data attributes
        private dataNextPage: string = "nextpage";
        private dataPageSize: string = "pagesize";
        private dataQuery: string = "query";
        private dataNuumberOfHits: string = "numberofhits";
        private dataLanguage: string = "language";


        constructor(mainBase: MainBase)
        {
            //We expect this method to be run after document ready
            var base = this;
            base.MainBase = mainBase;
        }

        Init()
        {
            var base = this,
                $loadMoreButtons = $(base.loadMoreSelector);

            if ($loadMoreButtons.length !== 0)
            {
                $loadMoreButtons.on('click', $.proxy(base.onLoadMoreButtonClick, base));
            }
        }

        private onLoadMoreButtonClick(e)
        {
            e.preventDefault();
            var base = this;
            var $loadMoreButton = $(e.currentTarget);

            var nextPage = $loadMoreButton.data(base.dataNextPage),
                pageSize = $loadMoreButton.data(base.dataPageSize),
                query = $loadMoreButton.data(base.dataQuery),
                lang = $loadMoreButton.data(base.dataLanguage),
                numberOfHits = $loadMoreButton.data(base.dataNuumberOfHits);

            var params = { Query: query, Language: lang, PageSize: pageSize, Offset: nextPage };

            base.doSearch(params);

            if (nextPage * pageSize >= numberOfHits)
            {
                $loadMoreButton.hide();
            }

            $loadMoreButton.data(base.dataNextPage, nextPage + 1);
        }

        private doSearch(params)
        {
            var base = this;
            $.ajax({
                url: base.requestUrl,
                type: 'POST',
                dataType: 'json',
                data: params,
                success: function (response) {
                    if (response.length !== 0) {
                        base.handleResponse(response);
                    }
                },
                error: function (x, y, z) {
                    alert(x + '\n' + y + '\n' + z);
                }
            });
        }

        private handleResponse(response)
        {
            var base = this;
            for (var i = 0; i < response.length; i++)
            {
                base.addItem(response[i]);
            }
            $(base.searchListSelector).find('li:hidden').show(300);
        }

        private addItem(item)
        {
            var base = this;

            var ul = $(base.searchListSelector),
                li = $('<li>'),
                h3 = $('<h3>'),
                p = $('<p>'),
                a = $('<a>');

            li.hide();

            h3.append(item.Title);
            p.append(item.Snippet);
            a.append(item.Url);
            a.attr('Href', item.Url);
            li.append(h3);
            li.append(p);
            li.append(a);
            ul.append(li);
        }
    }
}