﻿var Comprend;
(function (Comprend) {
    (function (Layout) {
        var AccordionBase = (function () {
            function AccordionBase(mainBase) {
                //We expect this method to be run after document ready
                var base = this;
                base.MainBase = mainBase;
            }
            AccordionBase.prototype.Init = function () {
                var base = this;
                base.InitAccordion();
            };

            AccordionBase.prototype.InitAccordion = function () {
                if ($('.js-accordion-container').length !== 0) {
                    var timeMS = 300;
                    $('.js-accordion-item-selector').each(function (index) {
                        $(this).on('click', function () {
                            var li = $(this).closest('li');
                            li.find(".accordion-item-container").slideToggle(timeMS);
                            if (li.hasClass('is-opened')) {
                                li.removeClass('is-opened');
                                li.addClass('is-closed');
                                setTimeout(function () {
                                    li.find(".js-accordion-item-selector").removeClass('bottom-divider');
                                }, timeMS);
                            } else {
                                li.removeClass('is-closed');
                                li.addClass('is-opened');
                                li.find(".js-accordion-item-selector").addClass('bottom-divider');
                            }
                        });
                    });
                }
            };
            return AccordionBase;
        })();
        Layout.AccordionBase = AccordionBase;
    })(Comprend.Layout || (Comprend.Layout = {}));
    var Layout = Comprend.Layout;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=LayoutAccordion.js.map
