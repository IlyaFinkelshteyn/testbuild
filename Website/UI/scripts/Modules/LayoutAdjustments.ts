/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../main.ts" />


module Comprend.LayoutAdjustments
{
	export class Base
	{
		private MainBase: MainBase;

		constructor(mainBase: MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
		}

		public Init()
		{
			//After onload
			//Load all additional modules here
			var base = this;

			base.AccessibilityAdjustments();
		}

		/* ===================== Accessibility adjustments =====================*/
		//#region AccessibilityAdjustments
		public AccessibilityAdjustments()
		{
			var base = this;

			$("a[href^='#']").on('click', function ()
			{
				$("#" + $(this).attr("href").slice(1) + "").focus();
			});
		}
		//#endregion
	}
}