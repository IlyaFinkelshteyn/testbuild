﻿module Comprend.Layout
{
    export class AccordionBase {
        private MainBase: MainBase;

        constructor(mainBase: MainBase) {
            //We expect this method to be run after document ready
            var base = this;
			base.MainBase = mainBase;
		}

        Init()
        {
            var base = this;
            base.InitAccordion();
        }

        InitAccordion() {
            if ($('.js-accordion-container').length !== 0) {
				var timeMS = 300;
                $('.js-accordion-item-selector').each(function (index) {
                    $(this).on('click', function () {
                        var li = $(this).closest('li');
                        li.find(".accordion-item-container").slideToggle(timeMS);
                        if (li.hasClass('is-opened')) {
                            li.removeClass('is-opened');
                            li.addClass('is-closed');
							setTimeout(function () {
								li.find(".js-accordion-item-selector").removeClass('bottom-divider');
							}, timeMS);
							
                        }
                        else {
                            li.removeClass('is-closed');
                            li.addClass('is-opened');
							li.find(".js-accordion-item-selector").addClass('bottom-divider');
                        }
                    });
                });
            }
        }
    }
}