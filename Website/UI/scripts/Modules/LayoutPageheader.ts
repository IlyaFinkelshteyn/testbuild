﻿module Comprend.Layout
{
    export class PageheaderBase {
        private MainBase: MainBase;

        constructor(mainBase: MainBase) {
            //We expect this method to be run after document ready
            var base = this;
			base.MainBase = mainBase;
		}

        Init()
		{
            var base = this;
            base.InitPageheader();
        }

        InitPageheader() {
            //sticky main menu
			var offset = $('body').offset();
			$(window).scroll(function () {
				if ($(this).scrollTop() > offset.top + 29) {
					$('body').find('.pageheader').addClass('sticky');
				} else {
					$('body').find('.pageheader').removeClass('sticky');
				}
				if ($(this).scrollTop() > offset.top + 33) {
					$('body').find('.pageheader').addClass('animated');
				} else {
					$('body').find('.pageheader').removeClass('animated');
				}
			});
			$('body').find('.js-toggleheadersearch, .pageheader-searchclose').on('click', function (e) {
				e.preventDefault();
				$('body').find('.pageheader-headersearch').toggleClass('is-open');
			});
        }
    }
}