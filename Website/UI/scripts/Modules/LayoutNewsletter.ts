﻿module Comprend.Layout
{
    export class NewsletterBase {
        private MainBase: MainBase;

        constructor(mainBase: MainBase) {
            //We expect this method to be run after document ready
            var base = this;
			base.MainBase = mainBase;
        }

        Init()
		{
            var base = this;
            base.InitNewsletter();
        }

        InitNewsletter() {
            //Validate email
            $('.js-email').on('change blur keyup', validate);

            $('.newsletterblock .button.green').on('click', function (e) {
                e.preventDefault();
                var contentId = $(this).data('contentid');
                var emailAddress = $('.js-email').val();

                var params = { ContentId: contentId, EmailAddress: emailAddress };

                $.ajax({
                    url: '/webapi/NewsLetter/',
                    type: 'POST',
                    dataType: 'json',
                    data: params,
                    success: function (response) {
                        if (response) {
                            sendSuccess();
                        }
                        else {
                            sendFail();
                        }
                    },
                    error: function () {
                        sendFail();
                    }
                });
            });

            function validate() {
                var $theInput = $('.js-email');
                validateEmail($theInput);
                if ($('.js-email').hasClass('valid')) {
                    $('.newsletterblock .button.gray').css('display', 'none');
                    $('.newsletterblock .button.green').css('display', 'inline-block');
                } else {
                    $('.newsletterblock .button.gray').css('display', 'inline-block');
                    $('.newsletterblock .button.green').css('display', 'none');
                }
            }

			function sendSuccess() {
				$('.newsletterblock .button.gray').css('display', 'none');
				$('.newsletterblock .button.green').css('display', 'none');
				$('.newsletterblock .button.success').css('display', 'inline-block');
				$('.newsletterblock .button.fail').css('display', 'none');
			}

			function sendFail() {
				$('.newsletterblock .button.gray').css('display', 'none');
				$('.newsletterblock .button.green').css('display', 'none');
				$('.newsletterblock .button.success').css('display', 'none');
				$('.newsletterblock .button.fail').css('display', 'inline-block');
            }

            $('.newsletterblock .button.success, .newsletterblock .button.fail').on('click', function (theInput) {
                $('.js-email').off('change blur keyup', validate);
                $('input.newsletterblok-email').removeClass("error");
                $('input.newsletterblok-email').removeClass("valid");
                $('input.newsletterblok-email').val('');
                $('.newsletterblock .button.gray').css('display', 'inline-block');
                $('.newsletterblock .button.green').css('display', 'none');
                $('.newsletterblock .button.success').css('display', 'none');
                $('.newsletterblock .button.fail').css('display', 'none');
                $('.js-email').on('change blur keyup', validate);
            });

			function validateEmail(theInput) {
				//testing regular expression
				var theInputValue = theInput.val();
				var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
				//if it's valid email
				if (filter.test(theInputValue)) {
					theInput.removeClass("error");
					theInput.addClass("valid");
					theInput.siblings('p').removeClass('error');
					return true;
				}
				//if it's NOT valid
				else {
					theInput.addClass("error");
					theInput.removeClass("valid");
					theInput.siblings('p').addClass('error');
					return false;
				}
			}
        }
    }
}