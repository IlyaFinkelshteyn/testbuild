/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.SearchHandler
{
	export class Base
	{
		private MainBase : MainBase;
		private LangHand : Comprend.Utilities.LanguageHandlerBase;

		private searchBoxes : JQuery;

		constructor(mainBase : MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
		}

		public Init()
		{
			//After onload
			//Load all additional modules here
			
			var base = this;
			
			base.searchBoxes = $("input.searchfield");
			if(base.searchBoxes.length !== 0)
			{
				base.InitFocusBlur();
				base.AddEvents();
			}
		}

		//#region event handlers

		AddEvents()
		{
			var base = this;

			//Add keydown event for searchboxes
			base.searchBoxes.on('keydown',$.proxy(base.onKeyDown,base));

		}

		onKeyDown(e:JQueryEventObject)
		{
			var base = this,
				$target = $(e.target);

			if($target.length !== 0)
			{
				if(e.keyCode && e.keyCode === 13)
				{
					$('form:eq(0)').trigger('submit');
				}
			}
		}

		//#endregion

		//#region Focus/Blur functionality
		InitFocusBlur()
		{
			var base = this;

			base.searchBoxes.each(function(i,searchbox)
			{
				var $searchbox = $(searchbox),
					initiallabel = $searchbox.val(),
					boxId = $searchbox.attr('id'),
					relatedLabel = $searchbox.parent().find("label[for='"+ boxId +"']"),
					relatedLabelText = "";

				if(relatedLabel.length !== 0)
				{
					relatedLabelText = relatedLabel.eq(0).html();
				}

				$searchbox
					.focus(function()
					{
						if (this.value === initiallabel && initiallabel === relatedLabelText)
						{
							this.value = '';
						}
					})
					.blur(function()
					{
						if (this.value === '')
						{
							this.value = initiallabel;
						}
					});
			});
		}

		//#endregion

	}
}