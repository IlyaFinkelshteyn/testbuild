/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Definitions/google.jsapi.d.ts" />


module Comprend.GoogleChart
{
	export class Base
	{
		private MainBase : MainBase;
		private Utils : Comprend.Utilities.Base;
		private google : IGoogle;
		private options = { chartselector : '.googlechartblock' };
		private $chartContainers : JQuery = $();
        private colorScale = ['#b3dadf', '#8cb7c8', '#659fba', '#698eb8', '#165f82', '#0f4159'];

		constructor(mainBase : MainBase)
		{
			//Onload
			var base = this;
			base.MainBase = mainBase;
			base.Utils = base.MainBase.Utilities;
			base.google = window["google"];
            base.$chartContainers = $(base.options.chartselector);
			base.CheckForCharts();
		}

		CheckForCharts()
		{
			var base = this;
            
			if(base.$chartContainers.length !== 0)
			{
				//base.google.setOnLoadCallback($.proxy(base.InitiateCharts, base));
				base.InitiateCharts();
			}
		}

		InitiateCharts()
		{
            var base = this;

			base.$chartContainers.each(function()
			{
				var $chart = $(this),
                    strChartType = $chart.find('table').attr('data-chart-type');

				switch(strChartType)
				{
					case 'columnchart':
						base.InitiateColumnChart($chart);
						break;
					case 'piechart':
						base.InitiatePieChart($chart);
						break;
					case 'linechart':
						base.InitiateLineChart($chart);
						break;
					case 'combochart':
						base.InitiateComboChart($chart);
						break;
					default:
						break;
				}
			});
		}

		ReadData($targetTable : JQuery, targetNewChart, targetRow) {

			$targetTable.find('tr').each(function() {
					
				var $tr = $(this);
					
				$tr
					//get titles
					.find('th')
						.each(function()
						{
							targetRow.push(this.innerHTML);
						})
						.end()
					//get data rows
					.find('td')
						.each(function(i,item : HTMLElement)
						{
							var innerHTML : any = item.innerHTML;
							innerHTML = (i === 0) ? innerHTML : parseFloat(innerHTML);
							if(typeof innerHTML === "number" && $.isNumeric(innerHTML) === false)
							{
								innerHTML = 0;
							}
							targetRow.push(innerHTML);
						});
				
				if(targetRow.length > 0) {
					targetNewChart.push(targetRow);
				}
				//clear 
				targetRow = [];
			});
		}

		InitiateColumnChart($chart : JQuery)
		{
			var base = this;
			var currChart;
			var newChartData = [];
			var currRow = [];
            var chartId;
            var colorScale = ['#B4EEB4', '#7CCD7C', '#71C671', '#548B54', '#228B22', '#215E21'];

			currChart = $chart.find('.googlechartdata');
			chartId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			$chart.find('.chart-content').attr('id',chartId);
			
			base.ReadData(currChart, newChartData, currRow);

			var data = google["visualization"].arrayToDataTable(newChartData);

			var options = {
				title: currChart.data('chart-title'),
				hAxis: {title: currChart.data('haxis-title')},
				vAxis: {title: currChart.data('vaxis-title')},
                colors: base.colorScale,
				fontName: 'Arial',
				fontSize: '14px',
				legend: {position: 'bottom', alignment: 'start'},
				tooltip: {textStyle: {bold: false}}
			};

			var chart = new google["visualization"].ColumnChart(document.getElementById(chartId));

			chart.draw(data, options);
		}

		InitiatePieChart($chart : JQuery)
		{
			var base = this;
			var currChart;
			var newChartData = [];
			var currRow = [];
            var chartId;
            var colorScale = ['#B4EEB4', '#7CCD7C', '#71C671', '#548B54', '#228B22', '#215E21'];

			currChart = $chart.find('.googlechartdata');
			chartId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			$chart.find('.chart-content').attr('id',chartId);
			
			base.ReadData(currChart, newChartData, currRow);

			var data = google["visualization"].arrayToDataTable(newChartData);

			var options = {
				title: currChart.data('chart-title'),
				hAxis: {title: currChart.data('hAxis-title'), titleTextStyle: {color: 'black'}},
				vAxis: {title: currChart.data('vAxis-title'), titleTextStyle: {color: 'black'}},
                colors: base.colorScale,
				fontName: 'Arial',
				fontSize: '14px',
				pieSliceText: 'none',
				pieSliceBorderColor: 'none',
				legend: {position: 'right'},
				tooltip: {textStyle: {bold: false}}
			};

			var chart = new google["visualization"].PieChart(document.getElementById(chartId));
				
			chart.draw(data, options);
		}

		InitiateLineChart($chart : JQuery)
		{
			var base = this;
			var currChart;
			var newChartData = [];
			var currRow = [];
            var chartId;

			currChart = $chart.find('.googlechartdata');
			chartId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			$chart.find('.chart-content').attr('id',chartId);

			base.ReadData(currChart, newChartData, currRow);

			var data = google["visualization"].arrayToDataTable(newChartData);

			var options = {
				title: currChart.data('chart-title'),
				hAxis: {title: currChart.data('hAxis-title')},
				vAxis: {title: currChart.data('vAxis-title')},
                colors: base.colorScale,
				fontName: 'Arial',
				fontSize: '14px',
				legend: {position: 'bottom', alignment: 'start'},
				tooltip: {textStyle: {bold: false}}
			};

			var chart = new google["visualization"].LineChart(document.getElementById(chartId));
				
			chart.draw(data, options);
		}

		InitiateComboChart($chart : JQuery)
		{
			var base = this;
			var currChart;
			var newChartData = [];
			var currRow = [];
            var chartId;

			currChart = $chart.find('.googlechartdata');
			chartId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			$chart.find('.chart-content').attr('id',chartId);
			
			base.ReadData(currChart, newChartData, currRow);

			var data = google["visualization"].arrayToDataTable(newChartData);

			var options = {
				title: currChart.data('chart-title'),
				hAxis: {title: currChart.data('hAxis-title')},
				vAxis: {title: currChart.data('vAxis-title')},
                colors: base.colorScale,
				fontName: 'Arial',
				fontSize: '14px',
				legend: {position: 'bottom', alignment: 'start'},
				seriesType: "bars",
				series: {5: {type: "line"}},
				tooltip: {textStyle: {bold: false}}
			};

			var chart = new google["visualization"].ComboChart(document.getElementById(chartId));
				
			chart.draw(data, options);
		}

	}
}