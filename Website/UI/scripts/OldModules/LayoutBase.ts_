/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Definitions/jquery.tabs.d.ts" />
/// <reference path="../Definitions/jquery.easyPaginate.d.ts" />
/// <reference path="../Definitions/jquery.communicateArea.d.ts" />
/// <reference path="../Definitions/jquery.imagesloaded.d.ts" />
/// <reference path="../Definitions/jquery.actual.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../OldModules/LayoutTooltip.ts" />
/// <reference path="../Modules/LayoutVideo.ts" />
/// <reference path="../OldModules/LayoutMainMenu.ts" />
/// <reference path="../OldModules/LayoutCompactMode.ts" />
/// <reference path="../OldModules/LayoutImageslider.ts" />
/// <reference path="../OldModules/GoogleChartBase.ts" />
/// <reference path="../OldModules/InfoMapBase.ts" />
/// <reference path="../OldModules/LayoutExternalLinks.ts" />
/// <reference path="../OldModules/LayoutIframeHandler.ts" />
/// <reference path="../Main.ts" />

module Comprend.Layout
{
	export class Base
	{
		private MainBase: MainBase;
		public CompactMode: CompactModeBase;
		public MainMenu: MainMenuBase;
		public Video: VideoBase;
		private Tooltip: TooltipBase;
		public LangHand: Comprend.Utilities.LanguageHandlerBase;
		public ExternalLinks: Comprend.Layout.ExternalLinks;
		private IframeHandler: Comprend.Layout.IframeHandler;
		private AntiSpam: Comprend.Layout.AntiSpam;

		constructor(mainBase: MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
		}

		public Init()
		{
			//After onload
			//Load all additional modules here

			var base = this;

			base.MainMenu = new MainMenuBase();
			base.Video = new VideoBase(base.MainBase);
			base.Tooltip = new TooltipBase(base.MainBase);
			base.CompactMode = new CompactModeBase(base.MainBase);
			base.ExternalLinks = new Comprend.Layout.ExternalLinks(base.MainBase);
			base.IframeHandler = new Comprend.Layout.IframeHandler(base.MainBase);
			base.AntiSpam = new Comprend.Layout.AntiSpam(base.MainBase);

			base.AccessibilityAdjustments();
			base.getAndInitiatePlugins();
			base.GetPagination();
			base.ieDetect();
			base.extras();
		}

		/* ===================== Extras =====================*/
		public extras()
		{
			var base = this;

			$(document).ready(function ()
			{

				//testing
				//var dpi = 1;
				//if (window.devicePixelRatio > dpi) {
				//	$('body').addClass('high-dpi');
				//	$('body').append('high-dpi');
				//}
			});
		}

		/* ===================== Initiate plugins when required  getAndInitiatePlugins =====================*/
		//#region getAndInitiatePlugins
		public getAndInitiatePlugins()
		{
			var base = this;
			/* === Common plugins === */
			base.ExpandableList($('.expandablelist'));

			/* === Dependency injection plugins === */

			// Image slider init
			if ($('.carouselcontainerblock').length !== 0)
			{
				var $carouselBlocks = $('.carouselcontainerblock');

				if (typeof Comprend.Layout.ImageSliderBase === 'function')
				{
					//console.log(Comprend.Layout.ImageSliderBase)
					var imageslider = new Comprend.Layout.ImageSliderBase($carouselBlocks);
				}
			}

			// Google chart init
			if ($('.googlechartblock').length !== 0)
			{
				if (typeof Comprend.GoogleChart !== "undefined" || typeof Comprend.GoogleChart.Base === 'function')
				{
					var GoogleChartBase = new Comprend.GoogleChart.Base(base.MainBase);
				}

			}

			// jquery jvectormap lib
			if ($('.infomapblock').length !== 0)
			{
				if (typeof Comprend.InfoMap !== "undefined" || typeof Comprend.InfoMap.Base === 'function')
				{
					var infomap = new Comprend.InfoMap.Base(base.MainBase);
				}
			}

			// jquery accessiblee tabs init 
			if ($('.tabs').length !== 0)
			{
				$.getScript('/UI/scripts/Plugins/jquery.tabs.min.js', $.proxy(function ()
				{

					$(".tabs").accessibleTabs({
						wrapperClass: 'content', /* Classname to apply to the div that is wrapped around the original Markup */
						currentClass: 'current', /* Classname to apply to the LI of the selected Tab */
						tabhead: 'h4', /* Tag or valid Query Selector of the Elements to Transform the Tabs-Navigation from (originals are removed) */
						tabheadClass: '', /* Classname to apply to the target heading element for each tab div */
						tabbody: 'h4 + div', /* Tag or valid Query Selector of the Elements to be treated as the Tab Body */
						fx: 'show', /* can be "fadeIn", "slideDown", "show" */
						fxspeed: 0, /* speed (String|Number): "slow", "normal", or "fast") or the number of milliseconds to run the animation */
						currentInfoText: 'current tab: ', /* text to indicate for screenreaders which tab is the current one */
						currentInfoPosition: 'prepend', /* Definition where to insert the Info Text. Can be either "prepend" or "append" */
						currentInfoClass: 'current-info', /* Class to apply to the span wrapping the CurrentInfoText */
						tabsListClass: 'tabs-list', /* Class to apply to the generated list of tabs above the content */
						syncheights: false, /* syncs the heights of the tab contents when the SyncHeight plugin is available http:/*blog.ginader.de/dev/jquery/syncheight/index.php */
						syncHeightMethodName: 'syncHeight', /* set the Method name of the plugin you want to use to sync the tab contents. Defaults to the SyncHeight plugin: http:/*github.com/ginader/syncHeight */
						cssClassAvailable: false, /* Enable individual css classes for tabs. Gets the appropriate class name of a tabhead element and apply it to the tab list element. Boolean value */
						saveState: true, /* save the selected tab into a cookie so it stays selected after a reload. This requires that the wrapping div needs to have an ID (so we know which tab we're saving) */
						autoAnchor: true, /* will move over any existing id of a headline in tabs markup so it can be linked to it */
						pagination: false, /* adds buttons to each tab to switch to the next/previous tab */
						position: 'top', /* can be 'top' or 'bottom'. Defines where the tabs list is inserted.  */
						wrapInnerNavLinks: '', // inner wrap for a-tags in tab navigation. See http://api.jquery.com/wrapInner/ for further informations */
						firstNavItemClass: 'first', /* Classname of the first list item in the tab navigation */
						lastNavItemClass: 'last', /* Classname of the last list item in the tab navigation */
						clearfixClass: '' /* Name of the Class that is used to clear/contain floats */
					});

				}, this));
			}
		}
		//#endregion

		/* ===================== Pagination =====================*/
		//#region Pagination

		public GetPagination()
		{

			//script loaded dynamically in code behind of template
			//to load pagination add class "page-this" to item list parent, eg."ul"
			var base = this;
			var pageTarget = $('.page-this');
			var pageStep = 5; //items per page

			if (pageTarget.length !== 0)
			{
				pageTarget.each(function ()
				{
					console.log('paged');
					$(this).easyPaginate({
						step: pageStep,
						delay: 0,
					});

				});
			}
		}
		//#endregion

		/* ===================== Accessibility adjustments =====================*/
		//#region AccessibilityAdjustments

		public AccessibilityAdjustments()
		{
			var base = this;

			$(document).ready(function ()
			{
				$("a[href^='#']").click(function ()
				{
					$("#" + $(this).attr("href").slice(1) + "").focus();
				});
			});
		}
		//#endregion

		/* ===================== ExpandableList =====================*/
		//#region ExpandableList

		public ExpandableList($targets: JQuery)
		{
			var base = this;

			if (typeof $targets !== "undefined" && $targets.length !== 0)
			{
				console.log('found an expandable list');
				$targets.addClass('initiated');
			}
		}
		//#endregion

		/* ===================== Equal Heights =====================*/
		//#region equal heights
		//This script required jquery.actual
		public equalHeights($targets: JQuery, subtarget?: string)
		{
			var base = this;

			if (typeof $.fn.actual !== 'function')
			{
				$.getScript('/UI/scripts/Plugins/jquery.actual.min.js', $.proxy(base.equalHeights, this, $targets, subtarget));
				return;
			}

			var currentTallest = 0,
				currentRowStart = 0,
				rowDivs = [],
				topPosition = 0;

			$targets.each(function ()
			{

				var $el = $(this);
				topPosition = $el.position().top;

				if (currentRowStart !== topPosition)
				{

					// we just came to a new row.  Set all the heights on the completed row
					for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++)
					{
						rowDivs[currentDiv].height(currentTallest);
					}

					// set the variables for the new row
					rowDivs.length = 0; // empty the array
					currentRowStart = topPosition;
					currentTallest = $el.height();
					rowDivs.push($el);

				} else
				{

					// another div on the current row.  Add it to the list and check if it's taller
					rowDivs.push($el);
					currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
				}

				// do the last row
				for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++)
				{
					rowDivs[currentDiv].height(currentTallest);
				}

			});
		}
		//#endregion

		public ieDetect()
		{
			//set classes for versions of ie

			var base = this;

			getIE();

			if (getIE() < 10)
			{
				//these are oldies
			}

			function getIE()
			{
				var thisBrowser = navigator.userAgent, rootElem = $('html');

				// IE 11 is not triggered by this	
				if (thisBrowser.indexOf('MSIE') > 1)
				{
					rootElem.addClass('ie');

					if (thisBrowser.indexOf('MSIE 7') > 1)
					{
						rootElem.addClass('ie7');
						return 7;
					}
					else if (thisBrowser.indexOf('MSIE 8') > 1)
					{
						rootElem.addClass('ie8');
						return 8;
					}
					else if (thisBrowser.indexOf('MSIE 9') > 1)
					{
						rootElem.addClass('ie9');
						return 9;
					}
					else if (thisBrowser.indexOf('MSIE 10') > 1)
					{
						rootElem.addClass('ie10');
						return 10;
					}
				}
			}
		}
	}
}