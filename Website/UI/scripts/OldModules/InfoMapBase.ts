/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Definitions/jquery.jvectormap.d.ts" />

module Comprend.InfoMap
{
	export class Base
	{
		private MainBase: MainBase;
		private InfoMapContainer: JQuery;

		constructor(mainBase: MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
			base.Init();
		}

		public Init()
		{
			var base = this;
			var background = '#fff';
			var empty = '#e4e5ea';
			var active = '#5b8331';
			var inactive = '#7ea83b';
			var height = 540;
			var width = 540;

			//switcher
			$('.map-switcher li').on('click', function ()
			{
				var currentTab = $(this);
				var target = currentTab.data('target-map');
				$('.info-window').addClass('visuallyhidden');
				$('.map-switcher li').removeClass('selected');
				currentTab.addClass('selected');
				$('.info-map-container').removeClass('selected').hide();
				$('#' + target).addClass('selected').show();
			});

			//info window
			$('.bubble a').on('click', function ()
			{
				var currInfo = $(this).parent().data('identifier');
				var target = $('.info-map-container').find('.info-window[data-identifier=' + currInfo + ']');
				$('.info-window').addClass('visuallyhidden');
				target.removeClass('visuallyhidden');
				return false;
			});

			//info close
			$('.info-window').each(function ()
			{
				var currBubble = $(this);
				currBubble.append('<span class="close"></span>');
				currBubble.find('.close').on('click', function ()
				{
					currBubble.addClass('visuallyhidden');
				});
			});

			$('.info-map-container').each(function ()
			{
				var $map = $(this);
				var regionsData = {};
				var $iregions = $map.data('inactive-regions');
				if ($iregions !== undefined)
				{
					var inactiveregionsArray = $iregions.split(',');
					for (var i = 0; i < inactiveregionsArray.length; i++)
					{
						regionsData[inactiveregionsArray[i]] = "inactive";
					}
				}

				var $regions = $map.data('active-regions');
				if ($regions !== undefined)
				{
					var activeregionsArray = $regions.split(',');
					for (var j = 0; j < activeregionsArray.length; j++)
					{
						regionsData[activeregionsArray[i]] = "active";
					}
				}

				$map.find('.vector-img').vectorMap({
					map: 'europe_mill_en',
					focusOn: {
						x: 0.56,
						y: 0.3,
						scale: 1.5
					},
					backgroundColor: background,
					zoomOnScroll: false,
					series: {
						regions: [{
							values: regionsData,
							scale: {
								"active": active,
								"inactive": inactive
							}
						}]
					},
					regionStyle: {
						initial: {
							fill: empty
						}
					},
					onRegionLabelShow: function (event, code)
					{
						event.preventDefault();
					},
					onRegionClick: function (event, code)
					{
						event.preventDefault();
					}
				});
			});

			//after load hide non-active maps
			$('.info-map-container').not('.selected').hide();
		}
	}
}