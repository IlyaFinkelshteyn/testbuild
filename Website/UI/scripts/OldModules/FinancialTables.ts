/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.FinancialTables
{
	export class Base
	{
		private MainBase : MainBase;
		
		private tableContainer : JQuery;

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
			//base.Init();
		}

		Init()
		{
            var base = this;

			base.tableContainer = $('#financialtables');
			
			//check if table exists in page template
			if (base.tableContainer.length !== 0)
			{
				var barMaxHeight = 100;
				base.initDrilldown();
				base.initInlineChart(barMaxHeight, 'mouseenter');
				base.initFixedScroll(barMaxHeight);
				base.initExtras();
				//base.initCurrencyConverter();
			}
		}

		initDrilldown()
		{
			var base = this,
				$rows = base.tableContainer.find('tr.lev1');

			$rows
				.children('th')
					.wrapInner('<span />');
			
			var $btn = $('<a />')
					.addClass('collapsed')
						.click(function(e : JQueryEventObject)
						{
							var $a = $(e.target);
							$a.toggleClass('expanded')
								.closest('tr')
									.nextUntil('tr:not(.lev1)')
									.toggle();

							if($a.closest('table').hasClass('fixedcolumn'))
							{
								var subtable = $a.closest('table').prev().find('table tbody'),
									rowindex = $a.closest('tr').index(),
									tr = subtable.children('tr:nth-child(' + (rowindex + 1) + ')');

								tr
									.nextUntil('tr:not(.lev1)')
										.toggle();
							}
							event.stopPropagation();
						});

			$rows
				.prev(':not(tr.lev1)')
					.children('th')
						.wrapInner($btn);
		}

		/* original */
		initInlineChart(barMaxHeight, trigger) {

			var container = $('div#financialtables');
			if (trigger === 'mouseenter') {
				container.children().mouseleave(function () {
					$(this).find('tr').removeClass('chart');
				});
			}
			container.find('table').addClass('hascharts').children('tbody').children('tr').bind(trigger, function () {
				var tr = $(this);
				if ((trigger !== 'mouseenter') && tr.hasClass('chart')) {
					tr.parent().children('tr').removeClass('chart');
				}
				else {
					tr.parent().children('tr').removeClass('chart');
					// the bars will be visible when the selected-class is applied
					tr.addClass('chart');

					// only add bars if bars has not been added before
					if (tr.find('td span').length === 0) {
						var cells = tr.children('td');
						var getNumericValueOfCell = function (cell) {
							return parseFloat(cell.text().replace(/<[^>]+>/, "").replace(/[,.]/, "").replace("&nbsp;", ""));
						};
						var maxValue = 0;
						cells.each(function () {
							var value = Math.abs(getNumericValueOfCell($(this)));
							if (value && value > maxValue) {
								maxValue = value;
							}
						});
						var stapleHeight = 0;
						var firstvalue = getNumericValueOfCell(cells.first());

						cells.each(function () {
							var cell = $(this);
							var value = getNumericValueOfCell(cell);
							if (!isNaN(value)) {
								var height = Math.floor(barMaxHeight * (value / maxValue));
								var cssClass = 'bar';
								if (height < 0) {
									cssClass += ' negative';
									height = -height;
								}
								var span = $('<span />').addClass('bar');
								
								cell.append('<div class="' + cssClass + '" style="height: ' + height + 'px;"></div>').wrapInner(span);
							}
						});
					}
				}

				// add support for fixed column --------------- 
				var rowindex = tr.closest('tr').index();
				if (tr.closest('table').hasClass('fixedcolumn')) {
					var subtable = tr.closest('table').prev().find('table tbody');
					subtable.children('tr:nth-child(' + (rowindex + 1) + ')').trigger(trigger);
				}
				else if (tr.closest('div').hasClass('scrollable')) {
					var covertable = tr.closest('div.scrollpanel').next().children('tbody');
					covertable.children('tr').removeClass('chart').filter('tr:nth-child(' + (rowindex + 1) + ')')[0].className = tr[0].className;
				}
			});
		}

		//initInlineChart(barMaxHeight : number, trigger : string)
		//{
		//	var base = this;

		//	if (trigger === 'mouseenter')
		//	{
		//		base.tableContainer
		//			.children()
		//			.mouseleave(function()
		//				{
		//					$(this).find('tr').removeClass('chart');
		//				});
		//	}
		//	base.tableContainer
		//		.find('table')
		//			.addClass('hascharts')
		//				.children('tbody')
		//					.children('tr')
		//						.bind(trigger, function(e : JQueryEventObject)
		//						{
		//							var $tr = $(e.target);

		//							if ((trigger !== 'mouseenter') && $tr.hasClass('chart'))
		//							{
		//								$tr
		//									.parent()
		//										.children('tr')
		//											.removeClass('chart');
		//							}
		//							else
		//							{
		//								$tr
		//									.parent()
		//										.children('tr')
		//											.removeClass('chart');

		//								// the bars will be visible when the selected-class is applied

		//								if (!$tr.hasClass('subheading')) { //dont add class to subheadings
		//									$tr.addClass('chart');
		//								}

		//								// only add bars if bars has not been added before
		//								if ($tr.find('td span').length === 0)
		//								{
		//									var cells = $tr.children('td');
		//									var getNumericValueOfCell = function(cell)
		//									{
		//										return parseFloat(cell.text().replace(/<[^>]+>/, "").replace(/[,.]/, "").replace("&nbsp;", ""));
		//									};
		//									var maxValue = 0;
		//									cells.each(function(i,cell)
		//									{
		//										var value = Math.abs(getNumericValueOfCell($(cell)));
		//										if (value && value > maxValue)
		//										{
		//											maxValue = value;
		//										}
		//									});
		//									var stapleHeight = 0;
		//									var firstvalue = getNumericValueOfCell(cells.first());

		//									//cells.each(function(i,cell)
		//									//{
		//									//	var $cell = $(cell),
		//									//		value = getNumericValueOfCell($cell);

		//									//	if (!$.isNumeric(value))
		//									//	{
		//									//		var height = Math.floor(barMaxHeight * (value / maxValue));
		//									//		var cssClass = 'bar';
		//									//		if (height < 0)
		//									//		{
		//									//			cssClass += ' negative';
		//									//			height = -height;
		//									//		}
		//									//		var span = $('<span />').addClass('bar');
		//									//		var change = '';
		//									//		if ($cell.index() > 1)
		//									//		{
		//									//			var unitchange = Math.round(100 * ((firstvalue / value) - 1)),
		//									//				unitchangestring;
		//									//			if (unitchange === 0)
		//									//			{
		//									//				unitchangestring = '&plusmn;0';
		//									//			}
		//									//			else if (unitchange > 0)
		//									//			{
		//									//				unitchangestring = '+' + unitchange;
		//									//			}
		//									//			else
		//									//			{
		//									//				unitchangestring = (unitchange + "");
		//									//			}

		//									//			change = '<span class="change">' + unitchangestring + '%</span>';
		//									//		}
		//									//		$cell.append('<div class="' + cssClass + '" style="height: ' + height + 'px;">' + change + '</div>').wrapInner(span);
		//									//	}
		//									//});
		//									cells.each(function () {
		//										var cell = $(this);
		//										var value = getNumericValueOfCell(cell);
		//										if (!isNaN(value)) {
		//											var height = Math.floor(barMaxHeight * (value / maxValue));
		//											var cssClass = 'bar';
		//											if (height < 0) {
		//												cssClass += ' negative';
		//												height = -height;
		//											}
		//											var span = $('<span />').addClass('bar');
													
		//											cell.append('<div class="' + cssClass + '" style="height: ' + height + 'px;"></div>').wrapInner(span);
		//										}
		//									});
		//								}
		//							}

		//							// add support for fixed column --------------- 
		//							var rowindex = $tr.closest('tr').index();
		//							if ($tr.closest('table').hasClass('fixedcolumn'))
		//							{
		//								var subtable = $tr.closest('table').prev().find('table tbody');
		//								subtable
		//									.children('tr:nth-child(' + (rowindex + 1) + ')')
		//										.trigger(trigger);
		//							}
		//							else if ($tr.closest('div').hasClass('scrollable'))
		//							{
		//								var covertable = $tr.closest('div.scrollpanel').next().children('tbody');
		//								covertable
		//									.children('tr')
		//										.removeClass('chart')
		//											.filter('tr:nth-child(' + (rowindex + 1) + ')')[0].className = $tr[0].className;
		//							}
		//						});
		//}

		initFixedScroll(barMaxHeight : number)
		{
			var base = this;
			base.tableContainer
				.find('table')
					.each(function()
					{
						var origWidth = $(this).parent().width();
						var table = $(this);
						var tablepanel = table.parent().width(10000);
						var scrollpanel = $('<div />').addClass('scrollpanel');
						var scrollarea = $('<div />').addClass('scrollable').width(table.width());
						var newtable = $();

						tablepanel.addClass('tab-pane ');
						tablepanel.append(scrollpanel);
						scrollpanel.append(scrollarea);
						scrollarea.append(table);

						if (table.hasClass('hascharts'))
						{
							// add margin to the table to ensure chart bars are visible even for the top rows
							table.find('th').first().css('padding-top', barMaxHeight - table.children('thead').height() - 20);
						}
						if ($('html').hasClass('ie6') === false)
						{
                            newtable = table.clone(true).addClass('fixedcolumn');
							tablepanel.append(newtable);

							var sourceHeadRows = table.children('thead').children('tr');
							var targetHeadRows = newtable.children('thead').children('tr');
							targetHeadRows.each(function(index)
							{
								var row = $(this);
                                row.css('height', $(sourceHeadRows[index]).height() - 1);
							});
							targetHeadRows.children('th:not(:first-child)').remove();
							newtable.children('tbody').children('tr').children('td').remove();

							$("ul.nav-tabs a[href='#" + tablepanel.attr('id') + "']").on('click', function()
							{
								var id = $(this).attr('href');
								$(id).find('.scrollable').each(function(i, scrollable)
								{
									var $scrollable = $(scrollable);
									window.setTimeout(function()
									{
										$scrollable.width($scrollable.find('table').width());
									}, 200);
								});
							});
						}

						tablepanel.width('');
					});

		}

		initExtras()
		{
			var base = this;
			base.tableContainer.each(function(i,container)
			{
				var $container = $(container),
					tableextras = $container.next(),
					type = 'year';

				$container.find('table:not(.fixedcolumn)').each(function(j,table)
				{
					var $table = $(table),
						$cells = $table.find('td');
					$table
						.children('thead')
							.children('tr:last-child')
								.children('th:not(:first-child)')
									.each(function(k,cell)
									{
										var $cell = $(cell),
											extra = tableextras.children('div#tableextra-' + type + '-' + $cell.index());

										if (extra.length > 0)
										{
											var a = $('<a />').click(function()
											{
												$('div.tooltipabove').remove();
												var pos = $cell.offset();
												//TODO fix dialog functionality
												/*extra.dialog({
													dialogClass: 'tooltipabove',
													draggable: false,
													resizable: false,
													height: 100,
													position: [pos.left - 125, pos.top - 100]
												});*/
												event.stopPropagation();
											});
											$cell.wrapInner(a);
											$cells.filter(':nth-child(' + ($cell.index() + 1) + ')').mouseenter(function()
											{
												a.click();
											});
										}
									});
					type = 'quarter';
				});

				$container
					.find('table.fixedcolumn')
						.mouseenter(function()
						{
							$('div.tooltipabove').remove();
						});
			});
		}
	
		initCurrencyConverter()
		{
			var base = this;
			base.tableContainer.each(function(i,container)
			{
				var $container = $(container);


				$.get('/Scripts/Currencies.xml?rnd=' + Math.random(), function(data)
				{
					var xml = $(data);
					var currencyTools = $('<li />').attr('id', 'currencytools');
					$('<a />')
						.addClass('icon currency')
						.html('Change currency')
						.appendTo(currencyTools);

					var ul = $('<ul />').appendTo(currencyTools);

					var li = $('<li>' + xml.children('currencies').attr('base') + '</li>').appendTo(ul);
					li.click(function()
					{
						var cells = $container.find('td');
						if (cells.first().attr('value') !== undefined)
						{
							cells.each(function()
							{
								var cell = $(this);
								cell.html(cell.attr('value'));
							});
						}
					});
					xml.find('currency').each(function()
					{
						var currency = $(this);
						var currencyValue = parseFloat(currency.attr('value'));
						li = $('<li>' + currency.attr('id') + '</li>');
						li.click(function()
						{
							var cells = $container.find('td');

							// store the original value
							if (cells.first().attr('value') === undefined)
							{
								cells.each(function()
								{
									var cell = $(this);
									cell.attr('value', cell.text());
								});
							}
							
							

							// replace all values
							cells.each(function()
							{
								var cell = $(this);
								if (!$.isNumeric(cell.attr('value')))
								{
									var originalValue = parseFloat(cell.attr('value'));
									var newValue = Math.round(currencyValue * originalValue);
									var valueString = newValue + "";
									cell.html(valueString);
								}
							});
						})
						.appendTo(ul);
					});

					currencyTools.appendTo($('ul#tabletools'));
				});
			});

			
		}
	}
}