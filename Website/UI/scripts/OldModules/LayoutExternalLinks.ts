/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.Layout
{
	export class ExternalLinks
	{
		private MainBase : MainBase;
		private langOpensInNewWindow : string;
		private LanguageHandler : Comprend.Utilities.LanguageHandlerBase;
		private defaultSelector : string = 'a[target=blank],a[target=_blank],a[rel=external]';

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;

			base.LanguageHandler = base.MainBase.Utilities.LanguageHandler;
			base.Init();
		}

		Init()
		{
			var base = this;
			base.CheckForLinks();
		}

		public CheckForLinks(selector? : string)
		{
			var base = this,
				links = $((selector && selector.length !== 0) ? selector : base.defaultSelector);

			if(links.length !== 0)
			{
				if(typeof base.langOpensInNewWindow === "undefined")
				{
					base.langOpensInNewWindow = base.LanguageHandler.getTranslation("/common/opensinanewwindow","");
				}

				links.each(function(i, link)
				{
					base.ChangeLink($(link));
				});
			}
		}

		public ChangeLink($link : JQuery)
		{
			var base = this;
			var title = $link.attr('title');
			if($link.attr('title') === undefined && base.langOpensInNewWindow !== "" && $link.data('titlechanged') !== true)
			{
				title = ($link.attr('title') === undefined) ? base.langOpensInNewWindow : title + ', '+ base.langOpensInNewWindow;
				$link
					.attr('title', title)
					.data('titlechanged',true);
				
			}
			if($link.attr('rel') === 'external')
			{
				$link.attr('target','_blank');
			}
		}
	}
}

