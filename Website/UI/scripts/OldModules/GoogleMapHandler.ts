/// <reference path="../Definitions/google.jsapi.d.ts" />
/// <reference path="../Definitions/google.maps.d.ts" />
/// <reference path="../Utilities/UtilityBase.ts" />
/// <reference path="../Utilities/UtilityLanguageHandler.ts" />
/// <reference path="GoogleMapBase.ts" />
/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
//declare var google : any;
 

interface MapDataObject
{
	Id: string;
	MapContainer: JQuery;
	MapSettings: MapSettings;
	MapOptions: MapSettings;
	MarkerFilter: MarkerFilter[];
	MarkerClustererOptions: MarkerClustererOptions;
	infoWindowOptions: google.maps.InfoWindowOptions;
	markerStyles: MarkerStyle[];
	Language: any;
	MarkerClusterer: any;
	latlngbounds?: google.maps.LatLngBounds;
	Map: any;

	MapMarkers: GoogleMarkerData[];
	InfoWindow: google.maps.InfoWindow;
	Markers: MarkerData[];
}
//MapMarkers : GoogleMarker[];

interface MarkerClustererOptions
{
	averageCenter: boolean;
	gridSize: number;
	ignoreHidden: boolean;
	maxZoom: any;
	minimumClusterSize: number;
	zoomOnClick: boolean;
}

interface MarkerData
{
	Position:
	{
		Lat: string;
		Lng: string;
	};
	filterKeys?: string[];
	Headline: string;
	Content: string;
	data: any;
}

interface MarkerFilter
{
	useCount: number;
	value: string;
	key: string;
}

interface MapSettings extends google.maps.MapOptions
{
	mapType: string;
	useFitBounds?: boolean;
	markerCluster?: boolean;
	UseClusterSummary?: boolean;
	createFilter?: boolean;
	createLegend?: boolean;
	alwaysShowMarkersWithoutFilter?: boolean;
	streetViewControl?: boolean;
}

interface MarkerStyle
{
	name: string;
	usageCheck: any;
	markerImage: google.maps.MarkerImage;
	useCount: number;
}

interface Window
{
	GoogleMapHandler: Comprend.GoogleMap.Handler;
	mapsdata: any;
}

interface GoogleMarkerData extends google.maps.Marker
{
	content?: string;
	filterKeys?: string[];
	data?: any;
}

module Comprend.GoogleMap
{
	export class Handler
	{
		private MainBase: MainBase;
		private Utilities: Comprend.Utilities.Base;
		private GoogleMapBase: Comprend.GoogleMap.Base;

		private MapsData: any;

		constructor(mainBase: MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
			base.Utilities = base.MainBase.Utilities;
		}

		public Init()
		{
			//After onload
			//Load all additional modules here
			var base = this;

			window.GoogleMapHandler = base;
			base.MapsData = window.mapsdata;

			if ((base.MapsData) && (base.MapsData.length > 0) && (typeof google !== 'undefined') && google !== null)
			{
				base.Utilities.loadScript("http://maps.googleapis.com/maps/api/js?kay=AIzaSyD6prJuSDJ8NVyRX6gkx49uMjuGr_oEaDw&language=" + base.MainBase.pageLanguage + "&sensor=false&callback=base.GMapHandler.Handler.initiateMaps");
			}
		}

		public initiateMaps()
		{
			var base: Comprend.GoogleMap.Handler = window.GoogleMapHandler;

			if (base.MapsData && base.MapsData.length !== 0)
			{
				for (var i = 0; i < base.MapsData.length; i++)
				{
					var mapData: MapDataObject = base.MapsData[i];
					mapData.MapContainer = $('#' + mapData.Id);

					if (mapData.MapContainer.length !== 0)
					{
						base.createMapMarkup(mapData);
						base.createOptions(mapData);

						mapData.Map = new google.maps.Map(mapData.MapContainer.get(0), mapData.MapOptions);
						mapData.latlngbounds = new google.maps.LatLngBounds();
						mapData.MarkerClusterer = null;

						base.addMarkers(mapData);

						if (mapData.MapOptions.createLegend === true)
						{
							base.createLegend(mapData);
						}

						if (mapData.MapOptions.createFilter === true)
						{
							base.createFilter(mapData);
						}

						if (mapData.MapOptions.markerCluster === true)
						{
							base.Utilities.loadScript('/UI/scripts/Plugins/markerclusterer.min.js', function (data, textStatus)
							{
								var MarkerClusterer = (typeof MarkerClusterer !== 'undefined') ? MarkerClusterer : null;
								if (MarkerClusterer !== null)
								{
									mapData.MarkerClusterer = new MarkerClusterer(mapData.Map, mapData.MapMarkers, mapData.MarkerClustererOptions);
								}
							});
						}

						if (mapData.MapOptions.useFitBounds !== undefined && mapData.MapOptions.useFitBounds === true && (mapData.Markers && mapData.Markers.length > 0))
						{
							mapData.Map.fitBounds(mapData.latlngbounds);
						}

						$('body').addClass('hasmap');
					}

				}
			}
		}
		createFilter(mapData: MapDataObject)
		{
			var base = this;
			if (mapData.MarkerFilter !== undefined && mapData.MarkerFilter.length > 0)
			{
				var filterlist = $('<ul></ul>');
				for (var k = 0; k < mapData.MarkerFilter.length; k++)
				{
					var filterItem: MarkerFilter = mapData.MarkerFilter[k];

					if (filterItem.useCount !== undefined && filterItem.useCount > 0)
					{
						$('<li>' + filterItem.value + '<span></span></li>')
							.data('filterid', filterItem.key)
							.click(function ()
							{
								base.filterMarkers(mapData, this);
							})
							.appendTo(filterlist);
					}
				}

				var filtermarkup = $('<div id="' + mapData.Id + '-filter" class="filter"></div>');
				if (mapData.Language.filterheadline !== undefined && mapData.Language.filterheadline && mapData.Language.filterheadline.length > 0)
				{
					$('<h2>' + mapData.Language.filterheadline + '</h2>').appendTo(filtermarkup);
				}

				if (filterlist.find('li').length > 0)
				{
					filtermarkup
						.append(filterlist)
						.appendTo('#' + mapData.Id);
				}
			}
		}

		filterMarkers(mapData: MapDataObject, filterItemElement)
		{
			var filteritem = $(filterItemElement),
				filterId = filteritem.data('filterid'),
				arMarkersToShow = [];

			filteritem
				.siblings()
				.removeClass('selected')
				.end()
				.addClass('selected');

			if (filterId && mapData.MapMarkers && mapData.MapMarkers.length > 0)
			{
				for (var m = 0; m < mapData.MapMarkers.length; m++)
				{
					var marker: GoogleMarkerData = mapData.MapMarkers[m];

					if (marker.filterKeys && marker.filterKeys.length > 0)
					{
						if (marker.filterKeys.indexOf(filterId) !== -1)
						{
							marker.setVisible(true);
						}
						else
						{
							marker.setVisible(false);
						}
					}
					else
					{
						if (mapData.MapOptions.alwaysShowMarkersWithoutFilter)
						{
							marker.setVisible(true);
						}
					}
				}

				if (mapData.MarkerClusterer)
				{
					mapData.MarkerClusterer.repaint();
				}
			}
		}

		createLegend(mapData: MapDataObject)
		{
			//Legend goes by marker styles
			if (mapData.markerStyles !== undefined && mapData.markerStyles.length)
			{
				var legendList = $('<dl></dl>');
				for (var k = 0; k < mapData.markerStyles.length; k++)
				{
					var markerStyle: MarkerStyle = mapData.markerStyles[k];

					if (markerStyle.name !== undefined && markerStyle.name.length > 0 && markerStyle.useCount > 0)
					{
						$('<dt><img src="' + markerStyle.markerImage.url + '" alt=""/></dt><dd>' + markerStyle.name + '</dd>').appendTo(legendList);
					}
				}

				var legendMarkup = $('<div id="' + mapData.Id + '-legend" class="legend"/>').append(legendList);
				//Don't show an empty legendk
				if (legendList.find('dt').length > 0)
				{
					legendMarkup.appendTo('#' + mapData.Id);
				}
			}
		}

		public createMapMarkup(mapData: MapDataObject)
		{
			//Not required by default
		}

		public createOptions(mapData: MapDataObject)
		{

			/**** Map Options ****/
			//Important: mapOptions is used by the map, mapsettings is set by the usercontrol
			//Default options defined, to be overridden by individual map settings 
			//All available options: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
			// Stockholm: 59.332583, 18.065185, Goteborg: 57.702791, 11.973102

			mapData.MapOptions = {

				"center": new google.maps.LatLng(59.332583, 18.065185),
				"mapType": 'roadmap',
				"zoom": 8,
				"disableDoubleClickZoom": false, // Disable zoom on double click 
				"draggable": true,  // Enable dragging the map 
				"maxZoom": 18,
				"minZoom": 0,
				"panControl": false, // Show or hide the pan control 
				"zoomControl": true, // Show or hide the zoom control 
				"scaleControl": true, // Show or hide the scale control 
				"mapTypeControl": false, // Show or hide the map type control 
				"rotateControl": true, // Show or hide the rotation control 
				"streetViewControl": true, // Show or hide the street view control 
				"scrollwheel": false, // Use scroll to zoom in 
				// Plugin specific options 
				"useFitBounds": false, // let the map zoom in to cover all available markers 
				"markerCluster": false, // cluster markers if they are to close TO BE IMPLEMENTED AT A LATER STAGE 
				"createFilter": true,
				"createLegend": true,
				"alwaysShowMarkersWithoutFilter": true
			};

			///**** Set GoogleMapHandler Options ****/
			if (mapData.MapSettings.useFitBounds !== undefined) { mapData.MapOptions.useFitBounds = mapData.MapSettings.useFitBounds; }
			if (mapData.MapSettings.markerCluster !== undefined) { mapData.MapOptions.markerCluster = mapData.MapSettings.markerCluster; }
			if (mapData.MapSettings.UseClusterSummary !== undefined) { mapData.MapOptions.UseClusterSummary = mapData.MapSettings.UseClusterSummary; }
			if (mapData.MapSettings.createFilter !== undefined) { mapData.MapOptions.createFilter = mapData.MapSettings.createFilter; }
			if (mapData.MapSettings.createLegend !== undefined) { mapData.MapOptions.createLegend = mapData.MapSettings.createLegend; }
			if (mapData.MapSettings.alwaysShowMarkersWithoutFilter !== undefined) { mapData.MapOptions.alwaysShowMarkersWithoutFilter = mapData.MapSettings.alwaysShowMarkersWithoutFilter; }

			///**** GoogleMap Options, merged with default settings ****/
			if (mapData.MapSettings.center !== undefined && mapData.MapSettings.center !== null)
			{
				mapData.MapOptions.center = mapData.MapSettings.center;
			}
			if (mapData.MapSettings.zoom !== undefined) { mapData.MapOptions.zoom = mapData.MapSettings.zoom; }
			if (mapData.MapSettings.disableDoubleClickZoom !== undefined) { mapData.MapOptions.disableDoubleClickZoom = mapData.MapSettings.disableDoubleClickZoom; }
			if (mapData.MapSettings.draggable !== undefined) { mapData.MapOptions.draggable = mapData.MapSettings.draggable; }
			if (mapData.MapSettings.maxZoom !== undefined) { mapData.MapOptions.maxZoom = mapData.MapSettings.maxZoom; }
			if (mapData.MapSettings.minZoom !== undefined) { mapData.MapOptions.minZoom = mapData.MapSettings.minZoom; }
			if (mapData.MapSettings.panControl !== undefined) { mapData.MapOptions.panControl = mapData.MapSettings.panControl; }
			if (mapData.MapSettings.zoomControl !== undefined) { mapData.MapOptions.zoomControl = mapData.MapSettings.zoomControl; }
			if (mapData.MapSettings.scaleControl !== undefined) { mapData.MapOptions.scaleControl = mapData.MapSettings.scaleControl; }
			if (mapData.MapSettings.mapTypeControl !== undefined) { mapData.MapOptions.mapTypeControl = mapData.MapSettings.mapTypeControl; }
			if (mapData.MapSettings.rotateControl !== undefined) { mapData.MapOptions.rotateControl = mapData.MapSettings.rotateControl; }
			if (mapData.MapSettings.streetViewControl !== undefined) { mapData.MapOptions.streetViewControl = mapData.MapSettings.streetViewControl; }
			if (mapData.MapSettings.scrollwheel !== undefined) { mapData.MapOptions.scrollwheel = mapData.MapSettings.scrollwheel; }

			if (mapData.MapSettings.mapType !== undefined)
			{
				switch (mapData.MapSettings.mapType)
				{
					case 'hybrid':
						mapData.MapOptions.mapTypeId = google.maps.MapTypeId.HYBRID;
						break;
					case 'terrain':
						mapData.MapOptions.mapTypeId = google.maps.MapTypeId.TERRAIN;
						break;
					case 'satellite':
						mapData.MapOptions.mapTypeId = google.maps.MapTypeId.SATELLITE;
						break;
					case 'roadmap':
						mapData.MapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
						break;
					default:
						mapData.MapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
						break;
				}

			}

			if (mapData.Language === undefined) { mapData.Language = {}; }


			/**** InfoWindow Options ****/
			mapData.infoWindowOptions = {
				"content": "",
				"zIndex": 1000
			};

			if (mapData.MapContainer.parents('#secondarycontent').length === 1 && mapData.MapContainer.innerWidth() < 300)
			{
				mapData.infoWindowOptions.maxWidth = mapData.MapContainer.innerWidth() - 10;
			}
			/*
				"disableAutoPan":false,
				"maxWidth":300,
				"zIndex":10000
			"content": "",
				"zIndex": 1000
			*/

			/**** Markerclusterer options****/
			/* http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/docs/reference.html#MarkerClustererOptions */
			mapData.MarkerClustererOptions = {
				"averageCenter": true,
				"gridSize": 60, //The grid size of a cluster in pixels
				"ignoreHidden": true,
				"maxZoom": null,
				"minimumClusterSize": 2,
				"zoomOnClick": true
			};

			/**** Marker styles ****/
			/* Make sure that the default pin is the last and always returns true in usageCheck*/
			var arMarkerStyles: MarkerStyle[] = [];
			arMarkerStyles.push(
				{
					"name": (mapData.Language.HQ && mapData.Language.HQ !== undefined) ? mapData.Language.HQ : '',
					"usageCheck": function (markerdata) { return false; },
					//USING DEFAULT MARKER ICONS
					"markerImage": new google.maps.MarkerImage('/UI/gfx/mapicons/icon_map_marker.png',
						// This marker is 20 pixels wide by 32 pixels tall.
						new google.maps.Size(30, 46),
						// The origin for this image is 0,0.
						new google.maps.Point(0, 0),
						// The anchor for this image is the base of the flagpole at 0,32.
						new google.maps.Point(14, 39)),
					"useCount": 0
				});

			arMarkerStyles.push(
				{
					"name": (mapData.Language.Subsidiaries && mapData.Language.Subsidiaries !== undefined) ? mapData.Language.Subsidiaries : '',
					"usageCheck": function (markerdata) { return true; },
					//USING DEFAULT MARKER ICONS
					"markerImage": new google.maps.MarkerImage('/UI/gfx/mapicons/icon_map_marker.png',
						// This marker is 20 pixels wide by 32 pixels tall.
						new google.maps.Size(30, 46),
						// The origin for this image is 0,0.
						new google.maps.Point(0, 0),
						// The anchor for this image is the base of the flagpole at 0,32.
						new google.maps.Point(14, 39)),
					"useCount": 0
				});

			mapData.markerStyles = arMarkerStyles;

		}

		getMarkerStyle(markerData: MarkerData, mapData: MapDataObject)
		{
			var base = this;
			if (mapData.markerStyles !== undefined && mapData.markerStyles)
			{
				var markerImage = null;

				for (var k = 0; k < mapData.markerStyles.length; k++)
				{
					var makerStyleItem: MarkerStyle = mapData.markerStyles[k];
					if (typeof makerStyleItem.usageCheck === 'function')
					{
						if (makerStyleItem.usageCheck(markerData) === true && markerImage === null)
						{
							markerImage = makerStyleItem.markerImage;
							makerStyleItem.useCount++;
						}
					}
				}

				if (markerImage === null)
				{
					var markerStyle = mapData.markerStyles[mapData.markerStyles.length - 1];
					markerImage = markerStyle;
					markerStyle.useCount++;
				}
				return markerImage;
			}
		}

		checkFilterKeys(mapData: MapDataObject, markerData: MarkerData)
		{
			if (mapData.MarkerFilter !== undefined && mapData.MarkerFilter.length > 0 && mapData !== undefined && markerData.filterKeys !== undefined && markerData.filterKeys.length > 0)
			{
				for (var m = 0; m < mapData.MarkerFilter.length; m++)
				{
					var filterItem: MarkerFilter = mapData.MarkerFilter[m];

					if (filterItem.useCount === undefined) { filterItem.useCount = 0; }

					for (var n = 0; n < markerData.filterKeys.length; n++)
					{

						var markerFilterKey: string = markerData.filterKeys[n];
						if (filterItem.key === markerFilterKey)
						{
							filterItem.useCount++;
						}
					}
				}
			}
		}

		addMarkers(mapData: MapDataObject)
		{
			var base = this;
			if (mapData.Markers && mapData.Markers.length > 0)
			{
				mapData.MapMarkers = [];
				mapData.InfoWindow = new google.maps.InfoWindow(mapData.infoWindowOptions);
				for (var j = 0; j < mapData.Markers.length; j++)
				{
					var markerData: MarkerData = mapData.Markers[j],
						markerContent: string = '',
						markerLatLng: google.maps.LatLng = new google.maps.LatLng(parseFloat(markerData.Position.Lat), parseFloat(markerData.Position.Lng));



					markerContent += (markerData.Content && markerData.Content.length > 0) ? markerData.Content + "<br/>" : "";

					var marker: GoogleMarkerData = new google.maps.Marker({
						"position": markerLatLng,
						"map": mapData.Map,
						"icon": base.getMarkerStyle(markerData, mapData),
						"filterKeys": markerData.filterKeys,
						"title": markerData.Headline,
						"headline": markerData.Headline,
						"content": markerContent.length > 0 ? markerContent : ''
					});

					mapData.latlngbounds.extend(marker.getPosition());

					if (markerData.filterKeys !== undefined && markerData.filterKeys.length > 0)
					{
						base.checkFilterKeys(mapData, markerData);
					}

					mapData.MapMarkers.push(marker);

					google.maps.event.addListener(marker, 'click', $.proxy(base.onMarkerClickEvent, base, mapData, marker, markerData, markerContent));

					google.maps.event.addListener(mapData.InfoWindow, 'closeclick', function ()
					{
						mapData.MapContainer.find('.filter').fadeIn();
					});

				};

			}
		}
		onMarkerClickEvent(mapData: MapDataObject, marker: GoogleMarkerData, markerData: MarkerData, markerContent: string)
		{
			var base = this;

			if (marker.content && marker.content.length > 0)
			{
				mapData.MapContainer.find('.filter').fadeOut();
				mapData.InfoWindow.setContent('<div class="infowindowcontent">' + (markerData.Headline.length > 0 ? '<p class="headline">' + markerData.Headline + '</p>' : '') + marker.content + '</div>');
				mapData.InfoWindow.open(mapData.Map, marker);
				var picturefill = (typeof picturefill !== 'undefined') ? picturefill : function () { /* Empty function to avoid throwing an error */ };

				try { picturefill(); } catch (e) { /* Fail */ }
			}
		}

	}
}


