/// <reference path="../Main.ts" />

module Comprend.Layout
{
	export class IframeHandler
	{
		private MainBase : MainBase;
		private iFrames : NodeListOf<HTMLIFrameElement>;

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;

			base.Init();
		}

		Init()
		{
			var base = this;
			base.iFrames = document.getElementsByTagName("iframe");
			if(base.iFrames.length !== 0)
			{
				if (window.addEventListener)
				{
					window.addEventListener("message", $.proxy(base.ReceiveMessage,base),false);
				}
				else
				{
					window.attachEvent("onmessage", $.proxy(base.ReceiveMessage,base));//for ie
				}
				for (var i = 0; i < base.iFrames.length; i++)
				{
					var iframe = base.iFrames[i];
					
					base.initiationMessageToIframe(iframe);
				}

			}
		}

		private initiationMessageToIframe(iframe : HTMLIFrameElement)
		{
			var base = this,
				oldiframeonload : Function = iframe.onload,
				src = iframe.src,
				https = (src.indexOf('https') === 0),
				substrOffset = (https) ? 8 : 7,
				iframeDomain = src.substring(0,(src.substring(substrOffset,src.length).indexOf('/')+substrOffset)),
				fnSendInitiatoinMessage = function()
				{
					//console.log(iframeDomain)
					//We want the child to respond with it's height
					iframe.contentWindow.postMessage("parentreadytoreceive",iframeDomain);
				};
			
			
			if (typeof oldiframeonload !== 'function')
			{
				iframe.onload = fnSendInitiatoinMessage;
			}
			else
			{
				iframe.onload = function()
				{
					if (oldiframeonload)
					{
						oldiframeonload();
					}
					fnSendInitiatoinMessage();
				};
			}
		}

		public ReceiveMessage(e : MessageEvent)
		{
			var base = this,
				d = e.data,
				del = 0,
				action = '',
				data = '';
			
			if (typeof e.data === 'string')
			{
				del = d.indexOf('|');
				action = d.substring(0,del);
				data = d.substring((del+1),d.length);
			}
			switch(action)
			{
				case 'resizeiframe':
					base.ResizeIframe(e,data);
				break;
				default:break;
			}
		}

		private ResizeIframe(e : MessageEvent, data : any)
		{
			$('iframe').each(function()
			{
				var $iframe = $(this),
					src = $iframe.attr('src'),
					domain = src.substring(0,(src.substring(7,src.length).indexOf('/')+7));
				
				if(domain === e.origin)
				{
					$iframe.css({'height':data + 'px'});
				}
			});
		}
	}
}