/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />


module Comprend.Newsroom
{
	export class Timeline
	{
		private MainBase : MainBase;
		private NewsroomBase : Comprend.Newsroom.Base;
		private leftPosition : number;

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
			base.NewsroomBase = base.MainBase.Newsroom;

			base.leftPosition = 0;

			base.Init();
		}

		Init()
		{
			var base = this;
			
			if (base.NewsroomBase.newsItems.length !== 0)
			{
				var prevLeftbottom = 100000;
				base.leftPosition = base.NewsroomBase.newsItems.eq(0).position().left;

				// the lineup function simulates the facebook timeline look, where the news items is put on the left- and right-hand side of a central axis
				base.lineup();
			}
		}

		public lineup()
		{
			var base = this;
			
			if (base.NewsroomBase.enableLineup === true)
			{
				var leftPos = base.leftPosition;
				
				base.NewsroomBase.newsItems.each(function(i,newsItem)
				{
					var $newsItem = $(newsItem),
						prevLeftbottom = $newsItem.position().top + $newsItem.height();

					if ($newsItem.hasClass('year'))
					{
						/* Do nothing ?? */
					}
					else
					{
						var pointer = $('<span class="pointer"></span>');
						if ($newsItem.position().left === leftPos)
						{
							// this one is to the left
							if ($newsItem.hasClass('majoritem'))
							{
								pointer.addClass('toppointer');
							}
							else
							{
								pointer.addClass('rightpointer');
								// move it upwards if there is space
								if ($newsItem.position().top > prevLeftbottom)
								{
									var marginTop = $newsItem.position().top - prevLeftbottom - 20;
									if (marginTop > ($newsItem.height() - 20))
									{
										marginTop = $newsItem.height() - 20;
									}
									$newsItem.css('margin-top', -marginTop);
									pointer.css('top', marginTop + 20);
									prevLeftbottom = $newsItem.position().top + $newsItem.height() - marginTop;
								}
							}
						}
						else // this one is to the right
						{
							$newsItem.css('float', 'right');
							pointer.addClass('leftpointer');
						}
						$newsItem.append(pointer);
					}
				});
			}
		}
	}
}