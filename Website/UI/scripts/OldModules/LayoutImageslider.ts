/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Definitions/jquery.cycle2.d.ts" />

module Comprend.Layout
{
	export class ImageSliderBase
	{
		private MainBase: MainBase;
		private Utils: Comprend.Utilities.Base;

		constructor($targets?: JQuery)
		{
			//We expect this method to be run after document ready
			if (typeof $targets !== 'undefined')
			{
				this.Init.apply(this, arguments);
			}
		}

		Init($targets: JQuery)
		{
			var base = this;
			var currentSlideshow;

			$targets.each(function ()
			{
				currentSlideshow = $(this);

				//does not have 0 or 1 slide
				if (!currentSlideshow.hasClass('slides0') && !currentSlideshow.hasClass('slides1'))
				{

					//if ie, launch js slider, otherwise css3 slider
					if ($('html').hasClass('ie'))
					{

						if (typeof jQuery.fn.cycle !== 'function')
						{
							$.getScript('/UI/scripts/Plugins/jquery.cycle2.min.js', $.proxy(function (currentSlideshow)
							{

								var base = this;

								/* prepend next, prev, and pager elements to slider */
								currentSlideshow.prepend('<div class=\"cycle-pager\"></div><div class=\"cycle-prev\">prev</div><div class=\"cycle-next\">next</div>');

								if ($('body').hasClass('compactmode'))
								{
									$.getScript('/UI/scripts/Plugins/jquery.cycle2.swipe.min.js', $.proxy(function (currentSlideshow)
									{

										currentSlideshow.addClass('js-slider');
										currentSlideshow.find('.slideshow').cycle({
											fx: 'scrollHorz',
											slides: '.slide',
											overlay: '.caption',
											speed: 600,
											timeout: 5000,
											swipe: true,
											next: '.cycle-next',
											prev: '.cycle-prev'
										});
									}, base, currentSlideshow));
								}
								else
								{
									currentSlideshow.addClass('js-slider');
									currentSlideshow.find('.slideshow').cycle({
										fx: 'scrollHorz',
										slides: '.slide',
										overlay: '.caption',
										speed: 600,
										timeout: 5000,
										next: '.cycle-next',
										prev: '.cycle-prev',
										pager: '.cycle-pager'
									});
								}

							}, base, currentSlideshow));
						}
						else
						{
							//cycle is not loaded
						}
					}
					else
					{
						//use css3 slider

						currentSlideshow.addClass('css-slider');
						currentSlideshow.append('<span class="css-prev">prev</span><span class="css-next">next</span>');

						$('body').on('picturefillcomplete', function ()
						{
							currentSlideshow.imagesLoaded(function ()
							{
								//var mask = currentSlideshow.find('.slide img').first().attr('data-height');
								var mask = currentSlideshow.find('.slide').first().height();
								console.log(mask);
								currentSlideshow.find('.slideshow').css('height', mask);
								currentSlideshow.find('.slide').first().addClass('active-slide');
								currentSlideshow.find('.slide:nth-child(2)').addClass('next-slide');
								currentSlideshow.find('.slide').last().addClass('prev-slide');
							});

						});

						//next click listener
						currentSlideshow.find('.css-next').on('click', function ()
						{
							clearClasses(currentSlideshow);
							currentSlideshow.find('.active-slide').addClass('active-to-prev');
							currentSlideshow.find('.next-slide').addClass('next-to-active');
							currentSlideshow.find('.prev-slide').removeClass('prev-slide');
							currentSlideshow.find('.active-slide').addClass('prev-slide').removeClass('active-slide');
							currentSlideshow.find('.next-slide').addClass('active-slide').removeClass('next-slide');

							if (currentSlideshow.find('.active-slide').next().length < 1)
							{
								currentSlideshow.find('.slide').first().addClass('next-slide');
							}
							else
							{
								currentSlideshow.find('.active-slide').next().addClass('next-slide');
							}
						});

						//previous click listenter
						$('.css-prev').on('click', function ()
						{
							clearClasses(currentSlideshow);
							currentSlideshow.find('.active-slide').addClass('active-to-next');
							currentSlideshow.find('.prev-slide').addClass('prev-to-active');
							currentSlideshow.find('.next-slide').removeClass('next-slide');
							currentSlideshow.find('.active-slide').addClass('next-slide').removeClass('active-slide');
							currentSlideshow.find('.prev-slide').addClass('active-slide').removeClass('prev-slide');

							if (currentSlideshow.find('.active-slide').prev().length < 1)
							{
								currentSlideshow.find('.slide').last().addClass('prev-slide');
							}
							else
							{
								currentSlideshow.find('.active-slide').prev().addClass('prev-slide');
							}
						});

						function clearClasses(thisSlider)
						{
							thisSlider.find('.active-to-prev').removeClass('active-to-prev');
							thisSlider.find('.next-to-active').removeClass('next-to-active');
							thisSlider.find('.active-to-next').removeClass('active-to-next');
							thisSlider.find('.prev-to-active').removeClass('prev-to-active');
						}
					}
				}
				else
				{
					//not enough slides
				}
			});
		}
	}
}