/// <reference path="NewsroomTimeline.ts" />
/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />


module Comprend.Newsroom
{
	export class Base
	{
		private MainBase: MainBase;
		private LangHand: Comprend.Utilities.LanguageHandlerBase;
		public Timeline: Comprend.Newsroom.Timeline;
		public newsItems: JQuery;
		public showMoreLinks: JQuery;
		public enableLineup: boolean;
		public yearBoxesElements: JQuery;
		public timelineNavElement: JQuery;
		public baseElement: JQuery;
		public rightColElement: JQuery;

		constructor(mainBase: MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
			base.enableLineup = false;
		}

		public Init()
		{
			//After onload
			//Load all additional modules here

			var base = this;

			base.initiateVariablesAndEvents();

			if (base.baseElement.length !== 0)
			{

				//TODO Change when we have mobile detection etc. 
				if (false)
				{
					//Should only run when in desktop or tablet mode

					base.initiateTimelineNavigation();
				}

				base.timelineNavElement
					.find('a')
					.click($.proxy(base.onTimelineLinkClick, base));

				//TODO Change when we have mobile detection etc. 
				if (false)
				{
					//Should only run when in compact mode
					base.initiateCompactmodeAdjustments();
				}
				base.Timeline = new Comprend.Newsroom.Timeline(base.MainBase);
			}
		}

		initiateVariablesAndEvents()
		{
			var base = this;

			base.baseElement = $('.newstimeline');

			if (base.baseElement.length !== 0)
			{
				base.newsItems = base.baseElement.find('.tab-content > ul:eq(0)').children('li');
				base.showMoreLinks = base.newsItems.filter('.tab-content li.showmore');
				base.newsItems = base.newsItems.filter(':not(.tab-content li.showmore)');
				base.yearBoxesElements = base.newsItems.filter('.tab-content li.year');
				base.showMoreLinks.click($.proxy(base.onShowMoreClick, base));
				base.timelineNavElement = $('#timelinenav');
				base.rightColElement = $('#secondarycontent');

				base.timelineNavElement
					.find('li:eq(0)')
					.addClass('selected active')
					.find('a')
					.addClass('selected active');
			}
		}

		initiateTimelineNavigation()
		{
			var base = this,
				rightColBottom: number,
				rightColBottomPosition: number,
				doc = $(document);

			if (base.rightColElement.length !== 0)
			{
				rightColBottom = base.rightColElement.height();
				rightColBottomPosition = base.rightColElement.position().top + rightColBottom;

				base.timelineNavElement
					.css(
					{
						'left': base.rightColElement.find('> div:eq(0)').position().left
					}).show();

				$(window).scroll($.proxy(base.onWindowScrollTimelineNavigation, base, doc, rightColBottomPosition));
			}
		}

		/* ===================== Compact mode adjustments =====================*/
		//#region compactmode
		initiateCompactmodeAdjustments()
		{
			var base = this,
				$TimelineList = base.timelineNavElement.find('ul').clone(true),
				MobileTimeNav = $('<div id="timelineMobile" class="tabs hidden-tablet hidden-desktop"></div>')
					.append('<a href="#" class="toggle-tabs selector"><span>selected</span></a>');

			$TimelineList
				.addClass('nav nav-tabs')
				.find('li > a')
				.each(function (i, a)
				{
					$(a)
						.on('click', function ()
						{
							$(this).parents('.nav-tabs').hide();
						})
						.wrapInner('<span/>');
				})
				.end()
				.appendTo(MobileTimeNav);

			base.baseElement
				.find('.tabs')
				.after(MobileTimeNav);

			//TODO Initiate tabs
		}
		//#endregion

		/* ===================== Events =====================*/
		//#region events

		onTimelineLinkClick(e: JQueryEventObject)
		{
			e.preventDefault();

			var base = this,
				$link = $(e.target),
				year: string,
				items: JQuery;

			$link
				.parent()
				.siblings()
				.children('a')
				.removeClass('selected');

			$link.addClass('selected');

			year = $link.attr('href');
			year = year.substring(year.indexOf('year=') + 5);

			items = base.baseElement
				.find('ul:eq(0)')
				.children('li.y' + year);

			if (items.length !== 0)
			{
				// news item with the intended year was found, scroll to the first one
				$('html,body').animate({
					scrollTop: items.first().offset().top - 10
				}, 500);
			}
			else
			{
				// news items with the intended year was NOT found, load them through ajax
				var $showmore = $('<li class="showmore yearspecific"><span class="wanted-btn"><a href="' + $link.attr('href') + '" class="btn wanted"><span>' + year + '</span></a></span></li>');

				base.baseElement
					.find('ul:eq(0)')
					.append($showmore);

				$showmore
					.click($.proxy(base.onShowMoreClick, base))
					.click();
			}
		}

		onWindowScrollTimelineNavigation($doc: JQuery, rightColBottomPosition: number, e: JQueryEventObject)
		{
			var base = this,
				$yearBox: JQuery,
				$preYearBox: JQuery,
				scrollTop = $doc.scrollTop();

			if (scrollTop - (rightColBottomPosition + 50) > 0)
			{
				base.timelineNavElement
					.addClass('timelinenavfixed')
					.css({ 'left': base.rightColElement.offset().left });
			}
			else
			{
				base.timelineNavElement
					.removeClass('timelinenavfixed')
					.css({ 'left': base.rightColElement.find('> div:eq(0)').position().left });
			}

			// highlight the correct item in the left-hand timeline navigator

			base.yearBoxesElements
				.each(function (i, yearBox)
				{
					$yearBox = $(yearBox);
					if ($yearBox.offset().top - scrollTop > 10)
					{
						$yearBox = $preYearBox;
						return false;
					}
					$preYearBox = $yearBox;
				});

			if ($preYearBox === null)
			{
				base.timelineNavElement
					.children('li')
					.children('a')
					.removeClass('selected')
					.parent('li:eq(0)')
					.children('a')
					.addClass('selected');
			}
			else
			{
				var year = parseInt($yearBox.find('h3').html(), 10);
				base.timelineNavElement
					.children('li')
					.children('a')
					.removeClass('selected')
					.parent('li.y' + year)
					.children('a')
					.addClass('selected');
			}
		}

		onShowMoreClick(e: JQueryEventObject)
		{
			e.preventDefault();
			var base = this,
				$target = $(e.currentTarget),
				href = $target.find('a:eq(0)').attr('href'),
				url = document.location.href;

			//showmore = $(this);
			url = url.substring(0, url.lastIndexOf('/') + 1) + href;
			$target
				.html('<span><a href="#"  class="btn simple"><span>Loading...</span></a></span>');

			$.ajax(
				{
					"url": url,
					"dataType": "html",
					"type": "GET",
					"context": $target
				}).done($.proxy(base.onShowMoreClickAjaxComplete, base, $target));
		}

		onShowMoreClickAjaxComplete($showMoreElement: JQuery, data: any)
		{
			var base = this,
				$data = (data) ? $($.parseHTML(data)) : $(),
				$ol = base.newsItems.parent();

			if ($data.length !== 0)
			{
				var $newItems = $data.find('.newstimeline').find('.tab-content > ul:eq(0)').children('li');

				if ($showMoreElement.is(':not(:last-child)'))
				{
					// remove any item that has already been printed below via the left-hand shortcuts
					var printedyear = parseInt($showMoreElement.nextAll('.tab-content li.year').first().find('h3').html(), 10),
						removeMe = $newItems.first().hasClass('y' + printedyear);

					$newItems
						.each(function (i, newItem)
						{
							var $newItem = $(newItem);
							if (removeMe)
							{
								$newItem.addClass('removeMe');
							}
							else
							{
								removeMe = $newItem.hasClass('y' + printedyear);
								if (removeMe)
								{
									$newItem.addClass('removeMe');
								}
							}
						});

					$newItems = $newItems.filter(':not(li.removeMe)');
				}

				$showMoreElement.before($newItems.hide().fadeIn('slow'));
				$showMoreElement.hide();

				// if this was a click genererated from the left-hand timeline navigation - scroll down to the selected year
				if ($showMoreElement.hasClass('yearspecific'))
				{
					var $firstItem = $newItems.first();
					if ($firstItem.length !== 0)
					{
						$('html,body').animate(
							{
								scrollTop: $firstItem.offset().top - 10
							}, 500);
					}
				}

				$showMoreElement = $newItems.filter('li.showmore');
				base.showMoreLinks = $showMoreElement;
				$newItems = $newItems.filter(':not(li.showmore)');
				base.yearBoxesElements = base.baseElement
					.find('.tab-content ul:eq(0)')
					.children('.tab-content li.year');

				$showMoreElement.click($.proxy(base.onShowMoreClick, base));
				base.Timeline.lineup();
			}
		}
	}
}