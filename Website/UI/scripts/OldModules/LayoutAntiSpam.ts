﻿/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.Layout
{
	export class AntiSpam
	{
		private MainBase : MainBase;
		private defaultSelector : string = "a[href^=#antispam]";

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;

			base.init();
		}

		private init()
		{
			var base = this;
			base.CheckForLinks();
		}

		public CheckForLinks(selector? : string)
		{
			var base = this,
				$links = $((selector && selector.length !== 0) ? selector : base.defaultSelector);
			$links.on('click', $.proxy(base.onLinkClick, base));
		}

		public onLinkClick(e : JQueryEventObject)
		{
			e.preventDefault();
			e.stopPropagation();

			var base = this,
				$link = $(e.currentTarget),
				href = $link.attr('href');
			var 	id = href.substring(10,href.length);
			
			if(id)
			{
				base.MainBase.Utilities.PropertyHandler.getProperty(id, 'Email', function(result)
				{
					if (result.length !== 0 && result.indexOf('@') !== -1)
					{
						window.location.href = "mailto:" + result;
					}
				});
			}
		}
	}
}

