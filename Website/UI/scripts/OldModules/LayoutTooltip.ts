/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Comprend.Layout
{
	export class TooltipBase
	{
		private MainBase : MainBase;
		private TooltipElements : JQuery;

		constructor(mainBase : MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;

			base.Init();
		}

		Init()
		{
			var base = this;
			base.TooltipElements = $('td[title]');

			base.TooltipElements
				.each(function(i,element)
				{
					var $element = $(element),
						tooltipText = $element.attr('title');
					$element.removeAttr('title');

					var tooltip = $('<div />').html(tooltipText).appendTo(document.body).hide();

					var a = $('<a />')
						.html($element.html())
						.addClass('tooltip')
						.appendTo($element.html(''))
						.click(function(event)
						{
							$('div.tooltip').remove();
							var pos = $(this).offset();
							//TODO fix dialog
							/*tooltip.dialog({
								dialogClass: 'tooltip',
								draggable: false,
								resizable: false,
								position: [pos.left - 125, pos.top + 20]
							});*/
							event.stopPropagation();
						});
				});
		}
	}
}