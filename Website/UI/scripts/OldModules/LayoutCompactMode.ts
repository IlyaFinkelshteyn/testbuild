/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Main.ts" />

module Comprend.Layout
{
	export class CompactModeBase
	{
		private MainBase: MainBase;
		private LanguageHandler: Comprend.Utilities.LanguageHandlerBase;
		private clickEventName: string;
		private headerInitialized: boolean = false;

		constructor(mainBase: MainBase)
		{
			//We expect this method to be run after document ready
			var base = this;
			base.MainBase = mainBase;
			base.Init();
		}

		Init()
		{
			var base = this;
			base.checkViewport();
		}

		checkViewport()
		{
			var base = this,
				winWidth = $(window).width(),
				winHeight = $(window).height(),
				tabletBreakpoint = 769,
				phoneBreakpoint = 481;

			//onload
			if (winWidth < tabletBreakpoint)
			{
				base.headerInitialized = true;
				base.getMobileHeaderMarkup();
				$('body').addClass('compactmode');
				if (winWidth < phoneBreakpoint)
				{
					$('body').addClass('phone');
				}
			}
			else
			{
				$(window).resize(function ()
				{
					winWidth = $(window).width();
					if (winWidth < tabletBreakpoint && base.headerInitialized === false)
					{
						base.headerInitialized = true;
						base.getMobileHeaderMarkup();
					}
				});
			}
			// add/remove classes on resize
			$(window).resize(function ()
			{
				if (winWidth < tabletBreakpoint)
				{
					$('body').addClass('compactmode');
					if (winWidth < phoneBreakpoint)
					{
						$('body').addClass('phone');
					}
				}
				if (winWidth > phoneBreakpoint && $('body').hasClass('phone'))
				{
					$('body').removeClass('phone');
				}
				if (winWidth > tabletBreakpoint && $('body').hasClass('compactmode'))
				{
					$('body').removeClass('compactmode');
				}
			});
		}

		getMobileHeaderMarkup()
		{
			//generate markup from sitemap
			var base = this;
			var currentPage = $('body').attr('id').replace('page', '');
			var result = $.get('/templates/pages/mobilenavigation.aspx?id=' + currentPage, function (data)
			{
				$('#mobile-header').html(data);
				base.addEventsToMobileHeaderMarkup();
			});
		}

		addEventsToMobileHeaderMarkup()
		{
			//add events to menu items
			var base = this;
			document.body.addEventListener("touchstart", function () { /* IOS Hack */ });
			base.clickEventName = ("ontouchend" in document) ? 'touchend' : 'click';

			$('.toggle-menu').on('click', function ()
			{
				$('.mobile-search').hide();
				$('.mobile-menu').toggle();
				$('.toggle-search').removeClass('active');
				$('.toggle-menu').toggleClass('active');
				$('#mobile-header').toggleClass('open');
			});

			$('.toggle-search').on('click', function ()
			{
				$('.mobile-menu').hide();
				$('.mobile-search').toggle();
				$('.toggle-menu').removeClass('active');
				$('.toggle-search').toggleClass('active');
			});

			//close other root elements
			$('.mobile-menu > ul > li').find('> div > a.expand').each(function ()
			{
				var rootItem = $(this);
				var targetTree = $('.mobile-menu > ul > li');

				rootItem.on('click', function ()
				{
					if (!rootItem.hasClass('open'))
					{
						targetTree.find('ul').hide();
						targetTree.find('a.open').removeClass('open');
					}
				});
			});

			//expand logic
			$('.mobile-menu').find('a.expand').each(function ()
			{
				var thisExpand = $(this),
					expandRoot = thisExpand.closest('.haschildren');
				thisExpand.on('click', function (e)
				{
					e.preventDefault();
					thisExpand.toggleClass('open');
					expandRoot.find('> ul').toggle();
				});
			});
		}

		onExpandClick(e: JQueryEventObject)
		{
			var base = this,
				$expandButton = $(e.target);

			$expandButton.toggleClass('open');

			//Do stuff on event
		}

		onClick(e: JQueryEventObject)
		{
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	}
}