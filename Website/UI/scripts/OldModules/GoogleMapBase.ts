/// <reference path="../Utilities/UtilityLanguageHandler.ts" />
/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="../Utilities/ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="GoogleMapHandler.ts" />

module Comprend.GoogleMap
{
	export class Base
	{
		private MainBase : MainBase;
		private Utilities : Comprend.Utilities.Base;
		public Handler : Comprend.GoogleMap.Handler;

		constructor(mainBase : MainBase)
		{
			//Before onload
			var base = this;
			base.MainBase = mainBase;
            base.Utilities = base.MainBase.Utilities;
		}
		 
		public Init()
		{
			//After onload 
			//Load all additional modules here
			var base = this;
			
			if ((window.mapsdata) && (window.mapsdata.length > 0) && (typeof google !== 'undefined') && google !== null)
			{
//				$.getScript("/UI/scripts/Modules/GoogleMapHandlerBundle.min.js",$.proxy(function()
                $.getScript("/UI/scripts/Modules/GoogleMapHandler.js", $.proxy(function ()
				{
					var base : Comprend.GoogleMap.Base = this;
					base.Handler = new Comprend.GoogleMap.Handler(base.MainBase);
					base.Handler.Init();
				},base));
			}
		}
	}
}