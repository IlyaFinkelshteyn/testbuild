/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Utilities/UtilityStorageBase.ts" />
/// <reference path="Developertools.ts" />

module Comprend.Developertools
{
	export class ResourcesChecker
	{
		private DevTools : Comprend.Developertools.Base;
		private resourceList : string[] = [];
		private latestUpdated : number = 0;
		private pollRate : number = 1000;
		private lastKeyCode : number = 0;
		private timer : number;
		private runCheck : boolean = true;

		constructor(mainbase : Comprend.Developertools.Base)
		{
			var base = this;
			base.DevTools = mainbase;

			base.pollRate = base.DevTools.GetSetting("resourcecheckerpollrate",1000);

			base.collectResourcePaths();
			base.checkModifiedDate();
			base.setStopAction();
		}

		setStopAction()
		{
			var base = this;

			$('body').on('keydown',function(e : JQueryEventObject)
			{
				var keyCode : number = e.keyCode;
				if(keyCode === 27)
				{
					if(base.lastKeyCode === 27)
					{
						base.runCheck = false;
						window.clearTimeout(base.timer);
						base.lastKeyCode = 0;
					}
					base.lastKeyCode = keyCode;
				}
			});
		}

		collectResourcePaths()
		{
			var base = this;
			$('script').each(function()
			{
				var src = this.src;

				if(src.indexOf("/UI/scripts") !== -1)
				{

					src = src.substring(src.indexOf("/UI/scripts"),src.length);
					if(src.indexOf('?'))
					{
						src = src.substring(0,src.indexOf('?'));
					}
					base.resourceList.push(src);
				}
			});

			$('link').each(function()
			{
				var href = this.href;
				if(href.indexOf("/UI/styles") !== -1)
				{
					href = href.substring(href.indexOf("/UI/styles"),href.length);
					if(href.indexOf('?') !== -1)
					{
						href = href.substring(0,href.indexOf('?'));
					}
					base.resourceList.push(href);
				}
			});
		}

		checkModifiedDate()
		{
			var base = this;
			if(base.runCheck === true)
			{
				$.ajax({
					'url': "/DeveloperTools/filemodified.aspx?file=" + base.resourceList,
					'cache':false,
					'success': $.proxy(base.checkModifiedDateCallback, base)
				});
			}
		}

		checkModifiedDateCallback(data)
		{
			var base = this;
			if(data && data.length > 0)
			{
				var fileupdated : number = parseInt(data,10);
				if(base.latestUpdated === 0)
				{
					base.latestUpdated = fileupdated;
				}

				if(base.latestUpdated < fileupdated)
				{
					window.location.reload();
				}
			}

			base.timer = window.setTimeout($.proxy(base.checkModifiedDate,base),base.pollRate);
		}
	}
}