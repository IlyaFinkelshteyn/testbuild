var Comprend;
(function (Comprend) {
    /// <reference path="../Definitions/jquery.d.ts" />
    /// <reference path="../Utilities/UtilityStorageBase.ts" />
    /// <reference path="ResourcesChecker.ts" />
    /// <reference path="DeveloperToolsInterface.ts" />
    /* jshint scripturl:false */
    (function (Developertools) {
        var Base = (function () {
            function Base() {
                this.options = {
                    settingsbasename: "kwddevelopertools"
                };
                var base = this;
                base.DevTools = base;
                base.Storage = new Comprend.Utilities.StorageBase(true);
                base.InitiateModules();
            }
            Base.prototype.InitiateModules = function () {
                var base = this;
                if (base.GetSetting('runFollowMe', "false") === "true") {
                    base.FollowMe = new Comprend.Developertools.FollowMe(base.DevTools);
                }

                if (base.GetSetting('runResourcesChecker', false) === "true") {
                    base.ResourceChecker = new Comprend.Developertools.ResourcesChecker(base.DevTools);
                }

                if (base.GetSetting('runGruntWatchLiveReload', false) === "true") {
                    $('head').append('<script src="//localhost:35729/livereload.js"></script>');
                }

                base.DevToolsInterface = new Comprend.Developertools.DeveloperToolsInterface(base.DevTools);
            };

            Base.prototype.GetSetting = function (key, fallback) {
                var base = this;
                var usedfallback = (typeof fallback === "undefined") ? false : fallback;
                return base.Storage.get(base.options.settingsbasename + '-' + key, usedfallback, base.Storage.Type.LOCALSTORAGE);
            };
            Base.prototype.SetSetting = function (key, value) {
                var base = this;
                base.Storage.set(base.options.settingsbasename + '-' + key, value, base.Storage.Type.LOCALSTORAGE);
            };
            return Base;
        })();
        Developertools.Base = Base;
    })(Comprend.Developertools || (Comprend.Developertools = {}));
    var Developertools = Comprend.Developertools;
})(Comprend || (Comprend = {}));

$(document).ready(function () {
    var DevTools = new Comprend.Developertools.Base();
});
