﻿/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Utilities/UtilityStorageBase.ts" />
/// <reference path="Developertools.ts" />
var Comprend;
(function (Comprend) {
    (function (Developertools) {
        var ResourcesChecker = (function () {
            function ResourcesChecker(mainbase) {
                this.resourceList = [];
                this.latestUpdated = 0;
                this.pollRate = 1000;
                this.lastKeyCode = 0;
                this.runCheck = true;
                var base = this;
                base.DevTools = mainbase;

                base.pollRate = base.DevTools.GetSetting("resourcecheckerpollrate", 1000);

                base.collectResourcePaths();
                base.checkModifiedDate();
                base.setStopAction();
            }
            ResourcesChecker.prototype.setStopAction = function () {
                var base = this;

                $('body').on('keydown', function (e) {
                    var keyCode = e.keyCode;
                    if (keyCode === 27) {
                        if (base.lastKeyCode === 27) {
                            base.runCheck = false;
                            window.clearTimeout(base.timer);
                            base.lastKeyCode = 0;
                        }
                        base.lastKeyCode = keyCode;
                    }
                });
            };

            ResourcesChecker.prototype.collectResourcePaths = function () {
                var base = this;
                $('script').each(function () {
                    var src = this.src;

                    if (src.indexOf("/UI/scripts") !== -1) {
                        src = src.substring(src.indexOf("/UI/scripts"), src.length);
                        if (src.indexOf('?')) {
                            src = src.substring(0, src.indexOf('?'));
                        }
                        base.resourceList.push(src);
                    }
                });

                $('link').each(function () {
                    var href = this.href;
                    if (href.indexOf("/UI/styles") !== -1) {
                        href = href.substring(href.indexOf("/UI/styles"), href.length);
                        if (href.indexOf('?') !== -1) {
                            href = href.substring(0, href.indexOf('?'));
                        }
                        base.resourceList.push(href);
                    }
                });
            };

            ResourcesChecker.prototype.checkModifiedDate = function () {
                var base = this;
                if (base.runCheck === true) {
                    $.ajax({
                        'url': "/DeveloperTools/filemodified.aspx?file=" + base.resourceList,
                        'cache': false,
                        'success': $.proxy(base.checkModifiedDateCallback, base)
                    });
                }
            };

            ResourcesChecker.prototype.checkModifiedDateCallback = function (data) {
                var base = this;
                if (data && data.length > 0) {
                    var fileupdated = parseInt(data, 10);
                    if (base.latestUpdated === 0) {
                        base.latestUpdated = fileupdated;
                    }

                    if (base.latestUpdated < fileupdated) {
                        window.location.reload();
                    }
                }

                base.timer = window.setTimeout($.proxy(base.checkModifiedDate, base), base.pollRate);
            };
            return ResourcesChecker;
        })();
        Developertools.ResourcesChecker = ResourcesChecker;
    })(Comprend.Developertools || (Comprend.Developertools = {}));
    var Developertools = Comprend.Developertools;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=ResourcesChecker.js.map
