﻿/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Utilities/UtilityStorageBase.ts" />
/// <reference path="Developertools.ts" />

module Comprend.Developertools
{
	export class FollowMe
	{
		private DevTools : Comprend.Developertools.Base;
		private currentAddress : string = "";
		private savedAddress : string = "";
		private followMeUrl : string = "/DeveloperTools/followme.aspx";
		private isMaster = false;
		private checkRate : number = 2000;
		private timer = null;
		constructor(mainbase : Comprend.Developertools.Base)
		{
			var base = this;
			base.DevTools = mainbase;

			base.isMaster = base.DevTools.GetSetting("followMeMaster",false);

			base.currentAddress = window.location.href;

			if (base.isMaster)
			{
				base.saveAddress();
			}
			else
			{
				base.checkSavedAddress();
			}
		}

		private saveAddress()
		{
			var base = this;
			$.ajax({
				url: base.followMeUrl,
				cache: false,
				data:
				{
					"save": base.currentAddress
				}
			});
		}

		private checkSavedAddress()
		{
			var base = this;

			$.ajax({
				url : base.followMeUrl,
				cache: false,
				success: base.oncheckSavedAddress.bind(base)
			});
		}

		private oncheckSavedAddress(data)
		{
			var base = this;

			if (data && data.length !== -1)
			{
				base.savedAddress = data;

				if (base.savedAddress.length === 0)
				{
					base.savedAddress = base.currentAddress;
				}
				if(base.DevTools.GetSetting("runFollowMe",false))
				{
					if (base.isMaster === false && base.savedAddress !== base.currentAddress)
					{
						window.location.href = base.savedAddress;
					}
					else
					{
						//Check again
						base.timer = window.setTimeout(base.checkSavedAddress.bind(base),base.checkRate);
					}
				}
			}
		}
	}
}