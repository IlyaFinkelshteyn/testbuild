﻿/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Utilities/UtilityStorageBase.ts" />
/// <reference path="Developertools.ts" />
var Comprend;
(function (Comprend) {
    (function (Developertools) {
        var FollowMe = (function () {
            function FollowMe(mainbase) {
                this.currentAddress = "";
                this.savedAddress = "";
                this.followMeUrl = "/DeveloperTools/followme.aspx";
                this.isMaster = false;
                this.checkRate = 2000;
                this.timer = null;
                var base = this;
                base.DevTools = mainbase;

                base.isMaster = base.DevTools.GetSetting("followMeMaster", false);

                base.currentAddress = window.location.href;

                if (base.isMaster) {
                    base.saveAddress();
                } else {
                    base.checkSavedAddress();
                }
            }
            FollowMe.prototype.saveAddress = function () {
                var base = this;
                $.ajax({
                    url: base.followMeUrl,
                    cache: false,
                    data: {
                        "save": base.currentAddress
                    }
                });
            };

            FollowMe.prototype.checkSavedAddress = function () {
                var base = this;

                $.ajax({
                    url: base.followMeUrl,
                    cache: false,
                    success: base.oncheckSavedAddress.bind(base)
                });
            };

            FollowMe.prototype.oncheckSavedAddress = function (data) {
                var base = this;

                if (data && data.length !== -1) {
                    base.savedAddress = data;

                    if (base.savedAddress.length === 0) {
                        base.savedAddress = base.currentAddress;
                    }
                    if (base.DevTools.GetSetting("runFollowMe", false)) {
                        if (base.isMaster === false && base.savedAddress !== base.currentAddress) {
                            window.location.href = base.savedAddress;
                        } else {
                            //Check again
                            base.timer = window.setTimeout(base.checkSavedAddress.bind(base), base.checkRate);
                        }
                    }
                }
            };
            return FollowMe;
        })();
        Developertools.FollowMe = FollowMe;
    })(Comprend.Developertools || (Comprend.Developertools = {}));
    var Developertools = Comprend.Developertools;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=FollowMe.js.map
