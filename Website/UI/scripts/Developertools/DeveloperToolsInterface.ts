/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="Developertools.ts" />

/* jshint scripturl:false */

module Comprend.Developertools
{
	export class DeveloperToolsInterface
	{
		private DevTools : Comprend.Developertools.Base;
		private baseElem : JQuery;
		private currentUser : string;
		constructor(mainbase : Comprend.Developertools.Base)
		{ 
			var base = this;
			base.DevTools = mainbase;

			base.currentUser = $('script[data-weinre-user]').attr('data-weinre-user');

			base.createMarkup();
			base.addEvents();
		}
		 
		addBookmarklets()
		{
			var base = this,
				$list = base.baseElem.find('.bookmarklets ul'),
				ariaHighlighter = "javascript:(function (d) {var css = '[tabindex=0]{background:yellow}[tabindex]:before{content:attr(tabindex);color:red;font-weight:bold}[role]"+
					"{background:pink}[role]:before{content:attr(role);color:green;font-weight:bold}',s = d.getElementById('a11y-css-highlight');if (s) {s.disabled = !s.disabled;} "+
					"else {s = d.createElement('style');s.id = 'a11y-css-highlight';s.type = 'text/css';if (s.styleSheet) { s.styleSheet.cssText = css; } else "+
					"{ s.appendChild(d.createTextNode(css)); } d.getElementsByTagName('head')[0].appendChild(s);}})(document);";
			
			$list
				.append('<li>'+
							'<a href="'+ "javascript:(function(F,i,r,e,b,u,g,L,I,T,E){if(F.getElementById(b))return;E=F[i+'NS']&&F.documentElement.namespaceURI;E=E?F[i+'NS'](E,'script'):"+
								"F[i]('script');E[r]('id',b);E[r]('src',I+g+T);E[r](b,u);(F[e]('head')[0]||F[e]('body')[0]).appendChild(E);E=new Image;E[r]('src',I+L);})"+
								"(document,'createElement','setAttribute','getElementsByTagName','FirebugLite','4','firebug-lite.js','releases/lite/latest/skin/xp/sprite.png',"+
								"'https://getfirebug.com/','#startOpened');" +'">'+
								'Firebug Lite'+
							'</a>'+
						'</li>')
			
				.append('<li>'+
							'<a href="'+ ariaHighlighter +'">'+
								'Aria Highlighter'+
							'</a>'+
						'</li>')
				.append('<li>'+
							'<a href="'+ "javascript:void(window.open('http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl='+encodeURIComponent(location.href),'Qr code','top=100,left=200,width=350,height=350,status=yes'));" +'">'+
								'Pagelink in QR'+
							'</a>'+
						'</li>');
		}

		addLinks()
		{
			var base = this,
				$list = base.baseElem.find('.links ul');

			

			$list
				.append('<li>'+
							'<a href="'+ "http://hhsvnlab01:8080/client/#" + base.currentUser +'" target="_blank">'+
								'weinre'+
							'</a>'+
						'</li>')
				.append('<li>'+
							'<a href="'+ "http://webdevchecklist.com/" +'" target="_blank">'+
								'Developer Checklist'+
							'</a>'+
						'</li>')
				.append('<li>'+
							'<a href="'+ "http://www.google.com/webmasters/tools/richsnippets" +'" target="_blank">'+
								'Microformat test'+
							'</a>'+
						'</li>');
		}

		addSettings()
		{
			var base = this,
				$resourceloaderfieldset = base.baseElem.find('.settings.resourceloader fieldset'),
				$followmefieldset = base.baseElem.find('.settings.functions fieldset');

			$followmefieldset
				.append('<fieldset>' +
					'<label for="runFollowMe">Activate Follow Me</label>' +
					'<input type="checkbox" name="runFollowMe" id="runFollowMe"/>' +
					'</fieldset>')
				.append('<fieldset>' +
					'<label for="followMeMaster">Follow this browser</label>' +
					'<input type="checkbox" name="followMeMaster" id="followMeMaster"/>' +
					'</fieldset>')
				.append('<fieldset>' +
					'<label for="runWeinre">Activate Weinre</label>' +
					'<input type="checkbox" name="runWeinre" id="runWeinre"/>' +
					'</fieldset>');

			$resourceloaderfieldset
				.append('<fieldset>' +
					'<label for="runGruntWatchLiveReload">Grunt Watch Livereload</label>' +
					'<input type="checkbox" name="runGruntWatchLiveReload" id="runResourcesChecker"/>' +
					'</fieldset>')
				.append('<fieldset>' +
					'<label for="runResourcesChecker">Auto refresh</label>' +
					'<input type="checkbox" name="runResourcesChecker" id="runResourcesChecker"/>' +
					'</fieldset>')
				.append('<fieldset>' +
					'<label for="resourcecheckerpollrate">Refresh rate</label>' +
					'<input type="text" name="resourcecheckerpollrate" id="resourcecheckerpollrate" value="" size="6"/>' +
					'</fieldset>');
				
			base.setSettingsValues();
			base.addSettingsEvents();
		}
		setSettingsValues()
		{
			var base = this;
			base.baseElem
				.find('.settings')
					.find('input[type=checkbox][name!="runWeinre"]')
						.each(function()
						{
							var $elm = $(this);
							
							if(base.DevTools.GetSetting($elm.attr('name')) === "true")
							{
								$elm.attr('checked','checked');
							}
							else
							{
								$elm.removeAttr('checked');
							}
						})
						.end()
					.find('input[type=checkbox][name="runWeinre"]')
						.each(function()
						{
							
							var $elm = $(this);

							$.ajax(
							{
								url:'/DeveloperTools/runweinre.aspx',
								success:
								$.proxy(function(data : string)
								{
									if (typeof data === "string")
									{

										if (data.length > 0)
										{
											$elm.attr('checked','checked');
										}
										else
										{
											$elm.removeAttr('checked');
										}
									}
								},base)
							});
						})
						.end()
					.find('input[type=text]')
						.each(function()
						{
							var $elm = $(this),
								value = base.DevTools.GetSetting($elm.attr('name'),null);
							if(value !== null)
							{
								$elm.val(value);
							}
						})
						.end();
		}

		addSettingsEvents()
		{
			var base = this;

			//Checkboxes
			base.baseElem
				.find('.settings')
					.find('input[type=checkbox][name!="runWeinre"]')
						.on('change',function(e : JQueryEventObject)
						{
							var $elm = $(this);
							base.DevTools.SetSetting($elm.attr('name'),($elm.is(':checked')+''));
						})
						.end()
					.find('input[type=checkbox][name="runWeinre"]')
						.on('change',function(e : JQueryEventObject)
						{
							var $elm = $(this);

							$.ajax(
								{
									url:'/DeveloperTools/runweinre.aspx',
									cache: false,
									data:
									{
										"save": ($elm.is(':checked')) ? "runweinre" : ""
									}
								});
						})
						.end()
					.find('input[type=text]')
						.on('keydown',function(e : JQueryEventObject)
						{
							var $elm = $(this),
								val = $elm.val();

							if(val && val.length > 0)
							{
								base.DevTools.SetSetting($elm.attr('name'),val);
							}
							
						})
						.end();

		}


		createMarkup()
		{
			var base = this;
			//Base element
			base.baseElem = $(''+
				'<div class="developertools">'+
					'<h2>Developer Tools</h2>'+
					'<div class="bookmarklets">'+
						'<h3>Bookmarklets</h3>'+
						'<ul></ul>'+
					'</div>'+

					'<div class="links">'+
						'<h3>Links</h3>'+
						'<ul></ul>'+
					'</div>'+

					'<div class="settings resourceloader">'+
						'<h3>Resource loader</h3>'+
						'<fieldset></fieldset>'+
					'</div>'+
					'<div class="settings functions">'+
						'<h3>Functions</h3>'+
						'<fieldset></fieldset>'+
					'</div>'+
				'</div>');

			var bookmarklets = base.baseElem.find('.bookmarklets ul:eq(0)'),
				links = base.baseElem.find('.links ul:eq(0)'),
				settings = base.baseElem.find('.settings fieldset:eq(0)');
			
			
			base.addBookmarklets();
			base.addLinks();
			base.addSettings();

			//Add elements to DOM
			base.baseElem.appendTo('body');
		}

		addEvents()
		{
			var base = this;
			base.baseElem
				.find('h2')
					.on('click',$.proxy(base.toggleInterface,base))
					.end();

			//debug
		//	base.baseElem.find('h2').trigger('click');
		}

		toggleInterface()
		{
			var base = this;
			base.baseElem.toggleClass('open');
		}
	}
}
