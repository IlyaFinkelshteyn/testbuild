/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="../Utilities/UtilityStorageBase.ts" />
/// <reference path="ResourcesChecker.ts" />
/// <reference path="DeveloperToolsInterface.ts" />
/* jshint scripturl:false */
module Comprend.Developertools
{
	export class Base
	{
		private DevTools : Comprend.Developertools.Base;
		private ResourceChecker : Comprend.Developertools.ResourcesChecker;
		private FollowMe : Comprend.Developertools.FollowMe;
		private DevToolsInterface : Comprend.Developertools.DeveloperToolsInterface;
		public Storage : Comprend.Utilities.StorageBase;
		private options = {
			settingsbasename : "kwddevelopertools"
		};

		constructor()
		{
			var base = this;
			base.DevTools = base;
			base.Storage = new Comprend.Utilities.StorageBase(true);
			base.InitiateModules();
		}

		InitiateModules()
		{
			var base = this;
			if(base.GetSetting('runFollowMe',"false") === "true")
			{
				base.FollowMe = new Comprend.Developertools.FollowMe(base.DevTools);
			}

			if(base.GetSetting('runResourcesChecker',false) === "true")
			{
				base.ResourceChecker = new Comprend.Developertools.ResourcesChecker(base.DevTools);
			}

			if(base.GetSetting('runGruntWatchLiveReload',false) === "true")
			{
				$('head').append('<script src="//localhost:35729/livereload.js"></script>');
			}

			base.DevToolsInterface = new Comprend.Developertools.DeveloperToolsInterface(base.DevTools);
		}

		public GetSetting(key : string, fallback? :any)
		{
			var base = this;
			var usedfallback = (typeof fallback === "undefined") ? false : fallback;
			return base.Storage.get(base.options.settingsbasename +'-'+ key,usedfallback,base.Storage.Type.LOCALSTORAGE);
		}
		public SetSetting(key : string, value : any)
		{
			var base = this;
			base.Storage.set(base.options.settingsbasename +'-'+ key,value,base.Storage.Type.LOCALSTORAGE);
		}
	}
}

$(document).ready(function()
{
	var DevTools = new Comprend.Developertools.Base();
});