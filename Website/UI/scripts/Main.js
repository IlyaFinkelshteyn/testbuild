/// <reference path="Definitions/jquery.d.ts" />
/// <reference path="utilities/utilitybase.ts" />
/// <reference path="Modules/LayoutAccordion.ts" />
/// <reference path="Modules/LayoutPageheader.ts" />
/// <reference path="Modules/Newsfeed.ts" />
/// <reference path="Modules/LayoutVideo.ts" />
/// <reference path="Modules/LayoutNewsletter.ts" />
/// <reference path="Modules/LayoutSearch.ts" />
var MainBase = (function () {
    function MainBase() {
        this.Utilities = null;
        this.Accordion = null;
        this.Pageheader = null;
        this.NewsFeed = null;
        this.Video = null;
        this.Newsletter = null;
        this.Search = null;
        this.MobileMenu = null;
        this.debug = true;
        var base = this;
        //Before onload
        base.MainBase = base;
        base.Utilities = base.LoadModule("Comprend.Utilities.Base", base);
        base.Accordion = base.LoadModule("Comprend.Layout.AccordionBase", base);
        base.Pageheader = base.LoadModule("Comprend.Layout.PageheaderBase", base);
        base.NewsFeed = base.LoadModule("Comprend.Layout.NewsfeedBase", base);
        // base.Video = base.LoadModule("Comprend.Layout.VideoBase", base);
        base.Newsletter = base.LoadModule("Comprend.Layout.NewsletterBase", base);
        base.Search = base.LoadModule("Comprend.Layout.SearchBase", base);
        base.MobileMenu = base.LoadModule("Comprend.Layout.MobileMenuBase", base);
    }
    MainBase.prototype.init = function () {
        //After onload
        //Load all additional modules here
        var base = this;
        base.pageLanguage = $('html').attr('language');
        if (base.Utilities != null) {
            base.Utilities.Init();
        }
        if (base.Video != null) {
            base.Video.Init();
        }
        if (base.Accordion != null) {
            base.Accordion.Init();
        }
        if (base.Pageheader != null) {
            base.Pageheader.Init();
        }
        if (base.NewsFeed != null) {
            base.NewsFeed.Init();
        }
        if (base.Newsletter != null) {
            base.Newsletter.Init();
        }
        if (base.Search != null) {
            base.Search.Init();
        }
        if (base.MobileMenu != null) {
            base.MobileMenu.Init();
        }
    };
    /* ===================== Module Loading =====================*/
    //#region ModuleLoading
    MainBase.prototype.LoadModule = function (moduleNameSpace, startArgument) {
        var base = this;
        var namespaces = moduleNameSpace.split('.');
        var currentNamespace = window;
        var moduleConstructor;
        for (var i = 0; i < namespaces.length; i++) {
            if (typeof currentNamespace[namespaces[i]] === "object") {
                currentNamespace = currentNamespace[namespaces[i]];
            }
            if (typeof currentNamespace[namespaces[i]] === "function") {
                moduleConstructor = currentNamespace[namespaces[i]];
            }
        }
        if (typeof moduleConstructor === "function") {
            if (typeof startArgument !== "undefined") {
                return new moduleConstructor.prototype.constructor(startArgument);
            }
            else {
                return new moduleConstructor();
            }
        }
        else {
            base.log(moduleNameSpace + " is unavailable. Is it added to the project?");
            return null;
        }
    };
    //#endregion
    MainBase.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        if (this.debug) {
            console.log(arguments);
        }
    };
    return MainBase;
})();
var scriptbase = new MainBase();
$(document).ready(function () {
    scriptbase.init();
});
//# sourceMappingURL=Main.js.map