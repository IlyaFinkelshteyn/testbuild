﻿/* jshint undef:false, unused:false */
 
var EPiServerWidgetBase = {
	options:
	{

	},
	Init: function() {
		var base = EPiServerWidgetBase;
		base.EPiServerWidgetBaseManager.Init();
	},
	EPiServerWidgetBaseManager:
	{
		Init: function() {
			var base = EPiServerWidgetBase;
			base.LocalBlocksHider.Init();
		}
	},
	LocalBlocksHider:
	{
		timer: null,
		index: 0,
		Init: function() {
			var base = EPiServerWidgetBase;
			base.timer = window.setTimeout(base.findLocalItemBlockAndHide, 1000);
		},
		findLocalItemBlockAndHide: function() {
			var base = EPiServerWidgetBase.LocalBlocksHider,
				localblockitem = $('span.dijitTreeContent[title="LocalBlocks"]:not(.hiddenlocalblockitem)');

			base.index++;

			if (localblockitem.length > 0) {
				//console.log('about to remove the item');

				var tmp = localblockitem
					.addClass('hiddenlocalblockitem')
					.parents('.epi-folderTreeNode:eq(0)')
					.addClass('hiddenlocalblockitem')
					.prev('.epi-folderTreeNode')
					.find('.dijitTreeContent:eq(0)')
					.trigger('click');

				window.clearTimeout(base.timer);
			} else {
				base.timer = window.setTimeout(base.findLocalItemBlockAndHide, 1000);
			}

		}
	}
};

$(document).ready(function()
{
	EPiServerWidgetBase.Init();
});