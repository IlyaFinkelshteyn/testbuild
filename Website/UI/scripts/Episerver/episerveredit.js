﻿/* jshint undef:false */
var EPiServerEditAdditions = {
	options:
	{
		ContentAreaSelector: '.epi-editContainer',
		BlockSelector: '> div',
		WidthClass: 'col-',
		InlinePropertyManagerClass: '.InlinePropertyManager',
		InlinePropertyManagerPropertiesClass: '.Properties',
		InlinePropertyManagerPropertyClass: '.Property',
		InlinePropertyManagerEpiPropertyClass: '.epi-editContainer'
	},
	Init: function ()
	{
		var base = EPiServerEditAdditions;
		base.BlockLayoutManager.Init();
		base.InlinePropertyManager.Init();
	},
	InlinePropertyManager:
	{
		$PropertiesElm: null,
		$PreviewElm: null,
		Init: function ()
		{
			var base = EPiServerEditAdditions;

			this.InitiatePropertyTabs();

			this.$PreviewElm = $(base.options.InlinePropertyManagerClass);
			this.$PropertiesElm = $(base.options.InlinePropertyManagerPropertiesClass);

			var $EPiProperties = this.$PropertiesElm.find(base.options.InlinePropertyManagerEpiPropertyClass);

			//Manage width changes
			$EPiProperties.filter("[data-epi-property-name='Width']").watch('innerHTML', base.InlinePropertyManager.ManageWidthChange);

			//Manage theme changes
			$EPiProperties.filter("[data-epi-property-name='Theme']").watch('innerHTML', base.InlinePropertyManager.ManageThemeChange);

			//Image Placement change
			$EPiProperties.filter("[data-epi-property-name='WidgetImagePlacement']").watch('innerHTML', base.InlinePropertyManager.ManageImagePlacementChange);

			//Image Placement change
			$EPiProperties.filter("[data-epi-property-name='IframeURL']").watch('innerHTML', base.InlinePropertyManager.ManageIframeSRCChange);

			//ScriptText (Video embed)
			$EPiProperties.filter("[data-epi-property-name='ScriptText']").watch('innerHTML', base.InlinePropertyManager.ManageEmbedChange);

			this.BlockSpecificAdditions.init();

		},
		InitiatePropertyTabs: function ()
		{
			if ($(".pageproperties,.blockproperties:visible").length !== 0)
			{
				if(typeof $(".pageproperties,.blockproperties:visible").accessibleTabs !== 'function')
				{
					$.getScript('/UI/scripts/vendor/jquery.tabs.min.js', $.proxy(function (){
						this.InitiateTabs();
					}, this));
				}
				else
				{
					this.InitiateTabs();
				}
			}
		},
		InitiateTabs: function()
		{
			var base = this;
			$(".pageproperties,.blockproperties:visible").each(function()
			{
				var $propertyContainer = $(this),
					bolOpenFirst = $propertyContainer.hasClass('openfirst');

				$propertyContainer
					.accessibleTabs(
					{
						wrapperClass: 'content', /* Classname to apply to the div that is wrapped around the original Markup */
						currentClass: 'current', /* Classname to apply to the LI of the selected Tab */
						tabhead: 'h4', /* Tag or valid Query Selector of the Elements to Transform the Tabs-Navigation from (originals are removed) */
						tabheadClass: '', /* Classname to apply to the target heading element for each tab div */
						tabbody: 'h4 + div', /* Tag or valid Query Selector of the Elements to be treated as the Tab Body */
						fx: 'show', /* can be "fadeIn", "slideDown", "show" */
						fxspeed: 0, /* speed (String|Number): "slow", "normal", or "fast") or the number of milliseconds to run the animation */
						currentInfoText: 'current tab: ', /* text to indicate for screenreaders which tab is the current one */
						currentInfoPosition: 'prepend', /* Definition where to insert the Info Text. Can be either "prepend" or "append" */
						currentInfoClass: 'current-info', /* Class to apply to the span wrapping the CurrentInfoText */
						tabsListClass: 'tabs-list', /* Class to apply to the generated list of tabs above the content */
						syncheights: true, /* syncs the heights of the tab contents when the SyncHeight plugin is available http:/*blog.ginader.de/dev/jquery/syncheight/index.php */
						syncHeightMethodName: 'syncHeight', /* set the Method name of the plugin you want to use to sync the tab contents. Defaults to the SyncHeight plugin: http:/*github.com/ginader/syncHeight */
						cssClassAvailable: false, /* Enable individual css classes for tabs. Gets the appropriate class name of a tabhead element and apply it to the tab list element. Boolean value */
						saveState: true, /* save the selected tab into a cookie so it stays selected after a reload. This requires that the wrapping div needs to have an ID (so we know which tab we're saving) */
						autoAnchor: false, /* will move over any existing id of a headline in tabs markup so it can be linked to it */
						pagination: false, /* adds buttons to each tab to switch to the next/previous tab */
						position: 'top', /* can be 'top' or 'bottom'. Defines where the tabs list is inserted.  */
						wrapInnerNavLinks: '', // inner wrap for a-tags in tab navigation. See http://api.jquery.com/wrapInner/ for further informations */
						firstNavItemClass: 'first', /* Classname of the first list item in the tab navigation */
						lastNavItemClass: 'last', /* Classname of the last list item in the tab navigation */
						clearfixClass: '', /* Name of the Class that is used to clear/contain floats */
						openFirstTab: bolOpenFirst,
						callback: base.AfterTabsInitiation
					});
			});
				
				
		},
		AfterTabsInitiation: function($el)
		{

			/* This calculates the number of recommended fields and show an indication */
			var $propertyContainer = $el.find('.Properties');
			$propertyContainer.each(function(i, o)
			{
				var $container = $(o),
					usedCount = 0,
					$containerHeadline = $container.prev('h4'),
					$tabHeading = $el.find("a[href='#"+ $containerHeadline.attr('id') +"']"),
					$properties = $container.find('.Property'),
					totalCount = $properties.find('.recommended').length;

				$properties.each(function(i,p)
				{
					var $p = $(p).find('.recommended');
					 usedCount = ($p.text().length !== 0) ? usedCount + 1 : usedCount;
				});

				if (totalCount !== 0)
				{
					var $statusTag = $('<strong>&#160;&#160;(' + usedCount + '/' + totalCount + ')</strong>');
					if (usedCount === 0)
					{
						$statusTag.css({ 'color': '#f00' });	
					}
					$tabHeading.append($statusTag);
				}
			});
		},
		ManageEmbedChange: function (propName, oldVal, newVal)
		{
			var $videoWrapper = $('.videoWrapper');
			if ($videoWrapper.length !== 0)
			{
				//$videoWrapper.html()
				var decoded = $('<textarea/>').html(newVal).val();
				$videoWrapper.html(decoded);
				EPiServerEditAdditions.InlinePropertyManager.BlockSpecificAdditions.VideoBlock();
			}
		},
		ManageIframeSRCChange: function (propName, oldVal, newVal)
		{
			var base = EPiServerEditAdditions;
			if (newVal !== oldVal)
			{
				$('iframe.iframecontent').attr('src', newVal);
			}
		},
		ManageImagePlacementChange: function (propName, oldVal, newVal)
		{
			var base = EPiServerEditAdditions;

			var currentClass = '',
				$widget = base.InlinePropertyManager.$PreviewElm.find('.block'),
				classesWithPanelContent = 'imageleft, imagetop, imageright';

			if ($widget.length !== 0)
			{
				currentClass = $widget.attr('class');
				currentClass = currentClass.replace(oldVal, newVal);
				$widget.attr('class', currentClass);
			}
		},
		ManageWidthChange: function (propName, oldVal, newVal)
		{
			var base = EPiServerEditAdditions;

			var oldSpanValue = 'span' + (oldVal * 3),
				currentClass = '',
				$widget = base.InlinePropertyManager.$PreviewElm.find('.block.' + oldSpanValue);

			if ($widget.length !== 0)
			{
				currentClass = $widget.attr('class');
				currentClass = currentClass.replace('col' + oldVal + ' ' + oldSpanValue, 'col' + newVal + ' span' + (newVal * 3));
				$widget.attr('class', currentClass);
			}
		},
		ManageThemeChange: function (propName, oldVal, newVal)
		{
			var base = EPiServerEditAdditions;

			var currentClass = '',
				$widget = base.InlinePropertyManager.$PreviewElm.find('.block'),
				classesWithPanelContent = 'simple panel, panel, panel brown, panel green, panel mediumpetrol';

			if ($widget.length !== 0)
			{
				currentClass = $widget.attr('class');
				if (classesWithPanelContent.indexOf(newVal) !== -1)
				{
					$widget.find('div:eq(0)').addClass('widget panel-content');
				}
				else
				{
					$widget.find('div:eq(0)').removeClass('widget panel-content');
				}
				currentClass = currentClass.replace(oldVal, newVal);
				$widget.attr('class', currentClass);
			}
		},
		BlockSpecificAdditions:
		{
			init: function ()
			{
				this.ContentWithImage();
				this.VideoBlock();
			},
			VideoBlock: function ()
			{

				var $VideoBlock = $('.VideoBlock');
				if ($VideoBlock.length !== 0)
				{
					var $videoWrapper = $VideoBlock.find('.videoWrapper'),
						Layout = (typeof Layout !== 'undefined') ? Layout : { Video: { Init: function () { } } };

					if ($videoWrapper.width() !== 0)
					{
						Layout.Video.Init();
					}
					else
					{
						window.setTimeout(function () { EPiServerEditAdditions.InlinePropertyManager.BlockSpecificAdditions.VideoBlock(); }, 1000);
					}
				}

			},
			ContentWithImage: function ()
			{
				var base = EPiServerEditAdditions,
					$widget = base.InlinePropertyManager.$PreviewElm.find('.block');

				$widget.addClass('hasimage');
			}
		}
	},
	BlockLayoutManager:
	{
		Init: function ()
		{
			var base = EPiServerEditAdditions;
			base.BlockLayoutManager.checkWidgetWidth($(base.options.ContentAreaSelector + ' ' + base.options.BlockSelector));

			$(base.options.ContentAreaSelector).each(function ()
			{
				base.observeDOM(this, function (obj)
				{
					var base = EPiServerEditAdditions,
						originObject = (typeof obj !== 'undefined' && obj && obj.tagName) ? obj : this;
					if (originObject.tagName && originObject.tagName === 'DIV')
					{
						EPiServerEditAdditions.BlockLayoutManager.checkWidgetWidth($(originObject).find(base.options.BlockSelector));
					}
				});
			});
		},
		checkWidgetWidth: function ($arObjs)
		{
			if ($arObjs && $arObjs.length > 0)
			{

				var base = EPiServerEditAdditions;
				$arObjs.each(function (i, obj)
				{
					var $epiContainer = $(obj),
						$widget = $epiContainer.find(base.options.BlockSelector).not('.EditModeOnly'),
						strClass = $widget.attr('class'),
						width = '';

					/*if ($epiContainer.parent().parent().hasClass("WideBlockArea4"))
					{
						
					}*/
					if (strClass && strClass.indexOf(base.options.WidthClass) !== -1)
					{
						strClass = strClass.substring(strClass.indexOf(base.options.WidthClass), strClass.length);
						if (strClass.indexOf(' ') !== -1)
						{
							width = strClass.substring(0, strClass.indexOf(' '));
						}
						else
						{
							width = strClass;
						}
						/*if ($epiContainer.parent().parent().hasClass("WideBlockArea4"))
						{
							
						}*/
						$epiContainer.addClass(width);
					}
				});
			}
		}
	},
	observeDOM: (function ()
	{
		var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
			eventListenerSupported = window.addEventListener;

		return function (obj, callback)
		{

			if (MutationObserver)
			{
				// define a new observer
				var obs = new MutationObserver(function (mutations, observer)
				{
					if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
					{
						callback(obj);
					}

				});
				// have the observer observe foo for changes in children
				obs.observe(obj, { childList: true, subtree: true });
			}
			else if (eventListenerSupported)
			{
				obj.addEventListener('DOMNodeInserted', callback, false);
				obj.addEventListener('DOMNodeRemoved', callback, false);
			}
		};
	})()
};

jQuery.fn.watch = function (id, fn)
{
	return this.each(function ()
	{
		var self = this,
			oldVal = self[id];
		$(self).data(
			'watch_timer',
			setInterval(function ()
			{
				if (self[id] !== oldVal)
				{
					fn.call(self, id, oldVal, self[id]);
					oldVal = self[id];
				}
			}, 100)
		);
	});
};

jQuery.fn.unwatch = function (id)
{
	return this.each(function ()
	{
		clearInterval($(this).data('watch_timer'));
	});
};

$(document).ready(function ()
{
	EPiServerEditAdditions.Init();
});