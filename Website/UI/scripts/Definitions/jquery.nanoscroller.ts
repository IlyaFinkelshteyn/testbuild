﻿// Type definitions for jQuery.nanoScroller 
// https://jamesflorentino.github.io/nanoScrollerJS/
/// <reference path="jquery.d.ts" />

interface NanoScroller
{
	(method?: any): void;
}

interface JQuery
{
	nanoScroller: NanoScroller;
}