﻿/// <reference path="jquery.d.ts"/>

interface MatchHeight {
	(options?: any): JQuery;
}

interface JQuery {
	matchHeight : MatchHeight;
}