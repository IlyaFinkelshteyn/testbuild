// Type definitions for jQuery.visual 
/// <reference path="jquery.d.ts" />

interface Visible {
	(method?: any): void;
}

interface JQuery {
	visible: Visible;
}