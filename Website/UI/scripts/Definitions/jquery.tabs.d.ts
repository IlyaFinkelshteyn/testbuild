/// <reference path="jquery.d.ts"/>

interface AccessibleTabsOptions {
	wrapperClass?: string; // Classname to apply to the div that is wrapped around the original Markup
	currentClass?: string; // Classname to apply to the LI of the selected Tab
	tabhead?: string; // Tag or valid Query Selector of the Elements to Transform the Tabs-Navigation from (originals are removed)
	tabheadClass?: string; // Classname to apply to the target heading element for each tab div
	tabbody?: string; // Tag or valid Query Selector of the Elements to be treated as the Tab Body
	fx?:string; // can be "fadeIn", "slideDown", "show"
	fxspeed?: any; // speed (String|Number)?: "slow", "normal", or "fast") or the number of milliseconds to run the animation
	currentInfoText?: string; // text to indicate for screenreaders which tab is the current one
	currentInfoPosition?: string; // Definition where to insert the Info Text. Can be either "prepend" or "append"
	currentInfoClass?: string; // Class to apply to the span wrapping the CurrentInfoText
	tabsListClass?:string; // Class to apply to the generated list of tabs above the content
	syncheights?:boolean; // syncs the heights of the tab contents when the SyncHeight plugin is available http?://blog.ginader.de/dev/jquery/syncheight/index.php
	syncHeightMethodName?:string; // set the Method name of the plugin you want to use to sync the tab contents. Defaults to the SyncHeight plugin?: http?://github.com/ginader/syncHeight
	cssClassAvailable?:boolean; // Enable individual css classes for tabs. Gets the appropriate class name of a tabhead element and apply it to the tab list element. Boolean value
	saveState?:boolean; // save the selected tab into a cookie so it stays selected after a reload. This requires that the wrapping div needs to have an ID (so we know which tab we're saving)
	autoAnchor?:boolean; // will move over any existing id of a headline in tabs markup so it can be linked to it
	pagination?:boolean; // adds buttons to each tab to switch to the next/previous tab
	position?:string; // can be 'top' or 'bottom'. Defines where the tabs list is inserted. 
	wrapInnerNavLinks?: string; // inner wrap for a-tags in tab navigation. See http?://api.jquery.com/wrapInner/ for further informations
	firstNavItemClass?: string; // Classname of the first list item in the tab navigation
	lastNavItemClass?: string; // Classname of the last list item in the tab navigation
	clearfixClass?: string; // Name of the Class that is used to clear/contain floats
	openFirstTab?: boolean; //If set to false no tabs will be open
	callback?:Function;
}

interface AccessibleTabs {
    (options?: AccessibleTabsOptions): JQuery;
}

interface JQuery {
    accessibleTabs: AccessibleTabs;
}