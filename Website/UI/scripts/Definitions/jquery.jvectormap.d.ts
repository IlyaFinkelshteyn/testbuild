/// <reference path="jquery.d.ts"/>

interface VectorMap {
    (options?: any): JQuery;
}

interface JQuery {
    vectorMap: VectorMap;
}