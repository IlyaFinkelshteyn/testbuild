/// <reference path="jquery.d.ts"/>

interface EasyPaginateOptions {
	step?: string;
	delay?: string;
	numeric?: boolean;
	nextprev?: boolean;
	auto?: boolean;
	loop?: boolean;
	pause?: string;
	clickstop?: boolean;
	controls?: string;
	current?: string;
	randomstart?: boolean;
}

interface EasyPaginate {
	(options?: EasyPaginateOptions): JQuery;
}

interface JQuery {
    easyPaginate: any;
}