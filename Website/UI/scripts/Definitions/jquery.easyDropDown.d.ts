﻿/// <reference path="../Definitions/jquery.d.ts"/>
/* tslint:disable */

interface JQuery
{
	easyDropDown: EasyDropDown.easyDropDown;
}

declare module EasyDropDown
{
	interface easyDropDown
	{
		(Options, any?): JQuery;
	}
	interface Options
	{
		cutOff? : any;
		wrapperClass? : any;
	}
}