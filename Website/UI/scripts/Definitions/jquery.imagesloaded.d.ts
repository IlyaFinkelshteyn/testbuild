// ImagesLoaded
// Definitions: https://github.com/borisyankov/DefinitelyTyped

/// <reference path="jquery.d.ts"/>

interface ImagesLoaded {
	(options?: any): JQuery;
}

interface JQuery {
    imagesLoaded: ImagesLoaded;
} 