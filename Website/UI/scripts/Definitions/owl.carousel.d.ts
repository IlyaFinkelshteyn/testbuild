// ImagesLoaded
/// <reference path="jquery.d.ts"/>

interface OwlCarousel {
	(options?: any): JQuery;
}

interface JQuery {
	owlCarousel: OwlCarousel;
} 