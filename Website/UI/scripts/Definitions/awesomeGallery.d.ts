﻿/// <reference path="jquery.d.ts" />

interface AwesomeGallery {
	(options?: any): JQuery;
}

interface JQuery {
	awesomeGallery: any;
}