﻿interface IFastClickOptions
{
	touchBoundary? : any;
}

interface IFastClick
{
	(layer : HTMLElement, options? : IFastClickOptions) : void;
	attach(layer : HTMLElement, options? : IFastClickOptions) : void;
}

declare var FastClick: IFastClick;