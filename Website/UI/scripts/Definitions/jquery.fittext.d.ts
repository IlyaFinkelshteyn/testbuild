﻿/// <reference path="../Definitions/jquery.d.ts"/>
/* tslint:disable:class-name */
interface JQuery
{
	fitText: FitText.fitText;
}

declare module FitText
{
	interface fitText
	{
		(kompressor? : number, options?: FitText.Options): JQuery;
	}
	interface Options
	{
		minFontSize? : string;
		maxFontSize? : string;
	}
}