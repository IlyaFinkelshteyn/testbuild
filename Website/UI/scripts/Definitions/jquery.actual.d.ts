// Type definitions for jQuery.actual Version: 1.0.14
// Project: https://github.com/dreamerslab/jquery.actual

/// <reference path="jquery.d.ts" />

interface ActualOptions
{
	absolute? : boolean;
	clone? : boolean;
	includeMargin? : boolean;
}

interface Actual {
	(method: string): number;
    (method: string, options: ActualOptions): number;
}

interface JQuery {
    actual: Actual;
}