interface IGoogle
{
	load(str? : string) : void;
	setOnLoadCallback(fn : Function) : void;
	visualization : any;
}