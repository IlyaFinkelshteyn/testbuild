/// <reference path="jquery.d.ts"/>

interface CommunicateAreaOptions {

}

interface CommunicateArea {
	(): any;
	(options?: CommunicateAreaOptions): any;
    (el : JQuery, options?: CommunicateAreaOptions): any;
}

interface JQuery {
    communicateArea: CommunicateArea;
}