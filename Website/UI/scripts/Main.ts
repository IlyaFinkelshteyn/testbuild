/// <reference path="Definitions/jquery.d.ts" />
/// <reference path="utilities/utilitybase.ts" />
/// <reference path="Modules/LayoutAccordion.ts" />
/// <reference path="Modules/LayoutPageheader.ts" />
/// <reference path="Modules/Newsfeed.ts" />
/// <reference path="Modules/LayoutVideo.ts" />
/// <reference path="Modules/LayoutNewsletter.ts" />
/// <reference path="Modules/LayoutSearch.ts" />

class MainBase
{
	public MainBase : MainBase;

    public Utilities: Comprend.Utilities.Base = null;
    public Accordion: Comprend.Layout.AccordionBase = null;
	public Pageheader: Comprend.Layout.PageheaderBase = null;
    public NewsFeed: Comprend.Layout.NewsfeedBase = null;
    public Video: Comprend.Layout.VideoBase = null;
    public Newsletter: Comprend.Layout.NewsletterBase = null;
    public Search: Comprend.Layout.SearchBase = null;

	public MobileMenu: Comprend.Layout.MobileMenuBase = null;

	//public Newsroom : Comprend.Newsroom.Base = null;
	public pageLanguage : string;
	private debug = true;

	constructor()
	{
		var base = this;
		//Before onload
		base.MainBase = base;

        base.Utilities = base.LoadModule("Comprend.Utilities.Base", base);
        base.Accordion = base.LoadModule("Comprend.Layout.AccordionBase", base);
		base.Pageheader = base.LoadModule("Comprend.Layout.PageheaderBase", base);
        base.NewsFeed = base.LoadModule("Comprend.Layout.NewsfeedBase", base);
       // base.Video = base.LoadModule("Comprend.Layout.VideoBase", base);
        base.Newsletter = base.LoadModule("Comprend.Layout.NewsletterBase", base);
        base.Search = base.LoadModule("Comprend.Layout.SearchBase", base);
		
		base.MobileMenu = base.LoadModule("Comprend.Layout.MobileMenuBase", base);
	}

	public init()
	{
		//After onload
		//Load all additional modules here
		var base = this;

		base.pageLanguage = $('html').attr('language');

		if (base.Utilities != null)
		{
			base.Utilities.Init();
        }
        if (base.Video != null) {
            base.Video.Init();
        }
        if (base.Accordion != null)
        {
            base.Accordion.Init();
        }
		if (base.Pageheader != null) {
            base.Pageheader.Init();
        }
		if (base.NewsFeed != null)
		{
			base.NewsFeed.Init();
		}
		if (base.Newsletter != null) {
			base.Newsletter.Init();
        }
        if (base.Search != null) {
            base.Search.Init();
        }
		if (base.MobileMenu != null)
		{
			base.MobileMenu.Init();
		}
	}
	
	/* ===================== Module Loading =====================*/
	//#region ModuleLoading
	public LoadModule(moduleNameSpace : string, startArgument? : any)
	{
		var base = this;
		var namespaces : string[] = moduleNameSpace.split('.');

		var currentNamespace : any = window;
		var moduleConstructor : any;
		for (var i = 0; i < namespaces.length; i++)
		{
			if (typeof currentNamespace[namespaces[i]] === "object")
			{
				currentNamespace = currentNamespace[namespaces[i]];
			}
			if (typeof currentNamespace[namespaces[i]] === "function")
			{
				moduleConstructor = currentNamespace[namespaces[i]];
			}
		}
		
		if (typeof moduleConstructor === "function")
		{
			if (typeof startArgument !== "undefined")
			{
				return new moduleConstructor.prototype.constructor(startArgument);
			}
			else
			{
				return new moduleConstructor();
			}
		}
		else
		{
			base.log(moduleNameSpace + " is unavailable. Is it added to the project?");
			return null;
		}
		
	}

	//#endregion

	public log(...args)
	{
		if (this.debug)
		{
			console.log(arguments);
		}
	}

}

var scriptbase = new MainBase();

$(document).ready(function()
{
	scriptbase.init();
});