﻿ /// <reference path="../Definitions/jquery.d.ts" />

interface IGetPropertyPropertyReturn
{
	ContentReference : string;
	Items? : IGetPropertyPropertyItem[];
}

interface IGetPropertyPropertyItem
{
	PropertyName : string;
	PropertyValue : string;
	Type? : string;
}

module Comprend.Utilities
{
	export class GetPropertyHandler
	{
		private data : Object;
		private dataurl = "/WebApi/Properties/";

		constructor()
		{
			//We expect this method to be run after document ready
			//Example Usage
			/*
			console.log(this.getProperties("94", "Title"));

			this.getProperties("94", "Email, Title", function(dataobj)
			{
				console.log(dataobj);
			});

			console.log(this.getProperty("94", "Title"));
			
			*/
		}

		public getProperty(pageId : string, property : string, callback? : Function)
		{
			var base = this,
				propertyReturn: IGetPropertyPropertyReturn = base.getProperties(pageId, property);

			if (propertyReturn !== null && typeof propertyReturn !== "undefined" && propertyReturn.Items !== null && propertyReturn.Items.length !== 0)
			{
				for (var i = 0; i < propertyReturn.Items.length; i++)
				{
					var item : IGetPropertyPropertyItem= propertyReturn.Items[i];

					if (item.PropertyName === property)
					{
						if (typeof callback === 'function')
						{
							callback(item.PropertyValue);
						}
						else
						{
							return item.PropertyValue;
						}
					}
				}
			}
		}

		public getProperties(pageId : string, properties : string, callback? : Function)
		{
			var base = this,
				jsonData : any = null,
				async = false;
			 
			if (typeof callback === 'function')
			{
				//Async version
				$.ajax(base.dataurl,
				{
					"async":true,
					"cache":true,
					"dataType":"json",
					"type":"POST",
					"data":
					{
						"ContentReference":pageId,
						"Properties":properties
					},
					"complete":function(data)
					{
						if(data !== null && typeof data !== 'undefined' && data.responseText !== null && data.responseText.length !== 0)
						{
							jsonData = JSON.parse(data.responseText);
							callback(jsonData);
						}
					}
				});
			}
			else
			{
				//Not async
				$.ajax(base.dataurl,
				{
					"async":false,
					"cache":true,
					"dataType":"json",
					"type":"POST",
					"data":
					{
						"ContentReference":pageId,
						"Properties":properties
					},
					"complete":function(data)
					{
						if(data !== null && typeof data !== 'undefined' && data.responseText !== null && data.responseText.length !== 0)
						{
							jsonData = JSON.parse(data.responseText);
						}
					}
				});

				return jsonData;
			}
		}
	}
} 