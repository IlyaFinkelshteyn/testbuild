﻿/// <reference path="../Definitions/jquery.d.ts" />

var Comprend;
(function (Comprend) {
    (function (Utilities) {
        var GetPropertyHandler = (function () {
            function GetPropertyHandler() {
                this.dataurl = "/WebApi/Properties/";
                //We expect this method to be run after document ready
                //Example Usage
                /*
                console.log(this.getProperties("94", "Title"));
                
                this.getProperties("94", "Email, Title", function(dataobj)
                {
                console.log(dataobj);
                });
                
                console.log(this.getProperty("94", "Title"));
                
                */
            }
            GetPropertyHandler.prototype.getProperty = function (pageId, property, callback) {
                var base = this, propertyReturn = base.getProperties(pageId, property);

                if (propertyReturn !== null && typeof propertyReturn !== "undefined" && propertyReturn.Items !== null && propertyReturn.Items.length !== 0) {
                    for (var i = 0; i < propertyReturn.Items.length; i++) {
                        var item = propertyReturn.Items[i];

                        if (item.PropertyName === property) {
                            if (typeof callback === 'function') {
                                callback(item.PropertyValue);
                            } else {
                                return item.PropertyValue;
                            }
                        }
                    }
                }
            };

            GetPropertyHandler.prototype.getProperties = function (pageId, properties, callback) {
                var base = this, jsonData = null, async = false;

                if (typeof callback === 'function') {
                    //Async version
                    $.ajax(base.dataurl, {
                        "async": true,
                        "cache": true,
                        "dataType": "json",
                        "type": "POST",
                        "data": {
                            "ContentReference": pageId,
                            "Properties": properties
                        },
                        "complete": function (data) {
                            if (data !== null && typeof data !== 'undefined' && data.responseText !== null && data.responseText.length !== 0) {
                                jsonData = JSON.parse(data.responseText);
                                callback(jsonData);
                            }
                        }
                    });
                } else {
                    //Not async
                    $.ajax(base.dataurl, {
                        "async": false,
                        "cache": true,
                        "dataType": "json",
                        "type": "POST",
                        "data": {
                            "ContentReference": pageId,
                            "Properties": properties
                        },
                        "complete": function (data) {
                            if (data !== null && typeof data !== 'undefined' && data.responseText !== null && data.responseText.length !== 0) {
                                jsonData = JSON.parse(data.responseText);
                            }
                        }
                    });

                    return jsonData;
                }
            };
            return GetPropertyHandler;
        })();
        Utilities.GetPropertyHandler = GetPropertyHandler;
    })(Comprend.Utilities || (Comprend.Utilities = {}));
    var Utilities = Comprend.Utilities;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=utilitygetpropertyhandler.js.map
