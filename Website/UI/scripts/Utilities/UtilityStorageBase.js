﻿var Comprend;
(function (Comprend) {
    /* jshint unused:false */
    (function (Utilities) {
        var StorageBase = (function () {
            function StorageBase(bolAutoInitiate) {
                this.localStorage = null;
                this.sessionStorage = null;
                this.Type = {
                    LOCALSTORAGE: "localStorage",
                    SESSIONSTORAGE: "sessionStorage"
                };
                //Before onload
                var base = this;
                if (bolAutoInitiate === true) {
                    base.Init();
                }
            }
            StorageBase.prototype.Init = function () {
                //After onload
                //Load all additional modules here
                var base = this;

                if (base.hasStorageObject('localStorage') === true) {
                    base.localStorage = window.localStorage;
                } else {
                    console.error("No localStorage support");
                }

                if (base.hasStorageObject('sessionStorage') === true) {
                    base.sessionStorage = window.sessionStorage;
                } else {
                    console.error("No sessionStorage support");
                }
            };

            StorageBase.prototype.resolveStorageType = function (storageType) {
                var base = this, storageObject = null;

                if (storageType !== null || typeof storageType !== "undefined" || storageType.length !== 0) {
                    switch (storageType) {
                        case base.Type.SESSIONSTORAGE:
                            storageObject = (base.sessionStorage !== null) ? base.sessionStorage : null;
                            break;
                        case base.Type.LOCALSTORAGE:
                            storageObject = (base.localStorage !== null) ? base.localStorage : null;
                            break;
                        default:
                            storageObject = (base.localStorage !== null) ? base.localStorage : null;
                            break;
                    }
                }
                if (localStorage === null) {
                    storageObject = base.localStorage;
                }
                return storageObject;
            };

            StorageBase.prototype.hasStorageObject = function (storagetype) {
                try  {
                    return storagetype in window && window[storagetype] !== null;
                } catch (e) {
                    return false;
                }
            };

            StorageBase.prototype.set = function (key, value, storageType) {
                var base = this, storageObject = base.resolveStorageType(storageType);

                if (typeof value === "object") {
                    value = JSON.stringify(value);
                }

                //storageObject.setItem(key, value);
                storageObject[key] = value;
            };

            StorageBase.prototype.get = function (key, fallbackValue, storageType) {
                var base = this, storageObject = base.resolveStorageType(storageType);

                //var value = storageObject.getItem(key);
                var value = storageObject[key];

                // assume it is an object that has been stringified
                if (value && value[0] === "{") {
                    value = JSON.parse(value);
                }
                if (!value && typeof fallbackValue !== 'undefined') {
                    value = fallbackValue;
                }
                return value;
            };

            StorageBase.prototype.clearData = function (prefix, storageType) {
                if (typeof prefix !== "undefined" && prefix.length !== 0) {
                    var base = this, storageObject = base.resolveStorageType(storageType);
                    for (var i = (storageObject.length - 1); 0 <= i; i--) {
                        var key = storageObject.key(i);
                        if (key.indexOf(prefix) === 0) {
                            //storageObject.removeItem(key);
                            delete storageObject[key];
                        }
                    }
                }
            };
            return StorageBase;
        })();
        Utilities.StorageBase = StorageBase;
    })(Comprend.Utilities || (Comprend.Utilities = {}));
    var Utilities = Comprend.Utilities;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=UtilityStorageBase.js.map
