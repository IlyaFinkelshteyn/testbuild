﻿/// <reference path="UtilityLanguageHandler.ts" />
/// <reference path="../Definitions/modernizr.d.ts" />
/// <reference path="ExtendedNativeObjects.ts" />
/// <reference path="../Main.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
/// <reference path="utilitygetpropertyhandler.ts" />
var Comprend;
(function (Comprend) {
    (function (Utilities) {
        var Base = (function () {
            function Base(mainBase) {
                //Before onload
                var base = this;
                base.MainBase = mainBase;
            }
            Base.prototype.Init = function () {
                //After onload
                //Load all additional modules here
                var base = this;
                base.LanguageHandler = new Comprend.Utilities.LanguageHandlerBase();
                base.PropertyHandler = new Comprend.Utilities.GetPropertyHandler();
            };

            Base.prototype.loadScript = function (url, callback) {
                // Create a new script and setup the basics.
                var script = document.createElement("script"), firstScript = document.getElementsByTagName('script')[0], onloadFunction;

                script.async = true;
                script.src = url;
                onloadFunction = function () {
                    callback();

                    // Clear it out to avoid getting called more than once or any memory leaks.
                    script.onload = script.onreadystatechange = undefined;
                };

                // Handle the case where an optional callback was passed in.
                if ("function" === typeof (callback)) {
                    script.onload = onloadFunction;

                    script.onreadystatechange = function () {
                        if ("loaded" === script.readyState || "complete" === script.readyState) {
                            //script.onload();
                            onloadFunction();
                        }
                    };
                }

                // Attach the script tag to the page (before the first script) so the magic can happen.
                firstScript.parentNode.insertBefore(script, firstScript);
            };

            Base.prototype.getQueryVariable = function (parametername, url) {
                var query = (typeof url === "string" && url.length !== 0) ? url.substring((url.indexOf("?") + 1), url.length) : window.location.search.substring(1);
                var vars = query.split('&');
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split('=');
                    if (decodeURIComponent(pair[0]) === parametername) {
                        return decodeURIComponent(pair[1]);
                    }
                }
                return "";
            };
            return Base;
        })();
        Utilities.Base = Base;
    })(Comprend.Utilities || (Comprend.Utilities = {}));
    var Utilities = Comprend.Utilities;
})(Comprend || (Comprend = {}));
//# sourceMappingURL=utilitybase.js.map
