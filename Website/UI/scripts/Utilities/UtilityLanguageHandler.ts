/// <reference path="../Definitions/jquery.d.ts" />
module Comprend.Utilities
{
	export class LanguageHandlerBase
	{
		private data : Object;
		private dataurl = "/WebApi/Language/";

		constructor()
		{
			//We expect this method to be run after document ready
		}

		getTranslation(key : string, fallback? : string)
		{
			var base = this;

			if(typeof base.data === 'undefined')
			{
				base.data = base.getData();
			}

			if(base.data !== null)
			{
				var keyData = key.split("/"),
					dataObject = null,
					dataString = "";

				for (var i = 0; i < keyData.length; i++)
				{
					if(i === 0)
					{
						dataObject = this.data;
					}
					else
					{
						if(typeof dataObject[keyData[i]] === "string")
						{

							dataString = dataObject[keyData[i]];
						}
						if(typeof dataObject[keyData[i]] === "object")
						{
							dataObject = dataObject[keyData[i]];
						}
					}
				}
				if(dataString !== null && dataString.length !==0)
				{
					return dataString;
				}
				else if(typeof fallback !== "undefined")
				{
					return fallback;
				}
			}
			else if(typeof fallback !== "undefined")
			{
				return fallback;
			}
		}

		private getData()
		{
			var base = this,
				jsonData = null,
				documentLang = $('html').attr('lang');

			if(documentLang && documentLang.length > 0)
			{
				if(base.dataurl.lastIndexOf('/') !== (base.dataurl.length - 1))
				{
					base.dataurl += "/";
				}
				base.dataurl += documentLang;
			}

			$.ajax(base.dataurl,
			{
				"async":false,
				"cache":true,
				"dataType":"json",
				"type":"GET",
				"complete":function(data)
				{
					if(data !== null && data.responseText !== null && data.responseText.length !== 0)
					{
						jsonData = JSON.parse(data.responseText);
					}
				}
			});
			return jsonData;
		}
	}
}