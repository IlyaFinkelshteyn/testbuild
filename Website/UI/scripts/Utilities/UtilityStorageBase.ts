/* jshint unused:false */
module Comprend.Utilities
{
	export class StorageBase
	{
		private localStorage : Storage = null;
		private sessionStorage : Storage = null;

		public Type = {
			LOCALSTORAGE : "localStorage",
			SESSIONSTORAGE : "sessionStorage"
		};

		constructor(bolAutoInitiate? : boolean)
		{
			//Before onload
			var base = this;
			if(bolAutoInitiate === true)
			{
				base.Init();
			}
		}
		public Init()
		{
			//After onload
			//Load all additional modules here
			var base = this;

			if(base.hasStorageObject('localStorage') === true)
			{
				base.localStorage = window.localStorage;
			}
			else
			{
				console.error("No localStorage support");
			}

			if(base.hasStorageObject('sessionStorage') === true)
			{
				base.sessionStorage = window.sessionStorage;
			}
			else
			{
				console.error("No sessionStorage support");
			}
		}

		private resolveStorageType(storageType : string)
		{
			var base = this,
				storageObject : Storage = null;

			if(storageType !== null || typeof storageType !== "undefined" || storageType.length !== 0)
			{
				switch(storageType)
				{
					case base.Type.SESSIONSTORAGE:
						storageObject = (base.sessionStorage !== null) ? base.sessionStorage : null;
					break;
					case base.Type.LOCALSTORAGE:
						storageObject = (base.localStorage !== null) ? base.localStorage : null;
					break;
					default:
						storageObject = (base.localStorage !== null) ? base.localStorage : null;
					break;
				}
			}
			if(localStorage === null)
			{
				storageObject = base.localStorage;
			}
			return storageObject;
		}

		private hasStorageObject(storagetype : string)
		{
			try
			{
				return storagetype in window && window[storagetype] !== null;
			}
			catch (e)
			{
				return false;
			}
		}

		public set(key : string, value : any, storageType? : string)
		{
			var base = this,
				storageObject : Storage = base.resolveStorageType(storageType);

			if(typeof value === "object")
			{
				value = JSON.stringify(value);
			}
			//storageObject.setItem(key, value);
			storageObject[key] = value;
		}

		public get(key : string, fallbackValue? : any, storageType? : string)
		{
			var base = this,
				storageObject : Storage = base.resolveStorageType(storageType);

			//var value = storageObject.getItem(key);
			var value = storageObject[key];
			// assume it is an object that has been stringified
			if(value && value[0] === "{")
			{
				value = JSON.parse(value);
			}
			if(!value && typeof fallbackValue !== 'undefined')
			{
				value = fallbackValue;
			}
			return value;
		}

		public clearData(prefix : string, storageType? : string)
		{
			if(typeof prefix !== "undefined" && prefix.length !== 0)
			{
				var base = this,
					storageObject : Storage = base.resolveStorageType(storageType);
				for (var i=(storageObject.length-1); 0 <= i; i--)
				{
					var key = storageObject.key(i);
					if(key.indexOf(prefix) === 0)
					{
						//storageObject.removeItem(key);
						delete storageObject[key];
					}
				}
			}
		}
	}
}