var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.DevTools = a, b.currentUser = $("script[data-weinre-user]").attr("data-weinre-user"), 
                b.createMarkup(), b.addEvents();
            }
            return a.prototype.addBookmarklets = function() {
                var a = this, b = a.baseElem.find(".bookmarklets ul"), c = "javascript:(function (d) {var css = '[tabindex=0]{background:yellow}[tabindex]:before{content:attr(tabindex);color:red;font-weight:bold}[role]{background:pink}[role]:before{content:attr(role);color:green;font-weight:bold}',s = d.getElementById('a11y-css-highlight');if (s) {s.disabled = !s.disabled;} else {s = d.createElement('style');s.id = 'a11y-css-highlight';s.type = 'text/css';if (s.styleSheet) { s.styleSheet.cssText = css; } else { s.appendChild(d.createTextNode(css)); } d.getElementsByTagName('head')[0].appendChild(s);}})(document);";
                b.append("<li><a href=\"javascript:(function(F,i,r,e,b,u,g,L,I,T,E){if(F.getElementById(b))return;E=F[i+'NS']&&F.documentElement.namespaceURI;E=E?F[i+'NS'](E,'script'):F[i]('script');E[r]('id',b);E[r]('src',I+g+T);E[r](b,u);(F[e]('head')[0]||F[e]('body')[0]).appendChild(E);E=new Image;E[r]('src',I+L);})(document,'createElement','setAttribute','getElementsByTagName','FirebugLite','4','firebug-lite.js','releases/lite/latest/skin/xp/sprite.png','https://getfirebug.com/','#startOpened');\">Firebug Lite</a></li>").append('<li><a href="' + c + '">Aria Highlighter</a></li>').append("<li><a href=\"javascript:void(window.open('http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl='+encodeURIComponent(location.href),'Qr code','top=100,left=200,width=350,height=350,status=yes'));\">Pagelink in QR</a></li>");
            }, a.prototype.addLinks = function() {
                var a = this, b = a.baseElem.find(".links ul");
                b.append('<li><a href="http://hhsvnlab01:8080/client/#' + a.currentUser + '" target="_blank">weinre</a></li>').append('<li><a href="http://webdevchecklist.com/" target="_blank">Developer Checklist</a></li>').append('<li><a href="http://www.google.com/webmasters/tools/richsnippets" target="_blank">Microformat test</a></li>');
            }, a.prototype.addSettings = function() {
                var a = this, b = a.baseElem.find(".settings.resourceloader fieldset"), c = a.baseElem.find(".settings.functions fieldset");
                c.append('<fieldset><label for="runFollowMe">Activate Follow Me</label><input type="checkbox" name="runFollowMe" id="runFollowMe"/></fieldset>').append('<fieldset><label for="followMeMaster">Follow this browser</label><input type="checkbox" name="followMeMaster" id="followMeMaster"/></fieldset>').append('<fieldset><label for="runWeinre">Activate Weinre</label><input type="checkbox" name="runWeinre" id="runWeinre"/></fieldset>'), 
                b.append('<fieldset><label for="runGruntWatchLiveReload">Grunt Watch Livereload</label><input type="checkbox" name="runGruntWatchLiveReload" id="runResourcesChecker"/></fieldset>').append('<fieldset><label for="runResourcesChecker">Auto refresh</label><input type="checkbox" name="runResourcesChecker" id="runResourcesChecker"/></fieldset>').append('<fieldset><label for="resourcecheckerpollrate">Refresh rate</label><input type="text" name="resourcecheckerpollrate" id="resourcecheckerpollrate" value="" size="6"/></fieldset>'), 
                a.setSettingsValues(), a.addSettingsEvents();
            }, a.prototype.setSettingsValues = function() {
                var a = this;
                a.baseElem.find(".settings").find('input[type=checkbox][name!="runWeinre"]').each(function() {
                    var b = $(this);
                    "true" === a.DevTools.GetSetting(b.attr("name")) ? b.attr("checked", "checked") : b.removeAttr("checked");
                }).end().find('input[type=checkbox][name="runWeinre"]').each(function() {
                    var b = $(this);
                    $.ajax({
                        url: "/DeveloperTools/runweinre.aspx",
                        success: $.proxy(function(a) {
                            "string" == typeof a && (a.length > 0 ? b.attr("checked", "checked") : b.removeAttr("checked"));
                        }, a)
                    });
                }).end().find("input[type=text]").each(function() {
                    var b = $(this), c = a.DevTools.GetSetting(b.attr("name"), null);
                    null !== c && b.val(c);
                }).end();
            }, a.prototype.addSettingsEvents = function() {
                var a = this;
                a.baseElem.find(".settings").find('input[type=checkbox][name!="runWeinre"]').on("change", function(b) {
                    var c = $(this);
                    a.DevTools.SetSetting(c.attr("name"), c.is(":checked") + "");
                }).end().find('input[type=checkbox][name="runWeinre"]').on("change", function(a) {
                    var b = $(this);
                    $.ajax({
                        url: "/DeveloperTools/runweinre.aspx",
                        cache: !1,
                        data: {
                            save: b.is(":checked") ? "runweinre" : ""
                        }
                    });
                }).end().find("input[type=text]").on("keydown", function(b) {
                    var c = $(this), d = c.val();
                    d && d.length > 0 && a.DevTools.SetSetting(c.attr("name"), d);
                }).end();
            }, a.prototype.createMarkup = function() {
                var a = this;
                a.baseElem = $('<div class="developertools"><h2>Developer Tools</h2><div class="bookmarklets"><h3>Bookmarklets</h3><ul></ul></div><div class="links"><h3>Links</h3><ul></ul></div><div class="settings resourceloader"><h3>Resource loader</h3><fieldset></fieldset></div><div class="settings functions"><h3>Functions</h3><fieldset></fieldset></div></div>');
                var b = a.baseElem.find(".bookmarklets ul:eq(0)"), c = a.baseElem.find(".links ul:eq(0)"), d = a.baseElem.find(".settings fieldset:eq(0)");
                a.addBookmarklets(), a.addLinks(), a.addSettings(), a.baseElem.appendTo("body");
            }, a.prototype.addEvents = function() {
                var a = this;
                a.baseElem.find("h2").on("click", $.proxy(a.toggleInterface, a)).end();
            }, a.prototype.toggleInterface = function() {
                var a = this;
                a.baseElem.toggleClass("open");
            }, a;
        }();
        a.DeveloperToolsInterface = b;
    }(a.Developertools || (a.Developertools = {}));
    var b = a.Developertools;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                this.currentAddress = "", this.savedAddress = "", this.followMeUrl = "/DeveloperTools/followme.aspx", 
                this.isMaster = !1, this.checkRate = 2e3, this.timer = null;
                var b = this;
                b.DevTools = a, b.isMaster = b.DevTools.GetSetting("followMeMaster", !1), b.currentAddress = window.location.href, 
                b.isMaster ? b.saveAddress() : b.checkSavedAddress();
            }
            return a.prototype.saveAddress = function() {
                var a = this;
                $.ajax({
                    url: a.followMeUrl,
                    cache: !1,
                    data: {
                        save: a.currentAddress
                    }
                });
            }, a.prototype.checkSavedAddress = function() {
                var a = this;
                $.ajax({
                    url: a.followMeUrl,
                    cache: !1,
                    success: a.oncheckSavedAddress.bind(a)
                });
            }, a.prototype.oncheckSavedAddress = function(a) {
                var b = this;
                a && -1 !== a.length && (b.savedAddress = a, 0 === b.savedAddress.length && (b.savedAddress = b.currentAddress), 
                b.DevTools.GetSetting("runFollowMe", !1) && (b.isMaster === !1 && b.savedAddress !== b.currentAddress ? window.location.href = b.savedAddress : b.timer = window.setTimeout(b.checkSavedAddress.bind(b), b.checkRate)));
            }, a;
        }();
        a.FollowMe = b;
    }(a.Developertools || (a.Developertools = {}));
    var b = a.Developertools;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                this.resourceList = [], this.latestUpdated = 0, this.pollRate = 1e3, this.lastKeyCode = 0, 
                this.runCheck = !0;
                var b = this;
                b.DevTools = a, b.pollRate = b.DevTools.GetSetting("resourcecheckerpollrate", 1e3), 
                b.collectResourcePaths(), b.checkModifiedDate(), b.setStopAction();
            }
            return a.prototype.setStopAction = function() {
                var a = this;
                $("body").on("keydown", function(b) {
                    var c = b.keyCode;
                    27 === c && (27 === a.lastKeyCode && (a.runCheck = !1, window.clearTimeout(a.timer), 
                    a.lastKeyCode = 0), a.lastKeyCode = c);
                });
            }, a.prototype.collectResourcePaths = function() {
                var a = this;
                $("script").each(function() {
                    var b = this.src;
                    -1 !== b.indexOf("/UI/scripts") && (b = b.substring(b.indexOf("/UI/scripts"), b.length), 
                    b.indexOf("?") && (b = b.substring(0, b.indexOf("?"))), a.resourceList.push(b));
                }), $("link").each(function() {
                    var b = this.href;
                    -1 !== b.indexOf("/UI/styles") && (b = b.substring(b.indexOf("/UI/styles"), b.length), 
                    -1 !== b.indexOf("?") && (b = b.substring(0, b.indexOf("?"))), a.resourceList.push(b));
                });
            }, a.prototype.checkModifiedDate = function() {
                var a = this;
                a.runCheck === !0 && $.ajax({
                    url: "/DeveloperTools/filemodified.aspx?file=" + a.resourceList,
                    cache: !1,
                    success: $.proxy(a.checkModifiedDateCallback, a)
                });
            }, a.prototype.checkModifiedDateCallback = function(a) {
                var b = this;
                if (a && a.length > 0) {
                    var c = parseInt(a, 10);
                    0 === b.latestUpdated && (b.latestUpdated = c), b.latestUpdated < c && window.location.reload();
                }
                b.timer = window.setTimeout($.proxy(b.checkModifiedDate, b), b.pollRate);
            }, a;
        }();
        a.ResourcesChecker = b;
    }(a.Developertools || (a.Developertools = {}));
    var b = a.Developertools;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(b) {
        var c = function() {
            function b() {
                this.options = {
                    settingsbasename: "kwddevelopertools"
                };
                var b = this;
                b.DevTools = b, b.Storage = new a.Utilities.StorageBase(!0), b.InitiateModules();
            }
            return b.prototype.InitiateModules = function() {
                var b = this;
                "true" === b.GetSetting("runFollowMe", "false") && (b.FollowMe = new a.Developertools.FollowMe(b.DevTools)), 
                "true" === b.GetSetting("runResourcesChecker", !1) && (b.ResourceChecker = new a.Developertools.ResourcesChecker(b.DevTools)), 
                "true" === b.GetSetting("runGruntWatchLiveReload", !1) && $("head").append('<script src="//localhost:35729/livereload.js"></script>'), 
                b.DevToolsInterface = new a.Developertools.DeveloperToolsInterface(b.DevTools);
            }, b.prototype.GetSetting = function(a, b) {
                var c = this, d = "undefined" == typeof b ? !1 : b;
                return c.Storage.get(c.options.settingsbasename + "-" + a, d, c.Storage.Type.LOCALSTORAGE);
            }, b.prototype.SetSetting = function(a, b) {
                var c = this;
                c.Storage.set(c.options.settingsbasename + "-" + a, b, c.Storage.Type.LOCALSTORAGE);
            }, b;
        }();
        b.Base = c;
    }(a.Developertools || (a.Developertools = {}));
    var b = a.Developertools;
}(Comprend || (Comprend = {})), $(document).ready(function() {
    var a = new Comprend.Developertools.Base();
});