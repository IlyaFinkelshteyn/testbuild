Array.prototype.reduce || (Array.prototype.reduce = function(a) {
    "use strict";
    if (null === this) throw new TypeError("Array.prototype.reduce called on null or undefined");
    if ("function" != typeof a) throw new TypeError(a + " is not a function");
    var b = Object(this), c = b.length >>> 0, d = 0, e;
    if (2 === arguments.length) e = arguments[1]; else {
        for (;c > d && d in b == !1; ) d++;
        if (d >= c) throw new TypeError("Reduce of empty array with no initial value");
        e = b[d++];
    }
    for (;c > d; d++) d in b && (e = a(e, b[d], d, b));
    return e;
}), Array.prototype.map || (Array.prototype.map = function(a, b) {
    var c, d, e;
    if (null === this) throw new TypeError(" this is null or not defined");
    var f = Object(this), g = f.length >>> 0;
    if ("function" != typeof a) throw new TypeError(a + " is not a function");
    for (arguments.length > 1 && (c = b), d = new Array(g), e = 0; g > e; ) {
        var h, i;
        e in f && (h = f[e], i = a.call(c, h, e, f), d[e] = i), e++;
    }
    return d;
});

var Comprend;

!function(a) {
    !function(b) {
        var c = function() {
            function b(a) {
                var b = this;
                b.MainBase = a;
            }
            return b.prototype.Init = function() {
                var b = this;
                b.LanguageHandler = new a.Utilities.LanguageHandlerBase(), b.PropertyHandler = new a.Utilities.GetPropertyHandler();
            }, b.prototype.loadScript = function(a, b) {
                var c = document.createElement("script"), d = document.getElementsByTagName("script")[0], e;
                c.async = !0, c.src = a, e = function() {
                    b(), c.onload = c.onreadystatechange = void 0;
                }, "function" == typeof b && (c.onload = e, c.onreadystatechange = function() {
                    ("loaded" === c.readyState || "complete" === c.readyState) && e();
                }), d.parentNode.insertBefore(c, d);
            }, b.prototype.getQueryVariable = function(a, b) {
                for (var c = "string" == typeof b && 0 !== b.length ? b.substring(b.indexOf("?") + 1, b.length) : window.location.search.substring(1), d = c.split("&"), e = 0; e < d.length; e++) {
                    var f = d[e].split("=");
                    if (decodeURIComponent(f[0]) === a) return decodeURIComponent(f[1]);
                }
                return "";
            }, b;
        }();
        b.Base = c;
    }(a.Utilities || (a.Utilities = {}));
    var b = a.Utilities;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a() {
                this.dataurl = "/WebApi/Properties/";
            }
            return a.prototype.getProperty = function(a, b, c) {
                var d = this, e = d.getProperties(a, b);
                if (null !== e && "undefined" != typeof e && null !== e.Items && 0 !== e.Items.length) for (var f = 0; f < e.Items.length; f++) {
                    var g = e.Items[f];
                    if (g.PropertyName === b) {
                        if ("function" != typeof c) return g.PropertyValue;
                        c(g.PropertyValue);
                    }
                }
            }, a.prototype.getProperties = function(a, b, c) {
                var d = this, e = null, f = !1;
                return "function" != typeof c ? ($.ajax(d.dataurl, {
                    async: !1,
                    cache: !0,
                    dataType: "json",
                    type: "POST",
                    data: {
                        ContentReference: a,
                        Properties: b
                    },
                    complete: function(a) {
                        null !== a && "undefined" != typeof a && null !== a.responseText && 0 !== a.responseText.length && (e = JSON.parse(a.responseText));
                    }
                }), e) : void $.ajax(d.dataurl, {
                    async: !0,
                    cache: !0,
                    dataType: "json",
                    type: "POST",
                    data: {
                        ContentReference: a,
                        Properties: b
                    },
                    complete: function(a) {
                        null !== a && "undefined" != typeof a && null !== a.responseText && 0 !== a.responseText.length && (e = JSON.parse(a.responseText), 
                        c(e));
                    }
                });
            }, a;
        }();
        a.GetPropertyHandler = b;
    }(a.Utilities || (a.Utilities = {}));
    var b = a.Utilities;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a() {
                this.dataurl = "/WebApi/Language/";
            }
            return a.prototype.getTranslation = function(a, b) {
                var c = this;
                if ("undefined" == typeof c.data && (c.data = c.getData()), null !== c.data) {
                    for (var d = a.split("/"), e = null, f = "", g = 0; g < d.length; g++) 0 === g ? e = this.data : ("string" == typeof e[d[g]] && (f = e[d[g]]), 
                    "object" == typeof e[d[g]] && (e = e[d[g]]));
                    if (null !== f && 0 !== f.length) return f;
                    if ("undefined" != typeof b) return b;
                } else if ("undefined" != typeof b) return b;
            }, a.prototype.getData = function() {
                var a = this, b = null, c = $("html").attr("lang");
                return c && c.length > 0 && (a.dataurl.lastIndexOf("/") !== a.dataurl.length - 1 && (a.dataurl += "/"), 
                a.dataurl += c), $.ajax(a.dataurl, {
                    async: !1,
                    cache: !0,
                    dataType: "json",
                    type: "GET",
                    complete: function(a) {
                        null !== a && null !== a.responseText && 0 !== a.responseText.length && (b = JSON.parse(a.responseText));
                    }
                }), b;
            }, a;
        }();
        a.LanguageHandlerBase = b;
    }(a.Utilities || (a.Utilities = {}));
    var b = a.Utilities;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                this.localStorage = null, this.sessionStorage = null, this.Type = {
                    LOCALSTORAGE: "localStorage",
                    SESSIONSTORAGE: "sessionStorage"
                };
                var b = this;
                a === !0 && b.Init();
            }
            return a.prototype.Init = function() {
                var a = this;
                a.hasStorageObject("localStorage") === !0 ? a.localStorage = window.localStorage : console.error("No localStorage support"), 
                a.hasStorageObject("sessionStorage") === !0 ? a.sessionStorage = window.sessionStorage : console.error("No sessionStorage support");
            }, a.prototype.resolveStorageType = function(a) {
                var b = this, c = null;
                if (null !== a || "undefined" != typeof a || 0 !== a.length) switch (a) {
                  case b.Type.SESSIONSTORAGE:
                    c = null !== b.sessionStorage ? b.sessionStorage : null;
                    break;

                  case b.Type.LOCALSTORAGE:
                    c = null !== b.localStorage ? b.localStorage : null;
                    break;

                  default:
                    c = null !== b.localStorage ? b.localStorage : null;
                }
                return null === localStorage && (c = b.localStorage), c;
            }, a.prototype.hasStorageObject = function(a) {
                try {
                    return a in window && null !== window[a];
                } catch (b) {
                    return !1;
                }
            }, a.prototype.set = function(a, b, c) {
                var d = this, e = d.resolveStorageType(c);
                "object" == typeof b && (b = JSON.stringify(b)), e[a] = b;
            }, a.prototype.get = function(a, b, c) {
                var d = this, e = d.resolveStorageType(c), f = e[a];
                return f && "{" === f[0] && (f = JSON.parse(f)), f || "undefined" == typeof b || (f = b), 
                f;
            }, a.prototype.clearData = function(a, b) {
                if ("undefined" != typeof a && 0 !== a.length) for (var c = this, d = c.resolveStorageType(b), e = d.length - 1; e >= 0; e--) {
                    var f = d.key(e);
                    0 === f.indexOf(a) && delete d[f];
                }
            }, a;
        }();
        a.StorageBase = b;
    }(a.Utilities || (a.Utilities = {}));
    var b = a.Utilities;
}(Comprend || (Comprend = {}));

var log = function a() {
    if (log.history = log.history || [], log.history.push(arguments), this.console) {
        var a = arguments, b;
        a.callee = a.callee.caller, b = [].slice.call(a), "object" == typeof console.log ? log.apply.call(console.log, console, b) : console.log.apply(console, b);
    }
};

!function() {
    try {
        return console.log(), window.console;
    } catch (a) {
        return window.console = {};
    }
}(), window.log = log, function(a) {
    "use strict";
    function b(a) {
        var b = a.getAttribute("data-src");
        a.src !== b && (a.src = b, a.setAttribute("data-loaded", "true"));
    }
    function c() {
        for (var a = f.length, c = 0; a > c; c++) {
            var d = f[c];
            e(d, g) && b(d);
        }
    }
    function d(a, b) {
        if (document.createEvent) {
            var c = document.createEvent("HTMLEvents");
            return c.initEvent(b, !0, !0), a.dispatchEvent(c);
        }
        return a.fireEvent ? a.fireEvent("on" + b) : void 0;
    }
    function e(a, b) {
        if (null === a.getAttribute("data-loaded")) {
            var c = a.getBoundingClientRect();
            return b = "number" == typeof b ? b : 0, c.top >= 0 && c.left >= 0 && c.top <= (window.innerHeight || document.documentElement.clientHeight) + b;
        }
        return !1;
    }
    var f = [], g = 100, h = "/UI/images/blank.gif";
    a.picturefill = function() {
        for (var c = a.document.getElementsByTagName("div"), d = 0, i = c.length; i > d; d++) if (null !== c[d].getAttribute("data-picture")) {
            for (var j = c[d].getElementsByTagName("div"), k = [], l = 0, m = j.length; m > l; l++) {
                var n = j[l];
                if (!n.className || -1 == n.className.indexOf("attribution")) {
                    var o = n.getAttribute("data-media");
                    (!o || a.matchMedia && a.matchMedia(o).matches) && k.push(n);
                }
            }
            var p = c[d].getElementsByTagName("img")[0];
            if (k.length) {
                var q = k.pop();
                if (p || (p = a.document.createElement("img"), p.alt = c[d].getAttribute("data-alt"), 
                p.title = c[d].getAttribute("data-title"), p.setAttribute("data-media", q.getAttribute("data-media")), 
                p.setAttribute("data-width", q.getAttribute("data-width")), p.setAttribute("data-height", q.getAttribute("data-height")), 
                p.setAttribute("data-quality", q.getAttribute("data-quality")), p.setAttribute("data-maxwidth", q.getAttribute("data-maxwidth")), 
                c[d].appendChild(p)), f.push(p), p.removeAttribute("data-loaded"), p.setAttribute("data-src", q.getAttribute("data-src")), 
                e(p, g)) b(p); else {
                    p.src = h;
                    var r = parseInt(p.getAttribute("data-height"), 10) / parseInt(p.getAttribute("data-width"), 10), s = parseInt(p.parentElement.getBoundingClientRect().width * r, 10);
                    "number" == typeof s && 0 === p.height;
                }
            } else p && c[d].removeChild(p);
        }
        $("body").trigger("picturefillcomplete");
    }, a.addEventListener ? (a.addEventListener("resize", a.picturefill, !1), a.addEventListener("DOMContentLoaded", function() {
        a.picturefill(), a.removeEventListener("load", a.picturefill, !1);
    }, !1), a.addEventListener("load", a.picturefill, !1), a.addEventListener("scroll", c, !1)) : a.attachEvent && (a.attachEvent("onload", a.picturefill), 
    a.attachEvent("onscroll", c));
}(this), function(a, b, c, d) {
    var e = c("html"), f = c(a), g = c(b), h = c.fancybox = function() {
        h.open.apply(this, arguments);
    }, i = navigator.userAgent.match(/msie/i), j = null, k = b.createTouch !== d, l = function(a) {
        return a && a.hasOwnProperty && a instanceof c;
    }, m = function(a) {
        return a && "string" === c.type(a);
    }, n = function(a) {
        return m(a) && 0 < a.indexOf("%");
    }, o = function(a, b) {
        var c = parseInt(a, 10) || 0;
        return b && n(a) && (c *= h.getViewport()[b] / 100), Math.ceil(c);
    }, p = function(a, b) {
        return o(a, b) + "px";
    };
    c.extend(h, {
        version: "2.1.5",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            pixelRatio: 1,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !k,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: .5,
            leftRatio: .5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3e3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {
                dataType: "html",
                headers: {
                    "X-fancyBox": !0
                }
            },
            iframe: {
                scrolling: "auto",
                preload: !0
            },
            swf: {
                wmode: "transparent",
                allowfullscreen: "true",
                allowscriptaccess: "always"
            },
            keys: {
                next: {
                    13: "left",
                    34: "up",
                    39: "left",
                    40: "up"
                },
                prev: {
                    8: "right",
                    33: "down",
                    37: "right",
                    38: "down"
                },
                close: [ 27 ],
                play: [ 32 ],
                toggle: [ 70 ]
            },
            direction: {
                next: "left",
                prev: "right"
            },
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (i ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {
                overlay: !0,
                title: !0
            },
            onCancel: c.noop,
            beforeLoad: c.noop,
            afterLoad: c.noop,
            beforeShow: c.noop,
            afterShow: c.noop,
            beforeChange: c.noop,
            beforeClose: c.noop,
            afterClose: c.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {
            timer: null,
            isActive: !1
        },
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function(a, b) {
            return a && (c.isPlainObject(b) || (b = {}), !1 !== h.close(!0)) ? (c.isArray(a) || (a = l(a) ? c(a).get() : [ a ]), 
            c.each(a, function(e, f) {
                var g = {}, i, j, k, n, o;
                "object" === c.type(f) && (f.nodeType && (f = c(f)), l(f) ? (g = {
                    href: f.data("fancybox-href") || f.attr("href"),
                    title: f.data("fancybox-title") || f.attr("title"),
                    isDom: !0,
                    element: f
                }, c.metadata && c.extend(!0, g, f.metadata())) : g = f), i = b.href || g.href || (m(f) ? f : null), 
                j = b.title !== d ? b.title : g.title || "", n = (k = b.content || g.content) ? "html" : b.type || g.type, 
                !n && g.isDom && (n = f.data("fancybox-type"), n || (n = (n = f.prop("class").match(/fancybox\.(\w+)/)) ? n[1] : null)), 
                m(i) && (n || (h.isImage(i) ? n = "image" : h.isSWF(i) ? n = "swf" : "#" === i.charAt(0) ? n = "inline" : m(f) && (n = "html", 
                k = f)), "ajax" === n && (o = i.split(/\s+/, 2), i = o.shift(), o = o.shift())), 
                k || ("inline" === n ? i ? k = c(m(i) ? i.replace(/.*(?=#[^\s]+$)/, "") : i) : g.isDom && (k = f) : "html" === n ? k = i : !n && !i && g.isDom && (n = "inline", 
                k = f)), c.extend(g, {
                    href: i,
                    type: n,
                    content: k,
                    title: j,
                    selector: o
                }), a[e] = g;
            }), h.opts = c.extend(!0, {}, h.defaults, b), b.keys !== d && (h.opts.keys = b.keys ? c.extend({}, h.defaults.keys, b.keys) : !1), 
            h.group = a, h._start(h.opts.index)) : void 0;
        },
        cancel: function() {
            var a = h.coming;
            a && !1 !== h.trigger("onCancel") && (h.hideLoading(), h.ajaxLoad && h.ajaxLoad.abort(), 
            h.ajaxLoad = null, h.imgPreload && (h.imgPreload.onload = h.imgPreload.onerror = null), 
            a.wrap && a.wrap.stop(!0, !0).trigger("onReset").remove(), h.coming = null, h.current || h._afterZoomOut(a));
        },
        close: function(a) {
            h.cancel(), !1 !== h.trigger("beforeClose") && (h.unbindEvents(), h.isActive && (h.isOpen && !0 !== a ? (h.isOpen = h.isOpened = !1, 
            h.isClosing = !0, c(".fancybox-item, .fancybox-nav").remove(), h.wrap.stop(!0, !0).removeClass("fancybox-opened"), 
            h.transitions[h.current.closeMethod]()) : (c(".fancybox-wrap").stop(!0).trigger("onReset").remove(), 
            h._afterZoomOut())));
        },
        play: function(a) {
            var b = function() {
                clearTimeout(h.player.timer);
            }, c = function() {
                b(), h.current && h.player.isActive && (h.player.timer = setTimeout(h.next, h.current.playSpeed));
            }, d = function() {
                b(), g.unbind(".player"), h.player.isActive = !1, h.trigger("onPlayEnd");
            };
            !0 === a || !h.player.isActive && !1 !== a ? h.current && (h.current.loop || h.current.index < h.group.length - 1) && (h.player.isActive = !0, 
            g.bind({
                "onCancel.player beforeClose.player": d,
                "onUpdate.player": c,
                "beforeLoad.player": b
            }), c(), h.trigger("onPlayStart")) : d();
        },
        next: function(a) {
            var b = h.current;
            b && (m(a) || (a = b.direction.next), h.jumpto(b.index + 1, a, "next"));
        },
        prev: function(a) {
            var b = h.current;
            b && (m(a) || (a = b.direction.prev), h.jumpto(b.index - 1, a, "prev"));
        },
        jumpto: function(a, b, c) {
            var e = h.current;
            e && (a = o(a), h.direction = b || e.direction[a >= e.index ? "next" : "prev"], 
            h.router = c || "jumpto", e.loop && (0 > a && (a = e.group.length + a % e.group.length), 
            a %= e.group.length), e.group[a] !== d && (h.cancel(), h._start(a)));
        },
        reposition: function(a, b) {
            var d = h.current, e = d ? d.wrap : null, f;
            e && (f = h._getPosition(b), a && "scroll" === a.type ? (delete f.position, e.stop(!0, !0).animate(f, 200)) : (e.css(f), 
            d.pos = c.extend({}, d.dim, f)));
        },
        update: function(a) {
            var b = a && a.type, c = !b || "orientationchange" === b;
            c && (clearTimeout(j), j = null), h.isOpen && !j && (j = setTimeout(function() {
                var d = h.current;
                d && !h.isClosing && (h.wrap.removeClass("fancybox-tmp"), (c || "load" === b || "resize" === b && d.autoResize) && h._setDimension(), 
                "scroll" === b && d.canShrink || h.reposition(a), h.trigger("onUpdate"), j = null);
            }, c && !k ? 0 : 300));
        },
        toggle: function(a) {
            h.isOpen && (h.current.fitToView = "boolean" === c.type(a) ? a : !h.current.fitToView, 
            k && (h.wrap.removeAttr("style").addClass("fancybox-tmp"), h.trigger("onUpdate")), 
            h.update());
        },
        hideLoading: function() {
            g.unbind(".loading"), c("#fancybox-loading").remove();
        },
        showLoading: function() {
            var a, b;
            h.hideLoading(), a = c('<div id="fancybox-loading"><div></div></div>').click(h.cancel).appendTo("body"), 
            g.bind("keydown.loading", function(a) {
                27 === (a.which || a.keyCode) && (a.preventDefault(), h.cancel());
            }), h.defaults.fixed || (b = h.getViewport(), a.css({
                position: "absolute",
                top: .5 * b.h + b.y,
                left: .5 * b.w + b.x
            }));
        },
        getViewport: function() {
            var b = h.current && h.current.locked || !1, c = {
                x: f.scrollLeft(),
                y: f.scrollTop()
            };
            return b ? (c.w = b[0].clientWidth, c.h = b[0].clientHeight) : (c.w = k && a.innerWidth ? a.innerWidth : f.width(), 
            c.h = k && a.innerHeight ? a.innerHeight : f.height()), c;
        },
        unbindEvents: function() {
            h.wrap && l(h.wrap) && h.wrap.unbind(".fb"), g.unbind(".fb"), f.unbind(".fb");
        },
        bindEvents: function() {
            var a = h.current, b;
            a && (f.bind("orientationchange.fb" + (k ? "" : " resize.fb") + (a.autoCenter && !a.locked ? " scroll.fb" : ""), h.update), 
            (b = a.keys) && g.bind("keydown.fb", function(e) {
                var f = e.which || e.keyCode, g = e.target || e.srcElement;
                return 27 === f && h.coming ? !1 : void !(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey || g && (g.type || c(g).is("[contenteditable]")) || !c.each(b, function(b, g) {
                    return 1 < a.group.length && g[f] !== d ? (h[b](g[f]), e.preventDefault(), !1) : -1 < c.inArray(f, g) ? (h[b](), 
                    e.preventDefault(), !1) : void 0;
                }));
            }), c.fn.mousewheel && a.mouseWheel && h.wrap.bind("mousewheel.fb", function(b, d, e, f) {
                for (var g = c(b.target || null), i = !1; g.length && !i && !g.is(".fancybox-skin") && !g.is(".fancybox-wrap"); ) i = g[0] && !(g[0].style.overflow && "hidden" === g[0].style.overflow) && (g[0].clientWidth && g[0].scrollWidth > g[0].clientWidth || g[0].clientHeight && g[0].scrollHeight > g[0].clientHeight), 
                g = c(g).parent();
                0 !== d && !i && 1 < h.group.length && !a.canShrink && (f > 0 || e > 0 ? h.prev(f > 0 ? "down" : "left") : (0 > f || 0 > e) && h.next(0 > f ? "up" : "right"), 
                b.preventDefault());
            }));
        },
        trigger: function(a, b) {
            var d, e = b || h.coming || h.current;
            if (e) {
                if (c.isFunction(e[a]) && (d = e[a].apply(e, Array.prototype.slice.call(arguments, 1))), 
                !1 === d) return !1;
                e.helpers && c.each(e.helpers, function(b, d) {
                    d && h.helpers[b] && c.isFunction(h.helpers[b][a]) && h.helpers[b][a](c.extend(!0, {}, h.helpers[b].defaults, d), e);
                }), g.trigger(a);
            }
        },
        isImage: function(a) {
            return m(a) && a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
        },
        isSWF: function(a) {
            return m(a) && a.match(/\.(swf)((\?|#).*)?$/i);
        },
        _start: function(a) {
            var b = {}, d, e;
            if (a = o(a), d = h.group[a] || null, !d) return !1;
            if (b = c.extend(!0, {}, h.opts, d), d = b.margin, e = b.padding, "number" === c.type(d) && (b.margin = [ d, d, d, d ]), 
            "number" === c.type(e) && (b.padding = [ e, e, e, e ]), b.modal && c.extend(!0, b, {
                closeBtn: !1,
                closeClick: !1,
                nextClick: !1,
                arrows: !1,
                mouseWheel: !1,
                keys: null,
                helpers: {
                    overlay: {
                        closeClick: !1
                    }
                }
            }), b.autoSize && (b.autoWidth = b.autoHeight = !0), "auto" === b.width && (b.autoWidth = !0), 
            "auto" === b.height && (b.autoHeight = !0), b.group = h.group, b.index = a, h.coming = b, 
            !1 === h.trigger("beforeLoad")) h.coming = null; else {
                if (e = b.type, d = b.href, !e) return h.coming = null, h.current && h.router && "jumpto" !== h.router ? (h.current.index = a, 
                h[h.router](h.direction)) : !1;
                if (h.isActive = !0, ("image" === e || "swf" === e) && (b.autoHeight = b.autoWidth = !1, 
                b.scrolling = "visible"), "image" === e && (b.aspectRatio = !0), "iframe" === e && k && (b.scrolling = "scroll"), 
                b.wrap = c(b.tpl.wrap).addClass("fancybox-" + (k ? "mobile" : "desktop") + " fancybox-type-" + e + " fancybox-tmp " + b.wrapCSS).appendTo(b.parent || "body"), 
                c.extend(b, {
                    skin: c(".fancybox-skin", b.wrap),
                    outer: c(".fancybox-outer", b.wrap),
                    inner: c(".fancybox-inner", b.wrap)
                }), c.each([ "Top", "Right", "Bottom", "Left" ], function(a, c) {
                    b.skin.css("padding" + c, p(b.padding[a]));
                }), h.trigger("onReady"), "inline" === e || "html" === e) {
                    if (!b.content || !b.content.length) return h._error("content");
                } else if (!d) return h._error("href");
                "image" === e ? h._loadImage() : "ajax" === e ? h._loadAjax() : "iframe" === e ? h._loadIframe() : h._afterLoad();
            }
        },
        _error: function(a) {
            c.extend(h.coming, {
                type: "html",
                autoWidth: !0,
                autoHeight: !0,
                minWidth: 0,
                minHeight: 0,
                scrolling: "no",
                hasError: a,
                content: h.coming.tpl.error
            }), h._afterLoad();
        },
        _loadImage: function() {
            var a = h.imgPreload = new Image();
            a.onload = function() {
                this.onload = this.onerror = null, h.coming.width = this.width / h.opts.pixelRatio, 
                h.coming.height = this.height / h.opts.pixelRatio, h._afterLoad();
            }, a.onerror = function() {
                this.onload = this.onerror = null, h._error("image");
            }, a.src = h.coming.href, !0 !== a.complete && h.showLoading();
        },
        _loadAjax: function() {
            var a = h.coming;
            h.showLoading(), h.ajaxLoad = c.ajax(c.extend({}, a.ajax, {
                url: a.href,
                error: function(a, b) {
                    h.coming && "abort" !== b ? h._error("ajax", a) : h.hideLoading();
                },
                success: function(b, c) {
                    "success" === c && (a.content = b, h._afterLoad());
                }
            }));
        },
        _loadIframe: function() {
            var a = h.coming, b = c(a.tpl.iframe.replace(/\{rnd\}/g, new Date().getTime())).attr("scrolling", k ? "auto" : a.iframe.scrolling).attr("src", a.href);
            c(a.wrap).bind("onReset", function() {
                try {
                    c(this).find("iframe").hide().attr("src", "//about:blank").end().empty();
                } catch (a) {}
            }), a.iframe.preload && (h.showLoading(), b.one("load", function() {
                c(this).data("ready", 1), k || c(this).bind("load.fb", h.update), c(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), 
                h._afterLoad();
            })), a.content = b.appendTo(a.inner), a.iframe.preload || h._afterLoad();
        },
        _preloadImages: function() {
            var a = h.group, b = h.current, c = a.length, d = b.preload ? Math.min(b.preload, c - 1) : 0, e, f;
            for (f = 1; d >= f; f += 1) e = a[(b.index + f) % c], "image" === e.type && e.href && (new Image().src = e.href);
        },
        _afterLoad: function() {
            var a = h.coming, b = h.current, d, e, f, g, i;
            if (h.hideLoading(), a && !1 !== h.isActive) if (!1 === h.trigger("afterLoad", a, b)) a.wrap.stop(!0).trigger("onReset").remove(), 
            h.coming = null; else {
                switch (b && (h.trigger("beforeChange", b), b.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()), 
                h.unbindEvents(), d = a.content, e = a.type, f = a.scrolling, c.extend(h, {
                    wrap: a.wrap,
                    skin: a.skin,
                    outer: a.outer,
                    inner: a.inner,
                    current: a,
                    previous: b
                }), g = a.href, e) {
                  case "inline":
                  case "ajax":
                  case "html":
                    a.selector ? d = c("<div>").html(d).find(a.selector) : l(d) && (d.data("fancybox-placeholder") || d.data("fancybox-placeholder", c('<div class="fancybox-placeholder"></div>').insertAfter(d).hide()), 
                    d = d.show().detach(), a.wrap.bind("onReset", function() {
                        c(this).find(d).length && d.hide().replaceAll(d.data("fancybox-placeholder")).data("fancybox-placeholder", !1);
                    }));
                    break;

                  case "image":
                    d = a.tpl.image.replace("{href}", g);
                    break;

                  case "swf":
                    d = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + g + '"></param>', 
                    i = "", c.each(a.swf, function(a, b) {
                        d += '<param name="' + a + '" value="' + b + '"></param>', i += " " + a + '="' + b + '"';
                    }), d += '<embed src="' + g + '" type="application/x-shockwave-flash" width="100%" height="100%"' + i + "></embed></object>";
                }
                (!l(d) || !d.parent().is(a.inner)) && a.inner.append(d), h.trigger("beforeShow"), 
                a.inner.css("overflow", "yes" === f ? "scroll" : "no" === f ? "hidden" : f), h._setDimension(), 
                h.reposition(), h.isOpen = !1, h.coming = null, h.bindEvents(), h.isOpened ? b.prevMethod && h.transitions[b.prevMethod]() : c(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove(), 
                h.transitions[h.isOpened ? a.nextMethod : a.openMethod](), h._preloadImages();
            }
        },
        _setDimension: function() {
            var a = h.getViewport(), b = 0, d = !1, e = !1, d = h.wrap, f = h.skin, g = h.inner, i = h.current, e = i.width, j = i.height, k = i.minWidth, l = i.minHeight, m = i.maxWidth, q = i.maxHeight, r = i.scrolling, s = i.scrollOutside ? i.scrollbarWidth : 0, t = i.margin, u = o(t[1] + t[3]), v = o(t[0] + t[2]), w, x, y, z, A, B, C, D, E;
            if (d.add(f).add(g).width("auto").height("auto").removeClass("fancybox-tmp"), t = o(f.outerWidth(!0) - f.width()), 
            w = o(f.outerHeight(!0) - f.height()), x = u + t, y = v + w, z = n(e) ? (a.w - x) * o(e) / 100 : e, 
            A = n(j) ? (a.h - y) * o(j) / 100 : j, "iframe" === i.type) {
                if (E = i.content, i.autoHeight && 1 === E.data("ready")) try {
                    E[0].contentWindow.document.location && (g.width(z).height(9999), B = E.contents().find("body"), 
                    s && B.css("overflow-x", "hidden"), A = B.outerHeight(!0));
                } catch (F) {}
            } else (i.autoWidth || i.autoHeight) && (g.addClass("fancybox-tmp"), i.autoWidth || g.width(z), 
            i.autoHeight || g.height(A), i.autoWidth && (z = g.width()), i.autoHeight && (A = g.height()), 
            g.removeClass("fancybox-tmp"));
            if (e = o(z), j = o(A), D = z / A, k = o(n(k) ? o(k, "w") - x : k), m = o(n(m) ? o(m, "w") - x : m), 
            l = o(n(l) ? o(l, "h") - y : l), q = o(n(q) ? o(q, "h") - y : q), B = m, C = q, 
            i.fitToView && (m = Math.min(a.w - x, m), q = Math.min(a.h - y, q)), x = a.w - u, 
            v = a.h - v, i.aspectRatio ? (e > m && (e = m, j = o(e / D)), j > q && (j = q, e = o(j * D)), 
            k > e && (e = k, j = o(e / D)), l > j && (j = l, e = o(j * D))) : (e = Math.max(k, Math.min(e, m)), 
            i.autoHeight && "iframe" !== i.type && (g.width(e), j = g.height()), j = Math.max(l, Math.min(j, q))), 
            i.fitToView) if (g.width(e).height(j), d.width(e + t), a = d.width(), u = d.height(), 
            i.aspectRatio) for (;(a > x || u > v) && e > k && j > l && !(19 < b++); ) j = Math.max(l, Math.min(q, j - 10)), 
            e = o(j * D), k > e && (e = k, j = o(e / D)), e > m && (e = m, j = o(e / D)), g.width(e).height(j), 
            d.width(e + t), a = d.width(), u = d.height(); else e = Math.max(k, Math.min(e, e - (a - x))), 
            j = Math.max(l, Math.min(j, j - (u - v)));
            s && "auto" === r && A > j && x > e + t + s && (e += s), g.width(e).height(j), d.width(e + t), 
            a = d.width(), u = d.height(), d = (a > x || u > v) && e > k && j > l, e = i.aspectRatio ? B > e && C > j && z > e && A > j : (B > e || C > j) && (z > e || A > j), 
            c.extend(i, {
                dim: {
                    width: p(a),
                    height: p(u)
                },
                origWidth: z,
                origHeight: A,
                canShrink: d,
                canExpand: e,
                wPadding: t,
                hPadding: w,
                wrapSpace: u - f.outerHeight(!0),
                skinSpace: f.height() - j
            }), !E && i.autoHeight && j > l && q > j && !e && g.height("auto");
        },
        _getPosition: function(a) {
            var b = h.current, c = h.getViewport(), d = b.margin, e = h.wrap.width() + d[1] + d[3], f = h.wrap.height() + d[0] + d[2], d = {
                position: "absolute",
                top: d[0],
                left: d[3]
            };
            return b.autoCenter && b.fixed && !a && f <= c.h && e <= c.w ? d.position = "fixed" : b.locked || (d.top += c.y, 
            d.left += c.x), d.top = p(Math.max(d.top, d.top + (c.h - f) * b.topRatio)), d.left = p(Math.max(d.left, d.left + (c.w - e) * b.leftRatio)), 
            d;
        },
        _afterZoomIn: function() {
            var a = h.current;
            a && (h.isOpen = h.isOpened = !0, h.wrap.css("overflow", "visible").addClass("fancybox-opened"), 
            h.update(), (a.closeClick || a.nextClick && 1 < h.group.length) && h.inner.css("cursor", "pointer").bind("click.fb", function(b) {
                !c(b.target).is("a") && !c(b.target).parent().is("a") && (b.preventDefault(), h[a.closeClick ? "close" : "next"]());
            }), a.closeBtn && c(a.tpl.closeBtn).appendTo(h.skin).bind("click.fb", function(a) {
                a.preventDefault(), h.close();
            }), a.arrows && 1 < h.group.length && ((a.loop || 0 < a.index) && c(a.tpl.prev).appendTo(h.outer).bind("click.fb", h.prev), 
            (a.loop || a.index < h.group.length - 1) && c(a.tpl.next).appendTo(h.outer).bind("click.fb", h.next)), 
            h.trigger("afterShow"), a.loop || a.index !== a.group.length - 1 ? h.opts.autoPlay && !h.player.isActive && (h.opts.autoPlay = !1, 
            h.play()) : h.play(!1));
        },
        _afterZoomOut: function(a) {
            a = a || h.current, c(".fancybox-wrap").trigger("onReset").remove(), c.extend(h, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            }), h.trigger("afterClose", a);
        }
    }), h.transitions = {
        getOrigPosition: function() {
            var a = h.current, b = a.element, c = a.orig, d = {}, e = 50, f = 50, g = a.hPadding, i = a.wPadding, j = h.getViewport();
            return !c && a.isDom && b.is(":visible") && (c = b.find("img:first"), c.length || (c = b)), 
            l(c) ? (d = c.offset(), c.is("img") && (e = c.outerWidth(), f = c.outerHeight())) : (d.top = j.y + (j.h - f) * a.topRatio, 
            d.left = j.x + (j.w - e) * a.leftRatio), ("fixed" === h.wrap.css("position") || a.locked) && (d.top -= j.y, 
            d.left -= j.x), d = {
                top: p(d.top - g * a.topRatio),
                left: p(d.left - i * a.leftRatio),
                width: p(e + i),
                height: p(f + g)
            };
        },
        step: function(a, b) {
            var c, d, e = b.prop;
            d = h.current;
            var f = d.wrapSpace, g = d.skinSpace;
            ("width" === e || "height" === e) && (c = b.end === b.start ? 1 : (a - b.start) / (b.end - b.start), 
            h.isClosing && (c = 1 - c), d = "width" === e ? d.wPadding : d.hPadding, d = a - d, 
            h.skin[e](o("width" === e ? d : d - f * c)), h.inner[e](o("width" === e ? d : d - f * c - g * c)));
        },
        zoomIn: function() {
            var a = h.current, b = a.pos, d = a.openEffect, e = "elastic" === d, f = c.extend({
                opacity: 1
            }, b);
            delete f.position, e ? (b = this.getOrigPosition(), a.openOpacity && (b.opacity = .1)) : "fade" === d && (b.opacity = .1), 
            h.wrap.css(b).animate(f, {
                duration: "none" === d ? 0 : a.openSpeed,
                easing: a.openEasing,
                step: e ? this.step : null,
                complete: h._afterZoomIn
            });
        },
        zoomOut: function() {
            var a = h.current, b = a.closeEffect, c = "elastic" === b, d = {
                opacity: .1
            };
            c && (d = this.getOrigPosition(), a.closeOpacity && (d.opacity = .1)), h.wrap.animate(d, {
                duration: "none" === b ? 0 : a.closeSpeed,
                easing: a.closeEasing,
                step: c ? this.step : null,
                complete: h._afterZoomOut
            });
        },
        changeIn: function() {
            var a = h.current, b = a.nextEffect, c = a.pos, d = {
                opacity: 1
            }, e = h.direction, f;
            c.opacity = .1, "elastic" === b && (f = "down" === e || "up" === e ? "top" : "left", 
            "down" === e || "right" === e ? (c[f] = p(o(c[f]) - 200), d[f] = "+=200px") : (c[f] = p(o(c[f]) + 200), 
            d[f] = "-=200px")), "none" === b ? h._afterZoomIn() : h.wrap.css(c).animate(d, {
                duration: a.nextSpeed,
                easing: a.nextEasing,
                complete: h._afterZoomIn
            });
        },
        changeOut: function() {
            var a = h.previous, b = a.prevEffect, d = {
                opacity: .1
            }, e = h.direction;
            "elastic" === b && (d["down" === e || "up" === e ? "top" : "left"] = ("up" === e || "left" === e ? "-" : "+") + "=200px"), 
            a.wrap.animate(d, {
                duration: "none" === b ? 0 : a.prevSpeed,
                easing: a.prevEasing,
                complete: function() {
                    c(this).trigger("onReset").remove();
                }
            });
        }
    }, h.helpers.overlay = {
        defaults: {
            closeClick: !0,
            speedOut: 200,
            showEarly: !0,
            css: {},
            locked: !k,
            fixed: !0
        },
        overlay: null,
        fixed: !1,
        el: c("html"),
        create: function(a) {
            a = c.extend({}, this.defaults, a), this.overlay && this.close(), this.overlay = c('<div class="fancybox-overlay"></div>').appendTo(h.coming ? h.coming.parent : a.parent), 
            this.fixed = !1, a.fixed && h.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), 
            this.fixed = !0);
        },
        open: function(a) {
            var b = this;
            a = c.extend({}, this.defaults, a), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(a), 
            this.fixed || (f.bind("resize.overlay", c.proxy(this.update, this)), this.update()), 
            a.closeClick && this.overlay.bind("click.overlay", function(a) {
                return c(a.target).hasClass("fancybox-overlay") ? (h.isActive ? h.close() : b.close(), 
                !1) : void 0;
            }), this.overlay.css(a.css).show();
        },
        close: function() {
            var a, b;
            f.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (c(".fancybox-margin").removeClass("fancybox-margin"), 
            a = f.scrollTop(), b = f.scrollLeft(), this.el.removeClass("fancybox-lock"), f.scrollTop(a).scrollLeft(b)), 
            c(".fancybox-overlay").remove().hide(), c.extend(this, {
                overlay: null,
                fixed: !1
            });
        },
        update: function() {
            var a = "100%", c;
            this.overlay.width(a).height("100%"), i ? (c = Math.max(b.documentElement.offsetWidth, b.body.offsetWidth), 
            g.width() > c && (a = g.width())) : g.width() > f.width() && (a = g.width()), this.overlay.width(a).height(g.height());
        },
        onReady: function(a, b) {
            var d = this.overlay;
            c(".fancybox-overlay").stop(!0, !0), d || this.create(a), a.locked && this.fixed && b.fixed && (d || (this.margin = g.height() > f.height() ? c("html").css("margin-right").replace("px", "") : !1), 
            b.locked = this.overlay.append(b.wrap), b.fixed = !1), !0 === a.showEarly && this.beforeShow.apply(this, arguments);
        },
        beforeShow: function(a, b) {
            var d, e;
            b.locked && (!1 !== this.margin && (c("*").filter(function() {
                return "fixed" === c(this).css("position") && !c(this).hasClass("fancybox-overlay") && !c(this).hasClass("fancybox-wrap");
            }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), d = f.scrollTop(), 
            e = f.scrollLeft(), this.el.addClass("fancybox-lock"), f.scrollTop(d).scrollLeft(e)), 
            this.open(a);
        },
        onUpdate: function() {
            this.fixed || this.update();
        },
        afterClose: function(a) {
            this.overlay && !h.coming && this.overlay.fadeOut(a.speedOut, c.proxy(this.close, this));
        }
    }, h.helpers.title = {
        defaults: {
            type: "float",
            position: "bottom"
        },
        beforeShow: function(a) {
            var b = h.current, d = b.title, e = a.type;
            if (c.isFunction(d) && (d = d.call(b.element, b)), m(d) && "" !== c.trim(d)) {
                switch (b = c('<div class="fancybox-title fancybox-title-' + e + '-wrap">' + d + "</div>"), 
                e) {
                  case "inside":
                    e = h.skin;
                    break;

                  case "outside":
                    e = h.wrap;
                    break;

                  case "over":
                    e = h.inner;
                    break;

                  default:
                    e = h.skin, b.appendTo("body"), i && b.width(b.width()), b.wrapInner('<span class="child"></span>'), 
                    h.current.margin[2] += Math.abs(o(b.css("margin-bottom")));
                }
                b["top" === a.position ? "prependTo" : "appendTo"](e);
            }
        }
    }, c.fn.fancybox = function(a) {
        var b, d = c(this), e = this.selector || "", f = function(f) {
            var g = c(this).blur(), i = b, j, k;
            !(f.ctrlKey || f.altKey || f.shiftKey || f.metaKey || g.is(".fancybox-wrap") || (j = a.groupAttr || "data-fancybox-group", 
            k = g.attr(j), k || (j = "rel", k = g.get(0)[j]), k && "" !== k && "nofollow" !== k && (g = e.length ? c(e) : d, 
            g = g.filter("[" + j + '="' + k + '"]'), i = g.index(this)), a.index = i, !1 === h.open(g, a) || !f.preventDefault()));
        };
        return a = a || {}, b = a.index || 0, e && !1 !== a.live ? g.undelegate(e, "click.fb-start").delegate(e + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", f) : d.unbind("click.fb-start").bind("click.fb-start", f), 
        this.filter("[data-fancybox-start=1]").trigger("click"), this;
    }, g.ready(function() {
        var b, f;
        if (c.scrollbarWidth === d && (c.scrollbarWidth = function() {
            var a = c('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"), b = a.children(), b = b.innerWidth() - b.height(99).innerWidth();
            return a.remove(), b;
        }), c.support.fixedPosition === d) {
            b = c.support, f = c('<div style="position:fixed;top:20px;"></div>').appendTo("body");
            var g = 20 === f[0].offsetTop || 15 === f[0].offsetTop;
            f.remove(), b.fixedPosition = g;
        }
        c.extend(h.defaults, {
            scrollbarWidth: c.scrollbarWidth(),
            fixed: c.support.fixedPosition,
            parent: c("body")
        }), b = c(a).width(), e.addClass("fancybox-lock-test"), f = c(a).width(), e.removeClass("fancybox-lock-test"), 
        c("<style type='text/css'>.fancybox-margin{margin-right:" + (f - b) + "px;}</style>").appendTo("head");
    });
}(window, document, jQuery);

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.AccessibilityAdjustments();
            }, a.prototype.AccessibilityAdjustments = function() {
                var a = this;
                $("a[href^='#']").on("click", function() {
                    $("#" + $(this).attr("href").slice(1)).focus();
                });
            }, a;
        }();
        a.Base = b;
    }(a.LayoutAdjustments || (a.LayoutAdjustments = {}));
    var b = a.LayoutAdjustments;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.InitAccordion();
            }, a.prototype.InitAccordion = function() {
                if (0 !== $(".js-accordion-container").length) {
                    var a = 300;
                    $(".js-accordion-item-selector").each(function(b) {
                        $(this).on("click", function() {
                            var b = $(this).closest("li");
                            b.find(".accordion-item-container").slideToggle(a), b.hasClass("is-opened") ? (b.removeClass("is-opened"), 
                            b.addClass("is-closed"), setTimeout(function() {
                                b.find(".js-accordion-item-selector").removeClass("bottom-divider");
                            }, a)) : (b.removeClass("is-closed"), b.addClass("is-opened"), b.find(".js-accordion-item-selector").addClass("bottom-divider"));
                        });
                    });
                }
            }, a;
        }();
        a.AccordionBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.InitPageheader();
            }, a.prototype.InitPageheader = function() {
                var a = $("body").offset();
                $(window).scroll(function() {
                    $(this).scrollTop() > a.top + 29 ? $("body").find(".pageheader").addClass("sticky") : $("body").find(".pageheader").removeClass("sticky"), 
                    $(this).scrollTop() > a.top + 33 ? $("body").find(".pageheader").addClass("animated") : $("body").find(".pageheader").removeClass("animated");
                }), $("body").find(".js-toggleheadersearch, .pageheader-searchclose").on("click", function(a) {
                    a.preventDefault(), $("body").find(".pageheader-headersearch").toggleClass("is-open");
                });
            }, a;
        }();
        a.PageheaderBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.InitNewsfeed();
            }, a.prototype.InitNewsfeed = function() {
                if (0 !== $(".js-newsfeed-container").length) {
                    var a = this, b = $(".js-newsfeed-add").data("pageid"), c = $(".js-newsfeed-add").data("iteration"), d = $(".js-newsfeed-add").data("take");
                    a.getFeed(b, c, d), $(".js-newsfeed-add").on("click", function(b) {
                        c = $(".js-newsfeed-add").data("iteration") + 1, d = $(".js-newsfeed-add").data("take"), 
                        a.getFeed(null, c, d), $(".js-newsfeed-add").data("iteration", c);
                    });
                }
            }, a.prototype.setPageId = function(a) {
                this.pageId = a;
            }, a.prototype.getFeed = function(a, b, c) {
                var d = this;
                d.pageId = null == a ? d.pageId : a, d.iteration = null == b ? d.iteration : b, 
                d.take = null == c ? d.take : c;
                var e = {
                    id: d.pageId,
                    iterations: d.iteration,
                    count: d.take
                };
                jQuery.support.cors = !0, $.ajax({
                    url: "/webapi/news/",
                    type: "GET",
                    dataType: "json",
                    data: e,
                    success: function(a) {
                        for (var b = 0; b < a.length; b++) d.addNewsItem(a[b]);
                        $(".js-newsfeed-container").find("li:hidden").show(300), 0 == a.length && $(".js-newsfeed-add").hide(600);
                    },
                    error: function(a, b, c) {
                        alert(a + "\n" + b + "\n" + c);
                    }
                }), d.iteration++;
            }, a.prototype.addNewsItem = function(a) {
                var b = $(".js-newsfeed-container"), c = $("<li>");
                c.hide();
                var d = $("<a>");
                if (d.attr("title", a.Title), d.attr("Href", a.LinkUrl), null != a.ImageUrl && "" != a.ImageUrl) {
                    var e = $("<div>");
                    e.attr("class", "figure");
                    var f = $("<img>");
                    f.attr("src", a.ImageUrl), f.attr("title", a.Title), e.append(f), d.append(e);
                }
                var g = $("<div>");
                g.attr("class", "teasertext");
                var h = $("<h2>");
                h.text(a.Title), g.append(h);
                var i = $("<p>");
                i.attr("class", "lastupdated"), i.text(a.Date), g.append(i), g.append(a.Body), d.append(g), 
                c.append(d), b.append(c);
            }, a;
        }();
        a.NewsfeedBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a, b.Init();
            }
            return a.prototype.Init = function() {
                var a = this;
                a.VideoContainers = $("div.video-container"), 0 !== $(".fancybox").length && a.InitFancybox(), 
                0 !== a.VideoContainers.length && a.VideoContainers.each(function(b, c) {
                    var d = $(c), e = d.find("iframe"), f = d.find("object");
                    0 !== e.length ? (a.CheckAndSetWidthIframe(d), a.SetWmode(d)) : 0 !== f.length ? a.CheckAndSetObjectWidth(d) : 0 !== d.find("div[data-qbrick-mcid]").length && a.CheckWidthQBrick(d), 
                    a.InitiateCoverImage(d);
                });
            }, a.prototype.InitFancybox = function() {
                $(".fancybox").fancybox({
                    width: "70%",
                    height: "90%"
                });
            }, a.prototype.InitiateCoverImage = function(a) {
                var b = this;
                a.on("click touchend", "img", $.proxy(b.onCoverImageClick, b));
            }, a.prototype.CheckAndSetObjectWidth = function(a) {
                var b = a.width(), c = a.find("object"), d = parseInt(c.attr("width"), 10), e = parseInt(c.attr("height"), 10), f = 0;
                if (a.addClass("objectembed"), b > 0 && $.isNumeric(d) === !0 && $.isNumeric(e) === !0) {
                    f = e / d;
                    var g = "Video" + Math.floor(100 * Math.random()) + new Date().valueOf(), h = f * b;
                    c.attr("width", b).attr("height", h).attr("id", g);
                }
            }, a.prototype.onCoverImageClick = function(a) {
                var b = this, c = $(a.target), d = c.parents("div:eq(0)"), e = d.find("iframe:eq(0)");
                if (d.hasClass("youtube")) {
                    b.Init();
                    var f = e.attr("src"), g = "autoplay=1";
                    f = b.hasQueryString(f) === !0 ? f + "&" + g : f + "?" + g, c.css({
                        "z-index": "5",
                        visibility: "hidden"
                    }), e.css({
                        opacity: "1"
                    }).attr("src", f);
                }
            }, a.prototype.SetWmode = function(a) {
                var b = this, c = a.find("iframe:eq(0)"), d = "wmode=transparent";
                if (c.length > 0) {
                    var e = c.attr("src");
                    e += b.hasQueryString(e) === !0 ? "&" + d : "?" + d, c.attr("src", e);
                }
            }, a.prototype.hasQueryString = function(a) {
                return -1 !== a.indexOf("?");
            }, a.prototype.CheckAndSetWidthIframe = function(a) {
                var b = a.width(), c = a.find("iframe:eq(0)"), d = parseInt(c.attr("width"), 10), e = parseInt(c.attr("height"), 10), f = 0;
                if (a.addClass("youtube"), b > 0 && $.isNumeric(d) === !0 && $.isNumeric(e) === !0) {
                    f = e / d;
                    var g = "Video" + Math.floor(100 * Math.random()) + new Date().valueOf(), h = f * b;
                    c.attr("width", b).attr("height", h).attr("id", g);
                }
            }, a.prototype.CheckWidthQBrick = function(a) {
                var b = this, c = a.width(), d = a.find("> div:eq(0)"), e = parseInt(d.attr("data-qbrick-width"), 10), f = parseInt(d.attr("data-qbrick-height"), 10), g = 0;
                if (a.addClass("qbrick"), $.isNumeric(e) === !0 && $.isNumeric(f) === !0 && e > c) {
                    g = f / e;
                    var h = "Video" + Math.floor(100 * Math.random()) + new Date().valueOf(), i = g * c;
                    d.attr("data-qbrick-width", c).attr("data-qbrick-height", i).attr("id", h);
                    var j = $.proxy(function() {
                        b.SetWidthQBrich("#" + h);
                    }, b);
                    b.interval = window.setInterval(j, 200);
                }
            }, a.prototype.SetWidthQBrich = function(a) {
                var b = this, c = $(a);
                if (0 !== c.length) {
                    var d = c.find("object, embed, video");
                    0 !== d.length && (d.css({
                        width: c.attr("data-qbrick-width") + "px",
                        height: c.attr("data-qbrick-height") + "px"
                    }), window.clearInterval(b.interval));
                } else window.clearInterval(b.interval);
            }, a;
        }();
        a.VideoBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                this.requestUrl = "/webapi/search", this.loadMoreSelector = ".js-loadmore", this.searchListSelector = ".js-searchresult", 
                this.dataNextPage = "nextpage", this.dataPageSize = "pagesize", this.dataQuery = "query", 
                this.dataNuumberOfHits = "numberofhits", this.dataLanguage = "language";
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this, b = $(a.loadMoreSelector);
                0 !== b.length && b.on("click", $.proxy(a.onLoadMoreButtonClick, a));
            }, a.prototype.onLoadMoreButtonClick = function(a) {
                a.preventDefault();
                var b = this, c = $(a.currentTarget), d = c.data(b.dataNextPage), e = c.data(b.dataPageSize), f = c.data(b.dataQuery), g = c.data(b.dataLanguage), h = c.data(b.dataNuumberOfHits), i = {
                    Query: f,
                    Language: g,
                    PageSize: e,
                    Offset: d
                };
                b.doSearch(i), d * e >= h && c.hide(), c.data(b.dataNextPage, d + 1);
            }, a.prototype.doSearch = function(a) {
                var b = this;
                $.ajax({
                    url: b.requestUrl,
                    type: "POST",
                    dataType: "json",
                    data: a,
                    success: function(a) {
                        0 !== a.length && b.handleResponse(a);
                    },
                    error: function(a, b, c) {
                        alert(a + "\n" + b + "\n" + c);
                    }
                });
            }, a.prototype.handleResponse = function(a) {
                for (var b = this, c = 0; c < a.length; c++) b.addItem(a[c]);
                $(b.searchListSelector).find("li:hidden").show(300);
            }, a.prototype.addItem = function(a) {
                var b = this, c = $(b.searchListSelector), d = $("<li>"), e = $("<h3>"), f = $("<p>"), g = $("<a>");
                d.hide(), e.append(a.Title), f.append(a.Snippet), g.append(a.Url), g.attr("Href", a.Url), 
                d.append(e), d.append(f), d.append(g), c.append(d);
            }, a;
        }();
        a.SearchBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.InitNewsletter();
            }, a.prototype.InitNewsletter = function() {
                function a() {
                    $(".newsletterblock .button.gray").css("display", "none"), $(".newsletterblock .button.green").css("display", "none"), 
                    $(".newsletterblock .button.success").css("display", "inline-block"), $(".newsletterblock .button.fail").css("display", "none");
                }
                function b() {
                    $(".newsletterblock .button.gray").css("display", "none"), $(".newsletterblock .button.green").css("display", "none"), 
                    $(".newsletterblock .button.success").css("display", "none"), $(".newsletterblock .button.fail").css("display", "inline-block");
                }
                function c(a) {
                    var b = a.val(), c = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
                    return c.test(b) ? (a.removeClass("error"), a.addClass("valid"), a.siblings("p").removeClass("error"), 
                    !0) : (a.addClass("error"), a.removeClass("valid"), a.siblings("p").addClass("error"), 
                    !1);
                }
                $("body").find(".js-email").on("change blur keyup", function() {
                    var a = $(this);
                    c(a), $(".js-email").hasClass("valid") ? ($(".newsletterblock .button.gray").css("display", "none"), 
                    $(".newsletterblock .button.green").css("display", "inline-block")) : ($(".newsletterblock .button.gray").css("display", "inline-block"), 
                    $(".newsletterblock .button.green").css("display", "none"));
                }), $(".newsletterblock .button.green").on("click", function() {
                    a();
                }), $(".newsletterblock .button.success, .newsletterblock .button.fail").on("click", function(a) {
                    $("input.newsletterblok-email").removeClass("error"), $("input.newsletterblok-email").removeClass("valid"), 
                    $("form")[0].reset(), $(".newsletterblock .button.gray").css("display", "inline-block"), 
                    $(".newsletterblock .button.green").css("display", "none"), $(".newsletterblock .button.success").css("display", "none"), 
                    $(".newsletterblock .button.fail").css("display", "none");
                });
            }, a;
        }();
        a.NewsletterBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var Comprend;

!function(a) {
    !function(a) {
        var b = function() {
            function a(a) {
                this.mobilemenuSelector = ".js-mainmenu", this.$mobilemenu = null, this.iScroll = null;
                var b = this;
                b.MainBase = a;
            }
            return a.prototype.Init = function() {
                var a = this;
                a.$mobilemenu = $(a.mobilemenuSelector), a.addEvents(), a.InitMobileMenu(), $.getScript("/UI/scripts/Vendor/iscroll-lite.js", $.proxy(a.onGetIscroll, a));
            }, a.prototype.addEvents = function() {
                var a = this;
                $(window).on("orientationchange", $.proxy(a.onOrientationChange, a));
            }, a.prototype.onOrientationChange = function() {
                var a = this;
                null !== a.iScroll && setTimeout(function() {
                    a.iScroll.refresh();
                }, 100);
            }, a.prototype.onGetIscroll = function() {
                var a = this;
                a.initiateIScroll();
            }, a.prototype.initiateIScroll = function() {
                var a = this;
                a.iScroll = new IScroll(a.mobilemenuSelector, {
                    mouseWheel: !0,
                    scrollbars: !0,
                    click: !0
                });
            }, a.prototype.InitMobileMenu = function() {
                if (0 !== $(".js-mainmenu").length) {
                    var a = this, b = $("li.active").attr("data-path");
                    if (b && 0 !== b.length) {
                        var c = b.split(",");
                        $.each(c, function(a, b) {
                            "5" != b && ($("li[data-contentid=" + b + "]").addClass("semi-active"), $("ul[data-parent=" + b + "], li[data-contentid=" + b + "] > a.expand").addClass("is-open"));
                        });
                    }
                }
                $(".js-toggelmobilemenu").on("click", function(a) {
                    a.preventDefault(), $(".mobile.js-mainmenu").toggleClass("is-open");
                }), $(".mobile.js-mainmenu a.expand").on("click", function(a) {
                    var b = $(this).parent("li").attr("data-contentid"), c = $(this).parent("li");
                    $(this).toggleClass("is-open"), $("ul[data-parent=" + b + "]").toggleClass("is-open");
                });
            }, a;
        }();
        a.MobileMenuBase = b;
    }(a.Layout || (a.Layout = {}));
    var b = a.Layout;
}(Comprend || (Comprend = {}));

var MainBase = function() {
    function a() {
        this.Utilities = null, this.Accordion = null, this.Pageheader = null, this.NewsFeed = null, 
        this.Video = null, this.Newsletter = null, this.Search = null, this.MobileMenu = null, 
        this.debug = !0;
        var a = this;
        a.MainBase = a, a.Utilities = a.LoadModule("Comprend.Utilities.Base", a), a.Accordion = a.LoadModule("Comprend.Layout.AccordionBase", a), 
        a.Pageheader = a.LoadModule("Comprend.Layout.PageheaderBase", a), a.NewsFeed = a.LoadModule("Comprend.Layout.NewsfeedBase", a), 
        a.Newsletter = a.LoadModule("Comprend.Layout.NewsletterBase", a), a.Search = a.LoadModule("Comprend.Layout.SearchBase", a), 
        a.MobileMenu = a.LoadModule("Comprend.Layout.MobileMenuBase", a);
    }
    return a.prototype.init = function() {
        var a = this;
        a.pageLanguage = $("html").attr("language"), null != a.Utilities && a.Utilities.Init(), 
        null != a.Video && a.Video.Init(), null != a.Accordion && a.Accordion.Init(), null != a.Pageheader && a.Pageheader.Init(), 
        null != a.NewsFeed && a.NewsFeed.Init(), null != a.Newsletter && a.Newsletter.Init(), 
        null != a.Search && a.Search.Init(), null != a.MobileMenu && a.MobileMenu.Init();
    }, a.prototype.LoadModule = function(a, b) {
        for (var c = this, d = a.split("."), e = window, f, g = 0; g < d.length; g++) "object" == typeof e[d[g]] && (e = e[d[g]]), 
        "function" == typeof e[d[g]] && (f = e[d[g]]);
        return "function" == typeof f ? "undefined" != typeof b ? new f.prototype.constructor(b) : new f() : (c.log(a + " is unavailable. Is it added to the project?"), 
        null);
    }, a.prototype.log = function() {
        for (var a = [], b = 0; b < arguments.length; b++) a[b - 0] = arguments[b];
        this.debug && console.log(arguments);
    }, a;
}(), scriptbase = new MainBase();

$(document).ready(function() {
    scriptbase.init();
});